package com.app.kemenhub.jakbanapps.retrofit.model_data.hotel_fee;


import com.app.kemenhub.jakbanapps.retrofit.model_data.provinsi.Provinsi;

public class HotelFees {

	private String id;
	private Provinsi provinsi;
	private String unit;
	private String eselon1;
	private String eselon2;
	private String eselon3;
	private String eselon4;
	private String golongan1_2;

	
	public HotelFees(String id, Provinsi provinsi, String unit, String eselon1, String eselon2, String eselon3,
			String eselon4, String golongan1_2) {
		super();
		this.id = id;
		this.provinsi = provinsi;
		this.unit = unit;
		this.eselon1 = eselon1;
		this.eselon2 = eselon2;
		this.eselon3 = eselon3;
		this.eselon4 = eselon4;
		this.golongan1_2 = golongan1_2;
	}

	public HotelFees(){
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Provinsi getProvinsi() {
		return provinsi;
	}

	public void setProvinsi(Provinsi provinsi) {
		this.provinsi = provinsi;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getEselon1() {
		return eselon1;
	}

	public void setEselon1(String eselon1) {
		this.eselon1 = eselon1;
	}

	public String getEselon2() {
		return eselon2;
	}

	public void setEselon2(String eselon2) {
		this.eselon2 = eselon2;
	}

	public String getEselon3() {
		return eselon3;
	}

	public void setEselon3(String eselon3) {
		this.eselon3 = eselon3;
	}

	public String getEselon4() {
		return eselon4;
	}

	public void setEselon4(String eselon4) {
		this.eselon4 = eselon4;
	}

	public String getGolongan1_2() {
		return golongan1_2;
	}

	public void setGolongan1_2(String golongan1_2) {
		this.golongan1_2 = golongan1_2;
	}

}
