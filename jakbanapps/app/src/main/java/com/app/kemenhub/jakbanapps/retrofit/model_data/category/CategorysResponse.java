package com.app.kemenhub.jakbanapps.retrofit.model_data.category;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategorysResponse {
	@SerializedName("code")
	String code;
	@SerializedName("message")
	String message;
	@SerializedName("category")
	List<Kategori> category;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<Kategori> getCategory() {
		return category;
	}
	public void setCategory(List<Kategori> category) {
		this.category = category;
	}
	
	
}
