package com.app.kemenhub.jakbanapps.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.kemenhub.jakbanapps.R;
import com.app.kemenhub.jakbanapps.db.disposition;

import java.util.List;


public class DataDisposisiAdt extends RecyclerView.Adapter<DataDisposisiAdt.Holder> {
    List<disposition> itemList;
    Context ctx;
    private LayoutInflater inflater;
    Context context;
    private SparseBooleanArray mSelectedItemsIds;
    private int previous = 0;
    private String TAG_LOAD = null;

    private itemClickCallback itemClickCallback;
    int Type;
    ViewGroup parent;

    public interface itemClickCallback{
        void onItemClick(int p);
    }

    public void setItemClickCallback(final itemClickCallback itemClickCallback){
        this.itemClickCallback = itemClickCallback;
    }

    public DataDisposisiAdt(String tag, List<disposition> data, Context context){
        this.itemList = data;
        this.TAG_LOAD = tag;
        this.ctx = context;
        this.inflater = LayoutInflater.from(ctx);
        this.context = context;
        mSelectedItemsIds = new SparseBooleanArray();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        WindowManager windowManager = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);
        int width = windowManager.getDefaultDisplay().getWidth();
        view.setLayoutParams(new RecyclerView.LayoutParams(width, RecyclerView.LayoutParams.WRAP_CONTENT));

        Holder derpHolder = new Holder(view);

        return derpHolder;
    }

    public void runer(){
        onCreateViewHolder(parent, Type);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        switch(TAG_LOAD){
            case "DINAS":
                holder.item_meeting.setVisibility(View.GONE);
                holder.item_disposisi.setVisibility(View.GONE);
                holder.card_meeting.setVisibility(View.GONE);
                holder.card_disposisi.setVisibility(View.GONE);
                holder.card_dinas.setVisibility(View.VISIBLE);
                holder.item_dinas.setVisibility(View.VISIBLE);
                break;
            case "MEETING":
                holder.card_meeting.setVisibility(View.VISIBLE);
                holder.card_disposisi.setVisibility(View.GONE);
                holder.card_dinas.setVisibility(View.GONE);
                holder.item_meeting.setVisibility(View.VISIBLE);
                holder.item_disposisi.setVisibility(View.GONE);
                holder.item_dinas.setVisibility(View.GONE);
                break;
            case "DISPOSISI":
                holder.card_meeting.setVisibility(View.GONE);
                holder.card_disposisi.setVisibility(View.VISIBLE);
                holder.card_dinas.setVisibility(View.GONE);
                holder.item_meeting.setVisibility(View.GONE);
                holder.item_disposisi.setVisibility(View.VISIBLE);
                holder.item_disposisi.setClickable(true);
                holder.item_dinas.setVisibility(View.GONE);

                if(itemList.get(position).getNo_surat_perintah().length() > 20){
                    holder.disposisi_surat.setText("No. Surat : " + itemList.get(position).getNo_surat_perintah().substring(0,17) + "..");
                }else {
                    holder.disposisi_surat.setText("No. Surat : " + itemList.get(position).getNo_surat_perintah());
                }
                if(itemList.get(position).getNo_agenda().length() > 20){
                    holder.disposisi_agenda.setText("No. Agenda : " + itemList.get(position).getNo_agenda().substring(0,17) + "..");
                }else {
                    holder.disposisi_agenda.setText("No. Agenda : " + itemList.get(position).getNo_agenda());
                }
                if(itemList.get(position).getNotes().length() > 25){
                    holder.disposisi_desc.setText("Catatan : " + itemList.get(position).getNotes().replaceAll("\n", ", ").substring(0,17) + "..");
                }else {
                    holder.disposisi_desc.setText("Catatan : " + itemList.get(position).getNotes().replaceAll("\n", ", "));
                }
                holder.disposisi_dari.setText("Dari : " +itemList.get(position).getFrom());
                holder.disposisi_category.setText("Perihal : " + itemList.get(position).getTitle());
                ////Log.i("CLS", itemList.get(position).getCls());
                if(itemList.get(position).getCls().equals("1")){
                    holder.disposisi_klasifikasi.setText("B");
                    holder.disposisi_klasifikasi.setTextColor(context.getResources().getColor(R.color.md_blue_grey_300));
                    holder.disposisi_star.setVisibility(View.GONE);
                }else if(itemList.get(position).getCls().equals("2")){
                    holder.disposisi_klasifikasi.setText("S");
                    holder.disposisi_klasifikasi.setTextColor(context.getResources().getColor(R.color.md_light_blue_900));
                    holder.disposisi_star.setVisibility(View.GONE);
                }else if(itemList.get(position).getCls().equals("3")){
                    holder.disposisi_klasifikasi.setText("SS");
                    holder.disposisi_klasifikasi.setTextColor(context.getResources().getColor(R.color.md_orange_A400));
                    holder.disposisi_star.setVisibility(View.GONE);
                }else if(itemList.get(position).getCls().equals("4")){
                    holder.disposisi_klasifikasi.setText("R");
                    holder.disposisi_klasifikasi.setTextColor(context.getResources().getColor(R.color.md_cyan_A700));
                    holder.disposisi_star.setVisibility(View.VISIBLE);
                }
                break;
            default:
                holder.item_meeting.setVisibility(View.GONE);
                holder.item_disposisi.setVisibility(View.GONE);
                holder.item_dinas.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));
    }

    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }

    public void selectView(int position, boolean value) {
        if (value) {
            mSelectedItemsIds.put(position, value);
        }else {
            mSelectedItemsIds.delete(position);
        }
        notifyDataSetChanged();
    }


    class Holder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{

        public LinearLayout item;
        public LinearLayout item_dinas;
        public LinearLayout item_meeting;
        public LinearLayout item_disposisi;
        public TextView dinas_code, dinas_agenda, dinas_durasi, dinas_desc, dinas_kota, dinas_biaya;
        public ImageView dinas_star, meeting_star, disposisi_star;
        public TextView meeting_judul, meeting_tempat, meeting_durasi, meeting_category, meeting_desc, meeting_dari;
        public TextView disposisi_surat, disposisi_agenda, disposisi_klasifikasi, disposisi_category, disposisi_desc, disposisi_dari;
        public CardView card_meeting, card_dinas, card_disposisi;
        public Holder(View itemView) {
            super(itemView);
            item_dinas = (LinearLayout) itemView.findViewById(R.id.dinas_item);
            dinas_code = (TextView) itemView.findViewById(R.id.dinas_code);
            dinas_agenda = (TextView) itemView.findViewById(R.id.dinas_surat);
            dinas_durasi = (TextView) itemView.findViewById(R.id.dinas_durasi);
            dinas_desc = (TextView) itemView.findViewById(R.id.dinas_deskripsi);
            dinas_kota = (TextView) itemView.findViewById(R.id.dinas_kota);
            dinas_biaya = (TextView) itemView.findViewById(R.id.dinas_biaya);
            dinas_star = (ImageView) itemView.findViewById(R.id.dinas_star);
            meeting_star = (ImageView) itemView.findViewById(R.id.meeting_star);
            card_dinas = (CardView) itemView.findViewById(R.id.dinas_line_card);
            card_meeting = (CardView) itemView.findViewById(R.id.meeting_line_card);
            card_disposisi = (CardView) itemView.findViewById(R.id.disposisi_line_card);

            item_meeting = (LinearLayout) itemView.findViewById(R.id.meeting_item);
            meeting_judul = (TextView) itemView.findViewById(R.id.meeting_judul);
            meeting_tempat = (TextView) itemView.findViewById(R.id.meeting_tempat);
            meeting_durasi = (TextView) itemView.findViewById(R.id.meeting_durasi);
            meeting_category = (TextView) itemView.findViewById(R.id.meeting_category);
            meeting_desc = (TextView) itemView.findViewById(R.id.meeting_deskripsi);
            meeting_dari = (TextView) itemView.findViewById(R.id.meeting_dari);

            item_disposisi = (LinearLayout) itemView.findViewById(R.id.disposisi_item);
            disposisi_surat = (TextView) itemView.findViewById(R.id.disposisi_code);
            disposisi_klasifikasi = (TextView) itemView.findViewById(R.id.disposisi_durasi);
            disposisi_agenda = (TextView) itemView.findViewById(R.id.disposisi_agenda);
            disposisi_category = (TextView) itemView.findViewById(R.id.disposisi_kategori);
            disposisi_desc = (TextView) itemView.findViewById(R.id.disposisi_deskripsi);
            disposisi_dari = (TextView) itemView.findViewById(R.id.disposisi_dari);
            disposisi_star = (ImageView) itemView.findViewById(R.id.disposisi_star);

            item = (LinearLayout) itemView.findViewById(R.id.item);
            item_dinas.setClickable(true);
            item_meeting.setClickable(true);
            item_disposisi.setClickable(true);
            item.setClickable(true);

            item.setOnClickListener(this);
            item_dinas.setOnClickListener(this);
            item_meeting.setOnClickListener(this);
            item_disposisi.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.item:
                    itemClickCallback.onItemClick(getAdapterPosition());
                    break;
                case R.id.meeting_item:
                    itemClickCallback.onItemClick(getAdapterPosition());
                    break;
                case R.id.disposisi_item:
                    itemClickCallback.onItemClick(getAdapterPosition());
                    break;
            }
        }

        @Override
        public boolean onLongClick(View v) {
            switch (v.getId()){
            }
            return false;
        }
    }
}