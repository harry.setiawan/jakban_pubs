package com.app.kemenhub.jakbanapps.retrofit.model_data.taxi_fee;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TaxyFeesResponse {
	@SerializedName("code")
	String code;
	@SerializedName("message")
	String message;
	@SerializedName("taxyFees")
	List<TaxyFees> taxi_fee;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<TaxyFees> getTaxyFees() {
		return taxi_fee;
	}
	public void setTaxyFees(List<TaxyFees> taxi_fee) {
		this.taxi_fee = taxi_fee;
	}
}
