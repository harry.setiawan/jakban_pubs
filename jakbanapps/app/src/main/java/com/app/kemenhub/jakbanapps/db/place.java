package com.app.kemenhub.jakbanapps.db;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit. 
/**
 * Entity mapped to table "PLACE".
 */
public class place {

    private Long id;
    private String provinsi_id;
    private String name;
    private String type;

    public place() {
    }

    public place(Long id) {
        this.id = id;
    }

    public place(Long id, String provinsi_id, String name, String type) {
        this.id = id;
        this.provinsi_id = provinsi_id;
        this.name = name;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProvinsi_id() {
        return provinsi_id;
    }

    public void setProvinsi_id(String provinsi_id) {
        this.provinsi_id = provinsi_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
