package com.app.kemenhub.jakbanapps.retrofit.model_data.kabupaten;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class KabupatensResponse {
	@SerializedName("code")
	String code;
	@SerializedName("message")
	String message;
	@SerializedName("kabupaten")
	List<Kabupaten> kabupaten;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<Kabupaten> getKabupaten() {
		return kabupaten;
	}
	public void setKabupaten(List<Kabupaten> kabupaten) {
		this.kabupaten = kabupaten;
	}
	
}
