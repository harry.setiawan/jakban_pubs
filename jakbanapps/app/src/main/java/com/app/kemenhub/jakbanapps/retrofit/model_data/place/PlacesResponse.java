package com.app.kemenhub.jakbanapps.retrofit.model_data.place;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PlacesResponse {
	@SerializedName("code")
	String code;
	@SerializedName("message")
	String message;
	@SerializedName("place")
	List<Places> place;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<Places> getPlace() {
		return place;
	}
	public void setPlace(List<Places> place) {
		this.place = place;
	}
}
