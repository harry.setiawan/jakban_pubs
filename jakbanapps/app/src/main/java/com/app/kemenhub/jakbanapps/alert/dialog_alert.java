package com.app.kemenhub.jakbanapps.alert;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.app.kemenhub.jakbanapps.R;


public class dialog_alert {
    Context ac ;
    Activity ai;
    public void showDialog(Activity activity, String msg){
        ac = activity.getApplicationContext();
        ai = activity;
        _lockOrientation();
        final Dialog Dialog = new Dialog(new ContextThemeWrapper(ai, R.style.DialogSlideAnim));
        Dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
        Dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
        Dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);
        Dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Dialog.setCancelable(false);
        Dialog.setContentView(R.layout.alert);

        TextView text = (TextView) Dialog.findViewById(R.id.isi);
        text.setText(msg);

        Button dialogButton = (Button) Dialog.findViewById(R.id.btn_ok);

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _unlockOrientation();
                Dialog.dismiss();
            }
        });

        Dialog.show();

    }

    public void _lockOrientation() {
        if (ac.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            ai.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT);
        } else {
            ai.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_LANDSCAPE);

        }
    }

    public void _unlockOrientation() {
        ai.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    }
}