package com.app.kemenhub.jakbanapps.instance;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;

/**
 * Created by Axa.Laranza on 5/29/2017.
 */

public class MyFirebaseInstanceIdService
        extends com.google.firebase.iid.FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {

        //Getting registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        ////Log.i(TAG, "Refreshed token: " + refreshedToken);

    }
}