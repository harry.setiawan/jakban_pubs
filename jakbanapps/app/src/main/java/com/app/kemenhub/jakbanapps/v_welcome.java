package com.app.kemenhub.jakbanapps;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.transition.Explode;
import android.transition.Slide;
import android.transition.Transition;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.kemenhub.jakbanapps.retrofit.RCWelcome;
import com.app.kemenhub.jakbanapps.retrofit.Request_Inf;
import com.app.kemenhub.jakbanapps.retrofit.URLManager;
import com.app.kemenhub.jakbanapps.retrofit.model_data.airplane.AirPlaneTiketsResponse;
import com.app.kemenhub.jakbanapps.sys.ExceptionHandler;
import com.app.kemenhub.jakbanapps.sys.PrefManager;
import com.app.kemenhub.jakbanapps.sys.SessionManager;
import com.github.ybq.android.spinkit.style.DoubleBounce;
import com.github.ybq.android.spinkit.style.Pulse;
import com.github.ybq.android.spinkit.style.ThreeBounce;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class v_welcome extends AppCompatActivity {

    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private LinearLayout dotsLayout;
    private TextView[] dots;
    private int[] layouts;
    private PrefManager prefManager;
    public SessionManager SManager;
    ProgressBar progress;
    RCWelcome rc;
    public static boolean[] TAG_REQUEST = new boolean[20];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefManager = new PrefManager(this);
        SManager = new SessionManager();
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        if (!prefManager.isFirstTimeLaunch()) {
            launchHomeScreen();
            finish();
        }
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        setContentView(R.layout.activity_v_welcome);
        rc = new RCWelcome(this);
        _init_anim();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sync_data();
            }
        },5000);
    }

    private void sync_data() {
        rc.request_all_airplane();
        rc.request_all_category();
        rc.request_all_class();
        rc.request_all_dinas();
        rc.request_all_disposisi();
        rc.request_all_file();
        rc.request_all_folder();
        rc.request_all_hotel_fee();
        rc.request_all_jenis_dinas();
        rc.request_all_kabupaten();
        rc.request_all_kecamatan();
        //rc.request_all_kelurahan();
        rc.request_all_mobile();
        rc.request_all_place();
        rc.request_all_position();
        rc.request_all_provinsi();
        rc.request_all_role();
        rc.request_all_schedule();
        rc.request_all_taxi_fee();
        rc.request_all_user();
        rc.request_all_vehicle_rent();
    }

    public void check_data_sync(){
        try {
            int X = 0;
            for (int i = 0; i < TAG_REQUEST.length; i++) {
                X = 0;
                if (!TAG_REQUEST[i]) {
                    X = 1;
                    //////Log.i("SERVERS SYNC ", String.valueOf(i) + "");
                    break;
                } else if (i == (TAG_REQUEST.length - 1)) {
                    timer.stopTimer();
                    Handler h = new Handler();
                    v_event.DBManager.closeDbConnections();
                    h.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            SManager.setPreferences(v_welcome.this, "welcome", null);
                            go_to_action(v_event.class);
                        }
                    }, 1000);
                }
            }
        }catch (Exception e){
            go_to_action(v_event.class);
        }
    }

    public void go_to_action(final Class classes) {
        if (Build.VERSION.SDK_INT >= 21) {
            Transition exitTrans = new Explode();
            getWindow().setExitTransition(exitTrans);

            Transition reenterTrans = new Slide();
            getWindow().setEnterTransition(reenterTrans);
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this);
            Intent intent = new Intent(this, classes);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent, options.toBundle());
        }else{
            Intent intent = new Intent(this, classes);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.startActivity(intent);
        }
    }

    private void _init_anim() {
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
        progress = (ProgressBar) findViewById(R.id.progress_rss);
        ThreeBounce ThreeBounce = new ThreeBounce();
        ThreeBounce.setBounds(0, 0, 100, 100);
        ThreeBounce.setColor(getResources().getColor(R.color.md_white_1000));
        progress.setIndeterminateDrawable(ThreeBounce);

        // layouts of all welcome sliders
        // add few more layouts if you want
        layouts = new int[]{
                R.layout.f_welcome_slide1,
                R.layout.f_welcome_slide2,
                R.layout.f_welcome_slide3};

        // adding bottom dots
        addBottomDots(0);

        // making notification bar transparent
        changeStatusBarColor();

        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        handler.post(timer);
    }

    final Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
        }
    };

    class Timer implements Runnable {

        Message message = null;
        int counter = 0;
        Handler handler;
        boolean stop = false;

        public Timer(Handler handler) {
            this.handler = handler;
        }

        public void stopTimer() {
            stop = true;
        }

        public void run() {
            if (!stop) {
                message = Message.obtain();
                message.arg1 = counter;
                handler.sendMessage(message);
                handler.postDelayed(this, 3000);
                int current = getItem(+1);
                if (current == layouts.length) {
                    current = 0;
                    viewPager.setCurrentItem(current);
                }else{
                    viewPager.setCurrentItem(current);
                }
            }
        }
    }

    private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.length];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    final Timer timer = new Timer(handler);

    private void launchHomeScreen() {
        prefManager.setFirstTimeLaunch(false);
        startActivity(new Intent(v_welcome.this, v_event.class));
        finish();
    }

    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }
}
