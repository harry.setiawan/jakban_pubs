package com.app.kemenhub.jakbanapps.retrofit.model_data.provinsi;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProvinsiResponse {
	@SerializedName("code")
	String code;
	@SerializedName("message")
	String message;
	@SerializedName("provinsi")
	Provinsi provinsi;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Provinsi getProvinsi() {
		return provinsi;
	}
	public void setProvinsi(Provinsi provinsi) {
		this.provinsi = provinsi;
	}
	
	
}
