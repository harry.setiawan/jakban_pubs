package com.app.kemenhub.jakbanapps.retrofit.model_data.schedule;


import java.util.ArrayList;

public class ScheduleAdd {

	private ArrayList<String> category_id;
	private String from;
	private String title;
	private String datetime_schedule_start;
	private String datetime_schedule_end;
	private String tgl_diterima;
	private String no_surat_perintah;
	private String no_agenda;
	private String place;
	private String desc;
	private String notes;
	private ArrayList<String> forward_to_user_id;
	private String cls;
	private String folder_id;
	private String file_id;

	public ArrayList<String> getCategory_id() {
		return category_id;
	}

	public void setCategory_id(ArrayList<String> category_id) {
		this.category_id = category_id;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDatetime_schedule_start() {
		return datetime_schedule_start;
	}

	public void setDatetime_schedule_start(String datetime_schedule_start) {
		this.datetime_schedule_start = datetime_schedule_start;
	}

	public String getDatetime_schedule_end() {
		return datetime_schedule_end;
	}

	public void setDatetime_schedule_end(String datetime_schedule_end) {
		this.datetime_schedule_end = datetime_schedule_end;
	}

	public String getTgl_diterima() {
		return tgl_diterima;
	}

	public void setTgl_diterima(String tgl_diterima) {
		this.tgl_diterima = tgl_diterima;
	}

	public String getNo_surat_perintah() {
		return no_surat_perintah;
	}

	public void setNo_surat_perintah(String no_surat_perintah) {
		this.no_surat_perintah = no_surat_perintah;
	}

	public String getNo_agenda() {
		return no_agenda;
	}

	public void setNo_agenda(String no_agenda) {
		this.no_agenda = no_agenda;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public ArrayList<String> getForward_to_user_id() {
		return forward_to_user_id;
	}

	public void setForward_to_user_id(ArrayList<String> forward_to_user_id) {
		this.forward_to_user_id = forward_to_user_id;
	}

	public String getCls() {
		return cls;
	}

	public void setCls(String cls) {
		this.cls = cls;
	}

	public String getFolder_id() {
		return folder_id;
	}

	public void setFolder_id(String folder_id) {
		this.folder_id = folder_id;
	}

	public String getFile_id() {
		return file_id;
	}

	public void setFile_id(String file_id) {
		this.file_id = file_id;
	}
}
