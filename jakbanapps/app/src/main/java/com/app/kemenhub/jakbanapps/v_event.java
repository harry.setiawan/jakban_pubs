package com.app.kemenhub.jakbanapps;

import android.app.ActivityOptions;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Explode;
import android.transition.Slide;
import android.transition.Transition;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.kemenhub.jakbanapps.alert.dialog_confirm;
import com.app.kemenhub.jakbanapps.alert.dialog_confirm_token;
import com.app.kemenhub.jakbanapps.db.disposition;
import com.app.kemenhub.jakbanapps.db.manager.DManager;
import com.app.kemenhub.jakbanapps.db.manager.IDManager;
import com.app.kemenhub.jakbanapps.db.perjalanan_dinas;
import com.app.kemenhub.jakbanapps.db.schedule;
import com.app.kemenhub.jakbanapps.db.user_mobile;
import com.app.kemenhub.jakbanapps.db.user_role;
import com.app.kemenhub.jakbanapps.fragment.f_dash;
import com.app.kemenhub.jakbanapps.fragment.f_dinas;
import com.app.kemenhub.jakbanapps.fragment.f_disposisi;
import com.app.kemenhub.jakbanapps.fragment.f_miting;
import com.app.kemenhub.jakbanapps.retrofit.RCEvent;
import com.app.kemenhub.jakbanapps.retrofit.model_data.mobile.Mobile;
import com.app.kemenhub.jakbanapps.service.ConnectivityChanged;
import com.app.kemenhub.jakbanapps.service.RBAlarm;
import com.app.kemenhub.jakbanapps.service.RBData;
import com.app.kemenhub.jakbanapps.service.RBMaster;
import com.app.kemenhub.jakbanapps.sys.ExceptionHandler;
import com.app.kemenhub.jakbanapps.sys.SessionManager;
import com.app.kemenhub.jakbanapps.sys.styler;
import com.app.kemenhub.jakbanapps.sys.utils;
import com.github.ybq.android.spinkit.style.RotatingCircle;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.List;

public class v_event extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ConnectivityChanged.Broadcaast_Connection {

    public static IDManager DBManager;
    public SessionManager SManager;
    public RCEvent rc;
    public ConnectivityChanged cc;
    styler styler;

    public TextView title;
    public Toolbar toolbar;
    public DrawerLayout drawer;
    public NavigationView navigationView;
    public static TextView count_notif;

    public android.support.v4.app.FragmentManager frag;
    f_dash dash;
    f_dinas dinas;
    f_miting meeting;
    f_disposisi disposisi;
    public static String waktu_offline = "";
    ProgressDialog dialog_progress;
    ProgressDialog sync_progress;

    Intent alarmIntent;
    PendingIntent pendingIntent;
    AlarmManager manager;
    Intent alarmIntent2;
    PendingIntent pendingIntent2;
    AlarmManager manager2;
    Intent alarmIntent3;
    PendingIntent pendingIntent3;
    AlarmManager manager3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DBManager = new DManager(this);
        SManager = new SessionManager();
        cc = new ConnectivityChanged(this);
        rc = new RCEvent(this);
        set_dialog();
        session_account();

    }

    @Override
    protected void onDestroy() {
        utils.TAG_LAYOUT = "";
        super.onDestroy();
    }

    private void _init_service() {
        SManager.setPreferences(this, "ServiceLoad", "first");
        alarmIntent = new Intent(getApplicationContext(), RBData.class);
        pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, alarmIntent, 0);
        manager = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        int interval = 1000 * 10 * 1;
        manager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);

        alarmIntent2 = new Intent(getApplicationContext(), RBMaster.class);
        pendingIntent2 = PendingIntent.getBroadcast(getApplicationContext(), 1, alarmIntent2, 0);
        manager2 = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        int interval2 = 1000 * 10 * 4;
        manager2.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval2, pendingIntent2);

        alarmIntent3 = new Intent(getApplicationContext(), RBAlarm.class);
        pendingIntent3 = PendingIntent.getBroadcast(getApplicationContext(), 2, alarmIntent3, 0);
        manager3 = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        int interval3 = 1000 * 1 * 6;
        manager3.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval3, pendingIntent3);
    }
    private void session_account() {
        if(SManager.getPreferences(this, "status").toString().equals("")){
            go_to_action(v_main.class);
        }else {
            if(SManager.getPreferences(this, "welcome").toString().equals("1")){
                go_to_action(v_welcome.class);
            }else {
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                    List<user_role> a = DBManager.list_user_role();
                    ////Log.i("USES COUNT", a.size() +"");
                    if (SManager.getPreferences(getApplicationContext(), "alarm").equals("") ||
                            SManager.getPreferences(getApplicationContext(), "alarm") == null) {
                        _init_service();
                        setContentView(R.layout.activity_v_event);
                        styler = new styler(this);
                        final String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                        ////Log.i("Refresh", "Refreshed token: " + refreshedToken);

                        set_dialog();
                        _init_toolbar();
                        _init_drawer();
                        _set_count_notif();
                        _init_fragment();
                        _set_lay_show_tag();
                        rc.request_byID_mobile(SManager.getPreferences(this, "status").toString());
                        Handler h = new Handler();
                        h.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                user_mobile um = DBManager.get_user_mobileById(SManager.getPreferences(v_event.this, "status").toString());

                                try {
                                    if (!refreshedToken.equals(um.getToken())) {
                                        manager.cancel(pendingIntent);
                                        stopService(new Intent(v_event.this, RBData.class));
                                        dialog_progress.hide();
                                        dialog_confirm_token D = new dialog_confirm_token();
                                        D.showDialog(v_event.this, "Akun anda telah masuk menggunakan device lain, harap Sign In kembali!", new dialog_confirm_token.Response() {
                                            @Override
                                            public void processFinish(boolean output) {
                                                Mobile mobile = new Mobile(
                                                        String.valueOf(SManager.getPreferences(v_event.this, "status")),
                                                        FirebaseInstanceId.getInstance().getToken()
                                                );
                                                SManager.setPreferences(v_event.this, "welcome", "");
                                                SManager.setPreferences(v_event.this, "status", "");
                                                rc.request_mobil_logout(mobile);
                                            }
                                        });
                                    } else {
                                        _init_service();
                                        if(SManager.getPreferences(v_event.this, "ntf_show").toString().equals("true")){
                                            SManager.setPreferences(v_event.this, "ntf_show", "false");
                                            _init_showIn_meeting();
                                        }

                                    }
                                } catch (Exception e) {
                                    _init_service();
                                }
                            }
                        }, 2000);
                    } else {
                        Intent intent = new Intent(getApplicationContext(), v_alarm.class);
                        intent.putExtra("ALARM", String.valueOf(SManager.getPreferences(getApplicationContext(), "alarm")));
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
            }
        }
    }

    private void _set_count_notif() {
        List<disposition> disposisi = DBManager.list_dispositionNotif();
        List<schedule> meeting = DBManager.list_scheduleNotif();
        List<perjalanan_dinas> dinas = DBManager.list_perjalanan_dinasNotif();
        int total = disposisi.size() + meeting.size() + dinas.size();
        try {
            count_notif.setText(String.valueOf(total));
            count_notif.setVisibility(View.VISIBLE);
        }catch (Exception e){

        }
    }

    private void _set_lay_show_tag() {
        String TAG = utils.TAG_LAYOUT;
        switch (TAG){
            case "f_disposisi":
                _init_showIn_disposisi();
                break;
            case "f_meeting":
                _init_showIn_meeting();
                break;
            case "f_dinas":
                _init_showIn_dinas();
                break;
            default:
                _init_showIn_dash();
                break;
        }
    }

    private void set_dialog() {
        dialog_progress = new ProgressDialog(this);
        sync_progress = new ProgressDialog(this);
        RotatingCircle ThreeBounce = new RotatingCircle();
        ThreeBounce.setBounds(0, 0, 100, 100);
        ThreeBounce.setColor(getResources().getColor(R.color.md_light_green_500_25));
        dialog_progress.setIndeterminateDrawable(ThreeBounce);
        dialog_progress.setMessage("Mohon Tunggu..");
        dialog_progress.setCancelable(false);
        sync_progress.setIndeterminateDrawable(ThreeBounce);
        sync_progress.setMessage("Sinkronisasi Data..");
        sync_progress.setCancelable(false);
    }
    public void show_dialog(){
        dialog_progress.show();
    }
    public void hide_dialog(){
        dialog_progress.hide();
    }
    public void show_sync(){
        sync_progress.show();
    }
    public void hide_sync(){
        sync_progress.hide();
    }
    private void _init_toolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(getString(R.string.app_name));
    }
    private void _init_fragment() {
        dash = new f_dash();
        dinas = new f_dinas();
        meeting = new f_miting();
        disposisi = new f_disposisi();
    }
    private void _init_showIn_dash() {
        title.setText("Dashboard");
        frag = getSupportFragmentManager();
        frag.beginTransaction().setCustomAnimations(R.anim.fadein, R.anim.fadeout).replace(R.id.content_menu_in, dash).commit();
    }
    private void _init_showIn_disposisi() {
        title.setText("Data Disposisi");
        frag = getSupportFragmentManager();
        frag.beginTransaction().setCustomAnimations(R.anim.fadein, R.anim.fadeout).replace(R.id.content_menu_in, disposisi).commit();
    }
    private void _init_showIn_meeting() {
        title.setText("Agenda Acara");
        frag = getSupportFragmentManager();
        frag.beginTransaction().setCustomAnimations(R.anim.fadein, R.anim.fadeout).replace(R.id.content_menu_in, meeting).commit();
    }
    private void _init_showIn_dinas() {
        title.setText("Perjalanan Dinas");
        frag = getSupportFragmentManager();
        frag.beginTransaction().setCustomAnimations(R.anim.fadein, R.anim.fadeout).replace(R.id.content_menu_in, dinas).commit();
    }
    private void _init_drawer() {
        styler.drawer();
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.v_event, menu);

        MenuItem item1 = menu.findItem(R.id.action_alert);
        MenuItemCompat.setActionView(item1, R.layout.menu_budge_notif);

        RelativeLayout badgeLayout = (RelativeLayout) item1.getActionView();
        count_notif = (TextView) badgeLayout.findViewById(R.id.notif_count);
        List<disposition> disposisi =  DBManager.list_dispositionNotif();
        try {
            if (disposisi.size() > 0) {
                count_notif.setText(String.valueOf(disposisi.size()));
                count_notif.setVisibility(View.VISIBLE);
            } else {
                count_notif.setText("X");
                count_notif.setVisibility(View.GONE);
            }
        }catch (Exception e){
            count_notif.setText("X");
            count_notif.setVisibility(View.GONE);
        }
        badgeLayout.setClickable(true);
        badgeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                go_to_action(v_notif.class);
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.action_alert:
                go_to_action(v_notif.class);
                break;
            case R.id.action_tentang:
                go_to_action(v_tentang.class);
                break;
        }

        return super.onOptionsItemSelected(item);
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.nav_dashboard:
                _init_showIn_dash();
                break;

            case R.id.nav_disposisi:
                _init_showIn_disposisi();
                break;

            case R.id.nav_pertemuan:
                _init_showIn_meeting();
                break;

            case R.id.nav_dinas:
                _init_showIn_dinas();
                break;

            case R.id.nav_logout:
                dialog_confirm confirm = new dialog_confirm();
                confirm.showDialog(this, "Apakah anda yakin ingin keluar dari sistem?", new dialog_confirm.Response() {
                    @Override
                    public void processFinish(boolean output) {
                        if(output){
                            utils.TAG_LAYOUT = "";
                            manager.cancel(pendingIntent);
                            manager2.cancel(pendingIntent2);
                            manager3.cancel(pendingIntent3);
                            stopService(new Intent(v_event.this,RBData.class));
                            show_dialog();
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Mobile mobile = new Mobile(
                                            String.valueOf(SManager.getPreferences(v_event.this, "status")),
                                            FirebaseInstanceId.getInstance().getToken()
                                    );
                                    rc.request_mobil_logout(mobile);
                                }
                            },2000);
                        }
                    }
                });
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public void go_to_action(final Class classes) {
        if (Build.VERSION.SDK_INT >= 21) {
            Transition exitTrans = new Explode();
            getWindow().setExitTransition(exitTrans);

            Transition reenterTrans = new Slide();
            getWindow().setEnterTransition(reenterTrans);
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this);
            Intent intent = new Intent(this, classes);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent, options.toBundle());
        }else{
            Intent intent = new Intent(this, classes);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.startActivity(intent);
        }
    }
    @Override
    protected void onResume() {
        try {
            registerReceiver(cc.mConnReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }catch (Exception e){}
        super.onResume();
    }
    @Override
    protected void onPause() {
        super.onPause();
    }
    @Override
    public void Broadcaast_Connection(boolean output) {
        try {
            if (output) {
                styler.mode.setText("ONLINE");
                ////Log.i("WAKTU OFF TADI ", waktu_offline);
            } else {
                styler.mode.setText("OFFLINE");
                SManager.setPreferences(this, "ServiceLoad", "run");
            }
        }catch (Exception e){}
    }
}
