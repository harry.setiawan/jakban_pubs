package com.app.kemenhub.jakbanapps.retrofit.model_data.category;

import com.google.gson.annotations.SerializedName;

public class CategoryResponse {
	@SerializedName("code")
	String code;
	@SerializedName("message")
	String message;
	@SerializedName("category")
    Kategori category;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Kategori getCategory() {
		return category;
	}
	public void setCategory(Kategori category) {
		this.category = category;
	}
	
	
}
