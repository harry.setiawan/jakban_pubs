package com.app.kemenhub.jakbanapps.retrofit.model_data.schedule;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SchedulesResponse {
	@SerializedName("code")
	String code;
	@SerializedName("message")
	String message;
	@SerializedName("schedule")
	List<Scheduler> schedule;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<Scheduler> getSchedule() {
		return schedule;
	}
	public void setSchedule(List<Scheduler> schedule) {
		this.schedule = schedule;
	}
	
}
