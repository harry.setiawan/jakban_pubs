package com.app.kemenhub.jakbanapps;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Explode;
import android.transition.Slide;
import android.transition.Transition;
import android.view.MenuItem;
import android.view.WindowManager;

import com.app.kemenhub.jakbanapps.alert.dialog_alert;
import com.app.kemenhub.jakbanapps.fragment.add.f_add_meeting;
import com.app.kemenhub.jakbanapps.fragment.add.f_edit_meeting;
import com.app.kemenhub.jakbanapps.sys.ExceptionHandler;
import com.app.kemenhub.jakbanapps.sys.filechooser.FileUtils;
import com.github.ybq.android.spinkit.style.RotatingCircle;

public class v_edit extends AppCompatActivity {
    public String id_data = "";
    f_edit_meeting meeting;

    private static final String TAG = "FileChooserExampleActivity";
    private static final int REQUEST_CODE = 6384; // onActivityResult request
    // code
    public ProgressDialog dialog_progress;

    public android.support.v4.app.FragmentManager frag;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_v_detil);
        set_dialog();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        frag = getSupportFragmentManager();
        _init_fragment();
        _set_view();
    }

    private void _set_view() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Ubah Agenda Acara");
        id_data = getIntent().getStringExtra("MEETING");
        frag.beginTransaction().setCustomAnimations(R.anim.fadein, R.anim.fadeout).replace(R.id.content_menu, meeting).commit();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    private void _init_fragment() {
        meeting = new f_edit_meeting();
    }


    private void set_dialog() {
        dialog_progress = new ProgressDialog(this);
        RotatingCircle ThreeBounce = new RotatingCircle();
        ThreeBounce.setBounds(0, 0, 100, 100);
        ThreeBounce.setColor(getResources().getColor(R.color.md_light_green_500_25));
        dialog_progress.setIndeterminateDrawable(ThreeBounce);
        dialog_progress.setMessage("Mohon Tunggu..");
        dialog_progress.setCancelable(false);
    }

    public void showChooser() {
        Intent target = FileUtils.createGetContentIntent();
        Intent intent = Intent.createChooser(
                target, "Pilih Dokumen");
        try {
            startActivityForResult(intent, REQUEST_CODE);
        } catch (ActivityNotFoundException e) {
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    if (data != null) {
                        final Uri uri = data.getData();
                        try {
                            meeting.tmp_path_file = FileUtils.getPath(this, uri);
                            String[] nama_file = meeting.tmp_path_file.split("\\/");
                            meeting.tmp_uri = uri;
                            meeting.tmp_nm_file = nama_file[nama_file.length-1];
                            meeting.edt_file.setText(meeting.tmp_nm_file);
                        } catch (Exception e) {
                            dialog_alert alert = new dialog_alert();
                            alert.showDialog(this, "Dokumen yang dipilih error");
                        }
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void go_to(final Class classes) {
        //activity.list_chat.clear();
        if (Build.VERSION.SDK_INT >= 21) {
            Transition exitTrans = new Explode();
            getWindow().setExitTransition(exitTrans);

            Transition reenterTrans = new Slide();
            getWindow().setEnterTransition(reenterTrans);
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this);
            Intent intent = new Intent(this, classes);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent, options.toBundle());
        }else{
            Intent intent = new Intent(this, classes);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.startActivity(intent);
        }
    }
    @Override
    public void onBackPressed() {
        go_to(v_event.class);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
}
