package com.app.kemenhub.jakbanapps.retrofit.model_data.kabupaten;

public class Kabupaten {
	private String id;
	private String provinsi_id;
	private String name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProvinsi_id() {
		return provinsi_id;
	}

	public void setProvinsi_id(String provinsi_id) {
		this.provinsi_id = provinsi_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
