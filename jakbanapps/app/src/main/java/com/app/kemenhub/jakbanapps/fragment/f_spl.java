package com.app.kemenhub.jakbanapps.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.kemenhub.jakbanapps.R;
import com.app.kemenhub.jakbanapps.v_main;
import com.github.ybq.android.spinkit.style.ThreeBounce;

public class f_spl extends Fragment {

    v_main activity;
    ProgressBar progress;
    TextView version;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.f_spl, container, false);
        activity = (v_main) getActivity();

        activity.logo = (ImageView) v.findViewById(R.id.logo);
        activity.static_logo = (ImageView) v.findViewById(R.id.logo);
        version = (TextView) v.findViewById(R.id.version);

        version.setText("Version " + activity.myVersionName);
        progress = (ProgressBar) v.findViewById(R.id.progress_rss);
        ThreeBounce ThreeBounce = new ThreeBounce();
        ThreeBounce.setBounds(0, 0, 100, 100);
        ThreeBounce.setColor(getResources().getColor(R.color.md_white_1000));
        progress.setIndeterminateDrawable(ThreeBounce);
        return v;
    }
}
