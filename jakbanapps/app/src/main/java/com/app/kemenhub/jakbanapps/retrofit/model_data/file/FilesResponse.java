package com.app.kemenhub.jakbanapps.retrofit.model_data.file;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FilesResponse {
	@SerializedName("code")
	String code;
	@SerializedName("message")
	String message;
	@SerializedName("file")
	List<File> file;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<File> getFile() {
		return file;
	}
	public void setFile(List<File> file) {
		this.file = file;
	}
	
	
}
