package com.app.kemenhub.jakbanapps.fragment.add;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import com.app.kemenhub.jakbanapps.R;
import com.app.kemenhub.jakbanapps.adapter.DataAddCategoryAdt;
import com.app.kemenhub.jakbanapps.adapter.DataAddForwardAdt;
import com.app.kemenhub.jakbanapps.adapter.DataEditCategoryAdt;
import com.app.kemenhub.jakbanapps.adapter.DataEditForwardAdt;
import com.app.kemenhub.jakbanapps.alert.dialog_alert;
import com.app.kemenhub.jakbanapps.alert.dialog_alert_confirm;
import com.app.kemenhub.jakbanapps.db.category;
import com.app.kemenhub.jakbanapps.db.folder;
import com.app.kemenhub.jakbanapps.db.schedule;
import com.app.kemenhub.jakbanapps.db.users;
import com.app.kemenhub.jakbanapps.process_action.add_meeting;
import com.app.kemenhub.jakbanapps.process_action.edit_meeting;
import com.app.kemenhub.jakbanapps.sys.utils;
import com.app.kemenhub.jakbanapps.v_add;
import com.app.kemenhub.jakbanapps.v_edit;
import com.app.kemenhub.jakbanapps.v_event;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class f_edit_meeting extends Fragment implements View.OnClickListener, DataEditCategoryAdt.itemCategoryClickCallback,
        DataEditForwardAdt.itemForwardClickCallback, edit_meeting.SavingCallback{

    public v_edit ac;
    View v;

    TextView subtitle;
    public EditText edt_tanggal_diterima, edt_no_surat, edt_no_agenda, edt_dari, edt_perihal, edt_tanggal_start, edt_tanggal_akhir, edt_waktu_start, edt_waktu_akhir, edt_tempat, edt_klasifikasi, edt_catatan, edt_desc, edt_folder, edt_file;
    AppCompatButton download;
    RecyclerView rc_category, rc_forward;
    Button edit;
    schedule schedule;

    List<category> categories = new ArrayList<>();
    List<users> forward = new ArrayList<>();
    DataEditCategoryAdt category_adapter;
    DataEditForwardAdt forward_adapter;
    CardView line, card_btn;

    public Uri tmp_uri = null;
    public String tmp_path_file = "", tmp_nm_file = "";
    public String Dyear = "", Dmonth = "";
    public String tmp_folder_id = "0", tmp_tanggal_diterima = "", tmp_tanggal_start = "", tmp_waktu_start = "", tmp_tanggal_akhr = "", tmp_waktu_akhir = "", tmp_klasifikasi = "0";
    public ArrayList<String> tmp_disposisi = new ArrayList<>();
    public ArrayList<String> tmp_forward = new ArrayList<>();

    edit_meeting process;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.f_add_meeting, container, false);
        ac = (v_edit) getActivity();
        process = new edit_meeting(this);
        process.setsavingCallback(this);
        _init();
        set_data();
        _shown_data();
        return v;
    }

    private void _shown_data() {
        if(!ac.id_data.equals("")){
            schedule = v_event.DBManager.get_scheduleById(ac.id_data);
            try {
                String[] dt_category = schedule.getCategory_id().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\"", "").replaceAll("null", "").split(",");
                for(int i = 0 ; i < dt_category.length ; i ++){
                    tmp_disposisi.add(i, dt_category[i]);
                }
            }catch (Exception x){

            }

            try {
                String[] dt_forward = schedule.getForward_to_user_id().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\"", "").replaceAll("null", "").split(",");
                for(int i = 0 ; i < dt_forward.length ; i ++){
                    tmp_forward.add(i, dt_forward[i]);
                }
            }catch (Exception x){

            }
            if(forward.size() == 0){
                forward.add(0, new users(null, "admin", null,null,null,null,null,null,null,null,null));
            }
            edt_tempat.setText(schedule.getPlace());
            try {
                utils.TAG_FILE = schedule.getFile();
                String[] nm_file = schedule.getFile().split("\\/");
                edt_file.setText(nm_file[nm_file.length - 1]);
                edt_folder.setText(nm_file[nm_file.length-2]);
            }catch (Exception e){
                edt_file.setText("Tidak ada file");
            }

            edt_no_surat.setText(schedule.getNo_surat_perintah());
            if(schedule.getNo_surat_perintah().equals("")){
                edt_no_surat.setText("-");
            }
            edt_no_agenda.setText(schedule.getNo_agenda());
            if(schedule.getNo_agenda().equals("")){
                edt_no_agenda.setText("-");
            }
            edt_dari.setText(schedule.getFrom());
            edt_perihal.setText(schedule.getTitle());
            String[] waktu_terima = schedule.getDatetime_schedule_start().split(" ");
            String[] tgl_terima = waktu_terima[0].split("-");
            if(tgl_terima[1].length() == 1){
                tgl_terima[1] = "0" + tgl_terima[1];
            }
            edt_tanggal_diterima.setText((tgl_terima[2] + "/" + (tgl_terima[1]) + "/" + tgl_terima[0]));
            tmp_tanggal_diterima = waktu_terima[0];

            String[] waktu_start = schedule.getDatetime_schedule_start().split(" ");
            String[] waktu_end = schedule.getDatetime_schedule_end().split(" ");
            String[] tgl_start = waktu_start[0].split("-");
            String[] tgl_end = waktu_end[0].split("-");

            edt_tanggal_start.setText(tgl_start[2] +"/" + tgl_start[1] + "/" +tgl_start[0]);
            tmp_tanggal_start = waktu_start[0];
            Dyear = tgl_start[0];
            Dmonth = tgl_start[1];

            edt_tanggal_akhir.setText(tgl_end[2] +"/" + tgl_end[1] + "/" +tgl_end[0]);
            tmp_tanggal_akhr = waktu_end[0];

            edt_waktu_start.setText(waktu_start[1].substring(0,waktu_start[1].length()-2));
            tmp_waktu_start = edt_waktu_start.getText().toString();
            edt_waktu_akhir.setText(waktu_end[1].substring(0,waktu_end[1].length()-2));
            tmp_waktu_akhir = edt_waktu_akhir.getText().toString();

            if(schedule.getNotes() == null){
                edt_catatan.setText("-");
            }else{
                edt_catatan.setText(schedule.getNotes());
            }

            if(schedule.getCls().toLowerCase().equals("biasa")){
                tmp_klasifikasi = "1";
                edt_klasifikasi.setText("Biasa");
            }else if(schedule.getCls().toLowerCase().equals("segera")){
                tmp_klasifikasi = "2";
                edt_klasifikasi.setText("Segera");
            }else if(schedule.getCls().toLowerCase().equals("sangat segera")){
                tmp_klasifikasi = "3";
                edt_klasifikasi.setText("Sangat Segera");
            }else if(schedule.getCls().toLowerCase().equals("rahasia")){
                tmp_klasifikasi = "4";
                edt_klasifikasi.setText("Rahasia");
            }else{
                tmp_klasifikasi = "0";
                edt_klasifikasi.setText("-");
            }
            category_adapter.notifyDataSetChanged();
            forward_adapter.notifyDataSetChanged();
        }
    }

    private void set_data() {
        categories.clear();
        categories.addAll(v_event.DBManager.list_category());
        forward.clear();
        forward.addAll(v_event.DBManager.list_users_add());
        category_adapter.notifyDataSetChanged();
        forward_adapter.notifyDataSetChanged();
    }

    private void _init() {
        line = (CardView) v.findViewById(R.id.line_card);
        card_btn = (CardView) v.findViewById(R.id.card_btn);
        if (Build.VERSION.SDK_INT < 21) {
            line.setVisibility(View.GONE);
            card_btn.setVisibility(View.GONE);
        }
        edt_no_surat = (EditText) v.findViewById(R.id.edt_no_surat);
        edt_no_agenda = (EditText) v.findViewById(R.id.edt_no_agenda);
        edt_tanggal_diterima = (EditText) v.findViewById(R.id.edt_tgl_diterima);
        edt_dari = (EditText) v.findViewById(R.id.edt_dari);
        edt_perihal = (EditText) v.findViewById(R.id.edt_perihal);
        edt_tanggal_start = (EditText) v.findViewById(R.id.edt_tanggal_agenda_start);
        edt_tanggal_akhir = (EditText) v.findViewById(R.id.edt_tanggal_agenda_akhir);
        edt_waktu_start = (EditText) v.findViewById(R.id.edt_jam_agenda_start);
        edt_waktu_akhir = (EditText) v.findViewById(R.id.edt_jam_agenda_akhir);
        edt_tempat = (EditText) v.findViewById(R.id.edt_tempat);
        edt_klasifikasi = (EditText) v.findViewById(R.id.edt_klasifikasi);
        edt_catatan = (EditText) v.findViewById(R.id.edt_catatan);
        edt_desc = (EditText) v.findViewById(R.id.edt_desc);
        edt_folder = (EditText) v.findViewById(R.id.edt_folder);
        edt_file = (EditText) v.findViewById(R.id.edt_file);
        edit = (Button) v.findViewById(R.id.btn_simpan);
        edit.setText("UBAH");

        edt_tanggal_diterima.setOnClickListener(this);
        edt_klasifikasi.setOnClickListener(this);
        edt_tanggal_start.setOnClickListener(this);
        edt_tanggal_akhir.setOnClickListener(this);
        edt_waktu_start.setOnClickListener(this);
        edt_waktu_akhir.setOnClickListener(this);
        edt_folder.setOnClickListener(this);
        edit.setOnClickListener(this);

        download = (AppCompatButton) v.findViewById(R.id.btn_download);
        rc_category = (RecyclerView) v.findViewById(R.id.rc_category);
        rc_forward = (RecyclerView) v.findViewById(R.id.rc_forward);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ac);
        rc_category.setLayoutManager(layoutManager);
        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(ac);
        rc_forward.setLayoutManager(layoutManager2);
        rc_forward.setVisibility(View.GONE);

        category_adapter = new DataEditCategoryAdt(categories, this);
        rc_category.setAdapter(category_adapter);
        forward_adapter = new DataEditForwardAdt(forward, this);
        rc_forward.setAdapter(forward_adapter);

        category_adapter.setItemCategoryClickCallback(this);
        forward_adapter.setItemForwardClickCallback(this);
        download.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        final int mYear, mMonth, mDay, mHour, mMinute;
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        DatePickerDialog datePickerDialog;
        TimePickerDialog timePickerDialog;
        final dialog_alert alert = new dialog_alert();
        switch (view.getId()){
            case R.id.btn_download:
                if(edt_folder.getText().equals("")){
                    alert.showDialog(ac, "Harap pilih letak dokumen yang akan diupload terlebih dahulu!");
                    edt_folder.requestFocus();
                }else {
                    ac.showChooser();
                }
                break;
            case R.id.edt_tgl_diterima:
                datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                String m = String.valueOf(monthOfYear + 1);
                                String d = String.valueOf(dayOfMonth);
                                if(m.length() == 1){
                                    m = "0" + m;
                                }
                                if(d.length() == 1){
                                    d = "0" + d;
                                }
                                tmp_tanggal_diterima = year + "-" + m + "-" + d;
                                edt_tanggal_diterima.setText(d + "/" + m + "/" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
                break;
            case R.id.edt_tanggal_agenda_start:
                datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                String m = String.valueOf(monthOfYear + 1);
                                String d = String.valueOf(dayOfMonth);
                                if(m.length() == 1){
                                    m = "0" + m;
                                }
                                if(d.length() == 1){
                                    d = "0" + d;
                                }
                                Dyear = String.valueOf(year);
                                Dmonth = String.valueOf(m);
                                tmp_tanggal_start = year + "-" + m + "-" + d;
                                edt_tanggal_start.setText(d + "/" + m + "/" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
                break;
            case R.id.edt_tanggal_agenda_akhir:
                datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                String m = String.valueOf(monthOfYear + 1);
                                String d = String.valueOf(dayOfMonth);
                                if(m.length() == 1){
                                    m = "0" + m;
                                }
                                if(d.length() == 1){
                                    d = "0" + d;
                                }
                                tmp_tanggal_akhr = year + "-" + m + "-" + d;
                                edt_tanggal_akhir.setText(d + "/" + m + "/" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
                break;
            case R.id.edt_jam_agenda_start:
                timePickerDialog = new TimePickerDialog(getContext(),
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                String jam = String.valueOf(hourOfDay);
                                String menit = String.valueOf(minute);
                                if(jam.length() == 1){
                                    jam = "0" + jam;
                                }
                                if(menit.length() == 1){
                                    menit = "0" + menit;
                                }
                                tmp_waktu_start = jam + ":" + menit + ":00";
                                edt_waktu_start.setText(jam + ":" + menit + ":00");
                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();
                break;
            case R.id.edt_jam_agenda_akhir:
                timePickerDialog = new TimePickerDialog(getContext(),
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                String jam = String.valueOf(hourOfDay);
                                String menit = String.valueOf(minute);
                                if(jam.length() == 1){
                                    jam = "0" + jam;
                                }
                                if(menit.length() == 1){
                                    menit = "0" + menit;
                                }
                                tmp_waktu_akhir = jam + ":" + menit + ":00";
                                edt_waktu_akhir.setText(jam + ":" + menit + ":00");
                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();
                break;
            case R.id.edt_klasifikasi:
                ShowAlertDialogKlasifikasi();
                break;
            case R.id.edt_folder:
                ShowAlertDialogFolder();
                break;
            case R.id.btn_simpan:
                if(edt_dari.getText().toString().equals("")){
                    call_alert("Harap isi Permintaan Agenda Acara dari!");
                    edt_dari.requestFocus();
                }else if(edt_perihal.getText().toString().equals("")){
                    call_alert("Harap isi Perihal Agenda Acara!");
                    edt_perihal.requestFocus();
                }else if(tmp_tanggal_start.toString().equals("")){
                    call_alert("Harap isi Tanggal Mulai Agenda Acara!");
                    edt_tanggal_start.requestFocus();
                }else if(tmp_tanggal_akhr.toString().equals("")){
                    call_alert("Harap isi Tanggal Selesai Agenda Acara!");
                    edt_tanggal_akhir.requestFocus();
                }else if(tmp_waktu_start.toString().equals("")){
                    call_alert("Harap isi Waktu Mulai Agenda Acara!");
                    edt_waktu_start.requestFocus();
                }else if(tmp_waktu_akhir.toString().equals("")){
                    call_alert("Harap isi Waktu Selesai Agenda Acara!");
                    edt_waktu_akhir.requestFocus();
                }else if(edt_tempat.getText().toString().equals("")){
                    call_alert("Harap isi Tempat Agenda Acara!");
                    edt_tempat.requestFocus();
                }else{
                    ac.dialog_progress.show();
                    for(int i = 0 ; i < tmp_forward.size(); i++) {
                        if(tmp_forward.get(i).equals("1")){
                            tmp_forward.remove(i);
                            break;
                        }
                    }
                    tmp_forward.add(tmp_forward.size(), "1");

                    tmp_forward.clear();
                    for(int i = 0 ; i < v_event.DBManager.list_users_add().size(); i++) {
                        tmp_forward.add( i, String.valueOf(v_event.DBManager.list_users_add().get(i).getId()));
                    }
                    process.saving();
                }
                break;
        }
    }

    public void call_alert(String msg){
        dialog_alert alert = new dialog_alert();
        alert.showDialog(ac, msg);
    }

    private void ShowAlertDialogFolder() {
        final List<folder> folder = v_event.DBManager.list_folder();
        final List<String> mnt = new ArrayList<String>();
        for(int i = 0 ; i < folder.size() ; i++) {
            mnt.add(folder.get(i).getFolder());
        }
        final CharSequence[] mnts = mnt.toArray(new String[mnt.size()]);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ac);
        dialogBuilder.setTitle("Pilih Folder");
        dialogBuilder.setItems(mnts, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                tmp_folder_id = String.valueOf(folder.get(item).getId());
                edt_folder.setText(mnt.get(item));
            }
        });
        AlertDialog alertDialogObject = dialogBuilder.create();
        alertDialogObject.show();
    }

    public void ShowAlertDialogKlasifikasi() {
        final List<String> mnt = new ArrayList<String>();
        mnt.add("Biasa");
        mnt.add("Segera");
        mnt.add("Sangat Segera");
        mnt.add("Rahasia");
        final CharSequence[] mnts = mnt.toArray(new String[mnt.size()]);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ac);
        dialogBuilder.setTitle("Pilih Klasifikasi");
        dialogBuilder.setItems(mnts, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                edt_klasifikasi.setText(mnt.get(item));
            }
        });
        AlertDialog alertDialogObject = dialogBuilder.create();
        alertDialogObject.show();
    }

    @Override
    public void onItemClick(int p, boolean data) {
        int isinya = 0;
        try {
            isinya = tmp_disposisi.size();
        }catch (Exception e){
        }
        if(data){
            tmp_disposisi.add( isinya, String.valueOf(categories.get(p).getId()));
        }else{
            for(int i = 0 ; i < tmp_disposisi.size(); i++) {
                if(tmp_disposisi.get(i).equals(String.valueOf(categories.get(p).getId()))){
                    tmp_disposisi.remove(i);
                    break;
                }
            }
        }
    }

    @Override
    public void onItemForwardClick(int p, boolean data) {
        int isinya = 0;
        try {
            isinya = tmp_forward.size();
        }catch (Exception e){
        }
        if(data){
            tmp_forward.add( isinya, String.valueOf(forward.get(p).getId()));
        }else{
            for(int i = 0 ; i < tmp_forward.size(); i++) {
                if(tmp_forward.get(i).equals(String.valueOf(forward.get(p).getId()))){
                    tmp_forward.remove(i);
                    break;
                }
            }
        }
    }

    @Override
    public void savefinishProcess(boolean data) {
        dialog_alert_confirm alert = new dialog_alert_confirm();

        ac.dialog_progress.hide();
        if(data){
            alert.showDialog(ac, "Agenda Acara berhasil di ubah.", new dialog_alert_confirm.Response() {
                @Override
                public void processFinish(boolean output) {
                    ac.onBackPressed();
                }
            });
        }else{
            alert.showDialog(ac, "Agenda Acara gagal di ubah.", new dialog_alert_confirm.Response() {
                @Override
                public void processFinish(boolean output) {
                    ac.onBackPressed();
                }
            });
        }
    }
}
