package com.app.kemenhub.jakbanapps.retrofit.model_data.dinas;

import com.app.kemenhub.jakbanapps.retrofit.model_data.cls.Cls;
import com.app.kemenhub.jakbanapps.retrofit.model_data.position.Position;

public class Employee {
	private String id;

	private String name;

	private String email;

	private String nip;

	private Position position;
	
	private Cls cls;

	public Employee(String id, String name, String email, String nip, Position position, Cls cls) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.nip = nip;
		this.position = position;
		this.cls = cls;
	}

	public Employee(){
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public Cls getCls() {
		return cls;
	}

	public void setCls(Cls cls) {
		this.cls = cls;
	}
}
