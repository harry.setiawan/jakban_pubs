package com.app.kemenhub.jakbanapps;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Vibrator;
import android.os.Bundle;
import android.transition.Explode;
import android.transition.Slide;
import android.transition.Transition;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.app.kemenhub.jakbanapps.db.manager.DManager;
import com.app.kemenhub.jakbanapps.db.manager.IDManager;
import com.app.kemenhub.jakbanapps.db.schedule;
import com.app.kemenhub.jakbanapps.sys.ExceptionHandler;
import com.app.kemenhub.jakbanapps.sys.SessionManager;

public class v_alarm extends Activity {

    String id_data= "";
    TextView perihal, waktu_acara, tempat_acara, catatan;
    Button ok;
    schedule sch = null;
    public static IDManager DBManager;
    public SessionManager SManager;
    Uri notification;
    Vibrator vib;
    VibratePattern task;
    MyTaskParams params;
    MediaPlayer mp;
    public static boolean loader = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_v_alarm);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        DBManager = new DManager(this);
        SManager = new SessionManager();
        loader = true;

        id_data = getIntent().getStringExtra("ALARM");
        ////Log.i("ALARMNYA", id_data);
        sch = DBManager.get_scheduleById(id_data);
        _init();
        set_data();

        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(loader) {
                    loader = false;
                    SManager.setPreferences(getApplicationContext(), "alarm", null);
                    try {
                        mp.stop();
                        vib.cancel();
                        task.cancel(true);
                    }catch (Exception e){}
                    finish();
                    System.exit(0);
                }
            }
        }, 1000 * 60);
    }

    private static class MyTaskParams {
        int dot, dash, gap;

        MyTaskParams (int dot, int dash, int gap) {
            this.dot = dot;
            this.dash = dash;
            this.gap = gap;
        }
    }

    private void startTask() {
        params = new MyTaskParams(200,500,200);
        task = new VibratePattern();
        task.execute(params);
    }

    public Integer onVibrate (Integer dot, Integer dash, Integer gap) {
        long[] pattern = {
                0,
                dot, gap, dash, gap, dot, gap, dot
        };

        vib.vibrate(pattern, -1);
        int span = dot + gap + dash + gap + dot + gap + dot + gap;
        return span;
    }

    private class VibratePattern extends AsyncTask<MyTaskParams, Void, Integer> {

        @Override
        protected Integer doInBackground(MyTaskParams... params) {
            int span;
            span = onVibrate(params[0].dot,params[0].dash,params[0].gap);
            return span;
        }

        @Override
        protected void onPostExecute(Integer span) {
            final android.os.Handler handler = new android.os.Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (!isCancelled()) {
                        startTask();
                    }
                }
            }, span);
        }
    }

    private void set_data() {
        perihal.setText(": " + sch.getTitle());
        String[] waktu_start = sch.getDatetime_schedule_start().split(" ");
        String[] waktu_end = sch.getDatetime_schedule_end().split(" ");

        waktu_acara.setText("" + waktu_start[1].substring(0,waktu_start[1].length()-2)+ " - " +waktu_end[1].substring(0,waktu_start[1].length()-2));
        tempat_acara.setText(": " + sch.getPlace());
        catatan.setText(": " + sch.getNotes());
    }

    private void _init() {
        perihal = (TextView) findViewById(R.id.perihal);
        waktu_acara = (TextView) findViewById(R.id.waktu_acara);
        tempat_acara = (TextView) findViewById(R.id.tempat_acara);
        catatan = (TextView) findViewById(R.id.catatan);
        ok = (Button) findViewById(R.id.btn_ok);

        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        mp = MediaPlayer.create(getApplicationContext(), notification);
        mp.setLooping(true);
        vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        AudioManager audio = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        switch (audio.getRingerMode()) {
            case AudioManager.RINGER_MODE_NORMAL:
                mp.start();
                startTask();
                break;
            case AudioManager.RINGER_MODE_SILENT:
                break;
            case AudioManager.RINGER_MODE_VIBRATE:
                startTask();
                break;
        }

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loader = false;
                SManager.setPreferences(getApplicationContext(), "alarm", null);
                try {
                    mp.stop();
                    vib.cancel();
                    task.cancel(true);
                }catch (Exception e){}
                task = null;
                go_to_action(v_event.class);
            }
        });
    }

    public void go_to_action(final Class classes) {
        if (Build.VERSION.SDK_INT >= 21) {
            Transition exitTrans = new Explode();
            getWindow().setExitTransition(exitTrans);

            Transition reenterTrans = new Slide();
            getWindow().setEnterTransition(reenterTrans);
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this);
            Intent intent = new Intent(this, classes);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent, options.toBundle());
        }else{
            Intent intent = new Intent(this, classes);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.startActivity(intent);
        }
    }
}
