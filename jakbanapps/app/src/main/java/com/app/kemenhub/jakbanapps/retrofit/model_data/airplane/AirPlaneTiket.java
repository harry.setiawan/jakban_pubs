package com.app.kemenhub.jakbanapps.retrofit.model_data.airplane;

import com.google.gson.annotations.SerializedName;

public class AirPlaneTiket {
	@SerializedName("id")
	private String id;
	@SerializedName("city_first_id")
	private String city_first_id;
	@SerializedName("city_destination_id")
	private String city_destination_id;
	@SerializedName("business")
	private String business;
	@SerializedName("economy")
	private String economy;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCity_first_id() {
		return city_first_id;
	}

	public void setCity_first_id(String city_first_id) {
		this.city_first_id = city_first_id;
	}

	public String getCity_destination_id() {
		return city_destination_id;
	}

	public void setCity_destination_id(String city_destination_id) {
		this.city_destination_id = city_destination_id;
	}

	public String getBusiness() {
		return business;
	}

	public void setBusiness(String business) {
		this.business = business;
	}

	public String getEconomy() {
		return economy;
	}

	public void setEconomy(String economy) {
		this.economy = economy;
	}
	
}
