package com.app.kemenhub.jakbanapps;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Explode;
import android.transition.Slide;
import android.transition.Transition;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import com.app.kemenhub.jakbanapps.fragment.detil.f_detil_dinas;
import com.app.kemenhub.jakbanapps.fragment.detil.f_detil_disposisi;
import com.app.kemenhub.jakbanapps.fragment.detil.f_detil_meeting;
import com.app.kemenhub.jakbanapps.sys.ExceptionHandler;
import com.app.kemenhub.jakbanapps.sys.utils;

public class v_detil extends AppCompatActivity {
    public String id_data = "";
    f_detil_disposisi disposisi;
    f_detil_meeting meeting;
    f_detil_dinas dinas;
    ActivityOptions options;

    public android.support.v4.app.FragmentManager frag;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_v_detil);
        options = ActivityOptions.makeSceneTransitionAnimation(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        frag = getSupportFragmentManager();
        _init_fragment();
        _set_view();
    }

    private void _set_view() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if(getIntent().getStringExtra("DISPOSISI") != null){
            ////Log.i("DISPOSISI", id_data);
            toolbar.setTitle("Detil Disposisi");
            id_data = getIntent().getStringExtra("DISPOSISI");
            frag.beginTransaction().setCustomAnimations(R.anim.fadein, R.anim.fadeout).replace(R.id.content_menu, disposisi).commit();
        }else if(getIntent().getStringExtra("MEETING") != null){
            ////Log.i("MEETING", id_data);
            toolbar.setTitle("Detil Agenda Acara");
            id_data = getIntent().getStringExtra("MEETING");
            frag.beginTransaction().setCustomAnimations(R.anim.fadein, R.anim.fadeout).replace(R.id.content_menu, meeting).commit();
        }else if(getIntent().getStringExtra("DINAS") != null){
            ////Log.i("DINAS", id_data);
            toolbar.setTitle("Detil Dinas");
            id_data = getIntent().getStringExtra("DINAS");
            frag.beginTransaction().setCustomAnimations(R.anim.fadein, R.anim.fadeout).replace(R.id.content_menu, dinas).commit();
        }else {
            go_to(v_event.class);
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    private void _init_fragment() {
        disposisi = new f_detil_disposisi();
        meeting = new f_detil_meeting();
        dinas = new f_detil_dinas();
    }

    public void go_to(final Class classes) {
        //activity.list_chat.clear();
        try {
            if (Build.VERSION.SDK_INT >= 21) {
                Transition exitTrans = new Explode();
                getWindow().setExitTransition(exitTrans);

                Transition reenterTrans = new Slide();
                getWindow().setEnterTransition(reenterTrans);
                Intent intent = new Intent(this, classes);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                this.startActivity(intent, options.toBundle());
            } else {
                Intent intent = new Intent(this, classes);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                this.startActivity(intent);
            }
        }catch (Exception e){
            Intent intent = new Intent(this, classes);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.startActivity(intent);
        }
    }
    @Override
    public void onBackPressed() {
        go_to(v_event.class);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
}
