package com.app.kemenhub.jakbanapps.retrofit;

import android.content.Context;
import android.util.Log;

import com.app.kemenhub.jakbanapps.db.DaoSession;
import com.app.kemenhub.jakbanapps.db.airplane_ticket;
import com.app.kemenhub.jakbanapps.db.category;
import com.app.kemenhub.jakbanapps.db.disposition;
import com.app.kemenhub.jakbanapps.db.file;
import com.app.kemenhub.jakbanapps.db.folder;
import com.app.kemenhub.jakbanapps.db.hotel_fee;
import com.app.kemenhub.jakbanapps.db.kabupaten;
import com.app.kemenhub.jakbanapps.db.kecamatan;
import com.app.kemenhub.jakbanapps.db.kelurahan;
import com.app.kemenhub.jakbanapps.db.manager.DManager;
import com.app.kemenhub.jakbanapps.db.manager.IDManager;
import com.app.kemenhub.jakbanapps.db.perjalanan_dinas;
import com.app.kemenhub.jakbanapps.db.perjalanan_dinas_jenis;
import com.app.kemenhub.jakbanapps.db.place;
import com.app.kemenhub.jakbanapps.db.position;
import com.app.kemenhub.jakbanapps.db.provinsi;
import com.app.kemenhub.jakbanapps.db.roles;
import com.app.kemenhub.jakbanapps.db.schedule;
import com.app.kemenhub.jakbanapps.db.taxi_fee;
import com.app.kemenhub.jakbanapps.db.user_class;
import com.app.kemenhub.jakbanapps.db.user_mobile;
import com.app.kemenhub.jakbanapps.db.user_role;
import com.app.kemenhub.jakbanapps.db.users;
import com.app.kemenhub.jakbanapps.db.vehicle_rent;
import com.app.kemenhub.jakbanapps.retrofit.model_data.airplane.AirPlaneTiket;
import com.app.kemenhub.jakbanapps.retrofit.model_data.category.Kategori;
import com.app.kemenhub.jakbanapps.retrofit.model_data.cls.Cls;
import com.app.kemenhub.jakbanapps.retrofit.model_data.dinas.Dinaser;
import com.app.kemenhub.jakbanapps.retrofit.model_data.disposisi.Disposiser;
import com.app.kemenhub.jakbanapps.retrofit.model_data.file.File;
import com.app.kemenhub.jakbanapps.retrofit.model_data.folder.Folder;
import com.app.kemenhub.jakbanapps.retrofit.model_data.hotel_fee.HotelFees;
import com.app.kemenhub.jakbanapps.retrofit.model_data.jenis_dinas.Jenis;
import com.app.kemenhub.jakbanapps.retrofit.model_data.kabupaten.Kabupaten;
import com.app.kemenhub.jakbanapps.retrofit.model_data.kecamatan.Kecamatan;
import com.app.kemenhub.jakbanapps.retrofit.model_data.kelurahan.Kelurahan;
import com.app.kemenhub.jakbanapps.retrofit.model_data.mobile.Mobile;
import com.app.kemenhub.jakbanapps.retrofit.model_data.place.Places;
import com.app.kemenhub.jakbanapps.retrofit.model_data.position.Position;
import com.app.kemenhub.jakbanapps.retrofit.model_data.provinsi.Provinsi;
import com.app.kemenhub.jakbanapps.retrofit.model_data.role.Role;
import com.app.kemenhub.jakbanapps.retrofit.model_data.schedule.Scheduler;
import com.app.kemenhub.jakbanapps.retrofit.model_data.taxi_fee.TaxyFees;
import com.app.kemenhub.jakbanapps.retrofit.model_data.user.User;
import com.app.kemenhub.jakbanapps.retrofit.model_data.vehicle.Vehicles;
import com.app.kemenhub.jakbanapps.v_event;

import java.util.ArrayList;
import java.util.List;

public class SDM {
    public static IDManager DBManager;

    public SDM(Context ctx){
        DBManager = new DManager(ctx);
    }

    public users set_user_FS(User user){
        Long id_usr = Long.parseLong(user.getId());
        position pst = new position();
        if(user.getPosition() == null){
            pst.setId(null);
        }else{
            pst.setId(Long.parseLong(user.getPosition().getId()));
        }
        user_class cls = new user_class();
        if(user.getCls() == null){
            cls.setId(null);
        }else{
            cls.setId(Long.parseLong(user.getCls().getId()));
        }
        users dt = new users(
                id_usr,
                user.getName(),
                user.getUsername(),
                user.getEmail(),
                user.getPassword(),
                user.getNip(),
                user.getPhone(),
                user.getBirth(),
                user.getAddress(),
                String.valueOf(pst.getId()),
                String.valueOf(cls.getId())
        );
        return dt;
    }
    public position set_position_FS(Position position){
        Long id_pst = Long.parseLong(position.getId());
        position dt = new position(
                id_pst,
                position.getPosition()
        );
        return dt;
    }
    public user_class set_cls_FS(Cls cls){
        Long id_cls = Long.parseLong(cls.getId());
        user_class dt = new user_class(id_cls,
                cls.getName(),
                cls.getType(), null);
        return dt;
    }
    public boolean save_list_airplane_ticket(List<AirPlaneTiket> airPlaneTiket) {
        if(airPlaneTiket.size() != 0){
            List<airplane_ticket> dt = new ArrayList<>();
            for(int i = 0 ; i < airPlaneTiket.size() ; i++) {
                dt.add(i, new airplane_ticket(Long.parseLong(airPlaneTiket.get(i).getId()),
                        airPlaneTiket.get(i).getCity_first_id(), airPlaneTiket.get(i).getCity_destination_id(),
                        airPlaneTiket.get(i).getBusiness() , airPlaneTiket.get(i).getEconomy(), "",""));
            }
            try {
                DBManager.delete_airplane_ticket();
            }catch (Exception e){}
            DBManager.insert_airplane_ticket_InTX(dt);
        }
        return true;
    }
    public boolean save_list_category(List<Kategori> kategori) {

        ////Log.i("ALL CATE", kategori.size()+"");
        if(kategori.size() != 0){
            List<category> dt = new ArrayList<>();
            for(int i = 0 ; i < kategori.size() ; i++) {
                dt.add(i, new category(Long.parseLong(kategori.get(i).getId()), kategori.get(i).getName()));
            }
            try {
                DBManager.delete_category();
            }catch (Exception e){}
            DBManager.insert_category_InTX(dt);
        }
        return true;
    }
    public boolean save_list_class(List<Cls> cls) {
        if(cls.size() != 0){
            List<user_class> dt = new ArrayList<>();
            for(int i = 0 ; i < cls.size() ; i++) {
                dt.add(i,new user_class(Long.parseLong(cls.get(i).getId()), cls.get(i).getName(), cls.get(i).getType(), ""));
            }
            try {
                DBManager.delete_user_class();
            }catch (Exception e){}
            DBManager.insert_user_class_InTX(dt);
        }
        return true;
    }
    public boolean save_list_dinas(List<Dinaser> dinas) {
        if(dinas.size() != 0){
            List<perjalanan_dinas> dt = new ArrayList<>();
            for(int i = 0 ; i < dinas.size() ; i++) {
                String emp_id = null, plane_id = null, Dplace_id = null, Aplace_id = null, HotelFee_id = null, VehiclePrice_id = null, TaxiTD = null, TaxiTA = null;
                if(dinas.get(i).getEmployee() != null){
                    emp_id = dinas.get(i).getEmployee().getId();
                }
                if(dinas.get(i).getAirplane_ticket() != null){
                    plane_id = dinas.get(i).getAirplane_ticket().getId();
                }
                if(dinas.get(i).getDeparture_place() != null){
                    Dplace_id = dinas.get(i).getDeparture_place().getId();
                }
                if(dinas.get(i).getArrival_place() != null){
                    Aplace_id = dinas.get(i).getArrival_place().getId();
                }
                if(dinas.get(i).getHotel_fee() != null){
                    HotelFee_id = dinas.get(i).getHotel_fee().getId();
                }
                if(dinas.get(i).getVehicle_price() != null){
                    VehiclePrice_id = dinas.get(i).getVehicle_price().getId();
                }
                if(dinas.get(i).getTaxi_ticket_departure() != null){
                    TaxiTD = dinas.get(i).getTaxi_ticket_departure().getId();
                }
                if(dinas.get(i).getTaxi_ticket_arrival() != null){
                    TaxiTA = dinas.get(i).getTaxi_ticket_arrival().getId();
                }
                dt.add(i,new perjalanan_dinas(
                        Long.parseLong(dinas.get(i).getId()),
                        dinas.get(i).getCode(),
                        emp_id,
                        dinas.get(i).getDescription(),
                        dinas.get(i).getRemarks(),
                        dinas.get(i).getNo_surat_perintah(),
                        dinas.get(i).getDate_departure(),
                        dinas.get(i).getDate_arrival(),
                        dinas.get(i).getDay_duration(),
                        dinas.get(i).getType().getCode(),
                        Dplace_id,
                        Aplace_id,
                        dinas.get(i).getTransportation_type(),
                        dinas.get(i).getTransportation_class(),
                        plane_id,
                        dinas.get(i).getWith_vehicle_rent(),
                        dinas.get(i).getVehicle_rent_type(),
                        VehiclePrice_id,
                        dinas.get(i).getWith_hotel(),
                        HotelFee_id,
                        dinas.get(i).getHotel_amount_type(),
                        dinas.get(i).getHotel_amount(),
                        dinas.get(i).getTransportation_amount_type(),
                        dinas.get(i).getTransportation_amount(),
                        dinas.get(i).getWith_taxi(),
                        dinas.get(i).getTaxi_qty_departure(),
                        dinas.get(i).getTaxi_qty_arrival(),
                        TaxiTD,
                        TaxiTA,
                        dinas.get(i).getTaxi_amount_departure_type(),
                        dinas.get(i).getTaxi_amount_arrival_type(),
                        dinas.get(i).getTaxi_amount_departure(),
                        dinas.get(i).getTaxi_amount_arrival(),
                        dinas.get(i).getTaxi_total_amount(),
                        dinas.get(i).getVehicle_amount_type(),
                        dinas.get(i).getVehicle_amount(),
                        dinas.get(i).getType_amount(),
                        dinas.get(i).getTotal_amount(),false
                ));
            }
            try {
                DBManager.delete_perjalanan_dinas();
            }catch (Exception e){}
            DBManager.insert_perjalanan_dinas_InTX(dt);
        }
        return true;
    }
    public boolean save_list_disposisi(List<Disposiser> disposisi) {
        if(disposisi.size() != 0){
            List<disposition> dt = new ArrayList<>();
            for(int i = 0 ; i < disposisi.size() ; i++) {
                String dt_category  = "";
                try {
                    if (disposisi.get(i).getCategories() != null || !disposisi.get(i).getCategories().equals("null")) {
                        if (disposisi.get(i).getCategories().size() != 0) {
                            for (int x = 0; x < disposisi.get(i).getCategories().size(); x++) {
                                dt_category += disposisi.get(i).getCategories().get(x).getId() + ",";
                            }
                            dt_category = dt_category.substring(0, dt_category.length() - 1);
                        }
                    }
                }catch (Exception e){}
                String dt_forward = null;
                try {
                    if (disposisi.get(i).getForward().size() != 0) {
                        for (int x = 0; x < disposisi.get(i).getForward().size(); x++) {
                            dt_forward += disposisi.get(i).getForward().get(x).getId() + ",";
                        }
                        dt_forward = dt_forward.substring(0, dt_forward.length() - 1);
                    }
                }catch (Exception e){}

                dt.add(i, new disposition(
                        Long.parseLong(disposisi.get(i).getId()),
                        dt_category,
                        disposisi.get(i).getFrom(),
                        disposisi.get(i).getNo_surat_perintah(),
                        disposisi.get(i).getNo_agenda(),
                        disposisi.get(i).getTitle(),
                        disposisi.get(i).getDesc(),
                        disposisi.get(i).getNotes(),
                        dt_forward,
                        disposisi.get(i).getCls(),
                        disposisi.get(i).getFile(),false));
            }
            try {
                DBManager.delete_disposition();
            }catch (Exception e){}
            DBManager.insert_disposition_InTX(dt);
        }
        return true;
    }
    public boolean save_list_file(List<File> files) {
        if(files.size() != 0){
            List<file> dt = new ArrayList<>();
            for(int i = 0 ; i < files.size() ; i++) {
                dt.add(i,new file(Long.parseLong(files.get(i).getId()),
                        files.get(i).getFolder_id(),
                        files.get(i).getPermalink(),
                        files.get(i).getFile(),
                        files.get(i).getExt(),
                        files.get(i).getTitle(),
                        files.get(i).getSize(),
                        files.get(i).getStatus()));
            }
            try {
                DBManager.delete_file();
            }catch (Exception e){}
            DBManager.insert_file_InTX(dt);
        }
        return true;
    }
    public boolean save_list_folder(List<Folder> folders) {
        if(folders.size() != 0){
            List<folder> dt = new ArrayList<>();
            for(int i = 0 ; i < folders.size() ; i++) {
                dt.add(i,new folder(Long.parseLong(folders.get(i).getId()),
                        folders.get(i).getParent_id(),
                        folders.get(i).getName(),
                        folders.get(i).getDesc()));
            }
            try {
                DBManager.delete_folder();
            }catch (Exception e){}
            DBManager.insert_folder_InTX(dt);
        }
        return true;
    }
    public boolean save_list_hotel_fee(List<HotelFees> hotelFees) {
        if(hotelFees.size() != 0){
            List<hotel_fee> dt = new ArrayList<>();
            for(int i = 0 ; i < hotelFees.size() ; i++) {
                dt.add(i,new hotel_fee(Long.parseLong(hotelFees.get(i).getId()),
                        hotelFees.get(i).getProvinsi().getId(),
                        hotelFees.get(i).getUnit(),
                        hotelFees.get(i).getEselon1(),
                        hotelFees.get(i).getEselon2(),
                        hotelFees.get(i).getEselon3(),
                        hotelFees.get(i).getEselon4(),
                        hotelFees.get(i).getGolongan1_2()));
            }
            try {
                DBManager.delete_hotel_fee();
            }catch (Exception e){}
            DBManager.insert_hotel_fee_InTX(dt);
        }
        return true;
    }
    public boolean save_list_jenis_dinas(List<Jenis> jenis) {
        if(jenis.size() != 0){
            List<perjalanan_dinas_jenis> dt = new ArrayList<>();
            for(int i = 0 ; i < jenis.size() ; i++) {
                dt.add(i, new perjalanan_dinas_jenis(
                        jenis.get(i).getCode(),
                        jenis.get(i).getJenis()));
            }
            try {
                DBManager.delete_jenis_dinas();
            }catch (Exception e){}
            DBManager.insert_jenis_dinas_InTX(dt);
        }
        return true;
    }
    public boolean save_list_kecamatan(List<Kecamatan> kcmt) {
        if(kcmt.size() != 0){
            List<kecamatan> dt = new ArrayList<>();
            for(int i = 0 ; i < kcmt.size() ; i++) {
                dt.add(i,new kecamatan(
                        Long.parseLong(kcmt.get(i).getId()),
                        kcmt.get(i).getKabupaten_id(),
                        kcmt.get(i).getName()));
            }
            try {
                DBManager.delete_kecamatan();
            }catch (Exception e){}
            DBManager.insert_kecamatan_InTX(dt);
        }
        return true;
    }
    public boolean save_list_kelurahan(List<Kelurahan> klr) {
        if(klr.size() != 0){
            List<kelurahan> dt = new ArrayList<>();
            for(int i = 0 ; i < klr.size() ; i++) {
                dt.add(i,new kelurahan(
                        Long.parseLong(klr.get(i).getId()),
                        klr.get(i).getKecamatan_id(),
                        klr.get(i).getName()));
            }
            try {
                DBManager.delete_kelurahan();
            }catch (Exception e){}
            DBManager.insert_kelurahan_InTX(dt);
        }
        return true;
    }
    public boolean save_list_kabupaten(List<Kabupaten> kab) {
        if(kab.size() != 0){
            List<kabupaten> dt = new ArrayList<>();
            for(int i = 0 ; i < kab.size() ; i++) {
                dt.add(i,new kabupaten(
                        Long.parseLong(kab.get(i).getId()),
                        kab.get(i).getProvinsi_id(),
                        kab.get(i).getName()));
            }
            try {
                DBManager.delete_kabupaten();
            }catch (Exception e){}
            DBManager.insert_kabupaten_InTX(dt);
        }
        return true;
    }
    public boolean save_list_mobile(List<Mobile> mbl) {
        if(mbl.size() != 0){
            List<user_mobile> dt = new ArrayList<>();
            for(int i = 0 ; i < mbl.size() ; i++) {
                dt.add(i,new user_mobile(
                        Long.parseLong(mbl.get(i).getId()),
                        mbl.get(i).getToken()));
            }
            try {
                DBManager.delete_user_mobile();
            }catch (Exception e){}
            DBManager.insert_user_mobile_InTX(dt);
        }
        return true;
    }
    public boolean save_list_place(List<Places> plc) {
        if(plc.size() != 0){
            List<place> dt = new ArrayList<>();
            for(int i = 0 ; i < plc.size() ; i++) {
                dt.add(i,new place(
                        Long.parseLong(plc.get(i).getId()),
                        plc.get(i).getProvinsi().getId(),
                        plc.get(i).getName(),
                        plc.get(i).getType()));
            }
            try {
                DBManager.delete_place();
            }catch (Exception e){}
            DBManager.insert_place_InTX(dt);
        }
        return true;
    }
    public boolean save_list_position(List<Position> posisi) {
        if(posisi.size() != 0){
            List<position> dt = new ArrayList<>();
            for(int i = 0 ; i < posisi.size() ; i++) {
                dt.add(i,new position(
                        Long.parseLong(posisi.get(i).getId()),
                        posisi.get(i).getPosition()));
            }
            try {
                DBManager.delete_position();
            }catch (Exception e){}
            DBManager.insert_position_InTX(dt);
        }
        return true;
    }
    public boolean save_list_provinsi(List<Provinsi> prov) {
        if(prov.size() != 0){
            List<provinsi> dt = new ArrayList<>();
            for(int i = 0 ; i < prov.size() ; i++) {
                dt.add(i,new provinsi(
                        Long.parseLong(prov.get(i).getId()),
                        prov.get(i).getName(),
                        prov.get(i).getCode()));
            }
            try {
                DBManager.delete_provinsi();
            }catch (Exception e){}
            DBManager.insert_provinsi_InTX(dt);
        }
        return true;
    }
    public boolean save_list_role(List<Role> rol) {
        if(rol.size() != 0) {
            List<roles> dt = new ArrayList<>();
            for (int i = 0; i < rol.size(); i++) {
                dt.add(i,new roles(
                        Long.parseLong(rol.get(i).getId()),
                        rol.get(i).getName(),
                        rol.get(i).getDisplay()));
            }
            try {
                DBManager.delete_roles();
            }catch (Exception e){}
            DBManager.insert_roles_InTX(dt);
        }
        return true;
    }
    public boolean save_list_meeting(List<Scheduler> schedu) {
        if(schedu.size() != 0) {
            List<schedule> dt = new ArrayList<>();
            for (int i = 0; i < schedu.size(); i++) {
                String dt_category  = "";
                try {
                    if (schedu.get(i).getCategories() != null || !schedu.get(i).getCategories().equals("null")) {
                        if (schedu.get(i).getCategories().size() != 0) {
                            for (int x = 0; x < schedu.get(i).getCategories().size(); x++) {
                                dt_category += schedu.get(i).getCategories().get(x).getId() + ",";
                            }
                            dt_category = dt_category.substring(0, dt_category.length() - 1);
                        }
                    }
                }catch (Exception e){}
                String dt_forward = null;
                try {
                    if (schedu.get(i).getForward().size() != 0) {
                        for (int x = 0; x < schedu.get(i).getForward().size(); x++) {
                            dt_forward += schedu.get(i).getForward().get(x).getId() + ",";
                        }
                        dt_forward = dt_forward.substring(0, dt_forward.length() - 1);
                    }
                }catch (Exception e){}
                dt.add(i,new schedule(
                        Long.parseLong(schedu.get(i).getId()),
                        dt_category,
                        schedu.get(i).getFrom(),
                        schedu.get(i).getTitle(),
                        schedu.get(i).getDatetime_schedule_start(),
                        schedu.get(i).getDatetime_schedule_end(),
                        schedu.get(i).getTgl_diterima(),
                        schedu.get(i).getNo_surat_perintah(),
                        schedu.get(i).getNo_agenda(),
                        schedu.get(i).getPlace(),
                        schedu.get(i).getDesc(),
                        schedu.get(i).getNotes(),
                        dt_forward,
                        schedu.get(i).getCls(),
                        schedu.get(i).getFile(),false));
            }
            try {
                DBManager.delete_schedule();
            }catch (Exception e){}
            DBManager.insert_schedule_InTX(dt);
        }
        return true;
    }
    public boolean save_taxi_fee(List<TaxyFees> taxyFes) {
        if(taxyFes.size() != 0) {
            List<taxi_fee> dt = new ArrayList<>();
            for (int i = 0; i < taxyFes.size(); i++) {
                dt.add(i, new taxi_fee(
                        Long.parseLong(taxyFes.get(i).getId()),
                        taxyFes.get(i).getProvinsi().getId(),
                        taxyFes.get(i).getUnit(),
                        taxyFes.get(i).getFee()));
            }
            try {
                DBManager.delete_taxi_fee();
            }catch (Exception e){}
            DBManager.insert_taxi_fee_InTX(dt);
        }
        return true;
    }
    public boolean save_list_user(List<User> usr) {
        if(usr.size() != 0) {
            List<users> dt = new ArrayList<>();
            for (int i = 0; i < usr.size(); i++) {
                position pst = new position();
                if(usr.get(i).getPosition() == null){
                    pst.setId(null);
                }else{
                    pst.setId(Long.parseLong(usr.get(i).getPosition().getId()));
                }
                user_class cls = new user_class();
                if(usr.get(i).getCls() == null){
                    cls.setId(null);
                }else{
                    cls.setId(Long.parseLong(usr.get(i).getCls().getId()));
                }
                dt.add(i,new users(
                        Long.parseLong(usr.get(i).getId()),
                        usr.get(i).getName(),
                        usr.get(i).getUsername(),
                        usr.get(i).getEmail(),
                        usr.get(i).getPassword(),
                        usr.get(i).getNip(),
                        usr.get(i).getPhone(),
                        usr.get(i).getBirth(),
                        usr.get(i).getAddress(),
                        String.valueOf(pst.getId()),
                        String.valueOf(cls.getId())));
            }
            try {
                DBManager.delete_users();
            }catch (Exception e){}
            DBManager.insert_users_InTX(dt);
        }
        return true;
    }

    public boolean save_list_user_role(List<User> usr) {
        List<user_role> roles = new ArrayList<>();
        int y = 0;
        if(usr.size() != 0) {
            for (int i = 0; i < usr.size(); i++) {
                List<Role> data = usr.get(i).getRole();
                for(int x = 0 ; x < data.size() ; x++){
                    roles.add(y, new user_role(Long.parseLong(String.valueOf(y)), usr.get(i).getId(), data.get(x).getId()));
                    y++;
                }
            }
            try {
                DBManager.delete_user_role();
            }catch (Exception e){}
            DBManager.insert_user_role_InTX(roles);
        }
        return true;
    }

    public boolean save_list_vehicle(List<Vehicles> vehicles) {
        if(vehicles.size() != 0) {
            List<vehicle_rent> dt = new ArrayList<>();
            for (int i = 0; i < vehicles.size(); i++) {
                provinsi pst = new provinsi();
                if(vehicles.get(i).getProvinsi() == null){
                    pst.setId(null);
                }else{
                    pst.setId(Long.parseLong(vehicles.get(i).getProvinsi().getId()));
                }
                dt.add(i,new vehicle_rent(
                        Long.parseLong(vehicles.get(i).getId()),
                        String.valueOf(pst.getId()),
                        vehicles.get(i).getUnit(),
                        vehicles.get(i).getCar(),
                        vehicles.get(i).getMidle_bus(),
                        vehicles.get(i).getBig_bus()));
            }
            try {
                DBManager.delete_vehicle_rent();
            }catch (Exception e){}
            DBManager.insert_vehicle_rent_InTX(dt);
        }
        return true;
    }

    public void save_airplane_ticket(AirPlaneTiket airPlaneTiket) {
        airplane_ticket dt = new airplane_ticket();
        dt.setId(Long.parseLong(airPlaneTiket.getId()));
        dt.setBusiness(airPlaneTiket.getBusiness());
        dt.setCity_first_id(airPlaneTiket.getCity_first_id());
        dt.setCity_destination_id(airPlaneTiket.getCity_destination_id());
        dt.setEconomy(airPlaneTiket.getEconomy());
        DBManager.insert_airplane_ticket(dt);
    }
    public void save_category(Kategori kategori) {
        category dt = new category();
        dt.setId(Long.parseLong(kategori.getId()));
        dt.setName(kategori.getName());
        DBManager.insert_category(dt);
    }
    public void save_class(Cls cls) {
        user_class dt = new user_class();
        dt.setId(Long.parseLong(cls.getId()));
        dt.setCls(cls.getName());
        dt.setType(cls.getType());
        dt.setDesc("");
        DBManager.insert_user_class(dt);
    }
    public void save_dinas(Dinaser dinas) {
        String emp_id = null, plane_id = null, Dplace_id = null, Aplace_id = null, HotelFee_id = null, VehiclePrice_id = null, TaxiTD = null, TaxiTA = null;
        if(dinas.getEmployee() != null){
            emp_id = dinas.getEmployee().getId();
        }
        if(dinas.getAirplane_ticket() != null){
            plane_id = dinas.getAirplane_ticket().getId();
        }
        if(dinas.getDeparture_place() != null){
            Dplace_id = dinas.getDeparture_place().getId();
        }
        if(dinas.getArrival_place() != null){
            Aplace_id = dinas.getArrival_place().getId();
        }
        if(dinas.getHotel_fee() != null){
            HotelFee_id = dinas.getHotel_fee().getId();
        }
        if(dinas.getVehicle_price() != null){
            VehiclePrice_id = dinas.getVehicle_price().getId();
        }
        if(dinas.getTaxi_ticket_departure() != null){
            TaxiTD = dinas.getTaxi_ticket_departure().getId();
        }
        if(dinas.getTaxi_ticket_arrival() != null){
            TaxiTA = dinas.getTaxi_ticket_arrival().getId();
        }
        perjalanan_dinas dt = new perjalanan_dinas(
                Long.parseLong(dinas.getId()),
                dinas.getCode(),
                emp_id,
                dinas.getDescription(),
                dinas.getRemarks(),
                dinas.getNo_surat_perintah(),
                dinas.getDate_departure(),
                dinas.getDate_arrival(),
                dinas.getDay_duration(),
                dinas.getType().getCode(),
                Dplace_id,
                Aplace_id,
                dinas.getTransportation_type(),
                dinas.getTransportation_class(),
                plane_id,
                dinas.getWith_vehicle_rent(),
                dinas.getVehicle_rent_type(),
                VehiclePrice_id,
                dinas.getWith_hotel(),
                HotelFee_id,
                dinas.getHotel_amount_type(),
                dinas.getHotel_amount(),
                dinas.getTransportation_amount_type(),
                dinas.getTransportation_amount(),
                dinas.getWith_taxi(),
                dinas.getTaxi_qty_departure(),
                dinas.getTaxi_qty_arrival(),
                TaxiTD,
                TaxiTA,
                dinas.getTaxi_amount_departure_type(),
                dinas.getTaxi_amount_arrival_type(),
                dinas.getTaxi_amount_departure(),
                dinas.getTaxi_amount_arrival(),
                dinas.getTaxi_total_amount(),
                dinas.getVehicle_amount_type(),
                dinas.getVehicle_amount(),
                dinas.getType_amount(),
                dinas.getTotal_amount(),true
        );
        DBManager.insert_perjalanan_dinas(dt);
    }

    public void delete_schedule(String id){
        DBManager.delete_scheduleById(id);
    }

    public void delete_dinas(String id){
        DBManager.delete_jenis_dinasById(id);
    }

    public void delete_disposisi(String id){
        DBManager.delete_dispositionById(id);
    }
    public void save_disposisi(Disposiser disposisi) {
        List<disposition> dt = new ArrayList<>();
        String dt_category  = "";
        try {
            if (disposisi.getCategories() != null || !disposisi.getCategories().equals("null")) {
                if (disposisi.getCategories().size() != 0) {
                    for (int x = 0; x < disposisi.getCategories().size(); x++) {
                        dt_category += disposisi.getCategories().get(x).getId() + ",";
                    }
                    dt_category = dt_category.substring(0, dt_category.length() - 1);
                }
            }
        }catch (Exception e){}
        String dt_forward = null;
        try {
            if (disposisi.getForward().size() != 0) {
                for (int x = 0; x < disposisi.getForward().size(); x++) {
                    dt_forward += disposisi.getForward().get(x).getId() + ",";
                }
                dt_forward = dt_forward.substring(0, dt_forward.length() - 1);
            }
        }catch (Exception e){}

        dt.add(0, new disposition(
                Long.parseLong(disposisi.getId()),
                dt_category,
                disposisi.getFrom(),
                disposisi.getNo_surat_perintah(),
                disposisi.getNo_agenda(),
                disposisi.getTitle(),
                disposisi.getDesc(),
                disposisi.getNotes(),
                dt_forward,
                disposisi.getCls(),
                disposisi.getFile(),true));

        DBManager.insert_disposition_InTX(dt);
    }
    public void save_file(File files) {
        file dt = new file(Long.parseLong(files.getId()),
                files.getFolder_id(),
                files.getPermalink(),
                files.getFile(),
                files.getExt(),
                files.getTitle(),
                files.getSize(),
                files.getStatus());
        DBManager.insert_file(dt);
    }
    public void save_folder(Folder folders) {
        folder dt = new folder(Long.parseLong(folders.getId()),
                folders.getParent_id(),
                folders.getName(),
                folders.getDesc());
        DBManager.insert_folder(dt);
    }
    public void save_hotel_fee(HotelFees hotelFees) {
        hotel_fee dt = new hotel_fee(Long.parseLong(hotelFees.getId()),
                hotelFees.getProvinsi().getId(),
                hotelFees.getUnit(),
                hotelFees.getEselon1(),
                hotelFees.getEselon2(),
                hotelFees.getEselon3(),
                hotelFees.getEselon4(),
                hotelFees.getGolongan1_2());
        DBManager.insert_hotel_fee(dt);
    }
    public void save_jenis_dinas(Jenis jenis) {
        perjalanan_dinas_jenis dt = new perjalanan_dinas_jenis(
                jenis.getCode(),
                jenis.getJenis());
        DBManager.insert_jenis_dinas(dt);
    }
    public void save_kecamatan(Kecamatan kcmt) {
        kecamatan dt = new kecamatan(
                Long.parseLong(kcmt.getId()),
                kcmt.getKabupaten_id(),
                kcmt.getName());
        DBManager.insert_kecamatan(dt);
    }
    public void save_kelurahan(Kelurahan klr) {
        kelurahan dt = new kelurahan(
                Long.parseLong(klr.getId()),
                klr.getKecamatan_id(),
                klr.getName());
        DBManager.insert_kelurahan(dt);
    }
    public void save_kabupaten(Kabupaten kab) {
        kabupaten dt = new kabupaten(
                Long.parseLong(kab.getId()),
                kab.getProvinsi_id(),
                kab.getName());
        DBManager.insert_kabupaten(dt);
    }
    public void save_mobile(List<Mobile> mbl) {
        List<user_mobile> dt = new ArrayList<>();
        if(mbl.size() != 0) {
            for (int i = 0; i < mbl.size(); i++) {
                dt.add(i, new user_mobile(
                        Long.parseLong(mbl.get(i).getId()),
                        mbl.get(i).getToken()));
            }
            try {
                DBManager.delete_user_mobile();
            }catch (Exception e){}
            DBManager.insert_user_mobile_InTX(dt);
        }
    }
    public void save_place(Places plc) {
        place dt = new place(
                Long.parseLong(plc.getId()),
                plc.getProvinsi().getId(),
                plc.getName(),
                plc.getType());
        DBManager.insert_place(dt);
    }
    public void save_position(Position posisi) {
        position dt = new position(
                Long.parseLong(posisi.getId()),
                posisi.getPosition());
        DBManager.insert_position(dt);
    }
    public void save_provinsi(Provinsi prov) {
        provinsi dt = new provinsi(
                Long.parseLong(prov.getId()),
                prov.getName(),
                prov.getCode());
        DBManager.insert_provinsi(dt);
    }
    public void save_role(Role rol) {
        roles dt = new roles(
                Long.parseLong(rol.getId()),
                rol.getName(),
                rol.getDisplay());
        DBManager.insert_roles(dt);
    }
    public void save_meeting(Scheduler schedu) {
        List<schedule> dt = new ArrayList<>();
        String dt_category  = "";
        try {
            if (schedu.getCategories() != null || !schedu.getCategories().equals("null")) {
                if (schedu.getCategories().size() != 0) {
                    for (int x = 0; x < schedu.getCategories().size(); x++) {
                        dt_category += schedu.getCategories().get(x).getId() + ",";
                    }
                    dt_category = dt_category.substring(0, dt_category.length() - 1);
                }
            }
        }catch (Exception e){}
        String dt_forward = null;
        try {
            if (schedu.getForward().size() != 0) {
                for (int x = 0; x < schedu.getForward().size(); x++) {
                    dt_forward += schedu.getForward().get(x).getId() + ",";
                }
                dt_forward = dt_forward.substring(0, dt_forward.length() - 1);
            }
        }catch (Exception e){}
        dt.add(0,new schedule(
                Long.parseLong(schedu.getId()),
                dt_category,
                schedu.getFrom(),
                schedu.getTitle(),
                schedu.getDatetime_schedule_start(),
                schedu.getDatetime_schedule_end(),
                schedu.getTgl_diterima(),
                schedu.getNo_surat_perintah(),
                schedu.getNo_agenda(),
                schedu.getPlace(),
                schedu.getDesc(),
                schedu.getNotes(),
                dt_forward,
                schedu.getCls(),
                schedu.getFile(),true));
        try {
            DBManager.delete_schedule();
        }catch (Exception e){}
        DBManager.insert_schedule_InTX(dt);
    }
    public void save_taxi_fee(TaxyFees taxyFes) {
        taxi_fee dt = new taxi_fee(
                Long.parseLong(taxyFes.getId()),
                taxyFes.getProvinsi().getId(),
                taxyFes.getUnit(),
                taxyFes.getFee());
        DBManager.insert_taxi_fee(dt);
    }
    public void save_user(User usr) {
        position pst = new position();
        if(usr.getPosition() == null){
            pst.setId(null);
        }else{
            pst.setId(Long.parseLong(usr.getPosition().getId()));
        }
        user_class cls = new user_class();
        if(usr.getCls() == null){
            cls.setId(null);
        }else{
            cls.setId(Long.parseLong(usr.getCls().getId()));
        }
        users dt = new users(
                Long.parseLong(usr.getId()),
                usr.getName(),
                usr.getUsername(),
                usr.getEmail(),
                usr.getPassword(),
                usr.getNip(),
                usr.getPhone(),
                usr.getBirth(),
                usr.getAddress(),
                String.valueOf(pst.getId()),
                String.valueOf(cls.getId()));
        DBManager.insert_users(dt);
    }
}
