package com.app.kemenhub.jakbanapps.retrofit.model_data.place;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PlaceResponse {
	@SerializedName("code")
	String code;
	@SerializedName("message")
	String message;
	@SerializedName("place")
	Places place;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Places getPlace() {
		return place;
	}
	public void setPlace(Places place) {
		this.place = place;
	}
	
	
}
