package com.app.kemenhub.jakbanapps.retrofit.model_data.vehicle;

public class Vehicle {
	private String id;
	private String provinsi_id;
	private String unit;
	private String car;
	private String midle_bus;
	private String big_bus;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProvinsi_id() {
		return provinsi_id;
	}

	public void setProvinsi_id(String provinsi_id) {
		this.provinsi_id = provinsi_id;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getCar() {
		return car;
	}

	public void setCar(String car) {
		this.car = car;
	}

	public String getMidle_bus() {
		return midle_bus;
	}

	public void setMidle_bus(String midle_bus) {
		this.midle_bus = midle_bus;
	}

	public String getBig_bus() {
		return big_bus;
	}

	public void setBig_bus(String big_bus) {
		this.big_bus = big_bus;
	}
}
