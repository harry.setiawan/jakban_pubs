package com.app.kemenhub.jakbanapps.retrofit;

import com.app.kemenhub.jakbanapps.db.users;
import com.app.kemenhub.jakbanapps.retrofit.model_data.airplane.AirPlaneTiketResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.airplane.AirPlaneTiketsResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.category.CategoryResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.category.CategorysResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.cls.Cls;
import com.app.kemenhub.jakbanapps.retrofit.model_data.dinas.DinasResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.dinas.DinassResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.disposisi.DisposisiResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.disposisi.DisposissResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.file.FileResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.file.FilesResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.folder.FolderResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.folder.FoldersResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.hotel_fee.HotelFeeResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.hotel_fee.HotelFeesResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.jenis_dinas.JenisResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.jenis_dinas.JenissResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.kabupaten.KabupatenResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.kabupaten.KabupatensResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.kecamatan.KecamatanResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.kecamatan.KecamatansResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.kelurahan.KelurahanResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.kelurahan.KelurahansResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.mobile.Mobile;
import com.app.kemenhub.jakbanapps.retrofit.model_data.mobile.MobileResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.place.PlaceResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.place.PlacesResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.position.PositionResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.position.PositionsResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.provinsi.ProvinsiResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.provinsi.ProvinsisResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.role.Role;
import com.app.kemenhub.jakbanapps.retrofit.model_data.schedule.ScheduleResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.schedule.SchedulesResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.taxi_fee.TaxyFeeResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.taxi_fee.TaxyFeesResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.user.UserResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.user.UsersResponse;
import com.app.kemenhub.jakbanapps.v_event;
import com.app.kemenhub.jakbanapps.v_main;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Axa.Laranza on 6/28/2017.
 */

public class RCMain {

    v_main ac;
    SDM SDM;

    public RCMain(v_main ac){
        this.ac = ac;
        SDM = new SDM(ac.getApplicationContext());
    }
    public void request_login(final users users){
        Retrofit.Builder build = new Retrofit.Builder()
                .baseUrl(URLManager.USER)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = build.build();
        final Request_Inf client = retrofit.create(Request_Inf.class);
        Call<UserResponse> call = client.login_account(users);
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if(response.body().getMessage().equals("failed")){
                    ac.SManager.setPreferences(ac, "status", "");
                    ac.hide_dialog();
                    ac.call_alert("Username atau Password yang anda masukan salah!");
                }else{
                    v_event.DBManager.insert_users(SDM.set_user_FS(response.body().getUser()));
                    if(response.body().getUser().getPosition() != null){
                        v_event.DBManager.insert_position(SDM.set_position_FS(response.body().getUser().getPosition()));
                    }
                    if(response.body().getUser().getCls() != null){
                        v_event.DBManager.insert_user_class(SDM.set_cls_FS(response.body().getUser().getCls()));
                    }
                    ac.SManager.setPreferences(ac, "welcome", "1");
                    ac.SManager.setPreferences(ac, "status", response.body().getUser().getId());
                    List<Role> role_user = response.body().getUser().getRole();
                    for(int i = 0 ; i < role_user.size() ; i++){
                        if(role_user.get(i).getName().toUpperCase().equals("ADMIN") ||
                            role_user.get(i).getName().toUpperCase().equals("ARSIP")){
                                ac.SManager.setPreferences(ac, "admin", "true");
                                break;
                        }
                    }

                    Mobile mobile = new Mobile(response.body().getUser().getId(),
                            FirebaseInstanceId.getInstance().getToken());
                    request_mobile_login(mobile);
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                ac.SManager.setPreferences(ac, "status", "");
                ac.hide_dialog();
                ac.call_err_conn();
            }
        });
        ac.logins.reset_lay();
    }
    public void request_mobile_login(Mobile mobile){
        Retrofit.Builder build = new Retrofit.Builder()
                .baseUrl(URLManager.MOBILE)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = build.build();
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<MobileResponse> call = client.login_mobile_account(mobile);
        call.enqueue(new Callback<MobileResponse>() {
            @Override
            public void onResponse(Call<MobileResponse> call, Response<MobileResponse> response) {
                if(response.body().getMessage().equals("failed")){
                    ac.SManager.setPreferences(ac, "status", "");
                    v_event.DBManager.dropDatabase();
                    ac.hide_dialog();
                    ac.call_alert("Correct the mistakes:\n" + "Entered username or password is incorrect.");
                }else{
                    ac.hide_dialog();
                    ac.go_to_action(v_event.class);
                }
            }

            @Override
            public void onFailure(Call<MobileResponse> call, Throwable t) {
                ac.SManager.setPreferences(ac, "status", "");
                ac.hide_dialog();
                ac.call_err_conn();
            }
        });
        ac.logins.reset_lay();
    }
}
