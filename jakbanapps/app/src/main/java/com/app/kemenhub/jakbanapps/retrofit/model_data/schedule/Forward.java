package com.app.kemenhub.jakbanapps.retrofit.model_data.schedule;

import com.app.kemenhub.jakbanapps.retrofit.model_data.position.Position;

public class Forward {
	private String id;

	private String name;

	private String email;


	private String nip;

	private Position position;

	public Forward(String id, String name, String email, String nip, Position position) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.nip = nip;
		this.position = position;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}
	
	
}
