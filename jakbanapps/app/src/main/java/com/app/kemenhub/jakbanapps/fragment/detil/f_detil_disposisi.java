package com.app.kemenhub.jakbanapps.fragment.detil;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.kemenhub.jakbanapps.R;
import com.app.kemenhub.jakbanapps.adapter.DataCategoryAdt;
import com.app.kemenhub.jakbanapps.adapter.DataForwardAdt;
import com.app.kemenhub.jakbanapps.db.category;
import com.app.kemenhub.jakbanapps.db.disposition;
import com.app.kemenhub.jakbanapps.db.users;
import com.app.kemenhub.jakbanapps.retrofit.URLManager;
import com.app.kemenhub.jakbanapps.retrofit.model_data.user.User;
import com.app.kemenhub.jakbanapps.service.Download;
import com.app.kemenhub.jakbanapps.service.DownloadService;
import com.app.kemenhub.jakbanapps.sys.utils;
import com.app.kemenhub.jakbanapps.v_detil;
import com.app.kemenhub.jakbanapps.v_event;
import com.app.kemenhub.jakbanapps.v_main;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class f_detil_disposisi extends Fragment implements View.OnClickListener, DataCategoryAdt.itemCategoryClickCallback, DataForwardAdt.itemForwardClickCallback{

    v_detil ac;
    View v;
    TextView file;
    EditText no_surat, no_agenda, dari, perihal, catatan, desc, klasifikasi;
    RecyclerView rc_category, rc_forward;
    disposition disposisi;
    AppCompatButton download;
    DataCategoryAdt category_adapter;
    DataForwardAdt forward_adapter;
    List<category> categories = new ArrayList<>();
    List<users> forward = new ArrayList<>();
    CardView line, card_btn;
    ProgressDialog mProgressDialog;

    public static final String MESSAGE_PROGRESS = "message_progress";
    private static final int PERMISSION_REQUEST_CODE = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.f_detil_disposisi, container, false);
        ac = (v_detil) getActivity();

        _init();
        _shown_data();

        ButterKnife.bind(ac);

        registerReceiver();
        return v;
    }

    private void _shown_data() {
        disposisi = v_event.DBManager.get_dispositionById(ac.id_data);
        try {
            String[] dt_category = disposisi.getCategory_id().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\"", "").replaceAll("null", "").split(",");
            for(int i = 0 ; i < dt_category.length ; i ++){
                categories.add(i, v_event.DBManager.get_categoryById(dt_category[i]));
            }
        }catch (Exception x){

        }

        try {
            String[] dt_forward = disposisi.getForward_to_user_id().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\"", "").replaceAll("null", "").split(",");
            for(int i = 0 ; i < dt_forward.length ; i ++){
                forward.add(i, v_event.DBManager.get_usersById(dt_forward[i]));
            }
        }catch (Exception x){

        }
        if(forward.size() == 0){
            forward.add(0, new users(null, "admin", null,null,null,null,null,null,null,null,null));
        }
        try {
            utils.TAG_FILE = disposisi.getFile();
            String[] nm_file = disposisi.getFile().split("\\/");
            file.setText(nm_file[nm_file.length - 1]);
        }catch (Exception e){
            file.setText("Tidak ada file");
            file.setTextColor(ac.getResources().getColor(R.color.md_red_700));
            download.setEnabled(false);
            download.setClickable(false);
            download.setVisibility(View.GONE);
        }
        no_surat.setText(disposisi.getNo_surat_perintah());
        no_agenda.setText(disposisi.getNo_agenda());
        dari.setText(disposisi.getFrom());
        perihal.setText(disposisi.getTitle());

        if(disposisi.getNotes() == null){
            catatan.setText("-");
        }else{
            catatan.setText(disposisi.getNotes());
        }

        if(disposisi.getDesc() == null){
            desc.setText("-");
        }else{
            desc.setText(disposisi.getDesc());
        }

        if(disposisi.getCls().equals("1")){
            klasifikasi.setText("Biasa");
        }else if(disposisi.getCls().equals("2")){
            klasifikasi.setText("Segera");
        }else if(disposisi.getCls().equals("3")){
            klasifikasi.setText("Sangat Segera");
        }else if(disposisi.getCls().equals("4")){
            klasifikasi.setText("Rahasia");
        }

        category_adapter.notifyDataSetChanged();
        forward_adapter.notifyDataSetChanged();
    }

    private void _init() {
        file = (TextView) v.findViewById(R.id.edt_file);
        no_surat = (EditText) v.findViewById(R.id.edt_surat);
        no_agenda = (EditText) v.findViewById(R.id.edt_agenda);
        dari = (EditText) v.findViewById(R.id.edt_dari);
        perihal = (EditText) v.findViewById(R.id.edt_perihal);
        catatan = (EditText) v.findViewById(R.id.edt_catatan);
        desc = (EditText) v.findViewById(R.id.edt_desc);
        klasifikasi = (EditText) v.findViewById(R.id.edt_klasifikasi);
        download = (AppCompatButton) v.findViewById(R.id.btn_download);
        rc_category = (RecyclerView) v.findViewById(R.id.rc_category);
        rc_forward = (RecyclerView) v.findViewById(R.id.rc_forward);
        line = (CardView) v.findViewById(R.id.line_card);
        card_btn = (CardView) v.findViewById(R.id.card_btn);
        if (Build.VERSION.SDK_INT < 21) {
            line.setVisibility(View.GONE);
            card_btn.setVisibility(View.GONE);
        }
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ac);
        rc_category.setLayoutManager(layoutManager);
        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(ac);
        rc_forward.setLayoutManager(layoutManager2);

        category_adapter = new DataCategoryAdt(categories, ac);
        rc_category.setAdapter(category_adapter);
        forward_adapter = new DataForwardAdt(forward, ac);
        rc_forward.setAdapter(forward_adapter);

        category_adapter.setItemCategoryClickCallback(this);
        forward_adapter.setItemForwardClickCallback(this);
        download.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_download:
                new DownloadFile().execute(URLManager.URI__DOWNLOAD_API + disposisi.getFile());
                break;
        }
    }

    // DownloadFile AsyncTask
    private class DownloadFile extends AsyncTask<String, Integer, String> {
        String nm_files = file.getText().toString();
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create progress dialog
            mProgressDialog = new ProgressDialog(ac);
            // Set your progress dialog Title
            mProgressDialog.setTitle(nm_files);
            // Set your progress dialog Message
            mProgressDialog.setMessage("Downloading, Please Wait!");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setMax(100);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            // Show progress dialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... Url) {
            try {
                URL url = new URL(Url[0]);
                URLConnection connection = url.openConnection();
                connection.connect();

                // Detect the file lenghth
                int fileLength = connection.getContentLength();

                // Locate storage location
                String folder_download = "Jakban";
                File f = new File(Environment.getExternalStorageDirectory(), folder_download);
                if (!f.exists()) {
                    f.mkdirs();
                }
                String folder_meeting = "disposition";
                File m = new File(Environment.getExternalStorageDirectory() + "/Jakban", folder_meeting);
                if (!m.exists()) {
                    m.mkdirs();
                }

                String filepath = Environment.getExternalStorageDirectory().getPath() +"/Jakban/disposition/";
                // Download the file
                InputStream input = new BufferedInputStream(url.openStream());

                // Save the downloaded file
                OutputStream output = new FileOutputStream(filepath + nm_files);

                byte data[] = new byte[1024];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    // Publish the progress
                    publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }

                // Close connection
                output.flush();
                output.close();
                input.close();
            } catch (Exception e) {
                // Error Log
                ////Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            // Update the progress dialog
            mProgressDialog.setProgress(progress[0]);
            if(progress[0] == 100){
                mProgressDialog.dismiss();
                mProgressDialog.hide();
            }
            // Dismiss the progress dialog
            //mProgressDialog.dismiss();
        }
    }

    private void registerReceiver(){

        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(ac);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MESSAGE_PROGRESS);
        bManager.registerReceiver(broadcastReceiver, intentFilter);

    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if(intent.getAction().equals(MESSAGE_PROGRESS)){

                Download download = intent.getParcelableExtra("download");
            }
        }
    };

    @Override
    public void onItemClick(int p) {

    }

    @Override
    public void onItemForwardClick(int p) {

    }
}
