package com.app.kemenhub.jakbanapps.retrofit.model_data.airplane;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AirPlaneTiketsResponse {
	@SerializedName("code")
	String code;
	@SerializedName("message")
	String message;
	@SerializedName("airPlaneTiket")
	List<AirPlaneTiket> airplane_ticket;

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<AirPlaneTiket> getAirPlaneTiket() {
		return airplane_ticket;
	}
	public void setAirPlaneTiket(List<AirPlaneTiket> airplane_ticket) {
		this.airplane_ticket = airplane_ticket;
	}
	
	
}
