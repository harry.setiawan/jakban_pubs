package com.app.kemenhub.jakbanapps.retrofit.model_data.dinas;

public class DinasAdd {
	private String code;
	private String employee_id;
	private String description;
	private String remarks;
	private String no_surat_perintah;
	private String date_departure;
	private String date_arrival;
	private String day_duration;
	private String type_code;
	private String departure_place_id;
	private String arrival_place_id;
	private String transportation_type;
	private String transportation_class;
	private String airplane_ticker_id;
	private String with_vehicle_rent;
	private String vehicle_rent_type;
	private String vehicle_rent_price_id;
	private String with_hotel;
	private String hotel_ticket_id;
	private String hotel_amount_type;
	private String hotel_amount;
	private String transportation_amount_type;
	private String transportation_amount;
	private String with_taxi;
	private String taxi_qty_departure;
	private String taxi_qty_arrival;
	private String taxi_ticket_departure;
	private String taxi_ticket_arrival;
	private String taxi_amount_departure_type;
	private String taxi_amount_arrival_type;
	private String taxi_amount_departure;
	private String taxi_amount_arrival;
	private String taxi_total_amount;
	private String vehicle_amount_type;
	private String vehicle_amount;
	private String type_amount;
	private String total_amount;

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getEmployee_id() {
		return employee_id;
	}
	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getNo_surat_perintah() {
		return no_surat_perintah;
	}
	public void setNo_surat_perintah(String no_surat_perintah) {
		this.no_surat_perintah = no_surat_perintah;
	}
	public String getDate_departure() {
		return date_departure;
	}
	public void setDate_departure(String date_departure) {
		this.date_departure = date_departure;
	}
	public String getDate_arrival() {
		return date_arrival;
	}
	public void setDate_arrival(String date_arrival) {
		this.date_arrival = date_arrival;
	}
	public String getDay_duration() {
		return day_duration;
	}
	public void setDay_duration(String day_duration) {
		this.day_duration = day_duration;
	}
	public String getType_code() {
		return type_code;
	}
	public void setType_code(String type_code) {
		this.type_code = type_code;
	}
	public String getDeparture_place_id() {
		return departure_place_id;
	}
	public void setDeparture_place_id(String departure_place_id) {
		this.departure_place_id = departure_place_id;
	}
	public String getArrival_place_id() {
		return arrival_place_id;
	}
	public void setArrival_place_id(String arrival_place_id) {
		this.arrival_place_id = arrival_place_id;
	}
	public String getTransportation_type() {
		return transportation_type;
	}
	public void setTransportation_type(String transportation_type) {
		this.transportation_type = transportation_type;
	}
	public String getTransportation_class() {
		return transportation_class;
	}
	public void setTransportation_class(String transportation_class) {
		this.transportation_class = transportation_class;
	}
	public String getAirplane_ticker_id() {
		return airplane_ticker_id;
	}
	public void setAirplane_ticker_id(String airplane_ticker_id) {
		this.airplane_ticker_id = airplane_ticker_id;
	}
	public String getWith_vehicle_rent() {
		return with_vehicle_rent;
	}
	public void setWith_vehicle_rent(String with_vehicle_rent) {
		this.with_vehicle_rent = with_vehicle_rent;
	}
	public String getVehicle_rent_type() {
		return vehicle_rent_type;
	}
	public void setVehicle_rent_type(String vehicle_rent_type) {
		this.vehicle_rent_type = vehicle_rent_type;
	}
	public String getVehicle_rent_price_id() {
		return vehicle_rent_price_id;
	}
	public void setVehicle_rent_price_id(String vehicle_rent_price_id) {
		this.vehicle_rent_price_id = vehicle_rent_price_id;
	}
	public String getWith_hotel() {
		return with_hotel;
	}
	public void setWith_hotel(String with_hotel) {
		this.with_hotel = with_hotel;
	}
	public String getHotel_ticket_id() {
		return hotel_ticket_id;
	}
	public void setHotel_ticket_id(String hotel_ticket_id) {
		this.hotel_ticket_id = hotel_ticket_id;
	}
	public String getHotel_amount_type() {
		return hotel_amount_type;
	}
	public void setHotel_amount_type(String hotel_amount_type) {
		this.hotel_amount_type = hotel_amount_type;
	}
	public String getHotel_amount() {
		return hotel_amount;
	}
	public void setHotel_amount(String hotel_amount) {
		this.hotel_amount = hotel_amount;
	}
	public String getTransportation_amount_type() {
		return transportation_amount_type;
	}
	public void setTransportation_amount_type(String transportation_amount_type) {
		this.transportation_amount_type = transportation_amount_type;
	}
	public String getTransportation_amount() {
		return transportation_amount;
	}
	public void setTransportation_amount(String transportation_amount) {
		this.transportation_amount = transportation_amount;
	}
	public String getWith_taxi() {
		return with_taxi;
	}
	public void setWith_taxi(String with_taxi) {
		this.with_taxi = with_taxi;
	}
	public String getTaxi_qty_departure() {
		return taxi_qty_departure;
	}
	public void setTaxi_qty_departure(String taxi_qty_departure) {
		this.taxi_qty_departure = taxi_qty_departure;
	}
	public String getTaxi_qty_arrival() {
		return taxi_qty_arrival;
	}
	public void setTaxi_qty_arrival(String taxi_qty_arrival) {
		this.taxi_qty_arrival = taxi_qty_arrival;
	}
	public String getTaxi_ticket_departure() {
		return taxi_ticket_departure;
	}
	public void setTaxi_ticket_departure(String taxi_ticket_departure) {
		this.taxi_ticket_departure = taxi_ticket_departure;
	}
	public String getTaxi_ticket_arrival() {
		return taxi_ticket_arrival;
	}
	public void setTaxi_ticket_arrival(String taxi_ticket_arrival) {
		this.taxi_ticket_arrival = taxi_ticket_arrival;
	}
	public String getTaxi_amount_departure_type() {
		return taxi_amount_departure_type;
	}
	public void setTaxi_amount_departure_type(String taxi_amount_departure_type) {
		this.taxi_amount_departure_type = taxi_amount_departure_type;
	}
	public String getTaxi_amount_arrival_type() {
		return taxi_amount_arrival_type;
	}
	public void setTaxi_amount_arrival_type(String taxi_amount_arrival_type) {
		this.taxi_amount_arrival_type = taxi_amount_arrival_type;
	}
	public String getTaxi_amount_departure() {
		return taxi_amount_departure;
	}
	public void setTaxi_amount_departure(String taxi_amount_departure) {
		this.taxi_amount_departure = taxi_amount_departure;
	}
	public String getTaxi_amount_arrival() {
		return taxi_amount_arrival;
	}
	public void setTaxi_amount_arrival(String taxi_amount_arrival) {
		this.taxi_amount_arrival = taxi_amount_arrival;
	}
	public String getTaxi_total_amount() {
		return taxi_total_amount;
	}
	public void setTaxi_total_amount(String taxi_total_amount) {
		this.taxi_total_amount = taxi_total_amount;
	}
	public String getVehicle_amount_type() {
		return vehicle_amount_type;
	}
	public void setVehicle_amount_type(String vehicle_amount_type) {
		this.vehicle_amount_type = vehicle_amount_type;
	}
	public String getVehicle_amount() {
		return vehicle_amount;
	}
	public void setVehicle_amount(String vehicle_amount) {
		this.vehicle_amount = vehicle_amount;
	}
	public String getType_amount() {
		return type_amount;
	}
	public void setType_amount(String type_amount) {
		this.type_amount = type_amount;
	}
	public String getTotal_amount() {
		return total_amount;
	}
	public void setTotal_amount(String total_amount) {
		this.total_amount = total_amount;
	}
	
	
}
