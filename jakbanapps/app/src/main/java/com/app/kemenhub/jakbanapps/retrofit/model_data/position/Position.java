package com.app.kemenhub.jakbanapps.retrofit.model_data.position;


public class Position {
    private String id;
    private String position;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

}
