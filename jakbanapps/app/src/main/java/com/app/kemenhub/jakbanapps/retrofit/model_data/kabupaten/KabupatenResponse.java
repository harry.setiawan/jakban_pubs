package com.app.kemenhub.jakbanapps.retrofit.model_data.kabupaten;

import com.google.gson.annotations.SerializedName;

public class KabupatenResponse {
	@SerializedName("code")
	String code;
	@SerializedName("message")
	String message;
	@SerializedName("kabupaten")
	Kabupaten kabupaten;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Kabupaten getKabupaten() {
		return kabupaten;
	}
	public void setKabupaten(Kabupaten kabupaten) {
		this.kabupaten = kabupaten;
	}	
}
