package com.app.kemenhub.jakbanapps.process_action;

import android.util.Log;

import com.app.kemenhub.jakbanapps.db.user_mobile;
import com.app.kemenhub.jakbanapps.fragment.f_miting;
import com.app.kemenhub.jakbanapps.retrofit.Request_Inf;
import com.app.kemenhub.jakbanapps.retrofit.SDM;
import com.app.kemenhub.jakbanapps.retrofit.URLManager;
import com.app.kemenhub.jakbanapps.retrofit.model_data.folder.FolderAddResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.mobile.Mobile;
import com.app.kemenhub.jakbanapps.v_event;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Axa.Laranza on 7/31/2017.
 */

public class delete_meeting {
    f_miting fragment;

    private DeletedCallback deletedCallback;

    public interface DeletedCallback{
        void deletedFinishProcess(boolean data);
    }

    public void setDeletedCallback(final DeletedCallback deletedCallback){
        this.deletedCallback = deletedCallback;
    }

    public delete_meeting(f_miting fragment){
        this.fragment = fragment;
    }

    public void request_data_mobile(final int n) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URLManager.URI_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<List<Mobile>> call = client.get_all_Mobile();
        call.enqueue(new Callback<List<Mobile>>() {
            @Override
            public void onResponse(Call<List<Mobile>> call, Response<List<Mobile>> response) {
                SDM SDM = new SDM(fragment.getContext());
                SDM.save_list_mobile(response.body());
                process_forward(n);
            }

            @Override
            public void onFailure(Call<List<Mobile>> call, Throwable t) {
            }
        });
    }

    private void process_forward(int n) {
        List<user_mobile> forward = new ArrayList<>();
        try {
            String[] dt_forward = fragment.data_meeting.get(n).getForward_to_user_id().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\"", "").replaceAll("null", "").split(",");
            for(int i = 0 ; i < dt_forward.length ; i ++){
                if(v_event.DBManager.get_user_mobileById(dt_forward[i]) != null) {
                    forward.add(forward.size(), v_event.DBManager.get_user_mobileById(dt_forward[i]));
                }
            }
        }catch (Exception x){}

        process_delete(n, forward);
    }

    private void process_delete(final int n, final List<user_mobile> forward) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URLManager.URI_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<FolderAddResponse> call = client.delete_Schedule(String.valueOf(fragment.data_meeting.get(n).getId()));
        call.enqueue(new Callback<FolderAddResponse>() {
            @Override
            public void onResponse(Call<FolderAddResponse> call, Response<FolderAddResponse> response) {
                if(response.body() != null) {
                    ////Log.i("DELETED RESP", response.body().toString());
                }
                process_push(n, response.body().getMessage(), forward);
            }

            @Override
            public void onFailure(Call<FolderAddResponse> call, Throwable t) {
                deletedCallback.deletedFinishProcess(false);
            }
        });
    }

    private void process_push(int n, String message, List<user_mobile> account) {
        if(message.equals("deleted")){
            if(account.size() > 0){
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(URLManager.URI_API)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                Request_Inf client = retrofit.create(Request_Inf.class);
                Call<FolderAddResponse> call = client.push_Mobile_notif(String.valueOf(fragment.data_meeting.get(n).getId()), account);
                call.enqueue(new Callback<FolderAddResponse>() {
                    @Override
                    public void onResponse(Call<FolderAddResponse> call, Response<FolderAddResponse> response) {
                        if(response.body() != null) {
                            ////Log.i("DELETED RESP PUSH", response.body().getMessage().toString());
                        }
                        deletedCallback.deletedFinishProcess(true);
                    }

                    @Override
                    public void onFailure(Call<FolderAddResponse> call, Throwable t) {
                        deletedCallback.deletedFinishProcess(false);
                    }
                });
            }
        }else{
            deletedCallback.deletedFinishProcess(true);
        }
    }
}
