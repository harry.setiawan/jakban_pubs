package com.app.kemenhub.jakbanapps.retrofit;

import android.nfc.Tag;
import android.util.Log;

import com.app.kemenhub.jakbanapps.retrofit.model_data.airplane.AirPlaneTiketResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.category.CategoryResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.cls.Cls;
import com.app.kemenhub.jakbanapps.retrofit.model_data.dinas.DinasResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.dinas.DinassResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.disposisi.DisposisiResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.disposisi.DisposissResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.file.FileResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.file.FilesResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.folder.FolderResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.folder.FoldersResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.hotel_fee.HotelFeeResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.hotel_fee.HotelFeesResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.jenis_dinas.JenisResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.jenis_dinas.JenissResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.kabupaten.KabupatenResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.kabupaten.KabupatensResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.kecamatan.KecamatanResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.kecamatan.KecamatansResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.kelurahan.KelurahanResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.kelurahan.KelurahansResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.mobile.Mobile;
import com.app.kemenhub.jakbanapps.retrofit.model_data.mobile.MobileResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.place.PlaceResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.place.PlacesResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.position.PositionResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.position.PositionsResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.provinsi.ProvinsiResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.provinsi.ProvinsisResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.role.Role;
import com.app.kemenhub.jakbanapps.retrofit.model_data.schedule.ScheduleResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.schedule.SchedulesResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.taxi_fee.TaxyFeeResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.user.UserResponse;
import com.app.kemenhub.jakbanapps.v_event;
import com.app.kemenhub.jakbanapps.v_main;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Axa.Laranza on 6/28/2017.
 */

public class RCEvent {

    v_event ac;
    SDM SDM;
    public Retrofit retrofit;
    public RCEvent(v_event ac){
        this.ac = ac;
        retro();
        SDM = new SDM(ac.getApplicationContext());
    }

    private void retro() {
        this.retrofit = new Retrofit.Builder()
                .baseUrl(URLManager.URI_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public void request_ByTO_disposisi(String to){
        SimpleDateFormat sdfdate = new SimpleDateFormat("yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);
        Date tgll = cal.getTime();

        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<DisposissResponse> call = client.get_disposisi_to(String.valueOf(sdfdate.format(tgll)), to);
        call.enqueue(new Callback<DisposissResponse>() {
            @Override
            public void onResponse(Call<DisposissResponse> call, Response<DisposissResponse> response) {
                SDM.save_list_disposisi(response.body().getDisposisi());
            }

            @Override
            public void onFailure(Call<DisposissResponse> call, Throwable t) {
                call.cancel();
            }
        });
    }
    public void request_ByTo_schedule(String t){

        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<SchedulesResponse> call = client.get_Schedule_to(t);
        call.enqueue(new Callback<SchedulesResponse>() {
            @Override
            public void onResponse(Call<SchedulesResponse> call, Response<SchedulesResponse> response) {
                SDM.save_list_meeting(response.body().getSchedule());
            }

            @Override
            public void onFailure(Call<SchedulesResponse> call, Throwable t) {
            }
        });
    }
    public void request_ByDATE_dinas(String year, String month){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<DinassResponse> call = client.get_all_dinas_date(year, month);
        call.enqueue(new Callback<DinassResponse>() {
            @Override
            public void onResponse(Call<DinassResponse> call, Response<DinassResponse> response) {
 //               ////Log.i("SERVER DINAS", response.body().getMessage() + " - " + response.body().getDinas().size());
                SDM.save_list_dinas(response.body().getDinas());
            }

            @Override
            public void onFailure(Call<DinassResponse> call, Throwable t) {
                call.cancel();
            }
        });
    }
    public void request_ByDATE_schedule(String year, String month){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<SchedulesResponse> call = client.get_Schedule_date(year, month);
        call.enqueue(new Callback<SchedulesResponse>() {
            @Override
            public void onResponse(Call<SchedulesResponse> call, Response<SchedulesResponse> response) {
//                ////Log.i("SERVER MEETING", response.body().getMessage() + " - " + response.body().getSchedule().size());
                SDM.save_list_meeting(response.body().getSchedule());
            }

            @Override
            public void onFailure(Call<SchedulesResponse> call, Throwable t) {
                call.cancel();
            }
        });
    }

    public void request_byID_airplane(String id){
        Retrofit.Builder build = new Retrofit.Builder()
                .baseUrl(id)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = build.build();
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<AirPlaneTiketResponse> call = client.get_airplaneticket_id(id);
        call.enqueue(new Callback<AirPlaneTiketResponse>() {
            @Override
            public void onResponse(Call<AirPlaneTiketResponse> call, Response<AirPlaneTiketResponse> response) {
                SDM.save_airplane_ticket(response.body().getAirPlaneTiket());
            }

            @Override
            public void onFailure(Call<AirPlaneTiketResponse> call, Throwable t) {
            }
        });
    }
    public void request_byID_category(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<CategoryResponse> call = client.get_category_id(id);
        call.enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                SDM.save_category(response.body().getCategory());
            }

            @Override
            public void onFailure(Call<CategoryResponse> call, Throwable t) {
            }
        });
    }
    public void request_byID_class(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<Cls> call = client.get_class_id(id);
        call.enqueue(new Callback<Cls>() {
            @Override
            public void onResponse(Call<Cls> call, Response<Cls> response) {
                SDM.save_class(response.body());
            }

            @Override
            public void onFailure(Call<Cls> call, Throwable t) {
            }
        });
    }
    public void request_byID_dinas(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<DinasResponse> call = client.get_dinas_id(id);
        call.enqueue(new Callback<DinasResponse>() {
            @Override
            public void onResponse(Call<DinasResponse> call, Response<DinasResponse> response) {
                SDM.save_dinas(response.body().getDinas());
            }

            @Override
            public void onFailure(Call<DinasResponse> call, Throwable t) {
            }
        });
    }
    public void request_byID_disposisi(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<DisposisiResponse> call = client.get_disposisi_id(id);
        call.enqueue(new Callback<DisposisiResponse>() {
            @Override
            public void onResponse(Call<DisposisiResponse> call, Response<DisposisiResponse> response) {
                SDM.save_disposisi(response.body().getDisposisi());
            }

            @Override
            public void onFailure(Call<DisposisiResponse> call, Throwable t) {
            }
        });
    }
    public void request_byID_file(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<FileResponse> call = client.get_file_id(id);
        call.enqueue(new Callback<FileResponse>() {
            @Override
            public void onResponse(Call<FileResponse> call, Response<FileResponse> response) {
                SDM.save_file(response.body().getFile());
            }

            @Override
            public void onFailure(Call<FileResponse> call, Throwable t) {
            }
        });
    }
    public void request_byID_folder(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<FolderResponse> call = client.get_folder_id(id);
        call.enqueue(new Callback<FolderResponse>() {
            @Override
            public void onResponse(Call<FolderResponse> call, Response<FolderResponse> response) {
                SDM.save_folder(response.body().getFolder());
            }

            @Override
            public void onFailure(Call<FolderResponse> call, Throwable t) {
            }
        });
    }
    public void request_byID_hotel_fee(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<HotelFeeResponse> call = client.get_hotel_fee_id(id);
        call.enqueue(new Callback<HotelFeeResponse>() {
            @Override
            public void onResponse(Call<HotelFeeResponse> call, Response<HotelFeeResponse> response) {
                SDM.save_hotel_fee(response.body().getHotelFees());
            }

            @Override
            public void onFailure(Call<HotelFeeResponse> call, Throwable t) {
            }
        });
    }
    public void request_byID_jenis_dinas(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<JenisResponse> call = client.get_jenis_dinas_id(id);
        call.enqueue(new Callback<JenisResponse>() {
            @Override
            public void onResponse(Call<JenisResponse> call, Response<JenisResponse> response) {
                SDM.save_jenis_dinas(response.body().getJenis());
            }

            @Override
            public void onFailure(Call<JenisResponse> call, Throwable t) {
            }
        });
    }
    public void request_byID_kecamatan(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<KecamatanResponse> call = client.get_Kecamatan_id(URLManager.URI_API);
        call.enqueue(new Callback<KecamatanResponse>() {
            @Override
            public void onResponse(Call<KecamatanResponse> call, Response<KecamatanResponse> response) {
                SDM.save_kecamatan(response.body().getKecamatan());
            }

            @Override
            public void onFailure(Call<KecamatanResponse> call, Throwable t) {
            }
        });
    }
    public void request_byID_kelurahan(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<KelurahanResponse> call = client.get_Kelurahan_id(id);
        call.enqueue(new Callback<KelurahanResponse>() {
            @Override
            public void onResponse(Call<KelurahanResponse> call, Response<KelurahanResponse> response) {
                SDM.save_kelurahan(response.body().getKelurahan());
            }

            @Override
            public void onFailure(Call<KelurahanResponse> call, Throwable t) {
            }
        });
    }
    public void request_byID_kabupaten(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<KabupatenResponse> call = client.get_kabupaten_id(id);
        call.enqueue(new Callback<KabupatenResponse>() {
            @Override
            public void onResponse(Call<KabupatenResponse> call, Response<KabupatenResponse> response) {
                SDM.save_kabupaten(response.body().getKabupaten());
            }

            @Override
            public void onFailure(Call<KabupatenResponse> call, Throwable t) {
            }
        });
    }
    public void request_byID_mobile(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<List<Mobile>> call = client.get_Mobile_id(id);
        call.enqueue(new Callback<List<Mobile>>() {
            @Override
            public void onResponse(Call<List<Mobile>> call, Response<List<Mobile>> response) {
                //////Log.i("TKN SERVER", response.body().get(0).getToken());
                SDM.save_mobile(response.body());
            }

            @Override
            public void onFailure(Call<List<Mobile>> call, Throwable t) {

            }
        });
    }
    public void request_byID_place(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<PlaceResponse> call = client.get_Place_id(id);
        call.enqueue(new Callback<PlaceResponse>() {
            @Override
            public void onResponse(Call<PlaceResponse> call, Response<PlaceResponse> response) {
                SDM.save_place(response.body().getPlace());
            }

            @Override
            public void onFailure(Call<PlaceResponse> call, Throwable t) {
            }
        });
    }
    public void request_byID_position(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<PositionResponse> call = client.get_Position_id(id);
        call.enqueue(new Callback<PositionResponse>() {
            @Override
            public void onResponse(Call<PositionResponse> call, Response<PositionResponse> response) {
                SDM.save_position(response.body().getPosition());
            }

            @Override
            public void onFailure(Call<PositionResponse> call, Throwable t) {
            }
        });
    }
    public void request_byID_provinsi(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<ProvinsiResponse> call = client.get_Provinsi_id(id);
        call.enqueue(new Callback<ProvinsiResponse>() {
            @Override
            public void onResponse(Call<ProvinsiResponse> call, Response<ProvinsiResponse> response) {
                SDM.save_provinsi(response.body().getProvinsi());
            }

            @Override
            public void onFailure(Call<ProvinsiResponse> call, Throwable t) {
            }
        });
    }
    public void request_byID_schedule(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<ScheduleResponse> call = client.get_Schedule_id(id);
        call.enqueue(new Callback<ScheduleResponse>() {
            @Override
            public void onResponse(Call<ScheduleResponse> call, Response<ScheduleResponse> response) {
                SDM.save_meeting(response.body().getSchedule());
            }

            @Override
            public void onFailure(Call<ScheduleResponse> call, Throwable t) {
            }
        });
    }
    public void request_byID_taxi_fee(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<TaxyFeeResponse> call = client.get_TaxyFee_id(id);
        call.enqueue(new Callback<TaxyFeeResponse>() {
            @Override
            public void onResponse(Call<TaxyFeeResponse> call, Response<TaxyFeeResponse> response) {
                SDM.save_taxi_fee(response.body().getTaxyFees());
            }

            @Override
            public void onFailure(Call<TaxyFeeResponse> call, Throwable t) {
            }
        });
    }
    public void request_byID_user(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<UserResponse> call = client.get_User_id(id);
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                SDM.save_user(response.body().getUser());
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
            }
        });
    }

    public void request_mobil_logout(Mobile mobile){
        Retrofit.Builder build = new Retrofit.Builder()
                .baseUrl(URLManager.MOBILE)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = build.build();
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<MobileResponse> call = client.logout_mobile_account(mobile);
        call.enqueue(new Callback<MobileResponse>() {
            @Override
            public void onResponse(Call<MobileResponse> call, Response<MobileResponse> response) {
                if(response.body().getMessage().equals("deleted")){
                    ac.SManager.setPreferences(ac, "status", "");
                    ac.SManager.setPreferences(ac,"welcome", "");
                    ac.SManager.setPreferences(ac,"status", "");
                    ac.SManager.setPreferences(ac,"admin", "");
                    ac.SManager.setPreferences(ac,"alarm", "");
                    ac.SManager.setPreferences(ac,"alarm_time", "");
                    v_event.DBManager.dropDatabase();
                    ac.hide_dialog();
                    ac.go_to_action(v_main.class);
                }else{
                    ac.SManager.setPreferences(ac, "status", "");
                    ac.SManager.setPreferences(ac,"welcome", "");
                    ac.SManager.setPreferences(ac,"status", "");
                    ac.SManager.setPreferences(ac,"admin", "");
                    ac.SManager.setPreferences(ac,"alarm", "");
                    ac.SManager.setPreferences(ac,"alarm_time", "");
                    v_event.DBManager.dropDatabase();
                    ac.hide_dialog();
                    ac.go_to_action(v_main.class);
                }
            }

            @Override
            public void onFailure(Call<MobileResponse> call, Throwable t) {
                ac.SManager.setPreferences(ac, "status", "");
                v_event.DBManager.dropDatabase();
                ac.hide_dialog();
                ac.go_to_action(v_main.class);
            }
        });
    }
}
