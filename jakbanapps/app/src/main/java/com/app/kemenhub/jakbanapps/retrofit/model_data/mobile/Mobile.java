package com.app.kemenhub.jakbanapps.retrofit.model_data.mobile;

import com.google.gson.annotations.SerializedName;

public class Mobile {
	@SerializedName("id")
	private String id;
	@SerializedName("token")
	private String token;

	public Mobile(String id, String token) {
		this.id = id;
		this.token = token;
	}

	public Mobile() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
