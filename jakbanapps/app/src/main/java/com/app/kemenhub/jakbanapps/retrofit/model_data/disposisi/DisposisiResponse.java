package com.app.kemenhub.jakbanapps.retrofit.model_data.disposisi;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DisposisiResponse {
	@SerializedName("code")
	String code;
	@SerializedName("message")
	String message;
	@SerializedName("disposisi")
	Disposiser disposisi;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Disposiser getDisposisi() {
		return disposisi;
	}
	public void setDisposisi(Disposiser disposisi) {
		this.disposisi = disposisi;
	}
	
}
