package com.app.kemenhub.jakbanapps.retrofit.model_data.taxi_fee;

import com.app.kemenhub.jakbanapps.retrofit.model_data.provinsi.Provinsi;

public class TaxyFees {

	private String id;
	private Provinsi provinsi;
	private String unit;
	private String fee;
	public TaxyFees(){
	}
	public TaxyFees(String id, Provinsi provinsi, String unit, String fee) {
		super();
		this.id = id;
		this.provinsi = provinsi;
		this.unit = unit;
		this.fee = fee;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Provinsi getProvinsi() {
		return provinsi;
	}
	public void setProvinsi(Provinsi provinsi) {
		this.provinsi = provinsi;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getFee() {
		return fee;
	}
	public void setFee(String fee) {
		this.fee = fee;
	}
	
}
