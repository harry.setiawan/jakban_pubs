package com.app.kemenhub.jakbanapps.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;

import com.app.kemenhub.jakbanapps.R;
import com.app.kemenhub.jakbanapps.db.disposition;
import com.app.kemenhub.jakbanapps.db.manager.DManager;
import com.app.kemenhub.jakbanapps.db.notif;
import com.app.kemenhub.jakbanapps.db.perjalanan_dinas;
import com.app.kemenhub.jakbanapps.db.schedule;
import com.app.kemenhub.jakbanapps.retrofit.Request_Inf;
import com.app.kemenhub.jakbanapps.retrofit.SDM;
import com.app.kemenhub.jakbanapps.retrofit.URLManager;
import com.app.kemenhub.jakbanapps.retrofit.model_data.dinas.DinasResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.disposisi.DisposisiResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.disposisi.DisposissResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.mobile.Mobile;
import com.app.kemenhub.jakbanapps.retrofit.model_data.schedule.ScheduleResponse;
import com.app.kemenhub.jakbanapps.sys.SessionManager;
import com.app.kemenhub.jakbanapps.v_event;
import com.app.kemenhub.jakbanapps.v_main;
import com.app.kemenhub.jakbanapps.v_notif;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.RemoteMessage;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MyFirebaseMessagingService
        extends com.google.firebase.messaging.FirebaseMessagingService {

    private static final String TAG = "Android News App";
    public SessionManager SManager;
    public DManager DManager;
    public Retrofit retrofit;
    com.app.kemenhub.jakbanapps.retrofit.SDM SDM;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        this.SManager = new SessionManager();
        this.DManager = new DManager(getApplicationContext());
        this.SDM = new SDM(getApplicationContext());
        retro();
        process_data(remoteMessage.getData().get("title"), remoteMessage.getData().get("body"));
    }

    private void process_data(String title, String body) {
        try {
            String[] msg = body.split(" : ");
            if (msg[0].equals("DISPOSISI")) {
                request_id_disposisi(msg[1]);
            }
            if (msg[0].equals("MEETING")) {
                request_id_meeting(msg[1]);
            }
            if (msg[0].equals("DINAS")) {
                request_id_dinas(msg[1]);
            }
        }catch (Exception e){}
    }

    private void retro() {
        this.retrofit = new Retrofit.Builder()
                .baseUrl(URLManager.URI_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
    public void request_id_disposisi(final String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<DisposisiResponse> call = client.get_disposisi_id(id);
        call.enqueue(new Callback<DisposisiResponse>() {
            @Override
            public void onResponse(Call<DisposisiResponse> call, final Response<DisposisiResponse> response) {
                try {
                    if(response.body().getDisposisi().getId() == null || response.body().getDisposisi().getId().equals("")){
                        DManager.delete_notifById(id, "DISPOSISI");
                        SDM.delete_disposisi(id);
                    }else {
                        List<notif> ntf = new ArrayList<>();
                        ntf.add(0, new notif(Long.parseLong(String.valueOf(DManager.list_notif().size() + 1)), id, "DISPOSISI"));
                        DManager.insert_notif_InTX(ntf);

                        SDM.save_disposisi(response.body().getDisposisi());
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                sendNotification();
                            }
                        }, 1000);
                    }
                }catch (Exception e){
                    List<notif> ntf = new ArrayList<>();
                    ntf.add(0, new notif(Long.parseLong(String.valueOf(DManager.list_notif().size() + 1)), id, "DISPOSISI"));
                    DManager.insert_notif_InTX(ntf);

                    SDM.save_disposisi(response.body().getDisposisi());
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            sendNotification();
                        }
                    }, 1000);}
            }

            @Override
            public void onFailure(Call<DisposisiResponse> call, Throwable t) {
                call.cancel();
            }
        });
    }

    public void request_id_meeting(final String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<ScheduleResponse> call = client.get_Schedule_id(id);
        call.enqueue(new Callback<ScheduleResponse>() {
            @Override
            public void onResponse(Call<ScheduleResponse> call, final Response<ScheduleResponse> response) {
                try {
                    if(response.body().getSchedule().getId() == null || response.body().getSchedule().getId().equals("")){
                        DManager.delete_notifById(id, "MEETING");
                        SDM.delete_schedule(id);
                    }else {
                        List<notif> ntf = new ArrayList<>();
                        ntf.add(0, new notif(Long.parseLong(String.valueOf(DManager.list_notif().size() + 1)), id, "MEETING"));
                        DManager.insert_notif_InTX(ntf);
                        if(!SManager.getPreferences(getApplicationContext(), "alarm").toString().equals(id)) {
                            SManager.setPreferences(getApplicationContext(), "alarm", "");
                        }

                        SDM.save_meeting(response.body().getSchedule());
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                sendNotification();
                            }
                        },1000);
                    }
                }catch (Exception e){
                    List<notif> ntf = new ArrayList<>();
                    ntf.add(0, new notif(Long.parseLong(String.valueOf(DManager.list_notif().size() + 1)), id, "MEETING"));
                    DManager.insert_notif_InTX(ntf);
                    if(!SManager.getPreferences(getApplicationContext(), "alarm").toString().equals(id)) {
                        SManager.setPreferences(getApplicationContext(), "alarm", "");
                    }
                    SDM.save_meeting(response.body().getSchedule());
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            sendNotification();
                        }
                    },1000);
                }
            }

            @Override
            public void onFailure(Call<ScheduleResponse> call, Throwable t) {
                call.cancel();
            }
        });
    }

    public void request_id_dinas(final String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<DinasResponse> call = client.get_dinas_id(id);
        call.enqueue(new Callback<DinasResponse>() {
            @Override
            public void onResponse(Call<DinasResponse> call, final Response<DinasResponse> response) {
                try {
                    if(response.body().getDinas().getId() == null || response.body().getDinas().getId().equals("")){
                        DManager.delete_notifById(id, "DINAS");
                        SDM.delete_dinas(id);
                    }else {
                        List<notif> ntf = new ArrayList<>();
                        ntf.add(0, new notif(Long.parseLong(String.valueOf(DManager.list_notif().size() + 1)), id, "DINAS"));
                        DManager.insert_notif_InTX(ntf);

                        SDM.save_dinas(response.body().getDinas());
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                sendNotification();
                            }
                        },1000);
                    }
                }catch (Exception e){
                    List<notif> ntf = new ArrayList<>();
                    ntf.add(0, new notif(Long.parseLong(String.valueOf(DManager.list_notif().size() + 1)), id, "DINAS"));
                    DManager.insert_notif_InTX(ntf);

                    SDM.save_dinas(response.body().getDinas());
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            sendNotification();
                        }
                    },1000);
                }
            }

            @Override
            public void onFailure(Call<DinasResponse> call, Throwable t) {
                call.cancel();
            }
        });
    }

    private void sendNotification() {
        if(!SManager.getPreferences(getApplicationContext(), "status").equals("")) {
            List<disposition> disposisi = DManager.list_dispositionNotif();
            List<schedule> meeting = DManager.list_scheduleNotif();
            List<perjalanan_dinas> dinas = DManager.list_perjalanan_dinasNotif();
            int total = disposisi.size() + meeting.size() + dinas.size();
            set_alert_action(total);
            String body_kecil = set_body(disposisi, meeting, dinas);
            int color = getApplicationContext().getResources().getColor(R.color.md_text_white);
            Intent intent = new Intent(this, v_notif.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("Jakban Apps")
                    .setContentText(body_kecil)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

            android.support.v7.app.NotificationCompat.InboxStyle inboxStyle = new android.support.v7.app.NotificationCompat.InboxStyle();
            inboxStyle.setBigContentTitle("Jakban Apps");
            for (int i = 0; i < disposisi.size(); i++) {
                inboxStyle.addLine("Disposisi : " + disposisi.get(i).getTitle());
            }
            for (int i = 0; i < meeting.size(); i++) {
                inboxStyle.addLine("Agenda Acara : " + meeting.get(i).getTitle());
            }
            for (int i = 0; i < dinas.size(); i++) {
                inboxStyle.addLine("Dinas : " + dinas.get(i).getDescription());
            }
            if (total > 1) {
                inboxStyle.setSummaryText(total + " Messages");
            } else {
                inboxStyle.setSummaryText(total + " Message");
            }
            builder.setStyle(inboxStyle);
            builder.setPriority(Notification.PRIORITY_HIGH);
            builder.setAutoCancel(true);
            AudioManager audio = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
            switch (audio.getRingerMode()) {
                case AudioManager.RINGER_MODE_NORMAL:
                    builder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
                    builder.setVibrate(new long[]{300, 300, 300, 300, 300});
                    break;
                case AudioManager.RINGER_MODE_SILENT:
                    builder.setVibrate(new long[]{300, 300, 300, 300, 300});
                    break;
                case AudioManager.RINGER_MODE_VIBRATE:
                    builder.setVibrate(new long[]{300, 300, 300, 300, 300});
                    break;
            }
            //LED
            builder.setLights(Color.RED, 1000, 1000);

            //builder.setSubText("Tap to view documentation about notifications.");
            if (Build.VERSION.SDK_INT >= 21) {
                builder.setColor(color);
            }

            NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(getApplicationContext().NOTIFICATION_SERVICE);
            notificationManager.notify(0, builder.build());
        }
    }
    private void set_alert_action(int total) {
        try {
            v_event.count_notif.setText(String.valueOf(total));
            v_event.count_notif.setVisibility(View.VISIBLE);
        }catch (Exception e){

        }
    }
    private String set_body(List<disposition> disposisi, List<schedule> meeting, List<perjalanan_dinas> dinas) {
        if(disposisi.size() > 0 && meeting.size() > 0 && dinas.size() > 0){
            return "Pesan Disposisi, Agenda Acara, dan Dinas";
        }else if(disposisi.size() > 0 && meeting.size() > 0 && dinas.size() == 0){
            return "Pesan Disposisi dan Agenda Acara";
        }else if(disposisi.size() > 0 && meeting.size() == 0 && dinas.size() > 0){
            return "Pesan Disposisi dan Dinas";
        }else if(disposisi.size() == 0 && meeting.size() > 0 && dinas.size() > 0){
            return "Pesan Agenda Acara dan Dinas";
        }else if(disposisi.size() == 0 && meeting.size() == 0 && dinas.size() > 0){
            return "Pesan Dinas";
        }else if(disposisi.size() >= 0 && meeting.size() == 0 && dinas.size() == 0){
            return "Pesan Disposisi";
        }else{
            return "Pesan Agenda Acara";
        }
    }
}