package com.app.kemenhub.jakbanapps;

import android.app.ActivityOptions;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.transition.Explode;
import android.transition.Slide;
import android.transition.Transition;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.app.kemenhub.jakbanapps.adapter.DataNotifAdt;
import com.app.kemenhub.jakbanapps.db.disposition;
import com.app.kemenhub.jakbanapps.db.manager.DManager;
import com.app.kemenhub.jakbanapps.db.manager.IDManager;
import com.app.kemenhub.jakbanapps.db.notif;
import com.app.kemenhub.jakbanapps.db.perjalanan_dinas;
import com.app.kemenhub.jakbanapps.db.schedule;
import com.app.kemenhub.jakbanapps.retrofit.Request_Inf;
import com.app.kemenhub.jakbanapps.retrofit.SDM;
import com.app.kemenhub.jakbanapps.retrofit.URLManager;
import com.app.kemenhub.jakbanapps.retrofit.model_data.dinas.DinasResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.disposisi.DisposisiResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.schedule.ScheduleResponse;
import com.app.kemenhub.jakbanapps.sys.ExceptionHandler;
import com.app.kemenhub.jakbanapps.sys.SessionManager;
import com.app.kemenhub.jakbanapps.sys.utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class v_notif extends AppCompatActivity implements DataNotifAdt.itemClickCallback{

    public static IDManager DBManager;
    SessionManager SManager;
    CardView line;
    RecyclerView recyclerview;
    DataNotifAdt adapter;
    List<notif> data_notif = new ArrayList<>();
    TextView nothing;
    public Retrofit retrofit;
    com.app.kemenhub.jakbanapps.retrofit.SDM SDM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_v_notif);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Notifikasi");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        DBManager = new DManager(this);
        SManager = new SessionManager();
        this.SDM = new SDM(getApplicationContext());
        retro();

        _init();
        set_ntf_data();
        set_data();

    }

    private void retro() {
        this.retrofit = new Retrofit.Builder()
                .baseUrl(URLManager.URI_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    private void _init() {
        line = (CardView) findViewById(R.id.line_card);
        if (Build.VERSION.SDK_INT < 21) {
            line.setVisibility(View.GONE);
        }
        recyclerview = (RecyclerView) findViewById(R.id.my_recycler_message);
        nothing = (TextView) findViewById(R.id.nothing);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerview.setLayoutManager(layoutManager);
        adapter = new DataNotifAdt(data_notif, this);
        recyclerview.setAdapter(adapter);
        adapter.setItemClickCallback(this);

    }

    private void set_data() {
        data_notif.clear();
        data_notif.addAll(DBManager.list_notif());
        for(int i = 0 ; i < data_notif.size() ; i++){
            schedule data_meeting = DBManager.get_scheduleById(data_notif.get(i).getData());
            try{
                if(data_meeting == null || data_meeting.getId() == null){
                    data_notif.remove(i);
                }
            }catch (Exception e){
                data_notif.remove(i);
            }
        }
        adapter.notifyDataSetChanged();
        if(data_notif.size() == 0){
            recyclerview.setVisibility(View.GONE);
            nothing.setVisibility(View.VISIBLE);
        }else{
            recyclerview.setVisibility(View.VISIBLE);
            nothing.setVisibility(View.GONE);
        }
    }

    private void set_ntf_data() {
        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(getApplicationContext().NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
        List<disposition> disposisi =  DBManager.list_dispositionNotif();
        for(int i = 0 ; i < disposisi.size() ; i++){
            disposisi.get(i).setNotif(false);
        }
        DBManager.insert_disposition_InTX(disposisi);

        List<schedule> meeting =  DBManager.list_scheduleNotif();
        for(int i = 0 ; i < meeting.size() ; i++){
            meeting.get(i).setNotif(false);
        }
        DBManager.insert_schedule_InTX(meeting);

        List<perjalanan_dinas> dinas =  DBManager.list_perjalanan_dinasNotif();
        for(int i = 0 ; i < dinas.size() ; i++){
            dinas.get(i).setNotif(false);
        }
        DBManager.insert_perjalanan_dinas_InTX(dinas);
    }

    public void go_to(final Class classes) {
        //activity.list_chat.clear();
        if (Build.VERSION.SDK_INT >= 21) {
            Transition exitTrans = new Explode();
            getWindow().setExitTransition(exitTrans);

            Transition reenterTrans = new Slide();
            getWindow().setEnterTransition(reenterTrans);
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this);
            Intent intent = new Intent(this, classes);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent, options.toBundle());
        }else{
            Intent intent = new Intent(this, classes);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.v_notif, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.action_bersih:
                DBManager.delete_notif();
                set_data();
                break;
            default:
                onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        go_to(v_event.class);
    }

    @Override
    public void onItemClick(int p) {
        if(data_notif.get(p).getTag().equals("DISPOSISI")){
            utils.TAG_LAYOUT = "f_disposisi";
        }else if(data_notif.get(p).getTag().equals("MEETING")){
            utils.TAG_LAYOUT = "f_meeting";
        }else if(data_notif.get(p).getTag().equals("DINAS")){
            utils.TAG_LAYOUT = "f_dinas";
        }
        schedule schedule = v_event.DBManager.get_scheduleById(data_notif.get(p).getData());
        try {
            if(schedule == null || schedule.getId().equals("")){
                go_to(v_event.class);
            }else {
                go_to_action(v_detil.class, p);
            }
        }catch (Exception e){
            SManager.setPreferences(this, "ntf_show", "true");
            go_to(v_event.class);
        }
    }

    public void go_to_action(final Class classes, int p) {
        if (Build.VERSION.SDK_INT >= 21) {
            Transition exitTrans = new Explode();
            getWindow().setExitTransition(exitTrans);

            Transition reenterTrans = new Slide();
            getWindow().setEnterTransition(reenterTrans);
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this);
            Intent intent = new Intent(this, classes);
            intent.putExtra(data_notif.get(p).getTag(), data_notif.get(p).getData());
            this.startActivity(intent, options.toBundle());
        }else{
            Intent intent = new Intent(this, classes);
            intent.putExtra(data_notif.get(p).getTag(), data_notif.get(p).getData());
            this.startActivity(intent);
        }
    }
}
