package com.app.kemenhub.jakbanapps.retrofit.model_data.vehicle;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VehiclesResponse {
	@SerializedName("code")
	String code;
	@SerializedName("message")
	String message;
	@SerializedName("vehicles")
	List<Vehicles> vehicle;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<Vehicles> getVehicles() {
		return vehicle;
	}
	public void setVehicles(List<Vehicles> vehicle) {
		this.vehicle = vehicle;
	}
	
	
}
