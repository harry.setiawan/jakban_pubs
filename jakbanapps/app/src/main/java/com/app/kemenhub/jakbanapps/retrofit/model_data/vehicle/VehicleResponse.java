package com.app.kemenhub.jakbanapps.retrofit.model_data.vehicle;

import com.google.gson.annotations.SerializedName;

public class VehicleResponse {
	@SerializedName("code")
	String code;
	@SerializedName("message")
	String message;
	@SerializedName("vehicle")
	Vehicles vehicle;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Vehicles getVehicles() {
		return vehicle;
	}
	public void setVehicles(Vehicles vehicle) {
		this.vehicle = vehicle;
	}
	
	
}
