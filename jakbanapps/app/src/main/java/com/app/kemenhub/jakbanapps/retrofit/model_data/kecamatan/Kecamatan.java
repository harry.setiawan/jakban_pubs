package com.app.kemenhub.jakbanapps.retrofit.model_data.kecamatan;

public class Kecamatan {
	private String id;
	private String kabupaten_id;
	private String name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getKabupaten_id() {
		return kabupaten_id;
	}

	public void setKabupaten_id(String kabupaten_id) {
		this.kabupaten_id = kabupaten_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
