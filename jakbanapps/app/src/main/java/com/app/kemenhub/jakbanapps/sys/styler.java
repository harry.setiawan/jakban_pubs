package com.app.kemenhub.jakbanapps.sys;

import android.content.ClipData;
import android.os.Build;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.app.kemenhub.jakbanapps.R;
import com.app.kemenhub.jakbanapps.db.users;
import com.app.kemenhub.jakbanapps.v_event;

public class styler {
    v_event activity;
    public TextView mode;

    public styler(v_event activity){
        this.activity = activity;
    }

    public void drawer(){
        activity.drawer = (DrawerLayout) activity.findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                activity, activity.drawer, activity.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        activity.drawer.setDrawerListener(toggle);
        toggle.syncState();

        activity.navigationView = (NavigationView) activity.findViewById(R.id.nav_view);
        Menu mn = activity.navigationView.getMenu();
        MenuItem dinas = mn.findItem(R.id.nav_dinas);
        //dinas.setVisible(false);
        View nav_header;

        if (Build.VERSION.SDK_INT >= 21) {
            nav_header = LayoutInflater.from(activity).inflate(R.layout.nav_header_v_event, null);
        }else{
            nav_header = LayoutInflater.from(activity).inflate(R.layout.nav_header_v_event, null);
        }
        TextView nama = (TextView) nav_header.findViewById(R.id.name);
        mode = (TextView) nav_header.findViewById(R.id.mode);
        TextView mail = (TextView) nav_header.findViewById(R.id.email);
        users user = activity.DBManager.get_usersById(activity.SManager.getPreferences(activity, "status"));
        nama.setText(user.getName());
        mail.setText(user.getNIP());

        activity.navigationView.addHeaderView(nav_header);
        activity.navigationView.setNavigationItemSelectedListener(activity);
    }
}
