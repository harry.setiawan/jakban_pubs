package com.app.kemenhub.jakbanapps.db.manager;

import com.app.kemenhub.jakbanapps.db.airplane_ticket;
import com.app.kemenhub.jakbanapps.db.category;
import com.app.kemenhub.jakbanapps.db.disposition;
import com.app.kemenhub.jakbanapps.db.file;
import com.app.kemenhub.jakbanapps.db.folder;
import com.app.kemenhub.jakbanapps.db.hotel_fee;
import com.app.kemenhub.jakbanapps.db.kabupaten;
import com.app.kemenhub.jakbanapps.db.kecamatan;
import com.app.kemenhub.jakbanapps.db.kelurahan;
import com.app.kemenhub.jakbanapps.db.notif;
import com.app.kemenhub.jakbanapps.db.perjalanan_dinas;
import com.app.kemenhub.jakbanapps.db.perjalanan_dinas_jenis;
import com.app.kemenhub.jakbanapps.db.place;
import com.app.kemenhub.jakbanapps.db.position;
import com.app.kemenhub.jakbanapps.db.provinsi;
import com.app.kemenhub.jakbanapps.db.roles;
import com.app.kemenhub.jakbanapps.db.schedule;
import com.app.kemenhub.jakbanapps.db.schedule_category;
import com.app.kemenhub.jakbanapps.db.taxi_fee;
import com.app.kemenhub.jakbanapps.db.user_class;
import com.app.kemenhub.jakbanapps.db.user_mobile;
import com.app.kemenhub.jakbanapps.db.user_role;
import com.app.kemenhub.jakbanapps.db.users;
import com.app.kemenhub.jakbanapps.db.vehicle_rent;

import java.util.List;

/**
 * Created by Axa.Laranza on 6/27/2017.
 */

public interface IDManager {
    void insert_airplane_ticket(airplane_ticket airplane_ticket);
    void insert_airplane_ticket_InTX(List<airplane_ticket> airplane_ticket);
    List<airplane_ticket> list_airplane_ticket();
    void update_airplane_ticket(airplane_ticket airplane_ticket);
    void delete_airplane_ticketById(String id);
    airplane_ticket get_airplane_ticketById(String id);
    void delete_airplane_ticket();

    void insert_disposition(disposition disposition);
    void insert_disposition_InTX(List<disposition> disposition);
    List<disposition> list_disposition();
    List<disposition> list_dispositionByFAC(String forward,String agenda,String category);
    List<disposition> list_dispositionByTO(String to);
    List<disposition> list_dispositionByTOdanClass(String to, String Cls);
    List<disposition> list_dispositionByCAT(String cateogory);
    void update_disposition(disposition disposition);
    void delete_dispositionById(String id);
    disposition get_dispositionById(String id);
    void delete_disposition();
    List<disposition> list_dispositionNotif();

    void insert_file(file file);
    void insert_file_InTX(List<file> file);
    List<file> list_file();
    void update_file(file file);
    void delete_fileById(String id);
    file get_fileById(String id);
    void delete_file();

    void insert_category(category category);
    void insert_category_InTX(List<category> category);
    List<category> list_category();
    void update_category(category category);
    void delete_categoryById(String id);
    category get_categoryById(String id);
    void delete_category();

    void insert_folder(folder folder);
    void insert_folder_InTX(List<folder> folder);
    List<folder> list_folder();
    void update_folder(folder folder);
    void delete_folderById(String id);
    folder get_folderById(String id);
    void delete_folder();

    void insert_hotel_fee(hotel_fee hotel_fee);
    void insert_hotel_fee_InTX(List<hotel_fee> hotel_fee);
    List<hotel_fee> list_hotel_fee();
    void update_hotel_fee(hotel_fee hotel_fee);
    void delete_hotel_feeById(String id);
    hotel_fee get_hotel_feeById(String id);
    void delete_hotel_fee();

    void insert_kabupaten(kabupaten kabupaten);
    void insert_kabupaten_InTX(List<kabupaten> kabupaten);
    List<kabupaten> list_kabupaten();
    void update_kabupaten(kabupaten kabupaten);
    void delete_kabupatenById(String id);
    kabupaten get_kabupatenById(String id);
    void delete_kabupaten();

    void insert_user_role(user_role user_role);
    void insert_user_role_InTX(List<user_role> user_role);
    List<user_role> list_user_role();
    void update_user_role(user_role user_role);
    void delete_user_roleById(String id);
    List<user_role> get_user_roleById(String id);
    void delete_user_role();

    void insert_kecamatan(kecamatan kecamatan);
    void insert_kecamatan_InTX(List<kecamatan> kecamatan);
    List<kecamatan> list_kecamatan();
    void update_kecamatan(kecamatan kecamatan);
    void delete_kecamatanById(String id);
    kecamatan get_kecamatanById(String id);
    void delete_kecamatan();

    void insert_kelurahan(kelurahan kelurahan);
    void insert_kelurahan_InTX(List<kelurahan> kelurahan);
    List<kelurahan> list_kelurahan();
    void update_kelurahan(kelurahan kelurahan);
    void delete_kelurahanById(String id);
    kelurahan get_kelurahanById(String id);
    void delete_kelurahan();

    void insert_provinsi(provinsi provinsi);
    void insert_provinsi_InTX(List<provinsi> provinsi);
    List<provinsi> list_provinsi();
    void update_provinsi(provinsi provinsi);
    void delete_provinsiById(String id);
    provinsi get_provinsiById(String id);
    void delete_provinsi();

    void insert_perjalanan_dinas(perjalanan_dinas perjalanan_dinas);
    void insert_perjalanan_dinas_InTX(List<perjalanan_dinas> perjalanan_dinas);
    List<perjalanan_dinas> list_perjalanan_dinas();
    List<perjalanan_dinas> list_perjalanan_dinasByEDT(String Employee_id,String Date_departure,String Type_code);
    List<perjalanan_dinas> list_perjalanan_dinasByD(String Employee_id,String date);
    void update_perjalanan_dinas(perjalanan_dinas perjalanan_dinas);
    void delete_perjalanan_dinasById(String id);
    perjalanan_dinas get_perjalanan_dinasById(String id);
    void delete_perjalanan_dinas();
    List<perjalanan_dinas> list_perjalanan_dinasNotif();

    void insert_jenis_dinas(perjalanan_dinas_jenis perjalanan_dinas_jenis);
    void insert_jenis_dinas_InTX(List<perjalanan_dinas_jenis> perjalanan_dinas_jenis);
    List<perjalanan_dinas_jenis> list_jenis_dinas();
    void update_jenis_dinas(perjalanan_dinas_jenis perjalanan_dinas_jenis);
    void delete_jenis_dinasById(String id);
    List<perjalanan_dinas_jenis> get_jenis_dinasById(List<perjalanan_dinas_jenis> id);
    perjalanan_dinas_jenis get_jenis_dinasById(String id);
    void delete_jenis_dinas();

    void insert_place(place place);
    void insert_place_InTX(List<place> place);
    List<place> list_place();
    void update_place(place place);
    void delete_placeById(String id);
    place get_placeById(String id);
    void delete_place();

    void insert_position(position position);
    void insert_position_InTX(List<position> position);
    List<position> list_position();
    void update_position(position position);
    void delete_positionById(String id);
    position get_positionById(String id);
    void delete_position();

    void insert_roles(roles roles);
    void insert_roles_InTX(List<roles> roles);
    List<roles> list_roles();
    void update_roles(roles roles);
    void delete_rolesById(String id);
    roles get_rolesById(String id);
    List<Long> get_rolesAddByIdRole();
    void delete_roles();

    void insert_schedule(schedule schedule);
    void insert_schedule_InTX(List<schedule> schedule);
    List<schedule> list_schedule();
    List<schedule> list_scheduleByFDCP(String forward,String date,String category,String place);
    List<schedule> list_scheduleByD(String forward,String date);
    void update_schedule(schedule schedule);
    void delete_scheduleById(String id);
    schedule get_scheduleById(String id);
    void delete_schedule();
    List<schedule> list_scheduleNotif();

    void insert_taxi_fee(taxi_fee taxi_fee);
    void insert_taxi_fee_InTX(List<taxi_fee> taxi_fee);
    List<taxi_fee> list_taxi_fee();
    void update_taxi_fee(taxi_fee taxi_fee);
    void delete_taxi_feeById(String id);
    taxi_fee get_taxi_feeById(String id);
    void delete_taxi_fee();

    void insert_user_class(user_class user_class);
    void insert_user_class_InTX(List<user_class> user_class);
    List<user_class> list_user_class();
    void update_user_class(user_class user_class);
    void delete_user_classById(String id);
    user_class get_user_classById(String id);
    void delete_user_class();

    void insert_notif_InTX(List<notif> notif);
    List<notif> list_notif();
    void delete_notif();
    void delete_notifById(String id, String tag);

    void insert_user_mobile(user_mobile user_mobile);
    void insert_user_mobile_InTX(List<user_mobile> user_mobile);
    List<user_mobile> list_user_mobile();
    void update_user_mobile(user_mobile user_mobile);
    void delete_user_mobileById(String id);
    user_mobile get_user_mobileById(String id);
    void delete_user_mobile();

    void insert_users(users users);
    void insert_users_InTX(List<users> users);
    List<users> list_users();
    List<users> list_users_add();
    void update_users(users users);
    void delete_usersById(String id);
    users get_usersById(String id);
    void delete_users();

    void insert_vehicle_rent(vehicle_rent vehicle_rent);
    void insert_vehicle_rent_InTX(List<vehicle_rent> vehicle_rent);
    List<vehicle_rent> list_vehicle_rent();
    void update_vehicle_rent(vehicle_rent vehicle_rent);
    void delete_vehicle_rentById(String id);
    vehicle_rent get_vehicle_rentById(String id);
    void delete_vehicle_rent();
    void delete_all_table();

    void closeDbConnections();
    void dropDatabase();
}
