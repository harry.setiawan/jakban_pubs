package com.app.kemenhub.jakbanapps.fragment.detil;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.app.kemenhub.jakbanapps.R;
import com.app.kemenhub.jakbanapps.adapter.DataCategoryAdt;
import com.app.kemenhub.jakbanapps.adapter.DataForwardAdt;
import com.app.kemenhub.jakbanapps.db.category;
import com.app.kemenhub.jakbanapps.db.disposition;
import com.app.kemenhub.jakbanapps.db.schedule;
import com.app.kemenhub.jakbanapps.db.users;
import com.app.kemenhub.jakbanapps.retrofit.URLManager;
import com.app.kemenhub.jakbanapps.sys.utils;
import com.app.kemenhub.jakbanapps.v_detil;
import com.app.kemenhub.jakbanapps.v_event;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class f_detil_meeting extends Fragment implements View.OnClickListener, DataCategoryAdt.itemCategoryClickCallback, DataForwardAdt.itemForwardClickCallback{

    v_detil ac;
    View v;

    TextView subtitle;
    EditText edt_tanggal_diterima, edt_no_surat, edt_no_agenda, edt_dari, edt_perihal, edt_tanggal, edt_waktu, edt_tempat, edt_klasifikasi, edt_catatan, edt_desc, edt_file;
    AppCompatButton download;
    RecyclerView rc_category, rc_forward;

    schedule schedule;

    List<category> categories = new ArrayList<>();
    List<users> forward = new ArrayList<>();
    DataCategoryAdt category_adapter;
    DataForwardAdt forward_adapter;
    CardView line;
    ProgressDialog mProgressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.f_detil_meeting, container, false);
        ac = (v_detil) getActivity();

        _init();
        _shown_data();
        return v;
    }

    private void _init() {
        line = (CardView) v.findViewById(R.id.line_card);
        if (Build.VERSION.SDK_INT < 21) {
            line.setVisibility(View.GONE);
        }
        edt_no_surat = (EditText) v.findViewById(R.id.edt_no_surat);
        edt_no_agenda = (EditText) v.findViewById(R.id.edt_no_agenda);
        edt_tanggal_diterima = (EditText) v.findViewById(R.id.edt_tgl_diterima);
        edt_dari = (EditText) v.findViewById(R.id.edt_dari);
        edt_perihal = (EditText) v.findViewById(R.id.edt_perihal);
        edt_tanggal = (EditText) v.findViewById(R.id.edt_tgl_agenda);
        edt_waktu = (EditText) v.findViewById(R.id.edt_jam_agenda);
        edt_tempat = (EditText) v.findViewById(R.id.edt_tempat);
        edt_klasifikasi = (EditText) v.findViewById(R.id.edt_klasifikasi);
        edt_catatan = (EditText) v.findViewById(R.id.edt_catatan);
        edt_desc = (EditText) v.findViewById(R.id.edt_desc);
        edt_file = (EditText) v.findViewById(R.id.edt_file);
        download = (AppCompatButton) v.findViewById(R.id.btn_download);
        rc_category = (RecyclerView) v.findViewById(R.id.rc_category);
        rc_forward = (RecyclerView) v.findViewById(R.id.rc_forward);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ac);
        rc_category.setLayoutManager(layoutManager);
        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(ac);
        rc_forward.setLayoutManager(layoutManager2);

        category_adapter = new DataCategoryAdt(categories, ac);
        rc_category.setAdapter(category_adapter);
        forward_adapter = new DataForwardAdt(forward, ac);
        rc_forward.setAdapter(forward_adapter);

        category_adapter.setItemCategoryClickCallback(this);
        forward_adapter.setItemForwardClickCallback(this);
        download.setOnClickListener(this);
    }

    private void _shown_data() {
        try {
            schedule = v_event.DBManager.get_scheduleById(ac.id_data);
            if (schedule.getId() == null || schedule == null) {
                ac.go_to(v_event.class);
            } else {
                try {
                    String[] dt_category = schedule.getCategory_id().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\"", "").replaceAll("null", "").split(",");
                    for (int i = 0; i < dt_category.length; i++) {
                        categories.add(i, v_event.DBManager.get_categoryById(dt_category[i]));
                    }
                } catch (Exception x) {

                }

                try {
                    String[] dt_forward = schedule.getForward_to_user_id().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\"", "").replaceAll("null", "").split(",");
                    for (int i = 0; i < dt_forward.length; i++) {
                        forward.add(i, v_event.DBManager.get_usersById(dt_forward[i]));
                    }
                } catch (Exception x) {

                }
                if (forward.size() == 0) {
                    forward.add(0, new users(null, "admin", null, null, null, null, null, null, null, null, null));
                }
                edt_tempat.setText(schedule.getPlace());
                try {
                    utils.TAG_FILE = schedule.getFile();
                    String[] nm_file = schedule.getFile().split("\\/");
                    edt_file.setText(nm_file[nm_file.length - 1]);
                } catch (Exception e) {
                    edt_file.setText("Tidak ada file");
                    edt_file.setTextColor(ac.getResources().getColor(R.color.md_red_700));
                    download.setEnabled(false);
                    download.setClickable(false);
                    download.setVisibility(View.GONE);
                }

                edt_no_surat.setText(schedule.getNo_surat_perintah());
                if (schedule.getNo_surat_perintah().equals("")) {
                    edt_no_surat.setText("-");
                }
                edt_no_agenda.setText(schedule.getNo_agenda());
                if (schedule.getNo_agenda().equals("")) {
                    edt_no_agenda.setText("-");
                }
                edt_dari.setText(schedule.getFrom());
                edt_perihal.setText(schedule.getTitle());
                String[] waktu_start = schedule.getDatetime_schedule_start().split(" ");
                String[] waktu_end = schedule.getDatetime_schedule_end().split(" ");

                String[] waktu_terima = schedule.getDatetime_schedule_start().split(" ");
                String[] tgl_terima = waktu_terima[0].split("-");
                edt_tanggal_diterima.setText((tgl_terima[2] + " " + _check_month(tgl_terima[1]) + " " + tgl_terima[0]));

                String[] tgl_start = waktu_start[0].split("-");
                String[] tgl_end = waktu_end[0].split("-");
                if ((tgl_start[2] + " " + _check_month(tgl_start[1]) + " " + tgl_start[0]).equals((tgl_end[2] + " " + _check_month(tgl_end[1]) + " " + tgl_end[0]))) {
                    edt_tanggal.setText((tgl_start[2] + " " + _check_month(tgl_start[1]) + " " + tgl_start[0]) + " - " + (tgl_end[2] + " " + _check_month(tgl_end[1]) + " " + tgl_end[0]));
                } else {
                    edt_tanggal.setText((tgl_start[2] + " " + _check_month(tgl_start[1]) + " " + tgl_start[0]));
                }
                edt_waktu.setText(waktu_start[1].substring(0, waktu_start[1].length() - 2) + " - " + waktu_end[1].substring(0, waktu_start[1].length() - 2));

                if (schedule.getNotes() == null) {
                    edt_catatan.setText("-");
                } else {
                    edt_catatan.setText(schedule.getNotes());
                }

                if (schedule.getDesc() == null) {
                    edt_desc.setText("-");
                } else {
                    edt_desc.setText(schedule.getDesc());
                }

                if (schedule.getCls().equals("1")) {
                    edt_klasifikasi.setText("Biasa");
                } else if (schedule.getCls().equals("2")) {
                    edt_klasifikasi.setText("Segera");
                } else if (schedule.getCls().equals("3")) {
                    edt_klasifikasi.setText("Sangat Segera");
                } else if (schedule.getCls().equals("4")) {
                    edt_klasifikasi.setText("Rahasia");
                } else {
                    //edt_klasifikasi.setText("-");
                    edt_klasifikasi.setText(schedule.getCls());
                }

                category_adapter.notifyDataSetChanged();
                forward_adapter.notifyDataSetChanged();
                ////Log.i("NXN", schedule.getNo_surat_perintah() +":"+ schedule.getNo_agenda() + ":" + schedule.getTgl_diterima());
            }
        }catch (Exception e){
            ac.go_to(v_event.class);
        }
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.btn_download:
                ////Log.i("URL DOWNLOAD TO", URLManager.URI__DOWNLOAD_API + schedule.getFile());
                new DownloadFile().execute(URLManager.URI__DOWNLOAD_API + schedule.getFile());

                break;
        }
    }

    @Override
    public void onItemClick(int p) {

    }

    // DownloadFile AsyncTask
    private class DownloadFile extends AsyncTask<String, Integer, String> {
        String nm_files = edt_file.getText().toString();
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create progress dialog
            mProgressDialog = new ProgressDialog(ac);
            // Set your progress dialog Title
            mProgressDialog.setTitle(nm_files);
            // Set your progress dialog Message
            mProgressDialog.setMessage("Downloading, Please Wait!");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setMax(100);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            // Show progress dialog
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... Url) {
            try {
                URL url = new URL(Url[0]);
                URLConnection connection = url.openConnection();
                connection.connect();

                // Detect the file lenghth
                int fileLength = connection.getContentLength();

                // Locate storage location
                String folder_download = "Jakban";
                File f = new File(Environment.getExternalStorageDirectory(), folder_download);
                if (!f.exists()) {
                    f.mkdirs();
                }
                String folder_meeting = "meeting";
                File m = new File(Environment.getExternalStorageDirectory() + "/Jakban", folder_meeting);
                if (!m.exists()) {
                    m.mkdirs();
                }

                String filepath = Environment.getExternalStorageDirectory().getPath() +"/Jakban/meeting/";
                // Download the file
                InputStream input = new BufferedInputStream(url.openStream());

                // Save the downloaded file
                OutputStream output = new FileOutputStream(filepath + nm_files);

                byte data[] = new byte[1024];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    // Publish the progress
                    publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }

                // Close connection
                output.flush();
                output.close();
                input.close();
            } catch (Exception e) {
                // Error Log
                ////Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            // Update the progress dialog
            mProgressDialog.setProgress(progress[0]);
            if(progress[0] == 100){
                mProgressDialog.dismiss();
                mProgressDialog.hide();
            }
            // Dismiss the progress dialog
            //mProgressDialog.dismiss();
        }
    }

    private String _check_month(String bulan){
        String nm_bln = null;
        switch (bulan){
            case "01":
                nm_bln = "Januari";
                break;
            case "02":
                nm_bln = "Februari";
                break;
            case "03":
                nm_bln = "Maret";
                break;
            case "04":
                nm_bln = "April";
                break;
            case "05":
                nm_bln = "Mei";
                break;
            case "06":
                nm_bln = "Juni";
                break;
            case "07":
                nm_bln = "Juli";
                break;
            case "08":
                nm_bln = "Agustus";
                break;
            case "09":
                nm_bln = "September";
                break;
            case "10":
                nm_bln = "Oktober";
                break;
            case "11":
                nm_bln = "November";
                break;
            case "12":
                nm_bln = "Desember";
                break;
        }
        if(nm_bln == null){
            return bulan;
        }else{
            return nm_bln;
        }
    }

    @Override
    public void onItemForwardClick(int p) {

    }
}
