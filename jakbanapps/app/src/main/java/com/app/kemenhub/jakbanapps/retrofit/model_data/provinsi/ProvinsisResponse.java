package com.app.kemenhub.jakbanapps.retrofit.model_data.provinsi;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProvinsisResponse {
	@SerializedName("code")
	String code;
	@SerializedName("message")
	String message;
	@SerializedName("provinsi")
	List<Provinsi> provinsi;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<Provinsi> getProvinsi() {
		return provinsi;
	}
	public void setProvinsi(List<Provinsi> provinsi) {
		this.provinsi = provinsi;
	}
	
	
}
