package com.app.kemenhub.jakbanapps.retrofit.model_data.schedule;

import com.app.kemenhub.jakbanapps.retrofit.model_data.category.Kategori;

import java.util.List;

public class Scheduler {
	private String id;

	private String from;
	
	private String title;
	
	private String datetime_schedule_start;

	private String datetime_schedule_end;

	private String tgl_diterima;

	private String no_surat_perintah;

	private String no_agenda;

	private String place;

	private String desc;

	private String notes;

	private List<Forward> forward;

	private List<Kategori> categories;
	
	private String cls;

	private String file;

	public Scheduler(){}

	public Scheduler(String id, String from, String title, String datetime_schedule_start, String datetime_schedule_end, String tgl_diterima, String no_surat_perintah, String no_agenda, String place, String desc, String notes, List<Forward> forward, List<Kategori> categories, String cls, String file) {
		this.id = id;
		this.from = from;
		this.title = title;
		this.datetime_schedule_start = datetime_schedule_start;
		this.datetime_schedule_end = datetime_schedule_end;
		this.tgl_diterima = tgl_diterima;
		this.no_surat_perintah = no_surat_perintah;
		this.no_agenda = no_agenda;
		this.place = place;
		this.desc = desc;
		this.notes = notes;
		this.forward = forward;
		this.categories = categories;
		this.cls = cls;
		this.file = file;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDatetime_schedule_start() {
		return datetime_schedule_start;
	}

	public void setDatetime_schedule_start(String datetime_schedule_start) {
		this.datetime_schedule_start = datetime_schedule_start;
	}

	public String getDatetime_schedule_end() {
		return datetime_schedule_end;
	}

	public void setDatetime_schedule_end(String datetime_schedule_end) {
		this.datetime_schedule_end = datetime_schedule_end;
	}

	public String getTgl_diterima() {
		return tgl_diterima;
	}

	public void setTgl_diterima(String tgl_diterima) {
		this.tgl_diterima = tgl_diterima;
	}

	public String getNo_surat_perintah() {
		return no_surat_perintah;
	}

	public void setNo_surat_perintah(String no_surat_perintah) {
		this.no_surat_perintah = no_surat_perintah;
	}

	public String getNo_agenda() {
		return no_agenda;
	}

	public void setNo_agenda(String no_agenda) {
		this.no_agenda = no_agenda;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public List<Forward> getForward() {
		return forward;
	}

	public void setForward(List<Forward> forward) {
		this.forward = forward;
	}

	public List<Kategori> getCategories() {
		return categories;
	}

	public void setCategories(List<Kategori> categories) {
		this.categories = categories;
	}

	public String getCls() {
		return cls;
	}

	public void setCls(String cls) {
		this.cls = cls;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}
}
