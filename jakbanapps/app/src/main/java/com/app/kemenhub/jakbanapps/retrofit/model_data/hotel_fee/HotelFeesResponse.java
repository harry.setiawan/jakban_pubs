package com.app.kemenhub.jakbanapps.retrofit.model_data.hotel_fee;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HotelFeesResponse {
	@SerializedName("code")
	String code;
	@SerializedName("message")
	String message;
	@SerializedName("hotelFees")
	List<HotelFees> hotel_fee;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<HotelFees> getHotelFees() {
		return hotel_fee;
	}
	public void setHotelFees(List<HotelFees> hotel_fee) {
		this.hotel_fee = hotel_fee;
	}
}
