package com.app.kemenhub.jakbanapps.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;

import com.app.kemenhub.jakbanapps.db.perjalanan_dinas;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "PERJALANAN_DINAS".
*/
public class perjalanan_dinasDao extends AbstractDao<perjalanan_dinas, Long> {

    public static final String TABLENAME = "PERJALANAN_DINAS";

    /**
     * Properties of entity perjalanan_dinas.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property Code = new Property(1, String.class, "code", false, "CODE");
        public final static Property Employee_id = new Property(2, String.class, "employee_id", false, "EMPLOYEE_ID");
        public final static Property Description = new Property(3, String.class, "description", false, "DESCRIPTION");
        public final static Property Remarks = new Property(4, String.class, "remarks", false, "REMARKS");
        public final static Property No_surat_perintah = new Property(5, String.class, "no_surat_perintah", false, "NO_SURAT_PERINTAH");
        public final static Property Date_departure = new Property(6, String.class, "date_departure", false, "DATE_DEPARTURE");
        public final static Property Date_arrival = new Property(7, String.class, "date_arrival", false, "DATE_ARRIVAL");
        public final static Property Day_duration = new Property(8, String.class, "day_duration", false, "DAY_DURATION");
        public final static Property Type_code = new Property(9, String.class, "type_code", false, "TYPE_CODE");
        public final static Property Departure_place_id = new Property(10, String.class, "departure_place_id", false, "DEPARTURE_PLACE_ID");
        public final static Property Arrival_place_id = new Property(11, String.class, "arrival_place_id", false, "ARRIVAL_PLACE_ID");
        public final static Property Transportation_type = new Property(12, String.class, "transportation_type", false, "TRANSPORTATION_TYPE");
        public final static Property Transportation_class = new Property(13, String.class, "transportation_class", false, "TRANSPORTATION_CLASS");
        public final static Property Airplane_ticker_id = new Property(14, String.class, "airplane_ticker_id", false, "AIRPLANE_TICKER_ID");
        public final static Property With_vehicle_rent = new Property(15, String.class, "with_vehicle_rent", false, "WITH_VEHICLE_RENT");
        public final static Property Vehicle_rent_type = new Property(16, String.class, "vehicle_rent_type", false, "VEHICLE_RENT_TYPE");
        public final static Property Vehicle_rent_price_id = new Property(17, String.class, "vehicle_rent_price_id", false, "VEHICLE_RENT_PRICE_ID");
        public final static Property With_hotel = new Property(18, String.class, "with_hotel", false, "WITH_HOTEL");
        public final static Property Hotel_ticket_id = new Property(19, String.class, "hotel_ticket_id", false, "HOTEL_TICKET_ID");
        public final static Property Hotel_amount_type = new Property(20, String.class, "hotel_amount_type", false, "HOTEL_AMOUNT_TYPE");
        public final static Property Hotel_amount = new Property(21, String.class, "hotel_amount", false, "HOTEL_AMOUNT");
        public final static Property Transportation_amount_type = new Property(22, String.class, "transportation_amount_type", false, "TRANSPORTATION_AMOUNT_TYPE");
        public final static Property Transportation_amount = new Property(23, String.class, "transportation_amount", false, "TRANSPORTATION_AMOUNT");
        public final static Property With_taxi = new Property(24, String.class, "with_taxi", false, "WITH_TAXI");
        public final static Property Taxi_qty_departure = new Property(25, String.class, "taxi_qty_departure", false, "TAXI_QTY_DEPARTURE");
        public final static Property Taxi_qty_arrival = new Property(26, String.class, "taxi_qty_arrival", false, "TAXI_QTY_ARRIVAL");
        public final static Property Taxi_ticket_departure = new Property(27, String.class, "taxi_ticket_departure", false, "TAXI_TICKET_DEPARTURE");
        public final static Property Taxi_ticket_arrival = new Property(28, String.class, "taxi_ticket_arrival", false, "TAXI_TICKET_ARRIVAL");
        public final static Property Taxi_amount_departure_type = new Property(29, String.class, "taxi_amount_departure_type", false, "TAXI_AMOUNT_DEPARTURE_TYPE");
        public final static Property Taxi_amount_arrival_type = new Property(30, String.class, "taxi_amount_arrival_type", false, "TAXI_AMOUNT_ARRIVAL_TYPE");
        public final static Property Taxi_amount_departure = new Property(31, String.class, "taxi_amount_departure", false, "TAXI_AMOUNT_DEPARTURE");
        public final static Property Taxi_amount_arrival = new Property(32, String.class, "taxi_amount_arrival", false, "TAXI_AMOUNT_ARRIVAL");
        public final static Property Taxi_total_amount = new Property(33, String.class, "taxi_total_amount", false, "TAXI_TOTAL_AMOUNT");
        public final static Property Vehicle_amount_type = new Property(34, String.class, "vehicle_amount_type", false, "VEHICLE_AMOUNT_TYPE");
        public final static Property Vehicle_amount = new Property(35, String.class, "vehicle_amount", false, "VEHICLE_AMOUNT");
        public final static Property Type_amount = new Property(36, String.class, "type_amount", false, "TYPE_AMOUNT");
        public final static Property Total_amount = new Property(37, String.class, "total_amount", false, "TOTAL_AMOUNT");
        public final static Property Notif = new Property(38, Boolean.class, "notif", false, "NOTIF");
    };


    public perjalanan_dinasDao(DaoConfig config) {
        super(config);
    }
    
    public perjalanan_dinasDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"PERJALANAN_DINAS\" (" + //
                "\"_id\" INTEGER PRIMARY KEY ," + // 0: id
                "\"CODE\" TEXT," + // 1: code
                "\"EMPLOYEE_ID\" TEXT," + // 2: employee_id
                "\"DESCRIPTION\" TEXT," + // 3: description
                "\"REMARKS\" TEXT," + // 4: remarks
                "\"NO_SURAT_PERINTAH\" TEXT," + // 5: no_surat_perintah
                "\"DATE_DEPARTURE\" TEXT," + // 6: date_departure
                "\"DATE_ARRIVAL\" TEXT," + // 7: date_arrival
                "\"DAY_DURATION\" TEXT," + // 8: day_duration
                "\"TYPE_CODE\" TEXT," + // 9: type_code
                "\"DEPARTURE_PLACE_ID\" TEXT," + // 10: departure_place_id
                "\"ARRIVAL_PLACE_ID\" TEXT," + // 11: arrival_place_id
                "\"TRANSPORTATION_TYPE\" TEXT," + // 12: transportation_type
                "\"TRANSPORTATION_CLASS\" TEXT," + // 13: transportation_class
                "\"AIRPLANE_TICKER_ID\" TEXT," + // 14: airplane_ticker_id
                "\"WITH_VEHICLE_RENT\" TEXT," + // 15: with_vehicle_rent
                "\"VEHICLE_RENT_TYPE\" TEXT," + // 16: vehicle_rent_type
                "\"VEHICLE_RENT_PRICE_ID\" TEXT," + // 17: vehicle_rent_price_id
                "\"WITH_HOTEL\" TEXT," + // 18: with_hotel
                "\"HOTEL_TICKET_ID\" TEXT," + // 19: hotel_ticket_id
                "\"HOTEL_AMOUNT_TYPE\" TEXT," + // 20: hotel_amount_type
                "\"HOTEL_AMOUNT\" TEXT," + // 21: hotel_amount
                "\"TRANSPORTATION_AMOUNT_TYPE\" TEXT," + // 22: transportation_amount_type
                "\"TRANSPORTATION_AMOUNT\" TEXT," + // 23: transportation_amount
                "\"WITH_TAXI\" TEXT," + // 24: with_taxi
                "\"TAXI_QTY_DEPARTURE\" TEXT," + // 25: taxi_qty_departure
                "\"TAXI_QTY_ARRIVAL\" TEXT," + // 26: taxi_qty_arrival
                "\"TAXI_TICKET_DEPARTURE\" TEXT," + // 27: taxi_ticket_departure
                "\"TAXI_TICKET_ARRIVAL\" TEXT," + // 28: taxi_ticket_arrival
                "\"TAXI_AMOUNT_DEPARTURE_TYPE\" TEXT," + // 29: taxi_amount_departure_type
                "\"TAXI_AMOUNT_ARRIVAL_TYPE\" TEXT," + // 30: taxi_amount_arrival_type
                "\"TAXI_AMOUNT_DEPARTURE\" TEXT," + // 31: taxi_amount_departure
                "\"TAXI_AMOUNT_ARRIVAL\" TEXT," + // 32: taxi_amount_arrival
                "\"TAXI_TOTAL_AMOUNT\" TEXT," + // 33: taxi_total_amount
                "\"VEHICLE_AMOUNT_TYPE\" TEXT," + // 34: vehicle_amount_type
                "\"VEHICLE_AMOUNT\" TEXT," + // 35: vehicle_amount
                "\"TYPE_AMOUNT\" TEXT," + // 36: type_amount
                "\"TOTAL_AMOUNT\" TEXT," + // 37: total_amount
                "\"NOTIF\" INTEGER);"); // 38: notif
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"PERJALANAN_DINAS\"";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, perjalanan_dinas entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        String code = entity.getCode();
        if (code != null) {
            stmt.bindString(2, code);
        }
 
        String employee_id = entity.getEmployee_id();
        if (employee_id != null) {
            stmt.bindString(3, employee_id);
        }
 
        String description = entity.getDescription();
        if (description != null) {
            stmt.bindString(4, description);
        }
 
        String remarks = entity.getRemarks();
        if (remarks != null) {
            stmt.bindString(5, remarks);
        }
 
        String no_surat_perintah = entity.getNo_surat_perintah();
        if (no_surat_perintah != null) {
            stmt.bindString(6, no_surat_perintah);
        }
 
        String date_departure = entity.getDate_departure();
        if (date_departure != null) {
            stmt.bindString(7, date_departure);
        }
 
        String date_arrival = entity.getDate_arrival();
        if (date_arrival != null) {
            stmt.bindString(8, date_arrival);
        }
 
        String day_duration = entity.getDay_duration();
        if (day_duration != null) {
            stmt.bindString(9, day_duration);
        }
 
        String type_code = entity.getType_code();
        if (type_code != null) {
            stmt.bindString(10, type_code);
        }
 
        String departure_place_id = entity.getDeparture_place_id();
        if (departure_place_id != null) {
            stmt.bindString(11, departure_place_id);
        }
 
        String arrival_place_id = entity.getArrival_place_id();
        if (arrival_place_id != null) {
            stmt.bindString(12, arrival_place_id);
        }
 
        String transportation_type = entity.getTransportation_type();
        if (transportation_type != null) {
            stmt.bindString(13, transportation_type);
        }
 
        String transportation_class = entity.getTransportation_class();
        if (transportation_class != null) {
            stmt.bindString(14, transportation_class);
        }
 
        String airplane_ticker_id = entity.getAirplane_ticker_id();
        if (airplane_ticker_id != null) {
            stmt.bindString(15, airplane_ticker_id);
        }
 
        String with_vehicle_rent = entity.getWith_vehicle_rent();
        if (with_vehicle_rent != null) {
            stmt.bindString(16, with_vehicle_rent);
        }
 
        String vehicle_rent_type = entity.getVehicle_rent_type();
        if (vehicle_rent_type != null) {
            stmt.bindString(17, vehicle_rent_type);
        }
 
        String vehicle_rent_price_id = entity.getVehicle_rent_price_id();
        if (vehicle_rent_price_id != null) {
            stmt.bindString(18, vehicle_rent_price_id);
        }
 
        String with_hotel = entity.getWith_hotel();
        if (with_hotel != null) {
            stmt.bindString(19, with_hotel);
        }
 
        String hotel_ticket_id = entity.getHotel_ticket_id();
        if (hotel_ticket_id != null) {
            stmt.bindString(20, hotel_ticket_id);
        }
 
        String hotel_amount_type = entity.getHotel_amount_type();
        if (hotel_amount_type != null) {
            stmt.bindString(21, hotel_amount_type);
        }
 
        String hotel_amount = entity.getHotel_amount();
        if (hotel_amount != null) {
            stmt.bindString(22, hotel_amount);
        }
 
        String transportation_amount_type = entity.getTransportation_amount_type();
        if (transportation_amount_type != null) {
            stmt.bindString(23, transportation_amount_type);
        }
 
        String transportation_amount = entity.getTransportation_amount();
        if (transportation_amount != null) {
            stmt.bindString(24, transportation_amount);
        }
 
        String with_taxi = entity.getWith_taxi();
        if (with_taxi != null) {
            stmt.bindString(25, with_taxi);
        }
 
        String taxi_qty_departure = entity.getTaxi_qty_departure();
        if (taxi_qty_departure != null) {
            stmt.bindString(26, taxi_qty_departure);
        }
 
        String taxi_qty_arrival = entity.getTaxi_qty_arrival();
        if (taxi_qty_arrival != null) {
            stmt.bindString(27, taxi_qty_arrival);
        }
 
        String taxi_ticket_departure = entity.getTaxi_ticket_departure();
        if (taxi_ticket_departure != null) {
            stmt.bindString(28, taxi_ticket_departure);
        }
 
        String taxi_ticket_arrival = entity.getTaxi_ticket_arrival();
        if (taxi_ticket_arrival != null) {
            stmt.bindString(29, taxi_ticket_arrival);
        }
 
        String taxi_amount_departure_type = entity.getTaxi_amount_departure_type();
        if (taxi_amount_departure_type != null) {
            stmt.bindString(30, taxi_amount_departure_type);
        }
 
        String taxi_amount_arrival_type = entity.getTaxi_amount_arrival_type();
        if (taxi_amount_arrival_type != null) {
            stmt.bindString(31, taxi_amount_arrival_type);
        }
 
        String taxi_amount_departure = entity.getTaxi_amount_departure();
        if (taxi_amount_departure != null) {
            stmt.bindString(32, taxi_amount_departure);
        }
 
        String taxi_amount_arrival = entity.getTaxi_amount_arrival();
        if (taxi_amount_arrival != null) {
            stmt.bindString(33, taxi_amount_arrival);
        }
 
        String taxi_total_amount = entity.getTaxi_total_amount();
        if (taxi_total_amount != null) {
            stmt.bindString(34, taxi_total_amount);
        }
 
        String vehicle_amount_type = entity.getVehicle_amount_type();
        if (vehicle_amount_type != null) {
            stmt.bindString(35, vehicle_amount_type);
        }
 
        String vehicle_amount = entity.getVehicle_amount();
        if (vehicle_amount != null) {
            stmt.bindString(36, vehicle_amount);
        }
 
        String type_amount = entity.getType_amount();
        if (type_amount != null) {
            stmt.bindString(37, type_amount);
        }
 
        String total_amount = entity.getTotal_amount();
        if (total_amount != null) {
            stmt.bindString(38, total_amount);
        }
 
        Boolean notif = entity.getNotif();
        if (notif != null) {
            stmt.bindLong(39, notif ? 1L: 0L);
        }
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public perjalanan_dinas readEntity(Cursor cursor, int offset) {
        perjalanan_dinas entity = new perjalanan_dinas( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), // code
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // employee_id
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // description
            cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4), // remarks
            cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5), // no_surat_perintah
            cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6), // date_departure
            cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7), // date_arrival
            cursor.isNull(offset + 8) ? null : cursor.getString(offset + 8), // day_duration
            cursor.isNull(offset + 9) ? null : cursor.getString(offset + 9), // type_code
            cursor.isNull(offset + 10) ? null : cursor.getString(offset + 10), // departure_place_id
            cursor.isNull(offset + 11) ? null : cursor.getString(offset + 11), // arrival_place_id
            cursor.isNull(offset + 12) ? null : cursor.getString(offset + 12), // transportation_type
            cursor.isNull(offset + 13) ? null : cursor.getString(offset + 13), // transportation_class
            cursor.isNull(offset + 14) ? null : cursor.getString(offset + 14), // airplane_ticker_id
            cursor.isNull(offset + 15) ? null : cursor.getString(offset + 15), // with_vehicle_rent
            cursor.isNull(offset + 16) ? null : cursor.getString(offset + 16), // vehicle_rent_type
            cursor.isNull(offset + 17) ? null : cursor.getString(offset + 17), // vehicle_rent_price_id
            cursor.isNull(offset + 18) ? null : cursor.getString(offset + 18), // with_hotel
            cursor.isNull(offset + 19) ? null : cursor.getString(offset + 19), // hotel_ticket_id
            cursor.isNull(offset + 20) ? null : cursor.getString(offset + 20), // hotel_amount_type
            cursor.isNull(offset + 21) ? null : cursor.getString(offset + 21), // hotel_amount
            cursor.isNull(offset + 22) ? null : cursor.getString(offset + 22), // transportation_amount_type
            cursor.isNull(offset + 23) ? null : cursor.getString(offset + 23), // transportation_amount
            cursor.isNull(offset + 24) ? null : cursor.getString(offset + 24), // with_taxi
            cursor.isNull(offset + 25) ? null : cursor.getString(offset + 25), // taxi_qty_departure
            cursor.isNull(offset + 26) ? null : cursor.getString(offset + 26), // taxi_qty_arrival
            cursor.isNull(offset + 27) ? null : cursor.getString(offset + 27), // taxi_ticket_departure
            cursor.isNull(offset + 28) ? null : cursor.getString(offset + 28), // taxi_ticket_arrival
            cursor.isNull(offset + 29) ? null : cursor.getString(offset + 29), // taxi_amount_departure_type
            cursor.isNull(offset + 30) ? null : cursor.getString(offset + 30), // taxi_amount_arrival_type
            cursor.isNull(offset + 31) ? null : cursor.getString(offset + 31), // taxi_amount_departure
            cursor.isNull(offset + 32) ? null : cursor.getString(offset + 32), // taxi_amount_arrival
            cursor.isNull(offset + 33) ? null : cursor.getString(offset + 33), // taxi_total_amount
            cursor.isNull(offset + 34) ? null : cursor.getString(offset + 34), // vehicle_amount_type
            cursor.isNull(offset + 35) ? null : cursor.getString(offset + 35), // vehicle_amount
            cursor.isNull(offset + 36) ? null : cursor.getString(offset + 36), // type_amount
            cursor.isNull(offset + 37) ? null : cursor.getString(offset + 37), // total_amount
            cursor.isNull(offset + 38) ? null : cursor.getShort(offset + 38) != 0 // notif
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, perjalanan_dinas entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setCode(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setEmployee_id(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setDescription(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setRemarks(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
        entity.setNo_surat_perintah(cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5));
        entity.setDate_departure(cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6));
        entity.setDate_arrival(cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7));
        entity.setDay_duration(cursor.isNull(offset + 8) ? null : cursor.getString(offset + 8));
        entity.setType_code(cursor.isNull(offset + 9) ? null : cursor.getString(offset + 9));
        entity.setDeparture_place_id(cursor.isNull(offset + 10) ? null : cursor.getString(offset + 10));
        entity.setArrival_place_id(cursor.isNull(offset + 11) ? null : cursor.getString(offset + 11));
        entity.setTransportation_type(cursor.isNull(offset + 12) ? null : cursor.getString(offset + 12));
        entity.setTransportation_class(cursor.isNull(offset + 13) ? null : cursor.getString(offset + 13));
        entity.setAirplane_ticker_id(cursor.isNull(offset + 14) ? null : cursor.getString(offset + 14));
        entity.setWith_vehicle_rent(cursor.isNull(offset + 15) ? null : cursor.getString(offset + 15));
        entity.setVehicle_rent_type(cursor.isNull(offset + 16) ? null : cursor.getString(offset + 16));
        entity.setVehicle_rent_price_id(cursor.isNull(offset + 17) ? null : cursor.getString(offset + 17));
        entity.setWith_hotel(cursor.isNull(offset + 18) ? null : cursor.getString(offset + 18));
        entity.setHotel_ticket_id(cursor.isNull(offset + 19) ? null : cursor.getString(offset + 19));
        entity.setHotel_amount_type(cursor.isNull(offset + 20) ? null : cursor.getString(offset + 20));
        entity.setHotel_amount(cursor.isNull(offset + 21) ? null : cursor.getString(offset + 21));
        entity.setTransportation_amount_type(cursor.isNull(offset + 22) ? null : cursor.getString(offset + 22));
        entity.setTransportation_amount(cursor.isNull(offset + 23) ? null : cursor.getString(offset + 23));
        entity.setWith_taxi(cursor.isNull(offset + 24) ? null : cursor.getString(offset + 24));
        entity.setTaxi_qty_departure(cursor.isNull(offset + 25) ? null : cursor.getString(offset + 25));
        entity.setTaxi_qty_arrival(cursor.isNull(offset + 26) ? null : cursor.getString(offset + 26));
        entity.setTaxi_ticket_departure(cursor.isNull(offset + 27) ? null : cursor.getString(offset + 27));
        entity.setTaxi_ticket_arrival(cursor.isNull(offset + 28) ? null : cursor.getString(offset + 28));
        entity.setTaxi_amount_departure_type(cursor.isNull(offset + 29) ? null : cursor.getString(offset + 29));
        entity.setTaxi_amount_arrival_type(cursor.isNull(offset + 30) ? null : cursor.getString(offset + 30));
        entity.setTaxi_amount_departure(cursor.isNull(offset + 31) ? null : cursor.getString(offset + 31));
        entity.setTaxi_amount_arrival(cursor.isNull(offset + 32) ? null : cursor.getString(offset + 32));
        entity.setTaxi_total_amount(cursor.isNull(offset + 33) ? null : cursor.getString(offset + 33));
        entity.setVehicle_amount_type(cursor.isNull(offset + 34) ? null : cursor.getString(offset + 34));
        entity.setVehicle_amount(cursor.isNull(offset + 35) ? null : cursor.getString(offset + 35));
        entity.setType_amount(cursor.isNull(offset + 36) ? null : cursor.getString(offset + 36));
        entity.setTotal_amount(cursor.isNull(offset + 37) ? null : cursor.getString(offset + 37));
        entity.setNotif(cursor.isNull(offset + 38) ? null : cursor.getShort(offset + 38) != 0);
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(perjalanan_dinas entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(perjalanan_dinas entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
}
