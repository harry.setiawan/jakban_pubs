package com.app.kemenhub.jakbanapps.retrofit.model_data.schedule;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ScheduleResponse {
	@SerializedName("code")
	String code;
	@SerializedName("message")
	String message;
	@SerializedName("schedule")
	Scheduler schedule;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Scheduler getSchedule() {
		return schedule;
	}
	public void setSchedule(Scheduler schedule) {
		this.schedule = schedule;
	}
	
}
