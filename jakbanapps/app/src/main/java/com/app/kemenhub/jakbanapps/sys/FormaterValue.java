package com.app.kemenhub.jakbanapps.sys;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.utils.ValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

public class FormaterValue implements ValueFormatter {
    @Override
    public String getFormattedValue(float value) {
        return Math.round(value)+"";
    }
}