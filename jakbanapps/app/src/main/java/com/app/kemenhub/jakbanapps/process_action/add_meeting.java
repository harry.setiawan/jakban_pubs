package com.app.kemenhub.jakbanapps.process_action;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.app.kemenhub.jakbanapps.alert.dialog_alert;
import com.app.kemenhub.jakbanapps.db.user_mobile;
import com.app.kemenhub.jakbanapps.fragment.add.f_add_meeting;
import com.app.kemenhub.jakbanapps.fragment.f_miting;
import com.app.kemenhub.jakbanapps.retrofit.Request_Inf;
import com.app.kemenhub.jakbanapps.retrofit.SDM;
import com.app.kemenhub.jakbanapps.retrofit.URLManager;
import com.app.kemenhub.jakbanapps.retrofit.model_data.file.FileAddResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.folder.FolderAddResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.mobile.Mobile;
import com.app.kemenhub.jakbanapps.retrofit.model_data.schedule.ScheduleAdd;
import com.app.kemenhub.jakbanapps.retrofit.model_data.schedule.SchedulesResponse;
import com.app.kemenhub.jakbanapps.sys.filechooser.FileUtils;
import com.app.kemenhub.jakbanapps.v_event;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class add_meeting {
    f_add_meeting fragment;
    SDM SDM;

    private SavingCallback savingCallback;

    public interface SavingCallback{
        void savefinishProcess(boolean data);
    }

    public void setsavingCallback(final SavingCallback savingCallback){
        this.savingCallback = savingCallback;
    }

    public add_meeting(f_add_meeting fragment){
        this.fragment = fragment;
        this.SDM = new SDM(fragment.getContext());
    }

    public void upload_file(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URLManager.URI_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Request_Inf client = retrofit.create(Request_Inf.class);
        File file = FileUtils.getFile(fragment.getActivity(), fragment.tmp_uri);
        if(file.exists()) {
            RequestBody files = RequestBody.create(MediaType.parse("image/jpg"), file);
            RequestBody folder_id = RequestBody.create(MediaType.parse("text/plain"), fragment.tmp_folder_id);
            RequestBody title = RequestBody.create(MediaType.parse("text/plain"), fragment.tmp_nm_file);
            RequestBody status = RequestBody.create(MediaType.parse("text/plain"), "1");


            Call<FileAddResponse> call = client.add_file(files, folder_id, title, status);
            Log.i("UPLOADER", "READY :" + URLManager.URI_API + "file");
            call.enqueue(new Callback<FileAddResponse>() {
                @Override
                public void onResponse(Call<FileAddResponse> call, Response<FileAddResponse> response) {
                    Log.i("UPLOADER", response.code()+"");
                }

                @Override
                public void onFailure(Call<FileAddResponse> call, Throwable t) {
                    Log.i("UPLOADER", t.getMessage().toString());
                }
            });
        }else{
            dialog_alert aler = new dialog_alert();
            aler.showDialog(fragment.getActivity(), "Dokumen tidak terdefinisi.");
        }
    }

    public void saving() {
        ScheduleAdd data = new ScheduleAdd();
        data.setCategory_id(fragment.tmp_disposisi);
        data.setFrom(fragment.edt_dari.getText().toString());
        data.setTitle(fragment.edt_perihal.getText().toString());
        data.setDatetime_schedule_start(fragment.tmp_tanggal_start + " " + fragment.tmp_waktu_start);
        data.setDatetime_schedule_end(fragment.tmp_tanggal_akhr + " " + fragment.tmp_waktu_akhir);
        data.setTgl_diterima(fragment.tmp_tanggal_diterima + " 00:00:00");

        data.setNo_surat_perintah(fragment.edt_no_surat.getText().toString());
        data.setNo_agenda(fragment.edt_no_agenda.getText().toString());
        data.setPlace(fragment.edt_tempat.getText().toString());
        data.setDesc("");
        data.setNotes(fragment.edt_catatan.getText().toString());
        data.setForward_to_user_id(fragment.tmp_forward);
        data.setCls(fragment.tmp_klasifikasi);
        data.setFolder_id("0");
        data.setFile_id("0");

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URLManager.URI_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<FolderAddResponse> call = client.add_Schedule(data);
        call.enqueue(new Callback<FolderAddResponse>() {
            @Override
            public void onResponse(Call<FolderAddResponse> call, Response<FolderAddResponse> response) {
                ////Log.i("SAVING", response.body().toString());
                request_ByDATE_schedule();
            }

            @Override
            public void onFailure(Call<FolderAddResponse> call, Throwable t) {
                ////Log.i("SAVING", t.getMessage());
                savingCallback.savefinishProcess(false);
            }
        });
        ////Log.i("SAVING", "RUNNING");
    }

    public void request_ByDATE_schedule(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URLManager.URI_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<SchedulesResponse> call = client.get_Schedule_date(fragment.Dyear, fragment.Dmonth);
        call.enqueue(new Callback<SchedulesResponse>() {
            @Override
            public void onResponse(Call<SchedulesResponse> call, Response<SchedulesResponse> response) {
                SDM.save_list_meeting(response.body().getSchedule());
                try{
                    ////Log.i("A", response.body().getSchedule().get(0).getId());
                    for(int i = 0 ; i < response.body().getSchedule().size() ; i++){
                        if(response.body().getSchedule().get(i).getDatetime_schedule_start().contains(fragment.tmp_tanggal_start + " " + fragment.tmp_waktu_start)){
                            request_data_mobile(response.body().getSchedule().get(i).getId());
                            break;
                        }
                    }
                }catch (Exception e){
                    ////Log.i("A", "Null");
                    savingCallback.savefinishProcess(true);
                }
            }

            @Override
            public void onFailure(Call<SchedulesResponse> call, Throwable t) {
                ////Log.i("A", t.getMessage());
                call.cancel();
            }
        });
    }

    public void request_data_mobile(final String id) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URLManager.URI_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<List<Mobile>> call = client.get_all_Mobile();
        call.enqueue(new Callback<List<Mobile>>() {
            @Override
            public void onResponse(Call<List<Mobile>> call, Response<List<Mobile>> response) {
                SDM.save_list_mobile(response.body());
                process_forward(id);
            }

            @Override
            public void onFailure(Call<List<Mobile>> call, Throwable t) {
                ////Log.i("A", t.getMessage());
                savingCallback.savefinishProcess(false);
            }
        });
    }

    private void process_forward(String id) {
        List<user_mobile> forward = new ArrayList<>();
        try {
            for(int i = 0 ; i < fragment.tmp_forward.size() ; i ++){
                if(v_event.DBManager.get_user_mobileById(fragment.tmp_forward.get(i)) != null) {
                    forward.add(forward.size(), v_event.DBManager.get_user_mobileById(fragment.tmp_forward.get(i)));
                }
            }
        }catch (Exception x){}

        process_push(id, forward);
    }

    private void process_push(String id, List<user_mobile> account) {
        if(account.size() > 0){
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(URLManager.URI_API)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            Request_Inf client = retrofit.create(Request_Inf.class);
            Call<FolderAddResponse> call = client.push_Mobile_notif(id, account);
            call.enqueue(new Callback<FolderAddResponse>() {
                @Override
                public void onResponse(Call<FolderAddResponse> call, Response<FolderAddResponse> response) {
                    savingCallback.savefinishProcess(true);
                }

                @Override
                public void onFailure(Call<FolderAddResponse> call, Throwable t) {
                    savingCallback.savefinishProcess(false);
                }
            });
        }
    }
}
