package com.app.kemenhub.jakbanapps.fragment;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.kemenhub.jakbanapps.R;
import com.app.kemenhub.jakbanapps.db.users;
import com.app.kemenhub.jakbanapps.v_main;


public class f_login extends Fragment implements View.OnClickListener{

    v_main ac;
    View v;

    EditText username, password;
    TextView version;
    Button login;
    CheckBox remember;
    ImageView eye;

    String transitionName = "";
    Bitmap imageBitmap = null;
    Bundle bundle;
    boolean eye_pass = false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.f_login, container, false);
        ac = (v_main) getActivity();
        bundle = getArguments();
        st_lay();

        return v;
    }

    private void st_lay() {
        username = (EditText) v.findViewById(R.id.txt_Username);
        password = (EditText) v.findViewById(R.id.txt_Password);
        login    = (Button) v.findViewById(R.id.btn_login);
        remember = (CheckBox) v.findViewById(R.id.remember);
        eye = (ImageView) v.findViewById(R.id.img_eye);
        version = (TextView) v.findViewById(R.id.version);

        version.setText("Version " + ac.myVersionName);
        if (bundle != null) {
            transitionName = bundle.getString(ac.TRANSITION_NAME);
            imageBitmap = bundle.getParcelable(ac.IMAGENAME);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            v.findViewById(R.id.logo).setTransitionName(transitionName);
        }
        ((ImageView) v.findViewById(R.id.logo)).setImageBitmap(imageBitmap);

        eye.setOnClickListener(this);
        login.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_eye:
                if(!eye_pass) {
                    eye.setImageResource(R.mipmap.ic_eye_on);
                    eye_pass = true;
                    password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                }else{
                    eye.setImageResource(R.mipmap.ic_eye_off);
                    eye_pass = false;
                    password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                break;
            case R.id.btn_login:
                ac.show_dialog();
                if(username.getText().toString().equals("") || password.getText().toString().equals("")){
                    ac.hide_dialog();
                    ac.call_alert("Username atau Password yang anda masukan salah!");
                }else{
                    ac.show_dialog();
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            users user = new users();
                            user.setUsername(username.getText().toString());
                            user.setPassword(password.getText().toString());
                            ac.rc.request_login(user);
                        }
                    }, 2000);
                }
                break;
        }
    }

    public void reset_lay(){
        username.setText("");
        password.setText("");
        username.requestFocus();
        remember.setChecked(false);
    }

}
