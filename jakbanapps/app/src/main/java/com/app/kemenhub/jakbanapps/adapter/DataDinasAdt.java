package com.app.kemenhub.jakbanapps.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.kemenhub.jakbanapps.R;
import com.app.kemenhub.jakbanapps.db.perjalanan_dinas;
import com.app.kemenhub.jakbanapps.db.place;
import com.app.kemenhub.jakbanapps.sys.utils;
import com.app.kemenhub.jakbanapps.v_event;

import java.util.List;


public class DataDinasAdt extends RecyclerView.Adapter<DataDinasAdt.Holder> {
    List<perjalanan_dinas> itemList;
    Context ctx;
    private LayoutInflater inflater;
    Context context;
    private SparseBooleanArray mSelectedItemsIds;
    private int previous = 0;
    private String TAG_LOAD = null;

    private itemClickCallback itemClickCallback;
    int Type;
    ViewGroup parent;

    public interface itemClickCallback{
        void onItemClick(int p);
    }

    public void setItemClickCallback(final  itemClickCallback itemClickCallback){
        this.itemClickCallback = itemClickCallback;
    }

    public DataDinasAdt(String tag, List<perjalanan_dinas> data, Context context){
        this.itemList = data;
        this.TAG_LOAD = tag;
        this.ctx = context;
        this.inflater = LayoutInflater.from(ctx);
        this.context = context;
        mSelectedItemsIds = new SparseBooleanArray();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        WindowManager windowManager = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);
        int width = windowManager.getDefaultDisplay().getWidth();
        view.setLayoutParams(new RecyclerView.LayoutParams(width, RecyclerView.LayoutParams.WRAP_CONTENT));

        Holder derpHolder = new Holder(view);

        return derpHolder;
    }

    public void runer(){
        onCreateViewHolder(parent, Type);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        switch(TAG_LOAD){
            case "DINAS":
                holder.item_meeting.setVisibility(View.GONE);
                holder.item_disposisi.setVisibility(View.GONE);
                holder.card_meeting.setVisibility(View.GONE);
                holder.card_disposisi.setVisibility(View.GONE);
                holder.card_dinas.setVisibility(View.VISIBLE);
                holder.item_dinas.setVisibility(View.VISIBLE);

                if(itemList.get(position).getNo_surat_perintah().length() > 20){
                    holder.dinas_code.setText("Code : " + itemList.get(position).getCode().substring(0,17) + "..");
                }else {
                    holder.dinas_code.setText("Code : " + itemList.get(position).getCode());
                }
                if(itemList.get(position).getNo_surat_perintah().length() > 20){
                    holder.dinas_agenda.setText("No. Surat : " + itemList.get(position).getNo_surat_perintah().substring(0,17) + "..");
                }else {
                    holder.dinas_agenda.setText("No. Surat : " + itemList.get(position).getNo_surat_perintah());
                }
                if(itemList.get(position).getDescription().length() > 25){
                    holder.dinas_desc.setText("Deskripsi : " + itemList.get(position).getDescription().replaceAll("\n", ", ").substring(0,25) + "..");
                }else {
                    holder.dinas_desc.setText("Deskripsi : " + itemList.get(position).getDescription().replaceAll("\n", ", "));
                }
                place place_asal = v_event.DBManager.get_placeById(itemList.get(position).getDeparture_place_id());
                place place_tujuan = v_event.DBManager.get_placeById(itemList.get(position).getArrival_place_id());
                holder.dinas_kota.setText("Kota : " + place_asal.getName() + " - " + place_tujuan.getName());
                holder.dinas_biaya.setText("Total Biaya : " + utils.set_rupiah(utils.set_digit_rupiah(itemList.get(position).getTotal_amount())));
                int durasi = (Integer.parseInt(itemList.get(position).getDate_arrival().substring(8,10)) - Integer.parseInt(itemList.get(position).getDate_departure().substring(8,10)));
                if(itemList.get(position).getType_code().toUpperCase().contains("LUAR")){
                    holder.dinas_star.setVisibility(View.VISIBLE);
                }else{
                    holder.dinas_star.setVisibility(View.GONE);
                }
                holder.dinas_durasi.setText(durasi + "");

                break;
            case "MEETING":
                holder.card_meeting.setVisibility(View.VISIBLE);
                holder.card_disposisi.setVisibility(View.GONE);
                holder.card_dinas.setVisibility(View.GONE);
                holder.item_meeting.setVisibility(View.VISIBLE);
                holder.item_disposisi.setVisibility(View.GONE);
                holder.item_dinas.setVisibility(View.GONE);
                break;
            case "DISPOSISI":
                holder.card_meeting.setVisibility(View.GONE);
                holder.card_disposisi.setVisibility(View.VISIBLE);
                holder.card_dinas.setVisibility(View.GONE);
                holder.item_meeting.setVisibility(View.GONE);
                holder.item_disposisi.setVisibility(View.VISIBLE);
                holder.item_disposisi.setClickable(true);
                holder.item_dinas.setVisibility(View.GONE);
                break;
            default:
                holder.item_meeting.setVisibility(View.GONE);
                holder.item_disposisi.setVisibility(View.GONE);
                holder.item_dinas.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));
    }

    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }

    public void selectView(int position, boolean value) {
        if (value) {
            mSelectedItemsIds.put(position, value);
        }else {
            mSelectedItemsIds.delete(position);
        }
        notifyDataSetChanged();
    }


    class Holder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{

        public LinearLayout item;
        public LinearLayout item_dinas;
        public LinearLayout item_meeting;
        public LinearLayout item_disposisi;
        public TextView dinas_code, dinas_agenda, dinas_durasi, dinas_desc, dinas_kota, dinas_biaya;
        public ImageView dinas_star, meeting_star, disposisi_star;
        public TextView meeting_judul, meeting_tempat, meeting_durasi, meeting_category, meeting_desc, meeting_dari;
        public TextView disposisi_surat, disposisi_agenda, disposisi_category, disposisi_desc, disposisi_dari;
        public CardView card_meeting, card_dinas, card_disposisi;

        public Holder(View itemView) {
            super(itemView);
            card_dinas = (CardView) itemView.findViewById(R.id.dinas_line_card);
            card_meeting = (CardView) itemView.findViewById(R.id.meeting_line_card);
            card_disposisi = (CardView) itemView.findViewById(R.id.disposisi_line_card);

            item_dinas = (LinearLayout) itemView.findViewById(R.id.dinas_item);
            dinas_code = (TextView) itemView.findViewById(R.id.dinas_code);
            dinas_agenda = (TextView) itemView.findViewById(R.id.dinas_surat);
            dinas_durasi = (TextView) itemView.findViewById(R.id.dinas_durasi);
            dinas_desc = (TextView) itemView.findViewById(R.id.dinas_deskripsi);
            dinas_kota = (TextView) itemView.findViewById(R.id.dinas_kota);
            dinas_biaya = (TextView) itemView.findViewById(R.id.dinas_biaya);
            dinas_star = (ImageView) itemView.findViewById(R.id.dinas_star);
            meeting_star = (ImageView) itemView.findViewById(R.id.meeting_star);
            disposisi_star = (ImageView) itemView.findViewById(R.id.disposisi_star);

            item_meeting = (LinearLayout) itemView.findViewById(R.id.meeting_item);
            meeting_judul = (TextView) itemView.findViewById(R.id.meeting_judul);
            meeting_tempat = (TextView) itemView.findViewById(R.id.meeting_tempat);
            meeting_durasi = (TextView) itemView.findViewById(R.id.meeting_durasi);
            meeting_category = (TextView) itemView.findViewById(R.id.meeting_category);
            meeting_desc = (TextView) itemView.findViewById(R.id.meeting_deskripsi);
            meeting_dari = (TextView) itemView.findViewById(R.id.meeting_dari);

            item_disposisi = (LinearLayout) itemView.findViewById(R.id.disposisi_item);
            disposisi_surat = (TextView) itemView.findViewById(R.id.disposisi_code);
            disposisi_agenda = (TextView) itemView.findViewById(R.id.disposisi_agenda);
            disposisi_category = (TextView) itemView.findViewById(R.id.disposisi_kategori);
            disposisi_desc = (TextView) itemView.findViewById(R.id.disposisi_deskripsi);
            disposisi_dari = (TextView) itemView.findViewById(R.id.disposisi_dari);
            item = (LinearLayout) itemView.findViewById(R.id.item);
            item.setClickable(true);

            item.setOnClickListener(this);
            item_dinas.setOnClickListener(this);
            item_meeting.setOnClickListener(this);
            item_disposisi.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.item:
                    itemClickCallback.onItemClick(getAdapterPosition());
                    break;
                case R.id.dinas_item:
                    itemClickCallback.onItemClick(getAdapterPosition());
                    break;
                case R.id.disposisi_item:
                    itemClickCallback.onItemClick(getAdapterPosition());
                    break;
            }
        }

        @Override
        public boolean onLongClick(View v) {
            switch (v.getId()){
            }
            return false;
        }
    }
}