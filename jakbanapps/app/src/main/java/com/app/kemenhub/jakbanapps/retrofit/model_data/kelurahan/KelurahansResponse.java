package com.app.kemenhub.jakbanapps.retrofit.model_data.kelurahan;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class KelurahansResponse {
	@SerializedName("code")
	String code;
	@SerializedName("message")
	String message;
	@SerializedName("kelurahan")
	List<Kelurahan> kelurahan;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<Kelurahan> getKelurahan() {
		return kelurahan;
	}
	public void setKelurahan(List<Kelurahan> kelurahan) {
		this.kelurahan = kelurahan;
	}
}
