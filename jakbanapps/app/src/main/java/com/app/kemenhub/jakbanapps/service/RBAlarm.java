package com.app.kemenhub.jakbanapps.service;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.app.kemenhub.jakbanapps.db.manager.DManager;
import com.app.kemenhub.jakbanapps.db.manager.IDManager;
import com.app.kemenhub.jakbanapps.db.schedule;
import com.app.kemenhub.jakbanapps.retrofit.Request_Inf;
import com.app.kemenhub.jakbanapps.retrofit.SDM;
import com.app.kemenhub.jakbanapps.retrofit.URLManager;
import com.app.kemenhub.jakbanapps.retrofit.model_data.schedule.SchedulesResponse;
import com.app.kemenhub.jakbanapps.sys.ExceptionHandler;
import com.app.kemenhub.jakbanapps.sys.SessionManager;
import com.app.kemenhub.jakbanapps.sys.utils;
import com.app.kemenhub.jakbanapps.v_alarm;
import com.app.kemenhub.jakbanapps.v_event;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class RBAlarm extends BroadcastReceiver {
    public static String waktu_offline = "";
    public static String connTag = "";
    public SessionManager SManager;
    public Retrofit retrofit;
    public IDManager DBManager;
    SDM SDM;
    Context ctx;
    public static boolean alarmer = false;
    boolean GO = false;
    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            DBManager = new DManager(context);
            this.ctx = context;
            this.SManager = new SessionManager();
            this.SDM = new SDM(context);

            //////Log.i("DATA ALARM", "START");
            if(!SManager.getPreferences(ctx, "alarm").toString().equals(null)) {
                if (!SManager.getPreferences(ctx, "status").toString().equals("")) {
                    retro();
                    resuest();
                    request_BycreateTO__schedule(SManager.getPreferences(ctx, "status").toString());
                }
            }else{
                SManager.setPreferences(ctx, "alarm", "");
            }
        }catch (Exception e){}
    }

    private void retro()  throws Exception{
        this.retrofit = new Retrofit.Builder()
                .baseUrl(URLManager.URI_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
    private void resuest() {
        SimpleDateFormat sdfdate_sel = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);
        Date tgll = cal.getTime();
        String date_selected = sdfdate_sel.format(tgll);

        List<schedule> data_meeting =  DBManager.list_scheduleByD(SManager.getPreferences(ctx, "status"), date_selected);
        if(data_meeting.size() == 0 || data_meeting == null){
            Log.i("DATA ALARM", "NOTHING" + " : " + SManager.getPreferences(ctx, "status") + " : " + date_selected + " : " + data_meeting.size());
        }else{
            Log.i("DATA ALARM", "IN " + data_meeting.size());
            SimpleDateFormat sdfdates = new SimpleDateFormat("HH:mm:ss");
            Calendar cals = Calendar.getInstance();
            cal.add(Calendar.DATE, 0);
            Date tglls = cals.getTime();
            String waktunya = sdfdates.format(tglls);
            String[] waktu_sekarang = waktunya.split(":");
            Date data_date = null;
            Date data_tag = null;

            for (int i = 0; i < data_meeting.size(); i++) {
                String[] tanggalnya = data_meeting.get(i).getDatetime_schedule_start().split(" ");
                String[] waktu_meeting = tanggalnya[1].split(":");

                Log.i("DATA ALARM", "IN i : "  + i + " - " + data_meeting.get(i).getId());
                int nilai_waktu_data = (Integer.parseInt(waktu_meeting[0]) * 60) + (Integer.parseInt(waktu_meeting[1]));
                int nilai_waktu_skrg = (Integer.parseInt(waktu_sekarang[0]) * 60) + (Integer.parseInt(waktu_sekarang[1]));
                if ((nilai_waktu_data - nilai_waktu_skrg) <= 60) {
                    Log.i("DATA ALARM", "IN i : "+ (nilai_waktu_data - nilai_waktu_skrg) + " <= 60 ");

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    try {
                        data_date = sdf.parse(String.valueOf(data_meeting.get(i).getDatetime_schedule_start()));

                        cal = Calendar.getInstance();
                        cal.add(Calendar.DATE, 0);
                        tgll = cal.getTime();
                        if (SManager.getPreferences(ctx, "alarm_time").equals("") || SManager.getPreferences(ctx, "alarm_time") == null) {
                            SManager.setPreferences(ctx, "alarm_time", sdf.format(tgll));
                        }
                        data_tag = sdf.parse(SManager.getPreferences(ctx, "alarm_time"));

                    } catch (ParseException e) {
                        ////Log.e("DATEE", e.getMessage());
                    }
                    Log.i("ALARM READY", data_date.toString() + " - " + data_tag.toString());
                    if (data_date != null && data_tag != null) {
                        Log.i("ALARM READY", "1");
                        if (!v_alarm.loader && data_date.after(data_tag)) {
                            Log.i("ALARM READY", "2");
                            if (SManager.getPreferences(ctx, "alarm_time").equals(String.valueOf(data_meeting.get(i).getDatetime_schedule_start()))) {
                                GO = false;
                                Log.i("ALARM READY", "3");
                            } else {
                                Log.i("ALARM READY", "4");
                                GO = true;
                                SManager.setPreferences(ctx, "alarm", String.valueOf(data_meeting.get(i).getId()));
                                SManager.setPreferences(ctx, "alarm_time", String.valueOf(data_meeting.get(i).getDatetime_schedule_start()));
                                alarmer = true;
                                ////Log.i("DATA ALARM", "" + " SHOW DIALOG ALARM");
                                //////Log.i("DATA ALARM", "" + " SHOW DIALOG OPEN");
                                break;

                            }
                        } else {
                            Log.i("ALARM NOT READY", "5");
                            GO = false;
                        }
                    }
                } else {
                    GO = false;
                    //////Log.i("DATA ALARM", "IN i : "+ (nilai_waktu_data - nilai_waktu_skrg) + " < 30 ");
                }
            }
            if (GO) {
                go_to_action(v_event.class, SManager.getPreferences(ctx, "alarm"));
            }
        }
    }

    public void request_BycreateTO__schedule(String to)  throws Exception{
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<SchedulesResponse> call = client.get_Schedule_createTo(waktu_offline, to);
        call.enqueue(new Callback<SchedulesResponse>() {
            @Override
            public void onResponse(Call<SchedulesResponse> call, Response<SchedulesResponse> response) {
                if(response.body() != null) {
                    SDM.save_list_meeting(response.body().getSchedule());
                }
            }

            @Override
            public void onFailure(Call<SchedulesResponse> call, Throwable t) {
                call.cancel();
            }
        });
    }

    public void go_to_action(final Class classes, String id) {
        GO = false;
        Intent intent = new Intent(ctx.getApplicationContext(), classes);
        intent.putExtra("ALARM", String.valueOf(id));
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        ctx.startActivity(intent);
    }
}