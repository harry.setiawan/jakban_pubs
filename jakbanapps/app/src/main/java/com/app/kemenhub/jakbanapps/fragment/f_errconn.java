package com.app.kemenhub.jakbanapps.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.app.kemenhub.jakbanapps.R;
import com.app.kemenhub.jakbanapps.v_main;


public class f_errconn extends Fragment implements OnClickListener {

    v_main ac;
    View v;
    Button try_again;
    TextView isi;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.f_error_conn, container, false);
        ac = (v_main) getActivity();

        set_layout();
        //set_font();

        return v;
    }

    private void set_layout() {
        isi = (TextView) v.findViewById(R.id.isi);
        try_again = (Button) v.findViewById(R.id.btn_try);

        try_again.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_try:
                ac.call_splash();
                ac.set_show();
                break;
        }
    }
}
