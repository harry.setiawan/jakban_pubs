package com.app.kemenhub.jakbanapps.service;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.app.kemenhub.jakbanapps.retrofit.Request_Inf;
import com.app.kemenhub.jakbanapps.retrofit.SDM;
import com.app.kemenhub.jakbanapps.retrofit.URLManager;
import com.app.kemenhub.jakbanapps.retrofit.model_data.dinas.DinassResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.disposisi.DisposissResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.schedule.SchedulesResponse;
import com.app.kemenhub.jakbanapps.sys.SessionManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class RBData extends BroadcastReceiver {
    Context ctx;
    public static String waktu_offline = "";
    public static String connTag = "";
    public SessionManager SManager;
    public Retrofit retrofit;
    SDM SDM;
    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            this.ctx = context;
            this.SManager = new SessionManager();
            this.SDM = new SDM(context);
            retro();
            service_req();
        }catch (Exception e){}
    }

    private void service_req() throws Exception{
        Log.i("ALARM RESCHE", "RECHE ");
        ConnectivityManager cm =(ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&  activeNetwork.isConnectedOrConnecting();
        if(isConnected) {
            if(this.SManager.getPreferences(this.ctx, "ServiceLoad").equals("run")){
                SManager.setPreferences(this.ctx, "ServiceLoad", "notrun");
                request_BycreateTO_disposisi(SManager.getPreferences(this.ctx, "status"));
                request_BycreateTO__dinas(SManager.getPreferences(this.ctx, "status"));
                request_BycreateTO__schedule(SManager.getPreferences(this.ctx, "status"));
            }
        }else {
            ConnectivityManager connManager = (ConnectivityManager) ctx.getSystemService(ctx.CONNECTIVITY_SERVICE);
            NetworkInfo wifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobile = connManager .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if (wifi.isConnected()){
                SManager.setPreferences(this.ctx, "ServiceLoad", "notrun");
                request_BycreateTO_disposisi(SManager.getPreferences(this.ctx, "status"));
                request_BycreateTO__dinas(SManager.getPreferences(this.ctx, "status"));
                request_BycreateTO__schedule(SManager.getPreferences(this.ctx, "status"));
            }else{
                SManager.setPreferences(this.ctx, "ServiceLoad", "run");
                SimpleDateFormat sdfdate_sel = new SimpleDateFormat("yyyy-MM-dd hh:mm");
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, 0);
                Date tgll = cal.getTime();
                waktu_offline = sdfdate_sel.format(tgll);
            }

            if (mobile.isConnected()) {
                SManager.setPreferences(this.ctx, "ServiceLoad", "notrun");
                request_BycreateTO_disposisi(SManager.getPreferences(this.ctx, "status"));
                request_BycreateTO__dinas(SManager.getPreferences(this.ctx, "status"));
                request_BycreateTO__schedule(SManager.getPreferences(this.ctx, "status"));
            }else{
                SManager.setPreferences(this.ctx, "ServiceLoad", "run");
                SimpleDateFormat sdfdate_sel = new SimpleDateFormat("yyyy-MM-dd hh:mm");
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, 0);
                Date tgll = cal.getTime();
                waktu_offline = sdfdate_sel.format(tgll);
            }
        }
    }

    private void retro()  throws Exception{
        this.retrofit = new Retrofit.Builder()
                .baseUrl(URLManager.URI_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
    public void request_BycreateTO_disposisi(String to)  throws Exception{
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<DisposissResponse> call = client.get_disposisi_createto(waktu_offline, to);
        call.enqueue(new Callback<DisposissResponse>() {
            @Override
            public void onResponse(Call<DisposissResponse> call, Response<DisposissResponse> response) {
                if(response.body() != null) {
                    SDM.save_list_disposisi(response.body().getDisposisi());
                }
            }

            @Override
            public void onFailure(Call<DisposissResponse> call, Throwable t) {
                call.cancel();
            }
        });
    }
    public void request_BycreateTO__dinas(String to)  throws Exception{
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<DinassResponse> call = client.get_new_dinas_created(waktu_offline);
        call.enqueue(new Callback<DinassResponse>() {
            @Override
            public void onResponse(Call<DinassResponse> call, Response<DinassResponse> response) {
                if(response.body() != null) {
                    SDM.save_list_dinas(response.body().getDinas());
                }
            }

            @Override
            public void onFailure(Call<DinassResponse> call, Throwable t) {
                call.cancel();
            }
        });
    }
    public void request_BycreateTO__schedule(String to)  throws Exception{
        Log.i("ALARM RESCHE", "RECHE " + "RUN");
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<SchedulesResponse> call = client.get_Schedule_createTo(waktu_offline, to);
        call.enqueue(new Callback<SchedulesResponse>() {
            @Override
            public void onResponse(Call<SchedulesResponse> call, Response<SchedulesResponse> response) {
                if(response.body() != null) {
                    Log.i("ALARM RESCHE", "RECHE " + response.body().getSchedule().size());
                    SDM.save_list_meeting(response.body().getSchedule());
                }
            }

            @Override
            public void onFailure(Call<SchedulesResponse> call, Throwable t) {
                call.cancel();
            }
        });
    }
}