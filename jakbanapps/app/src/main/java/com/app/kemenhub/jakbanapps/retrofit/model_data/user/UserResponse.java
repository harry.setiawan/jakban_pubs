package com.app.kemenhub.jakbanapps.retrofit.model_data.user;

import com.app.kemenhub.jakbanapps.retrofit.model_data.user.User;
import com.google.gson.annotations.SerializedName;

public class UserResponse {
    @SerializedName("code")
    String code;
    @SerializedName("message")
    String message;
    @SerializedName("user")
    User users;
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public User getUser() {
        return users;
    }
    public void setUser(User user) {
        this.users = user;
    }
}
