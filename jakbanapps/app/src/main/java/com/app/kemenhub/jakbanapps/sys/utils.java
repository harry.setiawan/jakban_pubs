package com.app.kemenhub.jakbanapps.sys;

import java.text.DecimalFormat;

/**
 * Created by Axa.Laranza on 7/4/2017.
 */

public class utils {
    public static String TAG_LAYOUT = "";
    public static String TAG_FILE = "";

    public static String set_digit_rupiah(String hasil){
        float jumlah = Float.parseFloat(String.valueOf(hasil));
        DecimalFormat angka = new DecimalFormat("###,###");

        String ret = angka.format(jumlah).toString().replaceAll(",",".");

        return ret;
    }

    public static String set_rupiah(String hasil){
        String ret = "Rp. " + hasil +",-";
        return ret;
    }

    public static void set_tag_dashboard(){
        TAG_LAYOUT = "f_dash";
    }
    public static void set_tag_disposisi(){
        TAG_LAYOUT = "f_disposisi";
    }
    public static void set_tag_meeting(){
        TAG_LAYOUT = "f_meeting";
    }
    public static void set_tag_dinas(){
        TAG_LAYOUT = "f_dinas";
    }
}
