package com.app.kemenhub.jakbanapps.retrofit.model_data.place;

public class Place {
	private String id;
	private String provinsi_id;
	private String name;
	private String type;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProvinsi_id() {
		return provinsi_id;
	}

	public void setProvinsi_id(String provinsi_id) {
		this.provinsi_id = provinsi_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}


}
