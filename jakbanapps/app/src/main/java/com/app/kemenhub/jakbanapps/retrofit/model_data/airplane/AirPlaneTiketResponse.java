package com.app.kemenhub.jakbanapps.retrofit.model_data.airplane;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AirPlaneTiketResponse {
	@SerializedName("code")
	String code;
	@SerializedName("message")
	String message;
	@SerializedName("airplane_ticket")
	AirPlaneTiket airplane_ticket;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public AirPlaneTiket getAirPlaneTiket() {
		return airplane_ticket;
	}
	public void setAirPlaneTiket(AirPlaneTiket airplane_ticket) {
		this.airplane_ticket = airplane_ticket;
	}
	
	
}
