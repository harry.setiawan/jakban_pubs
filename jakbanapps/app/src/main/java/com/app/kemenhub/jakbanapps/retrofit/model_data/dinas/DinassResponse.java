package com.app.kemenhub.jakbanapps.retrofit.model_data.dinas;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DinassResponse {
	@SerializedName("code")
	String code;
	@SerializedName("message")
	String message;
	@SerializedName("dinas")
	List<Dinaser> dinas;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<Dinaser> getDinas() {
		return dinas;
	}
	public void setDinas(List<Dinaser> dinas) {
		this.dinas = dinas;
	}
	
}
