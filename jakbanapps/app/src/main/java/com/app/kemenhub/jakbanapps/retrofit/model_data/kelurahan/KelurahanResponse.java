package com.app.kemenhub.jakbanapps.retrofit.model_data.kelurahan;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class KelurahanResponse {
	@SerializedName("code")
	String code;
	@SerializedName("message")
	String message;
	@SerializedName("kelurahan")
	Kelurahan kelurahan;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Kelurahan getKelurahan() {
		return kelurahan;
	}
	public void setKelurahan(Kelurahan kelurahan) {
		this.kelurahan = kelurahan;
	}
	
}
