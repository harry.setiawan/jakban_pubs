package com.app.kemenhub.jakbanapps.retrofit.model_data.user;

import com.app.kemenhub.jakbanapps.retrofit.model_data.user.User;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UsersResponse {
    @SerializedName("code")
    String code;
    @SerializedName("message")
    String message;
    @SerializedName("user")
    List<User> user;
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public List<User> getUser() {
        return user;
    }
    public void setUser(List<User> user) {
        this.user = user;
    }
}
