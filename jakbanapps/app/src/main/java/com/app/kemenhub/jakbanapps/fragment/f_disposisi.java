package com.app.kemenhub.jakbanapps.fragment;

import android.app.ActivityOptions;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Explode;
import android.transition.Slide;
import android.transition.Transition;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.app.kemenhub.jakbanapps.R;
import com.app.kemenhub.jakbanapps.adapter.DataDisposisiAdt;
import com.app.kemenhub.jakbanapps.db.disposition;
import com.app.kemenhub.jakbanapps.fragment.detil.f_detil_disposisi;
import com.app.kemenhub.jakbanapps.sys.utils;
import com.app.kemenhub.jakbanapps.v_detil;
import com.app.kemenhub.jakbanapps.v_event;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class f_disposisi extends Fragment implements DataDisposisiAdt.itemClickCallback{
    View v;
    v_event activity;
    private SimpleDateFormat dateFormat;
    TextView nothing;
    SwipeRefreshLayout refresh;
    RecyclerView recyclerview;
    String date_selected = "";
    List<disposition> disposisi = new ArrayList<>();
    DataDisposisiAdt adapter;
    TextView edt_klasifikasi;
    CardView line;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.f_disposisi, container, false);
        activity = (v_event) getActivity();
        _init();
        _imp_data();

        return v;
    }

    private void set_adapter() {
        disposisi.clear();
        disposisi.addAll(activity.DBManager.list_dispositionByTO(activity.SManager.getPreferences(activity, "status")));
        if(disposisi.size() > 0 || disposisi != null){
            nothing.setVisibility(View.GONE);
            recyclerview.setVisibility(View.VISIBLE);
        }else{
            recyclerview.setVisibility(View.GONE);
            nothing.setVisibility(View.VISIBLE);
        }
        adapter.notifyDataSetChanged();
    }
    private void set_adapter_cari(String item) {
        disposisi.clear();
        disposisi.addAll(activity.DBManager.list_dispositionByTOdanClass(activity.SManager.getPreferences(activity, "status"), item));
        if(disposisi.size() != 0){
            nothing.setVisibility(View.GONE);
        }else{
            nothing.setVisibility(View.VISIBLE);
        }
        adapter.notifyDataSetChanged();
    }
    public void go_to_action(final Class classes, long id) {
        utils.TAG_LAYOUT = "f_disposisi";
        if (Build.VERSION.SDK_INT >= 21) {
            Transition exitTrans = new Explode();
            activity.getWindow().setExitTransition(exitTrans);

            Transition reenterTrans = new Slide();
            activity.getWindow().setEnterTransition(reenterTrans);
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(activity);
            Intent intent = new Intent(activity, classes);
            intent.putExtra("DISPOSISI", String.valueOf(id));
            this.startActivity(intent, options.toBundle());
        }else{
            Intent intent = new Intent(activity, classes);
            intent.putExtra("DISPOSISI", String.valueOf(id));
            this.startActivity(intent);
        }
    }
    private void _imp_data() {
        activity.show_sync();
        activity.rc.request_ByTO_disposisi(activity.SManager.getPreferences(activity, "status"));
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                activity.hide_sync();
                set_adapter();
            }
        },2000);
    }
    private void _init() {
        dateFormat = new SimpleDateFormat("MMMM - yyyy", Locale.getDefault());
        nothing = (TextView) v.findViewById(R.id.nothing);
        refresh = (SwipeRefreshLayout) v.findViewById(R.id.swipe_dash);
        edt_klasifikasi = (TextView) v.findViewById(R.id.edt_klasifikasi);
        edt_klasifikasi.setClickable(true);
        edt_klasifikasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showdialogcari();
            }
        });
        recyclerview = (RecyclerView) v.findViewById(R.id.my_recycler_message);
        line = (CardView) v.findViewById(R.id.line_card);
        if (Build.VERSION.SDK_INT < 21) {
            line.setVisibility(View.GONE);
        }
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(activity);
        recyclerview.setLayoutManager(layoutManager);
        adapter = new DataDisposisiAdt("DISPOSISI", disposisi, activity);
        recyclerview.setAdapter(adapter);
        adapter.setItemClickCallback(this);

        refresh.setColorSchemeColors(getResources().getColor(R.color.md_light_green_500_25));
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                activity.DBManager.delete_disposition();
                refresh.setRefreshing(true);
                activity.rc.request_ByTO_disposisi(activity.SManager.getPreferences(activity, "status"));
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        refresh.setRefreshing(false);
                        edt_klasifikasi.setText(null);
                        activity.rc.request_ByTO_disposisi(activity.SManager.getPreferences(activity, "status"));
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                activity.hide_sync();
                                set_adapter();
                            }
                        },2000);
                    }
                },2000);
            }
        });
    }
    private void showdialogcari() {
        final List<String> mnt = new ArrayList<String>();
        mnt.add("Semua");
        mnt.add("Biasa");
        mnt.add("Segera");
        mnt.add("Sangat Segera");
        mnt.add("Rahasia");
        final CharSequence[] mnts = mnt.toArray(new String[mnt.size()]);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        dialogBuilder.setTitle("Pilih Klasifikasi");
        dialogBuilder.setItems(mnts, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, final int item) {
                refresh.setRefreshing(true);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        refresh.setRefreshing(false);
                        String dt = "";
                        if(item == 0){
                            dt = "";
                            edt_klasifikasi.setText(mnt.get(item) + " Klasifikasi");
                        }else{
                            dt = String.valueOf( (item) );
                            edt_klasifikasi.setText("Klasifikasi " + mnt.get(item));
                        }
                        set_adapter_cari(dt);
                    }
                },1000);
            }
        });
        AlertDialog alertDialogObject = dialogBuilder.create();
        alertDialogObject.show();
    }
    @Override
    public void onItemClick(int p) {
        go_to_action(v_detil.class, disposisi.get(p).getId());
    }
}
