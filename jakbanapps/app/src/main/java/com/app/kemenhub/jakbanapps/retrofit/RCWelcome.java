package com.app.kemenhub.jakbanapps.retrofit;

import android.util.Log;

import com.app.kemenhub.jakbanapps.db.users;
import com.app.kemenhub.jakbanapps.retrofit.model_data.airplane.AirPlaneTiketResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.airplane.AirPlaneTiketsResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.category.CategoryResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.category.CategorysResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.cls.Cls;
import com.app.kemenhub.jakbanapps.retrofit.model_data.dinas.DinasResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.dinas.DinassResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.disposisi.DisposisiResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.disposisi.DisposissResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.file.FileResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.file.FilesResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.folder.FolderResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.folder.FoldersResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.hotel_fee.HotelFeeResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.hotel_fee.HotelFeesResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.jenis_dinas.JenisResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.jenis_dinas.JenissResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.kabupaten.KabupatenResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.kabupaten.KabupatensResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.kecamatan.KecamatanResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.kecamatan.KecamatansResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.kelurahan.KelurahanResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.kelurahan.KelurahansResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.mobile.Mobile;
import com.app.kemenhub.jakbanapps.retrofit.model_data.mobile.MobileResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.place.PlaceResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.place.PlacesResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.position.PositionResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.position.PositionsResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.provinsi.ProvinsiResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.provinsi.ProvinsisResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.role.Role;
import com.app.kemenhub.jakbanapps.retrofit.model_data.schedule.ScheduleResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.schedule.SchedulesResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.taxi_fee.TaxyFeeResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.taxi_fee.TaxyFeesResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.user.UserResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.user.UsersResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.vehicle.VehiclesResponse;
import com.app.kemenhub.jakbanapps.v_event;
import com.app.kemenhub.jakbanapps.v_main;
import com.app.kemenhub.jakbanapps.v_welcome;
import com.google.firebase.iid.FirebaseInstanceId;

import java.net.URL;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Axa.Laranza on 6/28/2017.
 */

public class RCWelcome {

    public RCWelcome(v_welcome ac){
        this.ac = ac;
        retro();
        SDM = new SDM(ac.getApplicationContext());
    }


    v_welcome ac;
    public Retrofit retrofit;
    SDM SDM;

    private void retro() {
        this.retrofit = new Retrofit.Builder()
                .baseUrl(URLManager.URI_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    //REQUEST ALL
    public void request_all_airplane(){
        Request_Inf request = retrofit.create(Request_Inf.class);
        Call<AirPlaneTiketsResponse> call = request.get_all_airplaneticket();
        call.enqueue(new Callback<AirPlaneTiketsResponse>() {
            @Override
            public void onResponse(Call<AirPlaneTiketsResponse> call, Response<AirPlaneTiketsResponse> response) {
                //////Log.i("SERVER AIRPLANE", response.body().getMessage());
                if(response.body() != null) {
                    SDM.save_list_airplane_ticket(response.body().getAirPlaneTiket());
                }
                ac.TAG_REQUEST[0] = true;
            }

            @Override
            public void onFailure(Call<AirPlaneTiketsResponse> call, Throwable t) {
                //////Log.i("SERVER AIRPLANE", t.getMessage());
                call.cancel();
                ac.TAG_REQUEST[0] = true;
            }
        });

    }
    public void request_all_category(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<CategorysResponse> call = client.get_all_category();
        call.enqueue(new Callback<CategorysResponse>() {
            @Override
            public void onResponse(Call<CategorysResponse> call, Response<CategorysResponse> response) {
                //////Log.i("SERVER CATEGORY", response.body().getMessage());
                if(response.body() != null) {
                    SDM.save_list_category(response.body().getCategory());
                }
                ac.TAG_REQUEST[1] = true;
                ac.check_data_sync(); call.cancel();
            }

            @Override
            public void onFailure(Call<CategorysResponse> call, Throwable t) {
                //////Log.i("SERVER CATEGORY", t.getMessage());
                ac.TAG_REQUEST[1] = true;
                ac.check_data_sync(); call.cancel();
            }
        });
    }
    public void request_all_class(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<List<Cls>> call = client.get_all_class();
        call.enqueue(new Callback<List<Cls>>() {
            @Override
            public void onResponse(Call<List<Cls>> call, Response<List<Cls>> response) {
                //////Log.i("SERVER CLASS", response.body().toString());
                if(response.body() != null) {
                    SDM.save_list_class(response.body());
                }
                ac.TAG_REQUEST[2] = true;
                ac.check_data_sync(); call.cancel();
            }

            @Override
            public void onFailure(Call<List<Cls>> call, Throwable t) {
                //////Log.i("SERVER CLASS", t.getMessage());
                ac.TAG_REQUEST[2] = true;
                ac.check_data_sync(); call.cancel();
            }
        });
    }
    public void request_all_dinas(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<DinassResponse> call = client.get_all_dinas();
        call.enqueue(new Callback<DinassResponse>() {
            @Override
            public void onResponse(Call<DinassResponse> call, Response<DinassResponse> response) {
                //////Log.i("SERVER DINAS", response.body().getMessage().toString());
                if(response.body() != null) {
                    SDM.save_list_dinas(response.body().getDinas());
                }
                ac.TAG_REQUEST[3] = true;
                ac.check_data_sync(); call.cancel();
            }

            @Override
            public void onFailure(Call<DinassResponse> call, Throwable t) {
                //////Log.i("SERVER DINAS", t.getMessage());
                ac.TAG_REQUEST[3] = true;
                ac.check_data_sync(); call.cancel();
            }
        });
    }
    public void request_all_disposisi(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<DisposissResponse> call = client.get_all_disposisi();
        call.enqueue(new Callback<DisposissResponse>() {
            @Override
            public void onResponse(Call<DisposissResponse> call, Response<DisposissResponse> response) {
                //////Log.i("SERVER DISPOSISI", response.body().getMessage().toString());

                if(response.body() != null) {
                    SDM.save_list_disposisi(response.body().getDisposisi());
                }
                ac.TAG_REQUEST[4] = true;
                ac.check_data_sync(); call.cancel();
            }

            @Override
            public void onFailure(Call<DisposissResponse> call, Throwable t) {
                //////Log.i("SERVER DISPOSISI", "ERROR");
                ac.TAG_REQUEST[4] = true;
                ac.check_data_sync(); call.cancel();
            }
        });
    }
    public void request_all_file(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<FilesResponse> call = client.get_all_file();
        call.enqueue(new Callback<FilesResponse>() {
            @Override
            public void onResponse(Call<FilesResponse> call, Response<FilesResponse> response) {
                //////Log.i("SERVER FILE", response.body().getMessage().toString());
                if(response.body() != null) {
                    SDM.save_list_file(response.body().getFile());
                }
                ac.TAG_REQUEST[5] = true;
                ac.check_data_sync(); call.cancel();
            }

            @Override
            public void onFailure(Call<FilesResponse> call, Throwable t) {
                //////Log.i("SERVER FILE", "ERROR");
                ac.TAG_REQUEST[5] = true;
                ac.check_data_sync(); call.cancel();
            }
        });
    }
    public void request_all_folder(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<FoldersResponse> call = client.get_all_folder();
        call.enqueue(new Callback<FoldersResponse>() {
            @Override
            public void onResponse(Call<FoldersResponse> call, Response<FoldersResponse> response) {
                //////Log.i("SERVER FOLDER", response.body().getMessage().toString());

                if(response.body() != null) {
                    SDM.save_list_folder(response.body().getFolder());
                }
                ac.TAG_REQUEST[6] = true;
                ac.check_data_sync(); call.cancel();
            }

            @Override
            public void onFailure(Call<FoldersResponse> call, Throwable t) {
                //////Log.i("SERVER FOLDER", "ERROR");
                ac.TAG_REQUEST[6] = true;
                ac.check_data_sync(); call.cancel();
            }
        });
    }
    public void request_all_hotel_fee (){
        try {
            Request_Inf client = retrofit.create(Request_Inf.class);
            Call<HotelFeesResponse> call = client.get_all_hotel_fee();
            call.enqueue(new Callback<HotelFeesResponse>() {
                @Override
                public void onResponse(Call<HotelFeesResponse> call, Response<HotelFeesResponse> response) {
                    //////Log.i("SERVER HOTEL", response.body().getMessage().toString());
                    if(response.body() != null) {
                        SDM.save_list_hotel_fee(response.body().getHotelFees());
                    }
                    ac.TAG_REQUEST[7] = true;
                    ac.check_data_sync();
                    call.cancel();
                }

                @Override
                public void onFailure(Call<HotelFeesResponse> call, Throwable t) {
                    //////Log.i("SERVER HOTEL", "ERROR");
                    ac.TAG_REQUEST[7] = true;
                    ac.check_data_sync();
                    call.cancel();
                }
            });
        }catch (Exception e){}
    }
    public void request_all_jenis_dinas(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<JenissResponse> call = client.get_all_jenis_dinas();
        call.enqueue(new Callback<JenissResponse>() {
            @Override
            public void onResponse(Call<JenissResponse> call, Response<JenissResponse> response) {
                //////Log.i("SERVER JENIS DINAS", response.body().getMessage().toString());
                if(response.body() != null) {
                    SDM.save_list_jenis_dinas(response.body().getJenis());
                }
                ac.TAG_REQUEST[8] = true;
                ac.check_data_sync(); call.cancel();
            }

            @Override
            public void onFailure(Call<JenissResponse> call, Throwable t) {
                //////Log.i("SERVER JENIS DINAS", "ERROR");
                ac.TAG_REQUEST[8] = true;
                ac.check_data_sync(); call.cancel();
            }
        });
    }
    public void request_all_kecamatan(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<KecamatansResponse> call = client.get_all_Kecamatan();
        call.enqueue(new Callback<KecamatansResponse>() {
            @Override
            public void onResponse(Call<KecamatansResponse> call, Response<KecamatansResponse> response) {
                //////Log.i("SERVER KECAMATAN", response.body().getMessage().toString());
                if(response.body() != null) {
                    SDM.save_list_kecamatan(response.body().getKecamatan());
                }
                ac.TAG_REQUEST[9] = true;
                ac.check_data_sync(); call.cancel();
            }

            @Override
            public void onFailure(Call<KecamatansResponse> call, Throwable t) {
                //////Log.i("SERVER KECAMATAN", "ERROR");
                ac.TAG_REQUEST[9] = true;
                ac.check_data_sync(); call.cancel();
            }
        });
    }
    public void request_all_kelurahan(){
        try{
            Request_Inf client = retrofit.create(Request_Inf.class);
            Call<KelurahansResponse> call = client.get_all_Kelurahan();
            call.enqueue(new Callback<KelurahansResponse>() {
                @Override
                public void onResponse(Call<KelurahansResponse> call, Response<KelurahansResponse> response) {
                    //////Log.i("SERVER KELURAHAN", response.body().getMessage().toString());
                    if(response.body() != null) {
                        SDM.save_list_kelurahan(response.body().getKelurahan());
                    }
                    ac.TAG_REQUEST[20] = true;
                    ac.check_data_sync(); call.cancel();
                }

                @Override
                public void onFailure(Call<KelurahansResponse> call, Throwable t) {
                    //////Log.i("SERVER KELURAHAN", "error");
                    ac.TAG_REQUEST[20] = true;
                    ac.check_data_sync(); call.cancel();
                }
            });
        }catch (Exception e){}
    }
    public void request_all_kabupaten(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<KabupatensResponse> call = client.get_all_kabupaten();
        call.enqueue(new Callback<KabupatensResponse>() {
            @Override
            public void onResponse(Call<KabupatensResponse> call, Response<KabupatensResponse> response) {
                //////Log.i("SERVER KABUPATEN", response.body().getMessage().toString());
                if(response.body() != null) {
                    SDM.save_list_kabupaten(response.body().getKabupaten());
                }
                ac.TAG_REQUEST[11] = true;
                ac.check_data_sync(); call.cancel();
            }

            @Override
            public void onFailure(Call<KabupatensResponse> call, Throwable t) {
                //////Log.i("SERVER KABUPATEN", "ERROR");
                ac.TAG_REQUEST[11] = true;
                ac.check_data_sync(); call.cancel();
            }
        });
    }
    public void request_all_mobile(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<List<Mobile>> call = client.get_all_Mobile();
        call.enqueue(new Callback<List<Mobile>>() {
            @Override
            public void onResponse(Call<List<Mobile>> call, Response<List<Mobile>> response) {
                //////Log.i("SERVER MOBILE", response.body().toString());
                if(response.body() != null) {
                    SDM.save_list_mobile(response.body());
                }
                ac.TAG_REQUEST[10] = true;
                ac.check_data_sync(); call.cancel();
            }

            @Override
            public void onFailure(Call<List<Mobile>> call, Throwable t) {
                //////Log.i("SERVER MOBILE", "ERROR");
                ac.TAG_REQUEST[10] = true;
                ac.check_data_sync(); call.cancel();
            }
        });
    }
    public void request_all_place(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<PlacesResponse> call = client.get_all_Place();
        call.enqueue(new Callback<PlacesResponse>() {
            @Override
            public void onResponse(Call<PlacesResponse> call, Response<PlacesResponse> response) {
                //////Log.i("SERVER PLACE", response.body().getMessage().toString());
                if(response.body() != null) {
                    SDM.save_list_place(response.body().getPlace());
                }
                ac.TAG_REQUEST[12] = true;
                ac.check_data_sync(); call.cancel();
            }

            @Override
            public void onFailure(Call<PlacesResponse> call, Throwable t) {
                //////Log.i("SERVER PLACE", "ERROR");
                ac.TAG_REQUEST[12] = true;
                ac.check_data_sync(); call.cancel();
            }
        });
    }
    public void request_all_position(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<PositionsResponse> call = client.get_all_Position();
        call.enqueue(new Callback<PositionsResponse>() {
            @Override
            public void onResponse(Call<PositionsResponse> call, Response<PositionsResponse> response) {
                //////Log.i("SERVER POSISI", response.body().getMessage().toString());
                ac.TAG_REQUEST[13] = true;
                if(response.body() != null) {
                    SDM.save_list_position(response.body().getPosition());
                }
                ac.check_data_sync(); call.cancel();
            }

            @Override
            public void onFailure(Call<PositionsResponse> call, Throwable t) {
                //////Log.i("SERVER POSISI", "ERROR");
                ac.TAG_REQUEST[13] = true;
                ac.check_data_sync(); call.cancel();
            }
        });
    }
    public void request_all_provinsi(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<ProvinsisResponse> call = client.get_all_Provinsi();
        call.enqueue(new Callback<ProvinsisResponse>() {
            @Override
            public void onResponse(Call<ProvinsisResponse> call, Response<ProvinsisResponse> response) {
                //////Log.i("SERVER PROVINSI", response.body().getMessage().toString());
                ac.TAG_REQUEST[14] = true;
                if(response.body() != null) {
                    SDM.save_list_provinsi(response.body().getProvinsi());
                }
                ac.check_data_sync(); call.cancel();
            }

            @Override
            public void onFailure(Call<ProvinsisResponse> call, Throwable t) {
                //////Log.i("SERVER PROVINSI", "ERROR");
                ac.TAG_REQUEST[14] = true;
                ac.check_data_sync(); call.cancel();
            }
        });
    }
    public void request_all_role(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<List<Role>> call = client.get_all_Role();
        call.enqueue(new Callback<List<Role>>() {
            @Override
            public void onResponse(Call<List<Role>> call, Response<List<Role>> response) {
                //////Log.i("SERVER ROLE", response.body().toString());
                ac.TAG_REQUEST[15] = true;
                if(response.body() != null) {
                    SDM.save_list_role(response.body());
                }
                ac.check_data_sync(); call.cancel();
            }

            @Override
            public void onFailure(Call<List<Role>> call, Throwable t) {
                //////Log.i("SERVER ROLE", "ERROR");
                ac.TAG_REQUEST[15] = true;
            }
        });
    }
    public void request_all_schedule(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<SchedulesResponse> call = client.get_all_Schedule();
        call.enqueue(new Callback<SchedulesResponse>() {
            @Override
            public void onResponse(Call<SchedulesResponse> call, Response<SchedulesResponse> response) {
                //////Log.i("SERVER MEETING", response.body().getMessage().toString());
                ac.TAG_REQUEST[16] = true;
                if(response.body() != null) {
                    SDM.save_list_meeting(response.body().getSchedule());
                }
                ac.check_data_sync(); call.cancel();
            }

            @Override
            public void onFailure(Call<SchedulesResponse> call, Throwable t) {
                //////Log.i("SERVER MEETING", "ERROR");
                ac.TAG_REQUEST[16] = true;
                ac.check_data_sync(); call.cancel();
            }
        });
    }
    public void request_all_taxi_fee(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<TaxyFeesResponse> call = client.get_all_TaxyFee();
        call.enqueue(new Callback<TaxyFeesResponse>() {
            @Override
            public void onResponse(Call<TaxyFeesResponse> call, Response<TaxyFeesResponse> response) {
                //////Log.i("SERVER TAXI-FEE", response.body().getMessage().toString());
                ac.TAG_REQUEST[17] = true;
                if(response.body() != null) {
                    SDM.save_taxi_fee(response.body().getTaxyFees());
                }
                ac.check_data_sync(); call.cancel();
            }

            @Override
            public void onFailure(Call<TaxyFeesResponse> call, Throwable t) {
                //////Log.i("SERVER TAXI-FEE", "ERROR");
                ac.TAG_REQUEST[17] = true;
                ac.check_data_sync(); call.cancel();
            }
        });
    }
    public void request_all_user(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<UsersResponse> call = client.get_all_User();
        call.enqueue(new Callback<UsersResponse>() {
            @Override
            public void onResponse(Call<UsersResponse> call, Response<UsersResponse> response) {
                //////Log.i("SERVER USER", response.body().getMessage().toString());
                ac.TAG_REQUEST[18] = true;
                if(response.body() != null) {
                    SDM.save_list_user(response.body().getUser());
                    SDM.save_list_user_role(response.body().getUser());
                }
                ac.check_data_sync(); call.cancel();
            }

            @Override
            public void onFailure(Call<UsersResponse> call, Throwable t) {
                //////Log.i("SERVER USER", "ERROR");
                ac.TAG_REQUEST[18] = true;
                ac.check_data_sync(); call.cancel();
            }
        });
    }
    public void request_all_vehicle_rent(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<VehiclesResponse> call = client.get_all_Vehicle();
        call.enqueue(new Callback<VehiclesResponse>() {
            @Override
            public void onResponse(Call<VehiclesResponse> call, Response<VehiclesResponse> response) {
                ac.TAG_REQUEST[19] = true;
                if(response.body() != null) {
                    SDM.save_list_vehicle(response.body().getVehicles());
                }
                ac.check_data_sync(); call.cancel();
            }

            @Override
            public void onFailure(Call<VehiclesResponse> call, Throwable t) {
                //////Log.i("SERVER USER", "ERROR");
                ac.TAG_REQUEST[19] = true;
                ac.check_data_sync(); call.cancel();
            }
        });
    }

    public void request_ByTo_schedule(String t){

        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<SchedulesResponse> call = client.get_Schedule_to(t);
        call.enqueue(new Callback<SchedulesResponse>() {
            @Override
            public void onResponse(Call<SchedulesResponse> call, Response<SchedulesResponse> response) {
                if(response.body() != null) {
                    SDM.save_list_meeting(response.body().getSchedule());
                }
            }

            @Override
            public void onFailure(Call<SchedulesResponse> call, Throwable t) {
            }
        });
    }
    public void request_ByDATE_dinas(String year, String month){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<DinassResponse> call = client.get_all_dinas_date(year, month);
        call.enqueue(new Callback<DinassResponse>() {
            @Override
            public void onResponse(Call<DinassResponse> call, Response<DinassResponse> response) {
                if(response.body() != null) {
                    SDM.save_list_dinas(response.body().getDinas());
                }
            }

            @Override
            public void onFailure(Call<DinassResponse> call, Throwable t) {
            }
        });
    }
    public void request_ByDATE_schedule(String year, String month){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<SchedulesResponse> call = client.get_Schedule_date(year, month);
        call.enqueue(new Callback<SchedulesResponse>() {
            @Override
            public void onResponse(Call<SchedulesResponse> call, Response<SchedulesResponse> response) {
                if(response.body() != null) {
                    SDM.save_list_meeting(response.body().getSchedule());
                }
            }

            @Override
            public void onFailure(Call<SchedulesResponse> call, Throwable t) {
            }
        });
    }

    public void request_byID_airplane(String id){
        Retrofit.Builder build = new Retrofit.Builder()
                .baseUrl(id)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = build.build();
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<AirPlaneTiketResponse> call = client.get_airplaneticket_id(id);
        call.enqueue(new Callback<AirPlaneTiketResponse>() {
            @Override
            public void onResponse(Call<AirPlaneTiketResponse> call, Response<AirPlaneTiketResponse> response) {
                if(response.body() != null) {
                    SDM.save_airplane_ticket(response.body().getAirPlaneTiket());
                }
            }

            @Override
            public void onFailure(Call<AirPlaneTiketResponse> call, Throwable t) {
            }
        });
    }
    public void request_byID_category(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<CategoryResponse> call = client.get_category_id(id);
        call.enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                if(response.body() != null) {
                    SDM.save_category(response.body().getCategory());
                }
            }

            @Override
            public void onFailure(Call<CategoryResponse> call, Throwable t) {
            }
        });
    }
    public void request_byID_class(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<Cls> call = client.get_class_id(id);
        call.enqueue(new Callback<Cls>() {
            @Override
            public void onResponse(Call<Cls> call, Response<Cls> response) {
                if(response.body() != null) {
                    SDM.save_class(response.body());
                }
            }

            @Override
            public void onFailure(Call<Cls> call, Throwable t) {
            }
        });
    }
    public void request_byID_dinas(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<DinasResponse> call = client.get_dinas_id(id);
        call.enqueue(new Callback<DinasResponse>() {
            @Override
            public void onResponse(Call<DinasResponse> call, Response<DinasResponse> response) {
                if(response.body() != null) {
                    SDM.save_dinas(response.body().getDinas());
                }
            }

            @Override
            public void onFailure(Call<DinasResponse> call, Throwable t) {
            }
        });
    }
    public void request_byID_disposisi(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<DisposisiResponse> call = client.get_disposisi_id(id);
        call.enqueue(new Callback<DisposisiResponse>() {
            @Override
            public void onResponse(Call<DisposisiResponse> call, Response<DisposisiResponse> response) {
                if(response.body() != null) {
                    SDM.save_disposisi(response.body().getDisposisi());
                }
            }

            @Override
            public void onFailure(Call<DisposisiResponse> call, Throwable t) {
            }
        });
    }
    public void request_byID_file(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<FileResponse> call = client.get_file_id(id);
        call.enqueue(new Callback<FileResponse>() {
            @Override
            public void onResponse(Call<FileResponse> call, Response<FileResponse> response) {
                if(response.body() != null) {
                    SDM.save_file(response.body().getFile());
                }
            }

            @Override
            public void onFailure(Call<FileResponse> call, Throwable t) {
            }
        });
    }
    public void request_byID_folder(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<FolderResponse> call = client.get_folder_id(id);
        call.enqueue(new Callback<FolderResponse>() {
            @Override
            public void onResponse(Call<FolderResponse> call, Response<FolderResponse> response) {
                if(response.body() != null) {
                    SDM.save_folder(response.body().getFolder());
                }
            }

            @Override
            public void onFailure(Call<FolderResponse> call, Throwable t) {
            }
        });
    }
    public void request_byID_hotel_fee(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<HotelFeeResponse> call = client.get_hotel_fee_id(id);
        call.enqueue(new Callback<HotelFeeResponse>() {
            @Override
            public void onResponse(Call<HotelFeeResponse> call, Response<HotelFeeResponse> response) {
                if(response.body() != null) {
                    SDM.save_hotel_fee(response.body().getHotelFees());
                }
            }

            @Override
            public void onFailure(Call<HotelFeeResponse> call, Throwable t) {
            }
        });
    }
    public void request_byID_jenis_dinas(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<JenisResponse> call = client.get_jenis_dinas_id(id);
        call.enqueue(new Callback<JenisResponse>() {
            @Override
            public void onResponse(Call<JenisResponse> call, Response<JenisResponse> response) {
                if(response.body() != null) {
                    SDM.save_jenis_dinas(response.body().getJenis());
                }
            }

            @Override
            public void onFailure(Call<JenisResponse> call, Throwable t) {
            }
        });
    }
    public void request_byID_kecamatan(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<KecamatanResponse> call = client.get_Kecamatan_id(URLManager.URI_API);
        call.enqueue(new Callback<KecamatanResponse>() {
            @Override
            public void onResponse(Call<KecamatanResponse> call, Response<KecamatanResponse> response) {
                if(response.body() != null) {
                    SDM.save_kecamatan(response.body().getKecamatan());
                }
            }

            @Override
            public void onFailure(Call<KecamatanResponse> call, Throwable t) {
            }
        });
    }
    public void request_byID_kelurahan(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<KelurahanResponse> call = client.get_Kelurahan_id(id);
        call.enqueue(new Callback<KelurahanResponse>() {
            @Override
            public void onResponse(Call<KelurahanResponse> call, Response<KelurahanResponse> response) {
                if(response.body() != null) {
                    SDM.save_kelurahan(response.body().getKelurahan());
                }
            }

            @Override
            public void onFailure(Call<KelurahanResponse> call, Throwable t) {
            }
        });
    }
    public void request_byID_kabupaten(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<KabupatenResponse> call = client.get_kabupaten_id(id);
        call.enqueue(new Callback<KabupatenResponse>() {
            @Override
            public void onResponse(Call<KabupatenResponse> call, Response<KabupatenResponse> response) {
                SDM.save_kabupaten(response.body().getKabupaten());
            }

            @Override
            public void onFailure(Call<KabupatenResponse> call, Throwable t) {
            }
        });
    }
    public void request_byID_mobile(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<List<Mobile>> call = client.get_Mobile_id(id);
        call.enqueue(new Callback<List<Mobile>>() {
            @Override
            public void onResponse(Call<List<Mobile>> call, Response<List<Mobile>> response) {
                SDM.save_mobile(response.body());
            }

            @Override
            public void onFailure(Call<List<Mobile>> call, Throwable t) {
            }
        });
    }
    public void request_byID_place(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<PlaceResponse> call = client.get_Place_id(id);
        call.enqueue(new Callback<PlaceResponse>() {
            @Override
            public void onResponse(Call<PlaceResponse> call, Response<PlaceResponse> response) {
                SDM.save_place(response.body().getPlace());
            }

            @Override
            public void onFailure(Call<PlaceResponse> call, Throwable t) {
            }
        });
    }
    public void request_byID_position(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<PositionResponse> call = client.get_Position_id(id);
        call.enqueue(new Callback<PositionResponse>() {
            @Override
            public void onResponse(Call<PositionResponse> call, Response<PositionResponse> response) {
                SDM.save_position(response.body().getPosition());
            }

            @Override
            public void onFailure(Call<PositionResponse> call, Throwable t) {
            }
        });
    }
    public void request_byID_provinsi(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<ProvinsiResponse> call = client.get_Provinsi_id(id);
        call.enqueue(new Callback<ProvinsiResponse>() {
            @Override
            public void onResponse(Call<ProvinsiResponse> call, Response<ProvinsiResponse> response) {
                SDM.save_provinsi(response.body().getProvinsi());
            }

            @Override
            public void onFailure(Call<ProvinsiResponse> call, Throwable t) {
            }
        });
    }
    public void request_byID_schedule(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<ScheduleResponse> call = client.get_Schedule_id(id);
        call.enqueue(new Callback<ScheduleResponse>() {
            @Override
            public void onResponse(Call<ScheduleResponse> call, Response<ScheduleResponse> response) {
                SDM.save_meeting(response.body().getSchedule());
            }

            @Override
            public void onFailure(Call<ScheduleResponse> call, Throwable t) {
            }
        });
    }
    public void request_byID_taxi_fee(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<TaxyFeeResponse> call = client.get_TaxyFee_id(id);
        call.enqueue(new Callback<TaxyFeeResponse>() {
            @Override
            public void onResponse(Call<TaxyFeeResponse> call, Response<TaxyFeeResponse> response) {
                SDM.save_taxi_fee(response.body().getTaxyFees());
            }

            @Override
            public void onFailure(Call<TaxyFeeResponse> call, Throwable t) {
            }
        });
    }
    public void request_byID_user(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<UserResponse> call = client.get_User_id(id);
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                SDM.save_user(response.body().getUser());
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
            }
        });
    }
}
