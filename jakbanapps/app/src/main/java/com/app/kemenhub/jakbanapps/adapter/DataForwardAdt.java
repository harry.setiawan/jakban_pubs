package com.app.kemenhub.jakbanapps.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.kemenhub.jakbanapps.R;
import com.app.kemenhub.jakbanapps.db.category;
import com.app.kemenhub.jakbanapps.db.users;
import com.app.kemenhub.jakbanapps.retrofit.model_data.user.User;

import java.util.List;


public class DataForwardAdt extends RecyclerView.Adapter<DataForwardAdt.Holder> {
    List<users> itemList;
    Context ctx;
    private LayoutInflater inflater;
    Context context;
    private SparseBooleanArray mSelectedItemsIds;
    private int previous = 0;

    private itemForwardClickCallback itemForwardClickCallback;
        int Type;
        ViewGroup parent;

        public interface itemForwardClickCallback{
            void onItemForwardClick(int p);
        }

    public void setItemForwardClickCallback(final itemForwardClickCallback itemForwardClickCallback){
        this.itemForwardClickCallback = itemForwardClickCallback;
    }

    public DataForwardAdt(List<users> data_selected, Context context){
        this.itemList = data_selected;
        this.ctx = context;
        this.inflater = LayoutInflater.from(ctx);
        this.context = context;
        mSelectedItemsIds = new SparseBooleanArray();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_checkbox, parent, false);
        WindowManager windowManager = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);
        int width = windowManager.getDefaultDisplay().getWidth();
        view.setLayoutParams(new RecyclerView.LayoutParams(width, RecyclerView.LayoutParams.WRAP_CONTENT));

        Holder derpHolder = new Holder(view);

        return derpHolder;
    }

    public void runer(){
        onCreateViewHolder(parent, Type);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        if(!itemList.get(position).getName().toUpperCase().contains("ADMIN")) {
            holder.checkBox.setChecked(true);
            holder.checkBox.setClickable(false);
            holder.nama.setText(itemList.get(position).getName() + " [NIP : " + itemList.get(position).getNIP() +"] ");
        }else{
            if(itemList.get(position).getName().toUpperCase().contains("ADMIN") && itemList.size() == 1){
                holder.checkBox.setChecked(true);
                holder.checkBox.setClickable(false);
                holder.nama.setText("Semua Pegawai");
            }else {
                holder.item.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));
    }

    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }

    public void selectView(int position, boolean value) {
        if (value) {
            mSelectedItemsIds.put(position, value);
        }else {
            mSelectedItemsIds.delete(position);
        }
        notifyDataSetChanged();
    }


    class Holder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public LinearLayout item;
        public CheckBox checkBox;
        public TextView nama;
        public Holder(View itemView) {
            super(itemView);
            item = (LinearLayout) itemView.findViewById(R.id.item);
            checkBox = (CheckBox) itemView.findViewById(R.id.checkbox);
            nama = (TextView) itemView.findViewById(R.id.nama);

            item.setClickable(true);
            item.setEnabled(true);
            item.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.item:
                    itemForwardClickCallback.onItemForwardClick(getAdapterPosition());
                    break;
            }
        }
    }
}