package com.app.kemenhub.jakbanapps.retrofit.model_data.jenis_dinas;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class JenisResponse {
	@SerializedName("code")
	String code;
	@SerializedName("message")
	String message;
	@SerializedName("jenis")
	Jenis jenis;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Jenis getJenis() {
		return jenis;
	}
	public void setJenis(Jenis jenis) {
		this.jenis = jenis;
	}
	
	
}
