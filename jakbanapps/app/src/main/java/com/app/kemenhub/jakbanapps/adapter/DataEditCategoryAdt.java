package com.app.kemenhub.jakbanapps.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.kemenhub.jakbanapps.R;
import com.app.kemenhub.jakbanapps.db.category;
import com.app.kemenhub.jakbanapps.fragment.add.f_edit_meeting;

import java.util.List;


public class DataEditCategoryAdt extends RecyclerView.Adapter<DataEditCategoryAdt.Holder> {
    List<category> itemList;
    Context ctx;
    private LayoutInflater inflater;
    f_edit_meeting frag;
    private SparseBooleanArray mSelectedItemsIds;
    private int previous = 0;

    int Type;
    ViewGroup parent;

    private itemCategoryClickCallback itemCategoryClickCallback;

    public interface itemCategoryClickCallback{
        void onItemClick(int p, boolean data);
    }

    public void setItemCategoryClickCallback(final itemCategoryClickCallback itemCategoryClickCallback){
        this.itemCategoryClickCallback = itemCategoryClickCallback;
    }

    public DataEditCategoryAdt(List<category> data_selected, f_edit_meeting frag){
        this.itemList = data_selected;
        this.ctx = frag.getContext();
        this.frag = frag;
        this.inflater = LayoutInflater.from(ctx);
        mSelectedItemsIds = new SparseBooleanArray();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_checkbox, parent, false);
        WindowManager windowManager = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);
        int width = windowManager.getDefaultDisplay().getWidth();
        view.setLayoutParams(new RecyclerView.LayoutParams(width, RecyclerView.LayoutParams.WRAP_CONTENT));

        Holder derpHolder = new Holder(view);

        return derpHolder;
    }

    public void runer(){
        onCreateViewHolder(parent, Type);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.checkBox.setClickable(true);
        holder.nama.setText(itemList.get(position).getName());
        for(int i = 0 ; i < frag.tmp_disposisi.size() ; i++){
            if(frag.tmp_disposisi.get(i).equals(String.valueOf(itemList.get(position).getId()))){
                holder.checkBox.setChecked(true);
                break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));
    }

    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }

    public void selectView(int position, boolean value) {
        if (value) {
            mSelectedItemsIds.put(position, value);
        }else {
            mSelectedItemsIds.delete(position);
        }
        notifyDataSetChanged();
    }


    class Holder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public LinearLayout item;
        public CheckBox checkBox;
        public TextView nama;
        public Holder(View itemView) {
            super(itemView);
            item = (LinearLayout) itemView.findViewById(R.id.item);
            checkBox = (CheckBox) itemView.findViewById(R.id.checkbox);
            nama = (TextView) itemView.findViewById(R.id.nama);

            item.setClickable(true);
            item.setEnabled(true);
            item.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.item:
                    if(checkBox.isChecked()){
                        checkBox.setChecked(false);
                        itemCategoryClickCallback.onItemClick(getAdapterPosition(), false);
                    }else{
                        checkBox.setChecked(true);
                        itemCategoryClickCallback.onItemClick(getAdapterPosition(), true);
                    }
                    break;
            }
        }
    }
}