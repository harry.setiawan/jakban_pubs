package com.app.kemenhub.jakbanapps.service;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ConnectivityChanged extends Activity {
    Context activity;

    public interface Broadcaast_Connection {
        void Broadcaast_Connection(boolean output);
    }

    private Broadcaast_Connection listener;


    public ConnectivityChanged(Context activity){
        try {
            this.activity = activity;
            listener = (Broadcaast_Connection) activity;
        }catch (Exception e){}
    }

    public BroadcastReceiver mConnReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            boolean noConnectivity = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
            String reason = intent.getStringExtra(ConnectivityManager.EXTRA_REASON);
            boolean isFailover = intent.getBooleanExtra(ConnectivityManager.EXTRA_IS_FAILOVER, false);

            NetworkInfo currentNetworkInfo = (NetworkInfo) intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
            NetworkInfo otherNetworkInfo = (NetworkInfo) intent.getParcelableExtra(ConnectivityManager.EXTRA_OTHER_NETWORK_INFO);

            if (currentNetworkInfo.isConnected()) {
                System.out.println("CONNECT");
                listener.Broadcaast_Connection(true);
            } else {
                System.out.println("NOT CONNECT");
                listener.Broadcaast_Connection(false);
            }
        }
    };
}