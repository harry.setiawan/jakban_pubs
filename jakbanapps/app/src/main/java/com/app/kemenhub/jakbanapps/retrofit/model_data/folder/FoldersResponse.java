package com.app.kemenhub.jakbanapps.retrofit.model_data.folder;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FoldersResponse {
	@SerializedName("code")
	String code;
	@SerializedName("message")
	String message;
	@SerializedName("folder")
	List<Folder> folder;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<Folder> getFolder() {
		return folder;
	}
	public void setFolder(List<Folder> folder) {
		this.folder = folder;
	}
	
	
}
