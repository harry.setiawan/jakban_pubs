package com.app.kemenhub.jakbanapps.retrofit.model_data.kelurahan;

public class Kelurahan {
	private String id;
	private String kecamatan_id;
	private String name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}


	public String getKecamatan_id() {
		return kecamatan_id;
	}

	public void setKecamatan_id(String kecamatan_id) {
		this.kecamatan_id = kecamatan_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
