package com.app.kemenhub.jakbanapps.retrofit;

import com.app.kemenhub.jakbanapps.db.user_mobile;
import com.app.kemenhub.jakbanapps.db.users;
import com.app.kemenhub.jakbanapps.retrofit.model_data.airplane.AirPlaneTiketResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.airplane.AirPlaneTiketsResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.category.CategoryResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.category.CategorysResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.cls.Cls;
import com.app.kemenhub.jakbanapps.retrofit.model_data.dinas.DinasAdd;
import com.app.kemenhub.jakbanapps.retrofit.model_data.dinas.DinasResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.dinas.DinassResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.disposisi.DisposisiAdd;
import com.app.kemenhub.jakbanapps.retrofit.model_data.disposisi.DisposisiResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.disposisi.DisposissResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.file.File;
import com.app.kemenhub.jakbanapps.retrofit.model_data.file.FileAddResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.file.FileResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.file.FilesResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.folder.Folder;
import com.app.kemenhub.jakbanapps.retrofit.model_data.folder.FolderAddResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.folder.FolderResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.folder.FoldersResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.hotel_fee.HotelFeeResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.hotel_fee.HotelFeesResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.jenis_dinas.JenisResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.jenis_dinas.JenissResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.kabupaten.KabupatenResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.kabupaten.KabupatensResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.kecamatan.KecamatanResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.kecamatan.KecamatansResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.kelurahan.KelurahanResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.kelurahan.KelurahansResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.mobile.Mobile;
import com.app.kemenhub.jakbanapps.retrofit.model_data.mobile.MobileResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.place.PlaceResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.place.PlacesResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.position.PositionResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.position.PositionsResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.provinsi.ProvinsiResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.provinsi.ProvinsisResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.role.Role;
import com.app.kemenhub.jakbanapps.retrofit.model_data.schedule.ScheduleAdd;
import com.app.kemenhub.jakbanapps.retrofit.model_data.schedule.ScheduleResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.schedule.SchedulesResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.taxi_fee.TaxyFeeResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.taxi_fee.TaxyFeesResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.user.UsersResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.user.UserResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.vehicle.VehicleResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.vehicle.VehiclesResponse;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface Request_Inf {

    @GET()
    @Streaming
    Call<ResponseBody> downloadFile(@Url String url);

    @GET("airplane/tiket")
    Call<AirPlaneTiketsResponse> get_all_airplaneticket();
    @GET("/airplane/tiket/{id}")
    Call<AirPlaneTiketResponse> get_airplaneticket_id(@Path("id") String id);

    @GET("category")
    Call<CategorysResponse> get_all_category();
    @GET("/category/{id}")
    Call<CategoryResponse> get_category_id(@Path("id") String id);

    @GET("class")
    Call<List<Cls>> get_all_class();
    @GET("/class/{id}")
    Call<Cls> get_class_id(@Path("id") String id);

    @POST("file")
    Call<FileAddResponse> add_file(@Part("file") RequestBody file, @Part("folder_id") RequestBody folder_id,
                                   @Part("title") RequestBody title, @Part("status") RequestBody status);

    @GET("dinas")
    Call<DinassResponse> get_all_dinas();
    @GET("dinas/year/{years}/month/{months}")
    Call<DinassResponse> get_all_dinas_date(@Path("years") String year,@Path("months") String month);
    @GET("dinas/created/{create}")
    Call<DinassResponse> get_new_dinas_created(@Path("create") String create);
    @GET("dinas/{id}")
    Call<DinasResponse> get_dinas_id(@Path("id") String id);
    @HTTP(method = "POST", path = "/", hasBody = true)
    Call<FolderAddResponse> add_dinas(@Body DinasAdd dinasAdd);
    @HTTP(method = "PUT", path = "/", hasBody = true)
    Call<FolderAddResponse> update_dinas(@Body DinasAdd dinasAdd);
    @HTTP(method = "DELETE", path = "/", hasBody = true)
    Call<FolderAddResponse> delete_dinas();

    @GET("disposisi")
    Call<DisposissResponse> get_all_disposisi();
    @GET("disposisi/year/{years}/to/{to}")
    Call<DisposissResponse> get_disposisi_to(@Path("years") String year,@Path("to") String to);
    @GET("disposisi/created/{create}/to/{to}")
    Call<DisposissResponse> get_disposisi_createto(@Path("create") String year,@Path("to") String to);
    @GET("disposisi/{id}")
    Call<DisposisiResponse> get_disposisi_id(@Path("id") String id);
    @HTTP(method = "POST", path = "/", hasBody = true)
    Call<FolderAddResponse> add_disposisi(@Body DisposisiAdd disposisiAdd);
    @HTTP(method = "PUT", path = "/", hasBody = true)
    Call<FolderAddResponse> update_disposisi(@Body DisposisiAdd disposisiAdd);
    @HTTP(method = "DELETE", path = "/", hasBody = true)
    Call<FolderAddResponse> delete_disposisi();

    @GET("file")
    Call<FilesResponse> get_all_file();
    @GET("file/{id}")
    Call<FileResponse> get_file_id(@Path("id") String id);
    @HTTP(method = "POST", path = "/", hasBody = true)
    Call<FileAddResponse> add_file(@Body File File);

    @GET("folder")
    Call<FoldersResponse> get_all_folder();
    @GET("folder/{id}")
    Call<FolderResponse> get_folder_id(@Path("id") String id);
    @HTTP(method = "POST", path = "/", hasBody = true)
    Call<FolderAddResponse> add_folder(@Body Folder folder);
    @HTTP(method = "PUT", path = "/", hasBody = true)
    Call<FolderAddResponse> update_folder(@Body Folder folder);
    @HTTP(method = "DELETE", path = "/", hasBody = true)
    Call<FolderAddResponse> delete_folder();

    @GET("hotel-fee")
    Call<HotelFeesResponse> get_all_hotel_fee();
    @GET("hotel-fee/{id}")
    Call<HotelFeeResponse> get_hotel_fee_id(@Path("id") String id);

    @GET("jenis/dinas")
    Call<JenissResponse> get_all_jenis_dinas();
    @GET("jenis/dinas/{id}")
    Call<JenisResponse> get_jenis_dinas_id(@Path("id") String id);

    @GET("kabupaten")
    Call<KabupatensResponse> get_all_kabupaten();
    @GET("kabupaten/provinsi/{id}")
    Call<KabupatensResponse> get_all_kabupaten_prov_id(@Path("id") String id);
    @GET("kabupaten/{id}")
    Call<KabupatenResponse> get_kabupaten_id(@Path("id") String id);

    @GET("kecamatan")
    Call<KecamatansResponse> get_all_Kecamatan();
    @GET("kecamatan/kabupaten/{id}")
    Call<KecamatansResponse> get_all_Kecamatan_Kab_id(@Path("id") String id);
    @GET("kecamatan/{id}")
    Call<KecamatanResponse> get_Kecamatan_id(@Path("id") String id);

    @GET("kelurahan")
    Call<KelurahansResponse> get_all_Kelurahan();
    @GET("kelurahan/kecamatan/{id}")
    Call<KelurahansResponse> get_all_Kelurahan_Kec_id(@Path("id") String id);
    @GET("kelurahan/{id}")
    Call<KelurahanResponse> get_Kelurahan_id(@Path("id") String id);

    @GET("mobile")
    Call<List<Mobile>> get_all_Mobile();
    @GET("mobile/{id}")
    Call<List<Mobile>> get_Mobile_id(@Path("id") String id);

    @HTTP(method = "POST", path = "mobile/push/meeting/{id}", hasBody = true)
    Call<FolderAddResponse> push_Mobile_notif(@Path("id") String id, @Body List<user_mobile> mobile);

    @HTTP(method = "POST", path = "login", hasBody = true)
    Call<MobileResponse> login_mobile_account(@Body Mobile mobile);

    @HTTP(method = "DELETE", path = "logout", hasBody = true)
    Call<MobileResponse> logout_mobile_account(@Body Mobile mobile);

    @GET("place")
    Call<PlacesResponse> get_all_Place();
    @GET("place/{id}")
    Call<PlaceResponse> get_Place_id(@Path("id") String id);

    @GET("position")
    Call<PositionsResponse> get_all_Position();
    @GET("position/{id}")
    Call<PositionResponse> get_Position_id(@Path("id") String id);

    @GET("provinsi")
    Call<ProvinsisResponse> get_all_Provinsi();
    @GET("provinsi/{id}")
    Call<ProvinsiResponse> get_Provinsi_id(@Path("id") String id);

    @GET("role")
    Call<List<Role>> get_all_Role();

    @GET("meeting")
    Call<SchedulesResponse> get_all_Schedule();
    @GET("meeting/{to}")
    Call<SchedulesResponse> get_Schedule_to(@Path("to") String id);
    @GET("meeting/year/{years}/month/{months}")
    Call<SchedulesResponse> get_Schedule_date(@Path("years") String years,@Path("months") String months);
    @GET("meeting/created/{create}/to/{to}")
    Call<SchedulesResponse> get_Schedule_createTo(@Path("create") String years,@Path("to") String months);
    @GET("meeting/{id}")
    Call<ScheduleResponse> get_Schedule_id(@Path("id") String id);
    @HTTP(method = "POST", path = "meeting", hasBody = true)
    Call<FolderAddResponse> add_Schedule(@Body ScheduleAdd ScheduleiAdd);

    @HTTP(method = "PUT", path = "meeting/{id}", hasBody = true)
    Call<FolderAddResponse> update_Schedule(@Path("id") String id, @Body ScheduleAdd ScheduleiAdd);

    @DELETE("meeting/{id}")
    Call<FolderAddResponse> delete_Schedule(@Path("id") String id);

    @GET("taxi-fee")
    Call<TaxyFeesResponse> get_all_TaxyFee();
    @GET("taxi-fee/{id}")
    Call<TaxyFeeResponse> get_TaxyFee_id(@Path("id") String id);

    @GET("user")
    Call<UsersResponse> get_all_User();
    @GET("user/id")
    Call<UserResponse> get_User_id(@Path("id") String id);
    @HTTP(method = "POST", path = "login", hasBody = true)
    Call<UserResponse> login_account(@Body users user);

    @GET("vehicle/rent")
    Call<VehiclesResponse> get_all_Vehicle();
    @GET("vehicle/rent/{id}")
    Call<VehicleResponse> get_Vehicle_id(@Path("id") String id);

}
