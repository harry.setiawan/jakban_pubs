package com.app.kemenhub.jakbanapps.alert;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.app.kemenhub.jakbanapps.R;

import java.util.ArrayList;


public class dialog_confirm {
    Context ac ;
    Activity ai;

    private Response listener;

    public interface Response {
        void processFinish(boolean output);
    }

    public void showDialog(Activity activity, String msg, final Response listener){
        ac = activity.getApplicationContext();
        ai = activity;
        this.listener = listener;
        final Dialog Dialog = new Dialog(new ContextThemeWrapper(ai, R.style.DialogSlideAnim));
        Dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
        Dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
        Dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);
        Dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Dialog.setCancelable(false);
        Dialog.setContentView(R.layout.confirm);

        TextView text = (TextView) Dialog.findViewById(R.id.isi);
        text.setText(msg);

        Button cancel = (Button) Dialog.findViewById(R.id.btn_cancel);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog.dismiss();
                listener.processFinish(false);
            }
        });

        Button ok = (Button) Dialog.findViewById(R.id.btn_ok);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog.dismiss();
                listener.processFinish(true);
            }
        });

        Dialog.show();

    }
}