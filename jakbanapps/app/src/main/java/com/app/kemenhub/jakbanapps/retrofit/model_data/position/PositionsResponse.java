package com.app.kemenhub.jakbanapps.retrofit.model_data.position;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PositionsResponse {
	@SerializedName("code")
	String code;
	@SerializedName("message")
	String message;
	@SerializedName("position")
	List<Position> position;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<Position> getPosition() {
		return position;
	}
	public void setPosition(List<Position> position) {
		this.position = position;
	}
	
	
}
