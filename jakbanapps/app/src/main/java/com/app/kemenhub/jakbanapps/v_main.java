package com.app.kemenhub.jakbanapps;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.transition.Explode;
import android.transition.Slide;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.widget.ImageView;

import com.app.kemenhub.jakbanapps.alert.dialog_alert;
import com.app.kemenhub.jakbanapps.fragment.f_errconn;
import com.app.kemenhub.jakbanapps.fragment.f_login;
import com.app.kemenhub.jakbanapps.fragment.f_spl;
import com.app.kemenhub.jakbanapps.retrofit.RCMain;
import com.app.kemenhub.jakbanapps.sys.ExceptionHandler;
import com.app.kemenhub.jakbanapps.sys.InternetConnection;
import com.app.kemenhub.jakbanapps.sys.SessionManager;
import com.github.ybq.android.spinkit.style.MultiplePulse;
import com.github.ybq.android.spinkit.style.RotatingCircle;
import com.github.ybq.android.spinkit.style.ThreeBounce;

import static android.content.pm.PackageManager.*;

public class v_main extends AppCompatActivity {

    PackageManager packageManager;
    public String packageName;

    public String myVersionName = "not available"; // initialize String

    public ImageView logo, static_logo;

    public static final String TRANSITION_NAME = "TRANSITION_NAME";
    public static final String ACTION = "ACTION";
    public static final String TRANSITION_TEXT = "TRANSITION_TEXT";
    public static final String IMAGENAME = "IMAGENAME";
    public SessionManager SManager;
    public RCMain rc;

    InternetConnection detectionNet;
    public android.support.v4.app.FragmentManager frag;
    f_spl splash;
    public f_login logins;
    public f_errconn errconn;

    ProgressDialog dialog_progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        SManager = new SessionManager();
        packageManager = getApplicationContext().getPackageManager();
        packageName = getApplicationContext().getPackageName();
        try {
            myVersionName = packageManager.getPackageInfo(packageName, 0).versionName;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }

        rc = new RCMain(this);

        __init();
    }

    private void __init() {
        set_dialog();
        set_fragment();
        detectionNet = new InternetConnection(getApplicationContext());
        frag = getSupportFragmentManager();

        frag.beginTransaction().setCustomAnimations(R.anim.fadein, R.anim.fadeout).replace(R.id.content_menu, splash).commit();
        set_show();
    }

    private void set_dialog() {
        dialog_progress = new ProgressDialog(this);
        RotatingCircle ThreeBounce = new RotatingCircle();
        ThreeBounce.setBounds(0, 0, 100, 100);
        ThreeBounce.setColor(getResources().getColor(R.color.md_light_green_500_25));
        dialog_progress.setIndeterminateDrawable(ThreeBounce);
        dialog_progress.setMessage("Mohon Tunggu..");
        dialog_progress.setCancelable(false);
    }

    private void set_fragment() {
        splash = new f_spl();
        logins = new f_login();
        errconn = new f_errconn();
    }

    public void set_show() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                boolean InternetAvailable = detectionNet.InternetConnecting();
                if (InternetAvailable) {
                    try {
                        logins = new f_login();

                        String imageTransitionName = "";
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            splash.setSharedElementReturnTransition(TransitionInflater.from(
                                    v_main.this).inflateTransition(R.transition.change_image_trans));
                            splash.setExitTransition(TransitionInflater.from(
                                    v_main.this).inflateTransition(android.R.transition.fade));

                            logins.setSharedElementEnterTransition(TransitionInflater.from(
                                    v_main.this).inflateTransition(R.transition.change_image_trans));
                            logins.setEnterTransition(TransitionInflater.from(
                                    v_main.this).inflateTransition(android.R.transition.fade));

                            imageTransitionName = logo.getTransitionName();
                        }

                        Bundle bundle = new Bundle();
                        bundle.putString(TRANSITION_NAME, imageTransitionName);
                        bundle.putParcelable(IMAGENAME, ((BitmapDrawable) logo.getDrawable()).getBitmap());
                        logins.setArguments(bundle);
                        frag.beginTransaction()
                                .setCustomAnimations(R.anim.fadein, R.anim.fadeout)
                                .replace(R.id.content_menu, logins)
                                .addSharedElement(static_logo, getString(R.string.fragment_image_trans))
                                .commit();
                    }catch (Exception e){}
                } else {
                    call_err_conn();
                }
            }
        }, 2000);
    }

    public void call_login() {
        logins = new f_login();
        frag.beginTransaction().setCustomAnimations(R.anim.fadein, R.anim.fadeout).replace(R.id.content_menu, logins).commit();
    }

    public void call_splash() {
        splash = new f_spl();
        frag.beginTransaction().setCustomAnimations(R.anim.fadein, R.anim.fadeout).replace(R.id.content_menu, splash).commit();
    }

    public void call_err_conn() {
        errconn = new f_errconn();
        frag.beginTransaction().setCustomAnimations(R.anim.fadein, R.anim.fadeout).replace(R.id.content_menu, errconn).commit();
    }

    public void show_dialog(){
        dialog_progress.show();
    }

    public void hide_dialog(){
        dialog_progress.hide();
    }

    public void call_alert(String msg){
        dialog_alert alert = new dialog_alert();
        alert.showDialog(this, msg);
    }

    public void go_to_action(final Class classes) {
        if (Build.VERSION.SDK_INT >= 21) {
            Transition exitTrans = new Explode();
            getWindow().setExitTransition(exitTrans);

            Transition reenterTrans = new Slide();
            getWindow().setEnterTransition(reenterTrans);
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this);
            Intent intent = new Intent(this, classes);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent, options.toBundle());
        }else{
            Intent intent = new Intent(this, classes);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.startActivity(intent);
        }
    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
