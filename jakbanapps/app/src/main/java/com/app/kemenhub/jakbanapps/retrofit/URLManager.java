package com.app.kemenhub.jakbanapps.retrofit;

public class URLManager {
    public static final String URI_API = "http://103.200.7.167:1701/jakban/";
    public static final String URI__DOWNLOAD_API = "https://jakban.taviahouse.com/";
    public static final String USER = URI_API + "/user/";
    public static final String MOBILE = URI_API + "/mobile/";
    public static final String AIRPLANE_TICKET = URI_API + "/airplane/tiket/";
    public static final String CATEGORY = URI_API + "/category/";
    public static final String CLASS = URI_API + "/class/";
    public static final String DINAS = URI_API + "/dinas/";
    public static final String DISPOSISI = URI_API + "/disposisi/";
    public static final String FILES = URI_API + "/file/";
    public static final String FOLDER = URI_API + "/folder/";
    public static final String HOTEL_FEE = URI_API + "/hotel-fee/";
    public static final String JENIS = URI_API + "/jenis/";
    public static final String JENIS_DINAS = URI_API + JENIS + "/dinas/";
    public static final String KABUPATEN = URI_API + "/kabupaten/";
    public static final String KECAMATAN = URI_API + "/kecamatan/";
    public static final String KELURAHAN = URI_API + "/kelurahan/";
    public static final String PLACE = URI_API + "/place/";
    public static final String POSITION = URI_API + "/position/";
    public static final String PROVINSI = URI_API + "/provinsi/";
    public static final String ROLE = URI_API + "/role/";
    public static final String SCHEDULE = URI_API + "/meeting/";
    public static final String TAXI_FEE = URI_API + "/taxi-fee/";
    public static final String VEHICLE = URI_API + "/vehicle/";
    public static final String VEHICLE_RENT = URI_API + VEHICLE + "/rent/";
}
