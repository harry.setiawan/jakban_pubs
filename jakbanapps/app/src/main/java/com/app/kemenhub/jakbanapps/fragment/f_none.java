package com.app.kemenhub.jakbanapps.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.kemenhub.jakbanapps.R;
import com.app.kemenhub.jakbanapps.v_main;

public class f_none extends Fragment {

    v_main ac;
    View v;

    TextView subtitle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.f_none, container, false);
        ac = (v_main) getActivity();

        return v;
    }

}
