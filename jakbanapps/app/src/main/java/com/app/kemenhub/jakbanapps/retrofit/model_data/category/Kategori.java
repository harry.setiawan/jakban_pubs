package com.app.kemenhub.jakbanapps.retrofit.model_data.category;

public class Kategori {
	private String id;
	private String name;

	public Kategori() {
	}
	
	public Kategori(String id, String name) {
		this.id = id;
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



}
