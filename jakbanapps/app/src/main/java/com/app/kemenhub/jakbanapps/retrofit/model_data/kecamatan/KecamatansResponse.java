package com.app.kemenhub.jakbanapps.retrofit.model_data.kecamatan;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class KecamatansResponse {
	@SerializedName("code")
	String code;
	@SerializedName("message")
	String message;
	@SerializedName("kecamatan")
	List<Kecamatan> kecamatan;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<Kecamatan> getKecamatan() {
		return kecamatan;
	}
	public void setKecamatan(List<Kecamatan> kecamatan) {
		this.kecamatan = kecamatan;
	}
}
