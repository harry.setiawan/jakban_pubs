package com.app.kemenhub.jakbanapps.db;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit. 
/**
 * Entity mapped to table "USER_CLASS".
 */
public class user_class {

    private Long id;
    private String cls;
    private String type;
    private String desc;

    public user_class() {
    }

    public user_class(Long id) {
        this.id = id;
    }

    public user_class(Long id, String cls, String type, String desc) {
        this.id = id;
        this.cls = cls;
        this.type = type;
        this.desc = desc;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCls() {
        return cls;
    }

    public void setCls(String cls) {
        this.cls = cls;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
