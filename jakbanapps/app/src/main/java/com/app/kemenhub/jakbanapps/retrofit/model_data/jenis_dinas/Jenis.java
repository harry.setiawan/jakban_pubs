package com.app.kemenhub.jakbanapps.retrofit.model_data.jenis_dinas;

public class Jenis {
	private String code;
	private String jenis;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getJenis() {
		return jenis;
	}

	public void setJenis(String jenis) {
		this.jenis = jenis;
	}

}
