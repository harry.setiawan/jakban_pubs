package com.app.kemenhub.jakbanapps.retrofit.model_data.user;

import com.app.kemenhub.jakbanapps.retrofit.model_data.cls.Cls;
import com.app.kemenhub.jakbanapps.retrofit.model_data.position.Position;
import com.app.kemenhub.jakbanapps.retrofit.model_data.role.Role;

import java.util.List;

public class User {
    private String id;

    private String name;

    private String username;

    private String email;

    private String password;

    private String nip;

    private String phone;

    private String birth;

    private String address;

    private Position position;

    private Cls cls;

    private List<Role> role;

    public User(){}

    public User(String id, String name, String username, String email, String password, String nip, String phone,
                String birth, String address, Position position, Cls cls, List<Role> role) {
        super();
        this.id = id;
        this.name = name;
        this.username = username;
        this.email = email;
        this.password = password;
        this.nip = nip;
        this.phone = phone;
        this.birth = birth;
        this.address = address;
        this.position = position;
        this.cls = cls;
        this.role = role;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Cls getCls() {
        return cls;
    }

    public void setCls(Cls cls) {
        this.cls = cls;
    }

    public List<Role> getRole() {
        return role;
    }

    public void setRole(List<Role> role) {
        this.role = role;
    }



}
