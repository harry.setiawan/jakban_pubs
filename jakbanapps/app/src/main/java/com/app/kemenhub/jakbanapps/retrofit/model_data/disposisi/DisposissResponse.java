package com.app.kemenhub.jakbanapps.retrofit.model_data.disposisi;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DisposissResponse {
	@SerializedName("code")
	String code;
	@SerializedName("message")
	String message;
	@SerializedName("disposisi")
	List<Disposiser> disposisi;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<Disposiser> getDisposisi() {
		return disposisi;
	}
	public void setDisposisi(List<Disposiser> disposisi) {
		this.disposisi = disposisi;
	}
}
