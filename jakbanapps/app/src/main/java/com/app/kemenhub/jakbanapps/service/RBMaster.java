package com.app.kemenhub.jakbanapps.service;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.app.kemenhub.jakbanapps.retrofit.Request_Inf;
import com.app.kemenhub.jakbanapps.retrofit.SDM;
import com.app.kemenhub.jakbanapps.retrofit.URLManager;
import com.app.kemenhub.jakbanapps.retrofit.model_data.airplane.AirPlaneTiketsResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.category.CategorysResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.cls.Cls;
import com.app.kemenhub.jakbanapps.retrofit.model_data.dinas.DinassResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.disposisi.DisposissResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.file.FilesResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.folder.FoldersResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.hotel_fee.HotelFeesResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.jenis_dinas.JenissResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.kabupaten.KabupatensResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.kecamatan.KecamatansResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.kelurahan.KelurahansResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.mobile.Mobile;
import com.app.kemenhub.jakbanapps.retrofit.model_data.place.PlacesResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.position.PositionsResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.provinsi.ProvinsisResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.role.Role;
import com.app.kemenhub.jakbanapps.retrofit.model_data.schedule.SchedulesResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.taxi_fee.TaxyFeesResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.user.UserResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.user.UsersResponse;
import com.app.kemenhub.jakbanapps.retrofit.model_data.vehicle.VehiclesResponse;
import com.app.kemenhub.jakbanapps.sys.SessionManager;
import com.app.kemenhub.jakbanapps.v_welcome;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class RBMaster extends BroadcastReceiver {
    Context ctx;
    public static String waktu_offline = "";
    public static String connTag = "";
    public SessionManager SManager;
    public Retrofit retrofit;
    SDM SDM;
    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            this.ctx = context;
            this.SManager = new SessionManager();
            this.SDM = new SDM(context);
            retro();
            resuest();
        }catch (Exception e){}
    }

    private void resuest() {
        ConnectivityManager cm =(ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&  activeNetwork.isConnectedOrConnecting();
        if(isConnected) {
            if(!SManager.getPreferences(this.ctx, "status").equals("")) {
                request_all_airplane();
                request_all_category();
                request_all_class();
                request_all_dinas();
                request_all_disposisi();
                request_all_file();
                request_all_folder();
                request_all_hotel_fee();
                request_all_jenis_dinas();
                request_all_kabupaten();
                request_all_kecamatan();
                //request_all_kelurahan();
                request_all_mobile();
                request_all_place();
                request_all_position();
                request_all_provinsi();
                request_all_role();
                request_all_schedule();
                request_all_taxi_fee();
                request_all_user();
                request_all_vehicle_rent();
                try {
                    request_byID_user(SManager.getPreferences(ctx, "status"));
                }catch (Exception e){}
            }
        }
    }

    private void retro() {
        this.retrofit = new Retrofit.Builder()
                .baseUrl(URLManager.URI_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public void request_byID_user(String id){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<UserResponse> call = client.get_User_id(id);
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                SDM.save_user(response.body().getUser());
                List<Role> role_user = response.body().getUser().getRole();
                for(int i = 0 ; i < role_user.size() ; i++){
                    if(role_user.get(i).getName().toUpperCase().equals("ADMIN")){
                        SManager.setPreferences(ctx, "admin", "true");
                        break;
                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
            }
        });
    }
    //REQUEST ALL
    public void request_all_airplane(){
        Request_Inf request = retrofit.create(Request_Inf.class);
        Call<AirPlaneTiketsResponse> call = request.get_all_airplaneticket();
        call.enqueue(new Callback<AirPlaneTiketsResponse>() {
            @Override
            public void onResponse(Call<AirPlaneTiketsResponse> call, Response<AirPlaneTiketsResponse> response) {
                //////Log.i("SERVER AIRPLANE", response.body().getMessage());
                SDM.save_list_airplane_ticket(response.body().getAirPlaneTiket());
            }

            @Override
            public void onFailure(Call<AirPlaneTiketsResponse> call, Throwable t) {
                //////Log.i("SERVER AIRPLANE", t.getMessage());
                call.cancel();
            }
        });

    }
    public void request_all_category(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<CategorysResponse> call = client.get_all_category();
        call.enqueue(new Callback<CategorysResponse>() {
            @Override
            public void onResponse(Call<CategorysResponse> call, Response<CategorysResponse> response) {
                //////Log.i("SERVER CATEGORY", response.body().getMessage());
                SDM.save_list_category(response.body().getCategory());
            }

            @Override
            public void onFailure(Call<CategorysResponse> call, Throwable t) {
                //////Log.i("SERVER CATEGORY", t.getMessage());
            }
        });
    }
    public void request_all_class(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<List<Cls>> call = client.get_all_class();
        call.enqueue(new Callback<List<Cls>>() {
            @Override
            public void onResponse(Call<List<Cls>> call, Response<List<Cls>> response) {
                //////Log.i("SERVER CLASS", response.body().toString());
                SDM.save_list_class(response.body());
            }

            @Override
            public void onFailure(Call<List<Cls>> call, Throwable t) {
            }
        });
    }
    public void request_all_dinas(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<DinassResponse> call = client.get_all_dinas();
        call.enqueue(new Callback<DinassResponse>() {
            @Override
            public void onResponse(Call<DinassResponse> call, Response<DinassResponse> response) {
                //////Log.i("SERVER DINAS", response.body().getMessage().toString());
                SDM.save_list_dinas(response.body().getDinas());
            }

            @Override
            public void onFailure(Call<DinassResponse> call, Throwable t) {
            }
        });
    }
    public void request_all_disposisi(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<DisposissResponse> call = client.get_all_disposisi();
        call.enqueue(new Callback<DisposissResponse>() {
            @Override
            public void onResponse(Call<DisposissResponse> call, Response<DisposissResponse> response) {
                //////Log.i("SERVER DISPOSISI", response.body().getMessage().toString());
                SDM.save_list_disposisi(response.body().getDisposisi());
            }

            @Override
            public void onFailure(Call<DisposissResponse> call, Throwable t) {
                //////Log.i("SERVER DISPOSISI", "ERROR");
            }
        });
    }
    public void request_all_file(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<FilesResponse> call = client.get_all_file();
        call.enqueue(new Callback<FilesResponse>() {
            @Override
            public void onResponse(Call<FilesResponse> call, Response<FilesResponse> response) {
                //////Log.i("SERVER FILE", response.body().getMessage().toString());
                SDM.save_list_file(response.body().getFile());
            }

            @Override
            public void onFailure(Call<FilesResponse> call, Throwable t) {
            }
        });
    }
    public void request_all_folder(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<FoldersResponse> call = client.get_all_folder();
        call.enqueue(new Callback<FoldersResponse>() {
            @Override
            public void onResponse(Call<FoldersResponse> call, Response<FoldersResponse> response) {
                SDM.save_list_folder(response.body().getFolder());
            }

            @Override
            public void onFailure(Call<FoldersResponse> call, Throwable t) {
            }
        });
    }
    public void request_all_hotel_fee (){
        try {
            Request_Inf client = retrofit.create(Request_Inf.class);
            Call<HotelFeesResponse> call = client.get_all_hotel_fee();
            call.enqueue(new Callback<HotelFeesResponse>() {
                @Override
                public void onResponse(Call<HotelFeesResponse> call, Response<HotelFeesResponse> response) {
                    SDM.save_list_hotel_fee(response.body().getHotelFees());
                    call.cancel();
                }

                @Override
                public void onFailure(Call<HotelFeesResponse> call, Throwable t) {
                    call.cancel();
                }
            });
        }catch (Exception e){}
    }
    public void request_all_jenis_dinas(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<JenissResponse> call = client.get_all_jenis_dinas();
        call.enqueue(new Callback<JenissResponse>() {
            @Override
            public void onResponse(Call<JenissResponse> call, Response<JenissResponse> response) {
                SDM.save_list_jenis_dinas(response.body().getJenis());
            }

            @Override
            public void onFailure(Call<JenissResponse> call, Throwable t) {
            }
        });
    }
    public void request_all_kecamatan(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<KecamatansResponse> call = client.get_all_Kecamatan();
        call.enqueue(new Callback<KecamatansResponse>() {
            @Override
            public void onResponse(Call<KecamatansResponse> call, Response<KecamatansResponse> response) {
                SDM.save_list_kecamatan(response.body().getKecamatan());
            }

            @Override
            public void onFailure(Call<KecamatansResponse> call, Throwable t) {
            }
        });
    }
    public void request_all_kelurahan(){
        try{
            Request_Inf client = retrofit.create(Request_Inf.class);
            Call<KelurahansResponse> call = client.get_all_Kelurahan();
            call.enqueue(new Callback<KelurahansResponse>() {
                @Override
                public void onResponse(Call<KelurahansResponse> call, Response<KelurahansResponse> response) {
                    SDM.save_list_kelurahan(response.body().getKelurahan());
                }

                @Override
                public void onFailure(Call<KelurahansResponse> call, Throwable t) {
                }
            });
        }catch (Exception e){}
    }
    public void request_all_kabupaten(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<KabupatensResponse> call = client.get_all_kabupaten();
        call.enqueue(new Callback<KabupatensResponse>() {
            @Override
            public void onResponse(Call<KabupatensResponse> call, Response<KabupatensResponse> response) {
                SDM.save_list_kabupaten(response.body().getKabupaten());
            }

            @Override
            public void onFailure(Call<KabupatensResponse> call, Throwable t) {
            }
        });
    }
    public void request_all_mobile(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<List<Mobile>> call = client.get_all_Mobile();
        call.enqueue(new Callback<List<Mobile>>() {
            @Override
            public void onResponse(Call<List<Mobile>> call, Response<List<Mobile>> response) {
                SDM.save_list_mobile(response.body());
            }

            @Override
            public void onFailure(Call<List<Mobile>> call, Throwable t) {
            }
        });
    }
    public void request_all_place(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<PlacesResponse> call = client.get_all_Place();
        call.enqueue(new Callback<PlacesResponse>() {
            @Override
            public void onResponse(Call<PlacesResponse> call, Response<PlacesResponse> response) {
                SDM.save_list_place(response.body().getPlace());
            }

            @Override
            public void onFailure(Call<PlacesResponse> call, Throwable t) {
            }
        });
    }
    public void request_all_position(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<PositionsResponse> call = client.get_all_Position();
        call.enqueue(new Callback<PositionsResponse>() {
            @Override
            public void onResponse(Call<PositionsResponse> call, Response<PositionsResponse> response) {
                SDM.save_list_position(response.body().getPosition());
            }

            @Override
            public void onFailure(Call<PositionsResponse> call, Throwable t) {
            }
        });
    }
    public void request_all_provinsi(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<ProvinsisResponse> call = client.get_all_Provinsi();
        call.enqueue(new Callback<ProvinsisResponse>() {
            @Override
            public void onResponse(Call<ProvinsisResponse> call, Response<ProvinsisResponse> response) {
                SDM.save_list_provinsi(response.body().getProvinsi());
            }

            @Override
            public void onFailure(Call<ProvinsisResponse> call, Throwable t) {
            }
        });
    }
    public void request_all_role(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<List<Role>> call = client.get_all_Role();
        call.enqueue(new Callback<List<Role>>() {
            @Override
            public void onResponse(Call<List<Role>> call, Response<List<Role>> response) {
                SDM.save_list_role(response.body());
            }

            @Override
            public void onFailure(Call<List<Role>> call, Throwable t) {
            }
        });
    }
    public void request_all_schedule(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<SchedulesResponse> call = client.get_all_Schedule();
        call.enqueue(new Callback<SchedulesResponse>() {
            @Override
            public void onResponse(Call<SchedulesResponse> call, Response<SchedulesResponse> response) {
                SDM.save_list_meeting(response.body().getSchedule());
            }

            @Override
            public void onFailure(Call<SchedulesResponse> call, Throwable t) {
            }
        });
    }
    public void request_all_taxi_fee(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<TaxyFeesResponse> call = client.get_all_TaxyFee();
        call.enqueue(new Callback<TaxyFeesResponse>() {
            @Override
            public void onResponse(Call<TaxyFeesResponse> call, Response<TaxyFeesResponse> response) {
                SDM.save_taxi_fee(response.body().getTaxyFees());
            }

            @Override
            public void onFailure(Call<TaxyFeesResponse> call, Throwable t) {
            }
        });
    }
    public void request_all_user(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<UsersResponse> call = client.get_all_User();
        call.enqueue(new Callback<UsersResponse>() {
            @Override
            public void onResponse(Call<UsersResponse> call, Response<UsersResponse> response) {
                SDM.save_list_user(response.body().getUser());
                SDM.save_list_user_role(response.body().getUser());
            }

            @Override
            public void onFailure(Call<UsersResponse> call, Throwable t) {
                //roles user.i("SERVER USER", "ERROR");
            }
        });
    }

    public void request_all_vehicle_rent(){
        Request_Inf client = retrofit.create(Request_Inf.class);
        Call<VehiclesResponse> call = client.get_all_Vehicle();
        call.enqueue(new Callback<VehiclesResponse>() {
            @Override
            public void onResponse(Call<VehiclesResponse> call, Response<VehiclesResponse> response) {
                if(response.body() != null) {
                    SDM.save_list_vehicle(response.body().getVehicles());
                }
            }

            @Override
            public void onFailure(Call<VehiclesResponse> call, Throwable t) {
            }
        });
    }
}