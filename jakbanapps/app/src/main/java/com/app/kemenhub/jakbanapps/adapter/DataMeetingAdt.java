package com.app.kemenhub.jakbanapps.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.kemenhub.jakbanapps.R;
import com.app.kemenhub.jakbanapps.db.category;
import com.app.kemenhub.jakbanapps.db.perjalanan_dinas;
import com.app.kemenhub.jakbanapps.db.place;
import com.app.kemenhub.jakbanapps.db.schedule;
import com.app.kemenhub.jakbanapps.sys.utils;
import com.app.kemenhub.jakbanapps.v_event;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class DataMeetingAdt extends RecyclerView.Adapter<DataMeetingAdt.Holder> {
    List<schedule> itemList;
    Context ctx;
    private LayoutInflater inflater;
    Context context;
    private SparseBooleanArray mSelectedItemsIds;
    private int previous = 0;
    private String TAG_LOAD = null;

    private itemClickCallback itemClickCallback;
    int Type;
    ViewGroup parent;

    public interface itemClickCallback{
        void onItemClick(int p);
        void onItemLongClick(int p);
    }

    public void setItemClickCallback(final  itemClickCallback itemClickCallback){
        this.itemClickCallback = itemClickCallback;
    }

    public DataMeetingAdt(String tag, List<schedule> data, Context context){
        this.itemList = data;
        this.TAG_LOAD = tag;
        this.ctx = context;
        this.inflater = LayoutInflater.from(ctx);
        this.context = context;
        mSelectedItemsIds = new SparseBooleanArray();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        WindowManager windowManager = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);
        int width = windowManager.getDefaultDisplay().getWidth();
        view.setLayoutParams(new RecyclerView.LayoutParams(width, RecyclerView.LayoutParams.WRAP_CONTENT));

        Holder derpHolder = new Holder(view);

        return derpHolder;
    }

    public void runer(){
        onCreateViewHolder(parent, Type);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        switch(TAG_LOAD){
            case "DINAS":
                holder.item_meeting.setVisibility(View.GONE);
                holder.item_disposisi.setVisibility(View.GONE);
                holder.card_meeting.setVisibility(View.GONE);
                holder.card_disposisi.setVisibility(View.GONE);
                holder.card_dinas.setVisibility(View.VISIBLE);
                holder.item_dinas.setVisibility(View.VISIBLE);
                break;
            case "MEETING":
                holder.card_meeting.setVisibility(View.VISIBLE);
                holder.card_disposisi.setVisibility(View.GONE);
                holder.card_dinas.setVisibility(View.GONE);
                holder.item_meeting.setVisibility(View.VISIBLE);
                holder.item_disposisi.setVisibility(View.GONE);
                holder.item_dinas.setVisibility(View.GONE);

                if(itemList.get(position).getTitle().length() > 20){
                    holder.meeting_judul.setText("Judul : " +itemList.get(position).getTitle().substring(0,19) + "..");
                }else {
                    holder.meeting_judul.setText("Judul : " +itemList.get(position).getTitle());
                }
                if(itemList.get(position).getPlace().length() > 22){
                    holder.meeting_tempat.setText("Tempat Acara : " + itemList.get(position).getPlace().substring(0,21) + "..");
                }else {
                    holder.meeting_tempat.setText("Tempat Acara : " + itemList.get(position).getPlace());
                }
                try {
                    if(itemList.get(position).getNotes().equals("null") || itemList.get(position).getNotes().equals("")){
                        itemList.get(position).setNotes(" - ");
                    }
                }catch (Exception e){
                    itemList.get(position).setDesc(" - ");
                }
                if(itemList.get(position).getNotes().length() > 25){
                    holder.meeting_category.setText("Catatan : " + itemList.get(position).getNotes().replaceAll("\n", ", ").substring(0,25) + "..");
                }else {
                    holder.meeting_category.setText("Catatan : " + itemList.get(position).getNotes().replaceAll("\n", ", "));
                }
                try {
                    if (itemList.get(position).getDesc().equals("null") || itemList.get(position).getDesc().equals("")) {
                        itemList.get(position).setDesc(" - ");
                    }
                }catch (Exception e){
                    itemList.get(position).setDesc(" - ");
                }
                if(itemList.get(position).getDesc().length() > 30){
                    holder.meeting_desc.setText("Deskripsi : " + itemList.get(position).getDesc().substring(0,30) + "..");
                }else {
                    holder.meeting_desc.setText("Deskripsi : " + itemList.get(position).getDesc());
                }
                holder.meeting_dari.setText("Dari : " + itemList.get(position).getFrom());
                holder.meeting_durasi.setText(set_duration(itemList.get(position).getDatetime_schedule_start(), itemList.get(position).getDatetime_schedule_end()));
                holder.meeting_durasi.setVisibility(View.GONE);

                try {
                    holder.meeting_star.setVisibility(View.VISIBLE);
                }catch (Exception e){
                    holder.meeting_star.setVisibility(View.GONE);
                }
                break;
            case "DISPOSISI":
                holder.card_meeting.setVisibility(View.GONE);
                holder.card_disposisi.setVisibility(View.VISIBLE);
                holder.card_dinas.setVisibility(View.GONE);
                holder.item_meeting.setVisibility(View.GONE);
                holder.item_disposisi.setVisibility(View.VISIBLE);
                holder.item_disposisi.setClickable(true);
                holder.item_dinas.setVisibility(View.GONE);
                break;
            default:
                holder.item_meeting.setVisibility(View.GONE);
                holder.item_disposisi.setVisibility(View.GONE);
                holder.item_dinas.setVisibility(View.GONE);
                break;
        }
    }

    private String set_duration(String datetime_schedule_start, String datetime_schedule_end) {
        String data = "0";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            Date d1 = format.parse(datetime_schedule_start);
            Date d2 = format.parse(datetime_schedule_end);

            long diff = d2.getTime() - d1.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffMinutes = (diff / (60 * 1000) % 60);
            long diffDays = diff / (24 * 60 * 60 * 1000);
            data = String.valueOf(((diffHours * 60) + diffMinutes));
        } catch (Exception e) {
            data = "0";
        }
        return  data;
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));
    }

    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }

    public void selectView(int position, boolean value) {
        if (value) {
            mSelectedItemsIds.put(position, value);
        }else {
            mSelectedItemsIds.delete(position);
        }
        notifyDataSetChanged();
    }


    class Holder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{

        public LinearLayout item;
        public LinearLayout item_dinas;
        public LinearLayout item_meeting;
        public LinearLayout item_disposisi;
        public TextView dinas_code, dinas_agenda, dinas_durasi, dinas_desc, dinas_kota, dinas_biaya;
        public ImageView dinas_star, meeting_star, disposisi_star;
        public TextView meeting_judul, meeting_tempat, meeting_durasi, meeting_category, meeting_desc, meeting_dari;
        public TextView disposisi_surat, disposisi_agenda, disposisi_category, disposisi_desc, disposisi_dari;
        public CardView card_meeting, card_dinas, card_disposisi;

        public Holder(View itemView) {
            super(itemView);
            card_dinas = (CardView) itemView.findViewById(R.id.dinas_line_card);
            card_meeting = (CardView) itemView.findViewById(R.id.meeting_line_card);
            card_disposisi = (CardView) itemView.findViewById(R.id.disposisi_line_card);

            item_dinas = (LinearLayout) itemView.findViewById(R.id.dinas_item);
            dinas_code = (TextView) itemView.findViewById(R.id.dinas_code);
            dinas_agenda = (TextView) itemView.findViewById(R.id.dinas_surat);
            dinas_durasi = (TextView) itemView.findViewById(R.id.dinas_durasi);
            dinas_desc = (TextView) itemView.findViewById(R.id.dinas_deskripsi);
            dinas_kota = (TextView) itemView.findViewById(R.id.dinas_kota);
            dinas_biaya = (TextView) itemView.findViewById(R.id.dinas_biaya);
            dinas_star = (ImageView) itemView.findViewById(R.id.dinas_star);
            meeting_star = (ImageView) itemView.findViewById(R.id.meeting_star);
            disposisi_star = (ImageView) itemView.findViewById(R.id.disposisi_star);

            item_meeting = (LinearLayout) itemView.findViewById(R.id.meeting_item);
            meeting_judul = (TextView) itemView.findViewById(R.id.meeting_judul);
            meeting_tempat = (TextView) itemView.findViewById(R.id.meeting_tempat);
            meeting_durasi = (TextView) itemView.findViewById(R.id.meeting_durasi);
            meeting_category = (TextView) itemView.findViewById(R.id.meeting_category);
            meeting_desc = (TextView) itemView.findViewById(R.id.meeting_deskripsi);
            meeting_dari = (TextView) itemView.findViewById(R.id.meeting_dari);

            item_disposisi = (LinearLayout) itemView.findViewById(R.id.disposisi_item);
            disposisi_surat = (TextView) itemView.findViewById(R.id.disposisi_code);
            disposisi_agenda = (TextView) itemView.findViewById(R.id.disposisi_agenda);
            disposisi_category = (TextView) itemView.findViewById(R.id.disposisi_kategori);
            disposisi_desc = (TextView) itemView.findViewById(R.id.disposisi_deskripsi);
            disposisi_dari = (TextView) itemView.findViewById(R.id.disposisi_dari);
            item = (LinearLayout) itemView.findViewById(R.id.item);
            item.setClickable(true);

            item.setOnClickListener(this);
            item_dinas.setOnClickListener(this);
            item_meeting.setOnClickListener(this);
            item_disposisi.setOnClickListener(this);
            item.setOnLongClickListener(this);
            item_dinas.setOnLongClickListener(this);
            item_meeting.setOnLongClickListener(this);
            item_disposisi.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.item:
                    itemClickCallback.onItemClick(getAdapterPosition());
                    break;
                case R.id.meeting_item:
                    itemClickCallback.onItemClick(getAdapterPosition());
                    break;
                case R.id.disposisi_item:
                    itemClickCallback.onItemClick(getAdapterPosition());
                    break;
            }
        }
        @Override
        public boolean onLongClick(View v) {
            switch (v.getId()){
                case R.id.item:
                    itemClickCallback.onItemLongClick(getAdapterPosition());
                    break;
                case R.id.meeting_item:
                    itemClickCallback.onItemLongClick(getAdapterPosition());
                    break;
                case R.id.disposisi_item:
                    itemClickCallback.onItemLongClick(getAdapterPosition());
                    break;
            }
            return false;
        }
    }
}