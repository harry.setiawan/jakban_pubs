package com.app.kemenhub.jakbanapps.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.kemenhub.jakbanapps.R;
import com.app.kemenhub.jakbanapps.db.perjalanan_dinas;
import com.app.kemenhub.jakbanapps.db.schedule;
import com.app.kemenhub.jakbanapps.sys.FormaterValue;
import com.app.kemenhub.jakbanapps.v_event;
import com.everalbum.roliedex.RoliedexLayout;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.ValueFormatter;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class f_dash extends Fragment implements View.OnClickListener {
    View v;
    v_event activity;
    public BarChart chart;
    public BarData data;
    private String int_bulan_grafik = "0";
    private String tanggal_grafik = "0";
    private String bulan_grafik = "";
    private String tahun_grafik = "";
    private String description_grafik = "Grafik ";
    TextView edt_bulan;
    TextView dinas_prev, dinas_next, meeting_prev, meeting_next;
    SwipeRefreshLayout refresh;
    CardView line;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.f_dash, container, false);
        activity = (v_event) getActivity();

        set_month_year();
        init();
        activity.show_sync();
        activity.rc.request_ByDATE_dinas(tahun_grafik, int_bulan_grafik);
        activity.rc.request_ByDATE_schedule(tahun_grafik, int_bulan_grafik);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                activity.hide_sync();
                set_chart();
            }
        },2000);

        return v;
    }

    public void set_chart() {
        List<schedule> data_chart_schedule =  activity.DBManager.list_scheduleByD(activity.SManager.getPreferences(activity, "status"), tahun_grafik+"-"+int_bulan_grafik);
        try {
            meeting_next.setText(String.valueOf(data_chart_schedule.size()));
        }catch (Exception e){
            meeting_next.setText("---");
        }
        List<perjalanan_dinas> data_chart_dinas =  activity.DBManager.list_perjalanan_dinasByD(activity.SManager.getPreferences(activity, "status"), tahun_grafik+"-"+int_bulan_grafik);
        try {
            dinas_next.setText(String.valueOf(data_chart_dinas.size()));
        }catch (Exception e){
            dinas_next.setText("---");
        }
        data = new BarData(getXAxisValues(), getDataSet());
        chart.setData(data);
        chart.setDescription("Grafik " + tahun_grafik);
        chart.animateXY(2000, 2000);
        chart.invalidate();
        chart.getAxisLeft().setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return String.valueOf((int) Math.floor(value));
            }
        });
        chart.getAxisRight().setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return String.valueOf((int) Math.floor(value));
            }
        });

        edt_bulan.setText(_check_month(bulan_grafik));
    }

    private void set_month_year() {
        SimpleDateFormat sdfTanggal = new SimpleDateFormat("dd");
        SimpleDateFormat sdfMonth = new SimpleDateFormat("MMMM");
        SimpleDateFormat sdfMonth_digit = new SimpleDateFormat("MM");
        SimpleDateFormat sdfYear = new SimpleDateFormat("yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);
        Date tgll = cal.getTime();
        tanggal_grafik = sdfTanggal.format(tgll);
        bulan_grafik = sdfMonth.format(tgll);
        tahun_grafik = sdfYear.format(tgll);
        int_bulan_grafik = sdfMonth_digit.format(tgll);
    }

    private void init() {
        chart = (BarChart) v.findViewById(R.id.chart);
        edt_bulan = (TextView) v.findViewById(R.id.edt_bulan);
        dinas_prev = (TextView) v.findViewById(R.id.dinas_prev);
        dinas_next = (TextView) v.findViewById(R.id.dinas_next);
        meeting_prev = (TextView) v.findViewById(R.id.meeting_prev);
        meeting_next = (TextView) v.findViewById(R.id.meeting_next);
        refresh = (SwipeRefreshLayout) v.findViewById(R.id.swipe_dash);
        line = (CardView) v.findViewById(R.id.line_card);
        if (Build.VERSION.SDK_INT < 21) {
            line.setVisibility(View.GONE);
        }

        refresh.setColorSchemeColors(getResources().getColor(R.color.md_light_green_500_25));
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh.setRefreshing(true);
                activity.rc.request_ByDATE_dinas(tahun_grafik, int_bulan_grafik);
                activity.rc.request_ByDATE_schedule(tahun_grafik, int_bulan_grafik);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        refresh.setRefreshing(false);
                        set_chart();
                    }
                },2000);
            }
        });

        edt_bulan.setClickable(true);
        edt_bulan.setOnClickListener(this);
    }

    private ArrayList<BarDataSet> getDataSet() {
        ArrayList<BarDataSet> dataSets = null;

        List<schedule> data_chart_schedule =  activity.DBManager.list_scheduleByD(activity.SManager.getPreferences(activity, "status"), tahun_grafik+"-"+int_bulan_grafik);
        List<perjalanan_dinas> data_chart_dinas =  activity.DBManager.list_perjalanan_dinasByD(activity.SManager.getPreferences(activity, "status"), tahun_grafik+"-"+int_bulan_grafik);

        ArrayList<BarEntry> valueSet_Sudah = new ArrayList<>();
        int meeting_belum = 0, meeting_sudah = 0, dinas_belum = 0, dinas_sudah = 0;
        for(int i = 0; i < data_chart_schedule.size(); i++){
            try {
                String tgl_data = data_chart_schedule.get(i).getDatetime_schedule_start();
                SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
                Date TglPinjam = date.parse(tgl_data);

                SimpleDateFormat sdfYear = new SimpleDateFormat("yyyy");
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, 0);
                Date tgll = cal.getTime();

                if(tgll.getTime() > TglPinjam.getTime()){
                    meeting_sudah ++;
                }else{
                    meeting_belum ++;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        for(int i = 0; i < data_chart_dinas.size(); i++){

            try {
                String tgl_data = data_chart_dinas.get(i).getDate_departure();
                SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
                Date TglPinjam = date.parse(tgl_data);

                SimpleDateFormat sdfYear = new SimpleDateFormat("yyyy");
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, 0);
                Date tgll = cal.getTime();
                if(tgll.getTime() > TglPinjam.getTime()){
                    dinas_sudah ++;
                }else{
                    dinas_belum ++;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        BarEntry v1e1 = new BarEntry(dinas_sudah, 0);
        valueSet_Sudah.add(v1e1);
        BarEntry v1e2 = new BarEntry(meeting_sudah, 1);
        valueSet_Sudah.add(v1e2);

        ArrayList<BarEntry> valueSet_Belum = new ArrayList<>();
        BarEntry v2e1 = new BarEntry(dinas_belum, 0);
        valueSet_Belum.add(v2e1);
        BarEntry v2e2 = new BarEntry(meeting_belum, 1);
        valueSet_Belum.add(v2e2);

        BarDataSet barDataSet1 = new BarDataSet(valueSet_Sudah, "Telah Terlaksana");
        barDataSet1.setColor(activity.getResources().getColor(R.color.md_red_500));
        BarDataSet barDataSet2 = new BarDataSet(valueSet_Belum, "Belum Terlaksana");
        barDataSet2.setColor(activity.getResources().getColor(R.color.md_orange_500));

        barDataSet1.setValueFormatter(new FormaterValue());
        barDataSet2.setValueFormatter(new FormaterValue());

        dataSets = new ArrayList<>();
        dataSets.add(barDataSet1);
        dataSets.add(barDataSet2);
        return dataSets;
    }

    private ArrayList<String> getXAxisValues() {
        ArrayList<String> xAxis = new ArrayList<>();
        xAxis.add("DINAS");
        xAxis.add("MEETING");
        return xAxis;
    }

    private String month_name(int n){
        String [] month_name = new DateFormatSymbols().getMonths();
        return (n >= 0 && n <= 11) ? month_name[n] : "NOT";
    }

    public void ShowAlertDialogMonth() {
        List<String> mnt = new ArrayList<String>();
        for(int i = 0 ; i < 12 ; i++) {
            mnt.add(_check_month(month_name(i)));
        }
        final CharSequence[] mnts = mnt.toArray(new String[mnt.size()]);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        dialogBuilder.setTitle("Pilih Bulan");
        dialogBuilder.setItems(mnts, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                refresh.setRefreshing(true);
                bulan_grafik =_check_month(mnts[item].toString());
                edt_bulan.setText(_check_month(bulan_grafik));
                int_bulan_grafik = "0" + String.valueOf(item + 1);
                ////Log.i("ADA", tahun_grafik + " - " + int_bulan_grafik);
                activity.rc.request_ByDATE_dinas(tahun_grafik, int_bulan_grafik);
                activity.rc.request_ByDATE_schedule(tahun_grafik, int_bulan_grafik);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        refresh.setRefreshing(false);
                        set_chart();
                    }
                },2000);
            }
        });
        AlertDialog alertDialogObject = dialogBuilder.create();
        alertDialogObject.show();
    }

    private String _check_month(String bulan){
        String nm_bln = null;
        switch (bulan){
            case "January":
                nm_bln = "Januari";
                int_bulan_grafik = "01";
                break;
            case "February":
                nm_bln = "Februari";
                int_bulan_grafik = "02";
                break;
            case "March":
                nm_bln = "Maret";
                int_bulan_grafik = "03";
                break;
            case "April":
                nm_bln = "April";
                int_bulan_grafik = "04";
                break;
            case "May":
                nm_bln = "Mei";
                int_bulan_grafik = "05";
                break;
            case "June":
                nm_bln = "Juni";
                int_bulan_grafik = "06";
                break;
            case "July":
                nm_bln = "Juli";
                int_bulan_grafik = "07";
                break;
            case "August":
                nm_bln = "Agustus";
                int_bulan_grafik = "08";
                break;
            case "September":
                nm_bln = "September";
                int_bulan_grafik = "09";
                break;
            case "October":
                nm_bln = "Oktober";
                int_bulan_grafik = "10";
                break;
            case "November":
                nm_bln = "November";
                int_bulan_grafik = "11";
                break;
            case "December":
                nm_bln = "Desember";
                int_bulan_grafik = "12";
                break;
        }
        if(nm_bln == null){
            return bulan;
        }else{
            return nm_bln;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.edt_bulan:
                ShowAlertDialogMonth();
                break;
        }
    }
}
