package com.app.kemenhub.jakbanapps.db.manager;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.app.kemenhub.jakbanapps.db.DaoMaster;
import com.app.kemenhub.jakbanapps.db.DaoSession;
import com.app.kemenhub.jakbanapps.db.airplane_ticket;
import com.app.kemenhub.jakbanapps.db.airplane_ticketDao;
import com.app.kemenhub.jakbanapps.db.category;
import com.app.kemenhub.jakbanapps.db.categoryDao;
import com.app.kemenhub.jakbanapps.db.disposition;
import com.app.kemenhub.jakbanapps.db.dispositionDao;
import com.app.kemenhub.jakbanapps.db.file;
import com.app.kemenhub.jakbanapps.db.fileDao;
import com.app.kemenhub.jakbanapps.db.folder;
import com.app.kemenhub.jakbanapps.db.folderDao;
import com.app.kemenhub.jakbanapps.db.hotel_fee;
import com.app.kemenhub.jakbanapps.db.hotel_feeDao;
import com.app.kemenhub.jakbanapps.db.kabupaten;
import com.app.kemenhub.jakbanapps.db.kabupatenDao;
import com.app.kemenhub.jakbanapps.db.kecamatan;
import com.app.kemenhub.jakbanapps.db.kecamatanDao;
import com.app.kemenhub.jakbanapps.db.kelurahan;
import com.app.kemenhub.jakbanapps.db.kelurahanDao;
import com.app.kemenhub.jakbanapps.db.notif;
import com.app.kemenhub.jakbanapps.db.notifDao;
import com.app.kemenhub.jakbanapps.db.perjalanan_dinas;
import com.app.kemenhub.jakbanapps.db.perjalanan_dinasDao;
import com.app.kemenhub.jakbanapps.db.perjalanan_dinas_jenis;
import com.app.kemenhub.jakbanapps.db.perjalanan_dinas_jenisDao;
import com.app.kemenhub.jakbanapps.db.place;
import com.app.kemenhub.jakbanapps.db.placeDao;
import com.app.kemenhub.jakbanapps.db.position;
import com.app.kemenhub.jakbanapps.db.positionDao;
import com.app.kemenhub.jakbanapps.db.provinsi;
import com.app.kemenhub.jakbanapps.db.provinsiDao;
import com.app.kemenhub.jakbanapps.db.roles;
import com.app.kemenhub.jakbanapps.db.rolesDao;
import com.app.kemenhub.jakbanapps.db.schedule;
import com.app.kemenhub.jakbanapps.db.scheduleDao;
import com.app.kemenhub.jakbanapps.db.taxi_fee;
import com.app.kemenhub.jakbanapps.db.taxi_feeDao;
import com.app.kemenhub.jakbanapps.db.user_class;
import com.app.kemenhub.jakbanapps.db.user_classDao;
import com.app.kemenhub.jakbanapps.db.user_mobile;
import com.app.kemenhub.jakbanapps.db.user_mobileDao;
import com.app.kemenhub.jakbanapps.db.user_role;
import com.app.kemenhub.jakbanapps.db.user_roleDao;
import com.app.kemenhub.jakbanapps.db.users;
import com.app.kemenhub.jakbanapps.db.usersDao;
import com.app.kemenhub.jakbanapps.db.vehicle_rent;
import com.app.kemenhub.jakbanapps.db.vehicle_rentDao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import de.greenrobot.dao.async.AsyncOperation;
import de.greenrobot.dao.async.AsyncOperationListener;
import de.greenrobot.dao.async.AsyncSession;
import de.greenrobot.dao.query.QueryBuilder;

public class DManager implements IDManager, AsyncOperationListener {

    private static final String TAG = DManager.class.getCanonicalName();
    private static DManager instance;
    private Context context;
    private DaoMaster.DevOpenHelper mHelper;
    private SQLiteDatabase database;
    private DaoMaster daoMaster;
    private DaoSession daoSession;
    private AsyncSession asyncSession;
    private List<AsyncOperation> completedOperations;

    public DManager(final Context context) {
        this.context = context;
        mHelper = new DaoMaster.DevOpenHelper(this.context, "kemenhub-jakban-database", null);
        completedOperations = new CopyOnWriteArrayList<AsyncOperation>();
    }
    public static DManager getInstance(Context context) {
        if (instance == null) {
            instance = new DManager(context);
        }
        return instance;
    }
    @Override
    public void onAsyncOperationCompleted(AsyncOperation operation) {
        completedOperations.add(operation);
    }
    @Override
    public void insert_airplane_ticket(final airplane_ticket airplane_ticket) {
        try {
            if (airplane_ticket != null) {
                openWritableDb();
                final airplane_ticketDao dDao = daoSession.getAirplane_ticketDao();
                dDao.insertOrReplace(airplane_ticket);
                ////Log.d(TAG, "airplane_ticket add : " + airplane_ticket.getId() + " to the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_airplane_ticket_InTX(final List<airplane_ticket> airplane_ticket) {
        try {
            if (airplane_ticket != null) {
                openWritableDb();
                final airplane_ticketDao dDao = daoSession.getAirplane_ticketDao();
                dDao.insertOrReplaceInTx(airplane_ticket);
                ////Log.d(TAG, "airplane_ticket add : " + " to the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public List<airplane_ticket> list_airplane_ticket() {
        List<airplane_ticket> d = null;
        try {
            openReadableDb();
            airplane_ticketDao dDao = daoSession.getAirplane_ticketDao();
            d = dDao.loadAll();
            ////Log.i(TAG, "airplane_ticket get : " + d.get(0).getId().toString());
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public void update_airplane_ticket(airplane_ticket airplane_ticket) {
        try {
            if (airplane_ticket != null) {
                openWritableDb();
                airplane_ticketDao dDao = daoSession.getAirplane_ticketDao();
                dDao.update(airplane_ticket);
                ////Log.d(TAG, "airplane_ticket put : " + airplane_ticket.getId() + " from the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void delete_airplane_ticketById(String id) {
        try {
            openWritableDb();
            airplane_ticketDao dDao = daoSession.getAirplane_ticketDao();
            QueryBuilder<airplane_ticket> queryBuilder = dDao.queryBuilder().where(airplane_ticketDao.Properties.Id.eq(id));
            List<airplane_ticket> dToDelete = queryBuilder.list();
            for (airplane_ticket d : dToDelete) {
                dDao.delete(d);
            }
            daoSession.clear();
            ////Log.d(TAG, "airplane_ticket del : " + dToDelete.size() + " entry. " + id + " from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public airplane_ticket get_airplane_ticketById(String id) {
        airplane_ticket d = null;
        try {
            openReadableDb();
            airplane_ticketDao dDao = daoSession.getAirplane_ticketDao();
            d = dDao.load(Long.parseLong(id));
            daoSession.clear();
            ////Log.i(TAG, "airplane_ticket gid : " + d.getId().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }
    @Override
    public void delete_airplane_ticket() {
        try {
            openWritableDb();
            airplane_ticketDao dDao = daoSession.getAirplane_ticketDao();
            dDao.deleteAll();
            daoSession.clear();
            ////Log.d(TAG, "Delete all airplane_ticket from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public List<disposition> list_dispositionByFAC(String forward,String agenda,String category) {
        List<disposition> d = null;
        try {
            openReadableDb();
            dispositionDao dDao = daoSession.getDispositionDao();

            d = dDao.queryBuilder()
                    .whereOr(dispositionDao.Properties.Forward_to_user_id.like("%" + forward + "%"),
                            dispositionDao.Properties.No_agenda.like("%" + agenda + "%"),
                            dispositionDao.Properties.Category_id.like("%" + category + "%")).list();

            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public List<disposition> list_dispositionByTO(String forward) {
        List<disposition> d = null;
        try {
            openReadableDb();
            dispositionDao dDao = daoSession.getDispositionDao();

            d = dDao.queryBuilder()
                    .whereOr(dispositionDao.Properties.Forward_to_user_id.like("%" + forward + "%"),
                            dispositionDao.Properties.Forward_to_user_id.isNull()
                    )
                    .orderDesc(dispositionDao.Properties.Id)
                    .list();

            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public List<disposition> list_dispositionByTOdanClass(String forward, String cls) {
        List<disposition> d = new ArrayList<>();
        try {
            openReadableDb();
            dispositionDao dDao = daoSession.getDispositionDao();

            d.addAll(dDao.queryBuilder()
                    .where(dispositionDao.Properties.Forward_to_user_id.like("%" + forward + "%"),
                            dispositionDao.Properties.Cls.like("%" + cls + "%")).list());
            d.addAll(dDao.queryBuilder()
                    .where(dispositionDao.Properties.Forward_to_user_id.isNull(),
                            dispositionDao.Properties.Cls.like("%" + cls + "%")).list());
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public List<disposition> list_dispositionByCAT(String category) {
        List<disposition> d = null;
        try {
            openReadableDb();
            dispositionDao dDao = daoSession.getDispositionDao();

            d = dDao.queryBuilder()
                    .where(dispositionDao.Properties.Category_id.like("%" + category + "%"),
                            dispositionDao.Properties.Category_id.like("%" + "" + "%"),
                            dispositionDao.Properties.Category_id.like("%" + "null" + "%"),
                            dispositionDao.Properties.Category_id.like(null)).list();

            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public void insert_disposition(final disposition disposition) {
        try {
            if (disposition != null) {
                openWritableDb();
                final dispositionDao dDao = daoSession.getDispositionDao();
                dDao.insertOrReplace(disposition);
                ////Log.d(TAG, "disposition add : " + disposition.getId() + " to the schema.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_disposition_InTX(List<disposition> disposition) {
        try {
            if (disposition != null) {
                openWritableDb();
                final dispositionDao dDao = daoSession.getDispositionDao();
                dDao.insertOrReplaceInTx(disposition);
                ////Log.d(TAG, "disposition add : " + " to the schema.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public List<disposition> list_disposition() {
        List<disposition> d = null;
        try {
            openReadableDb();
            dispositionDao dDao = daoSession.getDispositionDao();
            d = dDao.loadAll();
            ////Log.i(TAG, "disposition get : " + d.get(0).getId().toString());
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public void update_disposition(disposition disposition) {
        try {
            if (disposition != null) {
                openWritableDb();
                dispositionDao dDao = daoSession.getDispositionDao();
                dDao.update(disposition);
                ////Log.d(TAG, "disposition put : " + disposition.getId() + " from the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void delete_dispositionById(String id) {
        try {
            openWritableDb();
            dispositionDao dDao = daoSession.getDispositionDao();
            QueryBuilder<disposition> queryBuilder = dDao.queryBuilder().where(dispositionDao.Properties.Id.eq(id));
            List<disposition> dToDelete = queryBuilder.list();
            for (disposition d : dToDelete) {
                dDao.delete(d);
            }
            daoSession.clear();
            ////Log.d(TAG, "disposition del : " + dToDelete.size() + " entry. " + id + " from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public disposition get_dispositionById(String id) {
        disposition d = null;
        try {
            openReadableDb();
            dispositionDao dDao = daoSession.getDispositionDao();
            d = dDao.load(Long.parseLong(id));
            daoSession.clear();
            ////Log.i(TAG, "disposition gid : " + d.getId().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }
    @Override
    public void delete_disposition() {
        try {
            openWritableDb();
            dispositionDao dDao = daoSession.getDispositionDao();
            dDao.deleteAll();
            daoSession.clear();
            ////Log.d(TAG, "Delete all disposition from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public List<disposition> list_dispositionNotif() {
        List<disposition> d = null;
        try {
            openReadableDb();
            dispositionDao dDao = daoSession.getDispositionDao();

            d = dDao.queryBuilder().where(dispositionDao.Properties.Notif.eq(true))
                    .orderDesc(dispositionDao.Properties.Id)
                    .list();

            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public void insert_file(final file file) {
        try {
            if (file != null) {
                openWritableDb();
                final fileDao dDao = daoSession.getFileDao();
                dDao.insertOrReplace(file);
                ////Log.d(TAG, "disposition add : " + file.getId() + " to the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_file_InTX(List<file> file) {
        try {
            if (file != null) {
                openWritableDb();
                final fileDao dDao = daoSession.getFileDao();
                dDao.insertOrReplaceInTx(file);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public List<file> list_file() {
        List<file> d = null;
        try {
            openReadableDb();
            fileDao dDao = daoSession.getFileDao();
            d = dDao.loadAll();
            ////Log.i(TAG, "file get : " + d.get(0).getId().toString());
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public void update_file(file file) {
        try {
            if (file != null) {
                openWritableDb();
                fileDao dDao = daoSession.getFileDao();
                dDao.update(file);
                ////Log.d(TAG, "file put : " + file.getId() + " from the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void delete_fileById(String id) {
        try {
            openWritableDb();
            fileDao dDao = daoSession.getFileDao();
            QueryBuilder<file> queryBuilder = dDao.queryBuilder().where(fileDao.Properties.Id.eq(id));
            List<file> dToDelete = queryBuilder.list();
            for (file d : dToDelete) {
                dDao.delete(d);
            }
            daoSession.clear();
            ////Log.d(TAG, "file del : " + dToDelete.size() + " entry. " + id + " from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public file get_fileById(String id) {
        file d = null;
        try {
            openReadableDb();
            fileDao dDao = daoSession.getFileDao();
            d = dDao.load(Long.parseLong(id));
            daoSession.clear();
            ////Log.i(TAG, "file gid : " + d.getId().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }
    @Override
    public void delete_file() {
        try {
            openWritableDb();
            fileDao dDao = daoSession.getFileDao();
            dDao.deleteAll();
            daoSession.clear();
            ////Log.d(TAG, "Delete all file from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_category(final category category) {
        try {
            if (category != null) {
                openWritableDb();
                final categoryDao dDao = daoSession.getCategoryDao();
                dDao.insertOrReplace(category);
                ////Log.d(TAG, "category add : " + category.getId() + " to the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_category_InTX(List<category> category) {
        try {
            if (category != null) {
                openWritableDb();
                final categoryDao dDao = daoSession.getCategoryDao();
                dDao.insertOrReplaceInTx(category);
                ////Log.d(TAG, "category add : " + " to the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public List<category> list_category() {
        List<category> d = null;
        try {
            openReadableDb();
            categoryDao dDao = daoSession.getCategoryDao();
            d = dDao.loadAll();
            ////Log.i(TAG, "file get : " + d.get(0).getId().toString());
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public void update_category(category category) {
        try {
            if (category != null) {
                openWritableDb();
                categoryDao dDao = daoSession.getCategoryDao();
                dDao.update(category);
                ////Log.d(TAG, "category put : " + category.getId() + " from the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void delete_categoryById(String id) {
        try {
            openWritableDb();
            categoryDao dDao = daoSession.getCategoryDao();
            QueryBuilder<category> queryBuilder = dDao.queryBuilder().where(categoryDao.Properties.Id.eq(id));
            List<category> dToDelete = queryBuilder.list();
            for (category d : dToDelete) {
                dDao.delete(d);
            }
            daoSession.clear();
            ////Log.d(TAG, "category del : " + dToDelete.size() + " entry. " + id + " from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public category get_categoryById(String id) {
        category d = null;
        try {
            openReadableDb();
            categoryDao dDao = daoSession.getCategoryDao();
            d = dDao.load(Long.parseLong(id));
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }
    @Override
    public void delete_category() {
        try {
            openWritableDb();
            categoryDao dDao = daoSession.getCategoryDao();
            dDao.deleteAll();
            daoSession.clear();
            ////Log.d(TAG, "Delete all category from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_jenis_dinas(final perjalanan_dinas_jenis perjalanan_dinas_jenis) {
        try {
            if (perjalanan_dinas_jenis != null) {
                openWritableDb();
                final perjalanan_dinas_jenisDao dDao = daoSession.getPerjalanan_dinas_jenisDao();
                dDao.insertOrReplace(perjalanan_dinas_jenis);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_jenis_dinas_InTX(List<perjalanan_dinas_jenis> perjalanan_dinas_jenis) {
        try {
            if (perjalanan_dinas_jenis != null) {
                openWritableDb();
                final perjalanan_dinas_jenisDao dDao = daoSession.getPerjalanan_dinas_jenisDao();
                dDao.insertOrReplaceInTx(perjalanan_dinas_jenis);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public List<perjalanan_dinas_jenis> list_jenis_dinas() {
        List<perjalanan_dinas_jenis> d = null;
        try {
            openReadableDb();
            perjalanan_dinas_jenisDao dDao = daoSession.getPerjalanan_dinas_jenisDao();
            d = dDao.loadAll();
            ////Log.i(TAG, "perjalanan_dinas_jenis get : " + d.get(0).getCode().toString());
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public void update_jenis_dinas(perjalanan_dinas_jenis perjalanan_dinas_jenis) {
        try {
            if (perjalanan_dinas_jenis != null) {
                openWritableDb();
                perjalanan_dinas_jenisDao dDao = daoSession.getPerjalanan_dinas_jenisDao();
                dDao.update(perjalanan_dinas_jenis);
                ////Log.d(TAG, "perjalanan_dinas_jenis put : " + perjalanan_dinas_jenis.getCode() + " from the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void delete_jenis_dinasById(String id) {
        try {
            openWritableDb();
            perjalanan_dinas_jenisDao dDao = daoSession.getPerjalanan_dinas_jenisDao();
            QueryBuilder<perjalanan_dinas_jenis> queryBuilder = dDao.queryBuilder().where(categoryDao.Properties.Id.eq(id));
            List<perjalanan_dinas_jenis> dToDelete = queryBuilder.list();
            for (perjalanan_dinas_jenis d : dToDelete) {
                dDao.delete(d);
            }
            daoSession.clear();
            ////Log.d(TAG, "perjalanan_dinas_jenis del : " + dToDelete.size() + " entry. " + id + " from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public List<perjalanan_dinas_jenis> get_jenis_dinasById(List<perjalanan_dinas_jenis> id) {
        List<perjalanan_dinas_jenis> d = null;
        try {
            openReadableDb();
            perjalanan_dinas_jenisDao dDao = daoSession.getPerjalanan_dinas_jenisDao();
            d = dDao.queryBuilder()
                    .where(perjalanan_dinas_jenisDao.Properties.Code.in(id)).list();
            daoSession.clear();
            ////Log.i(TAG, "perjalanan_dinas_jenis gid : " + d.get(0).getCode().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }

    @Override
    public perjalanan_dinas_jenis get_jenis_dinasById(String id) {
        List<perjalanan_dinas_jenis> d = null;
        try {
            openReadableDb();
            perjalanan_dinas_jenisDao dDao = daoSession.getPerjalanan_dinas_jenisDao();
            d = dDao.queryBuilder()
                    .where(perjalanan_dinas_jenisDao.Properties.Code.eq(id)).list();
            daoSession.clear();
            ////Log.i(TAG, "perjalanan_dinas_jenis gid : " + d.get(0).getCode().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d.get(0);
    }
    @Override
    public void delete_jenis_dinas() {
        try {
            openWritableDb();
            perjalanan_dinas_jenisDao dDao = daoSession.getPerjalanan_dinas_jenisDao();
            dDao.deleteAll();
            daoSession.clear();
            ////Log.d(TAG, "Delete all perjalanan_dinas_jenis from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_folder(final folder folder) {
        try {
            if (folder != null) {
                openWritableDb();
                final folderDao dDao = daoSession.getFolderDao();
                dDao.insertOrReplace(folder);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_folder_InTX(List<folder> folder) {
        try {
            if (folder != null) {
                openWritableDb();
                final folderDao dDao = daoSession.getFolderDao();
                dDao.insertOrReplaceInTx(folder);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public List<folder> list_folder() {
        List<folder> d = null;
        try {
            openReadableDb();
            folderDao dDao = daoSession.getFolderDao();
            d = dDao.loadAll();
            ////Log.i(TAG, "folder get : " + d.get(0).getId().toString());
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public void update_folder(folder folder) {
        try {
            if (folder != null) {
                openWritableDb();
                folderDao dDao = daoSession.getFolderDao();
                dDao.update(folder);
                ////Log.d(TAG, "folder put : " + folder.getId() + " from the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void delete_folderById(String id) {
        try {
            openWritableDb();
            folderDao dDao = daoSession.getFolderDao();
            QueryBuilder<folder> queryBuilder = dDao.queryBuilder().where(folderDao.Properties.Id.eq(id));
            List<folder> dToDelete = queryBuilder.list();
            for (folder d : dToDelete) {
                dDao.delete(d);
            }
            daoSession.clear();
            ////Log.d(TAG, "folder del : " + dToDelete.size() + " entry. " + id + " from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public folder get_folderById(String id) {
        folder d = null;
        try {
            openReadableDb();
            folderDao dDao = daoSession.getFolderDao();
            d = dDao.load(Long.parseLong(id));
            daoSession.clear();
            ////Log.i(TAG, "folder gid : " + d.getId().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }
    @Override
    public void delete_folder() {
        try {
            openWritableDb();
            folderDao dDao = daoSession.getFolderDao();
            dDao.deleteAll();
            daoSession.clear();
            ////Log.d(TAG, "Delete all folder from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_hotel_fee(final hotel_fee hotel_fee) {
        try {
            if (hotel_fee != null) {
                openWritableDb();
                final hotel_feeDao dDao = daoSession.getHotel_feeDao();
                dDao.insertOrReplace(hotel_fee);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_hotel_fee_InTX(List<hotel_fee> hotel_fee) {
        try {
            if (hotel_fee != null) {
                openWritableDb();
                final hotel_feeDao dDao = daoSession.getHotel_feeDao();
                dDao.insertOrReplaceInTx(hotel_fee);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public List<hotel_fee> list_hotel_fee() {
        List<hotel_fee> d = null;
        try {
            openReadableDb();
            hotel_feeDao dDao = daoSession.getHotel_feeDao();
            d = dDao.loadAll();
            ////Log.i(TAG, "hotel_fee get : " + d.get(0).getId().toString());
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public void update_hotel_fee(hotel_fee hotel_fee) {
        try {
            if (hotel_fee != null) {
                openWritableDb();
                hotel_feeDao dDao = daoSession.getHotel_feeDao();
                dDao.update(hotel_fee);
                ////Log.d(TAG, "hotel_fee put : " + hotel_fee.getId() + " from the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void delete_hotel_feeById(String id) {
        try {
            openWritableDb();
            hotel_feeDao dDao = daoSession.getHotel_feeDao();
            QueryBuilder<hotel_fee> queryBuilder = dDao.queryBuilder().where(hotel_feeDao.Properties.Id.eq(id));
            List<hotel_fee> dToDelete = queryBuilder.list();
            for (hotel_fee d : dToDelete) {
                dDao.delete(d);
            }
            daoSession.clear();
            ////Log.d(TAG, "hotel_fee del : " + dToDelete.size() + " entry. " + id + " from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public hotel_fee get_hotel_feeById(String id) {
        hotel_fee d = null;
        try {
            openReadableDb();
            hotel_feeDao dDao = daoSession.getHotel_feeDao();
            d = dDao.load(Long.parseLong(id));
            daoSession.clear();
            ////Log.i(TAG, "hotel_fee gid : " + d.getId().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }
    @Override
    public void delete_hotel_fee() {
        try {
            openWritableDb();
            hotel_feeDao dDao = daoSession.getHotel_feeDao();
            dDao.deleteAll();
            daoSession.clear();
            ////Log.d(TAG, "Delete all hotel_fee from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void insert_user_role(final user_role user_role) {
        try {
            if (user_role != null) {
                openWritableDb();
                final user_roleDao dDao = daoSession.getUser_roleDao();
                dDao.insertOrReplace(user_role);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_user_role_InTX(List<user_role> user_role) {
        try {
            if (user_role != null) {
                openWritableDb();
                final user_roleDao dDao = daoSession.getUser_roleDao();
                dDao.insertOrReplaceInTx(user_role);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public List<user_role> list_user_role() {
        List<user_role> d = null;
        try {
            openReadableDb();
            user_roleDao dDao = daoSession.getUser_roleDao();
            d = dDao.loadAll();
            ////Log.i(TAG, "user_role get : " + d.get(0).getId().toString());
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public void update_user_role(user_role user_role) {
        try {
            if (user_role != null) {
                openWritableDb();
                user_roleDao dDao = daoSession.getUser_roleDao();
                dDao.update(user_role);
                ////Log.d(TAG, "user_role put : " + user_role.getId() + " from the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void delete_user_roleById(String id) {
        try {
            openWritableDb();
            user_roleDao dDao = daoSession.getUser_roleDao();
            QueryBuilder<user_role> queryBuilder = dDao.queryBuilder().where(user_roleDao.Properties.Id.eq(id));
            List<user_role> dToDelete = queryBuilder.list();
            for (user_role d : dToDelete) {
                dDao.delete(d);
            }
            daoSession.clear();
            ////Log.d(TAG, "user_role del : " + dToDelete.size() + " entry. " + id + " from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public List<user_role> get_user_roleById(String id) {
        List<user_role> d = null;
        try {
            openReadableDb();
            user_roleDao dDao = daoSession.getUser_roleDao();
            d = dDao.queryBuilder().where(user_roleDao.Properties.User_id.eq(id)).list();
            daoSession.clear();
            //////Log.i(TAG, "user_role gid : " + d.getId().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }
    @Override
    public void delete_user_role() {
        try {
            openWritableDb();
            user_roleDao dDao = daoSession.getUser_roleDao();
            dDao.deleteAll();
            daoSession.clear();
            ////Log.d(TAG, "Delete all user_role from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void insert_kabupaten(final kabupaten kabupaten) {
        try {
            if (kabupaten != null) {
                openWritableDb();
                final kabupatenDao dDao = daoSession.getKabupatenDao();
                dDao.insertOrReplace(kabupaten);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_kabupaten_InTX(List<kabupaten> kabupaten) {
        try {
            if (kabupaten != null) {
                openWritableDb();
                final kabupatenDao dDao = daoSession.getKabupatenDao();
                dDao.insertOrReplaceInTx(kabupaten);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public List<kabupaten> list_kabupaten() {
        List<kabupaten> d = null;
        try {
            openReadableDb();
            kabupatenDao dDao = daoSession.getKabupatenDao();
            d = dDao.loadAll();
            ////Log.i(TAG, "kabupaten get : " + d.get(0).getId().toString());
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public void update_kabupaten(kabupaten kabupaten) {
        try {
            if (kabupaten != null) {
                openWritableDb();
                kabupatenDao dDao = daoSession.getKabupatenDao();
                dDao.update(kabupaten);
                ////Log.d(TAG, "kabupaten put : " + kabupaten.getId() + " from the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void delete_kabupatenById(String id) {
        try {
            openWritableDb();
            kabupatenDao dDao = daoSession.getKabupatenDao();
            QueryBuilder<kabupaten> queryBuilder = dDao.queryBuilder().where(kabupatenDao.Properties.Id.eq(id));
            List<kabupaten> dToDelete = queryBuilder.list();
            for (kabupaten d : dToDelete) {
                dDao.delete(d);
            }
            daoSession.clear();
            ////Log.d(TAG, "kabupaten del : " + dToDelete.size() + " entry. " + id + " from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public kabupaten get_kabupatenById(String id) {
        kabupaten d = null;
        try {
            openReadableDb();
            kabupatenDao dDao = daoSession.getKabupatenDao();
            d = dDao.load(Long.parseLong(id));
            daoSession.clear();
            ////Log.i(TAG, "kabupaten gid : " + d.getId().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }
    @Override
    public void delete_kabupaten() {
        try {
            openWritableDb();
            kabupatenDao dDao = daoSession.getKabupatenDao();
            dDao.deleteAll();
            daoSession.clear();
            ////Log.d(TAG, "Delete all kabupaten from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_kecamatan(kecamatan kecamatan) {
        try {
            if (kecamatan != null) {
                openWritableDb();
                kecamatanDao dDao = daoSession.getKecamatanDao();
                dDao.insertOrReplace(kecamatan);
                ////Log.d(TAG, "kecamatan add : " + kecamatan.getId() + " to the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_kecamatan_InTX(List<kecamatan> kecamatan) {
        try {
            if (kecamatan != null) {
                openWritableDb();
                kecamatanDao dDao = daoSession.getKecamatanDao();
                dDao.insertOrReplaceInTx(kecamatan);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public List<kecamatan> list_kecamatan() {
        List<kecamatan> d = null;
        try {
            openReadableDb();
            kecamatanDao dDao = daoSession.getKecamatanDao();
            d = dDao.loadAll();
            ////Log.i(TAG, "kecamatan get : " + d.get(0).getId().toString());
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public void update_kecamatan(kecamatan kecamatan) {
        try {
            if (kecamatan != null) {
                openWritableDb();
                kecamatanDao dDao = daoSession.getKecamatanDao();
                dDao.update(kecamatan);
                ////Log.d(TAG, "kecamatan put : " + kecamatan.getId() + " from the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void delete_kecamatanById(String id) {
        try {
            openWritableDb();
            kecamatanDao dDao = daoSession.getKecamatanDao();
            QueryBuilder<kecamatan> queryBuilder = dDao.queryBuilder().where(kecamatanDao.Properties.Id.eq(id));
            List<kecamatan> dToDelete = queryBuilder.list();
            for (kecamatan d : dToDelete) {
                dDao.delete(d);
            }
            daoSession.clear();
            ////Log.d(TAG, "kecamatan del : " + dToDelete.size() + " entry. " + id + " from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public kecamatan get_kecamatanById(String id) {
        kecamatan d = null;
        try {
            openReadableDb();
            kecamatanDao dDao = daoSession.getKecamatanDao();
            d = dDao.load(Long.parseLong(id));
            daoSession.clear();
            ////Log.i(TAG, "kecamatan gid : " + d.getId().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }
    @Override
    public void delete_kecamatan() {
        try {
            openWritableDb();
            kecamatanDao dDao = daoSession.getKecamatanDao();
            dDao.deleteAll();
            daoSession.clear();
            ////Log.d(TAG, "Delete all kecamatan from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_kelurahan(kelurahan kelurahan) {
        try {
            if (kelurahan != null) {
                openWritableDb();
                kelurahanDao dDao = daoSession.getKelurahanDao();
                dDao.insertOrReplace(kelurahan);
                ////Log.d(TAG, "kelurahan add : " + kelurahan.getId() + " to the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_kelurahan_InTX(List<kelurahan> kelurahan) {
        try {
            if (kelurahan != null) {
                openWritableDb();
                kelurahanDao dDao = daoSession.getKelurahanDao();
                dDao.insertOrReplaceInTx(kelurahan);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public List<kelurahan> list_kelurahan() {
        List<kelurahan> d = null;
        try {
            openReadableDb();
            kelurahanDao dDao = daoSession.getKelurahanDao();
            d = dDao.loadAll();
            ////Log.i(TAG, "kelurahan get : " + d.get(0).getId().toString());
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public void update_kelurahan(kelurahan kelurahan) {
        try {
            if (kelurahan != null) {
                openWritableDb();
                kelurahanDao dDao = daoSession.getKelurahanDao();
                dDao.update(kelurahan);
                ////Log.d(TAG, "kelurahan put : " + kelurahan.getId() + " from the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void delete_kelurahanById(String id) {
        try {
            openWritableDb();
            kelurahanDao dDao = daoSession.getKelurahanDao();
            QueryBuilder<kelurahan> queryBuilder = dDao.queryBuilder().where(kelurahanDao.Properties.Id.eq(id));
            List<kelurahan> dToDelete = queryBuilder.list();
            for (kelurahan d : dToDelete) {
                dDao.delete(d);
            }
            daoSession.clear();
            ////Log.d(TAG, "kelurahan del : " + dToDelete.size() + " entry. " + id + " from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public kelurahan get_kelurahanById(String id) {
        kelurahan d = null;
        try {
            openReadableDb();
            kelurahanDao dDao = daoSession.getKelurahanDao();
            d = dDao.load(Long.parseLong(id));
            daoSession.clear();
            ////Log.i(TAG, "kelurahan gid : " + d.getId().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }
    @Override
    public void delete_kelurahan() {
        try {
            openWritableDb();
            kelurahanDao dDao = daoSession.getKelurahanDao();
            dDao.deleteAll();
            daoSession.clear();
            ////Log.d(TAG, "Delete all kelurahan from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_provinsi(provinsi provinsi) {
        try {
            if (provinsi != null) {
                openWritableDb();
                provinsiDao dDao = daoSession.getProvinsiDao();
                dDao.insertOrReplace(provinsi);
                ////Log.d(TAG, "provinsi add : " + provinsi.getId() + " to the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_provinsi_InTX(List<provinsi> provinsi) {
        try {
            if (provinsi != null) {
                openWritableDb();
                provinsiDao dDao = daoSession.getProvinsiDao();
                dDao.insertOrReplaceInTx(provinsi);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public List<provinsi> list_provinsi() {
        List<provinsi> d = null;
        try {
            openReadableDb();
            provinsiDao dDao = daoSession.getProvinsiDao();
            d = dDao.loadAll();
            ////Log.i(TAG, "provinsi get : " + d.get(0).getId().toString());
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public void update_provinsi(provinsi provinsi) {
        try {
            if (provinsi != null) {
                openWritableDb();
                provinsiDao dDao = daoSession.getProvinsiDao();
                dDao.update(provinsi);
                ////Log.d(TAG, "provinsi put : " + provinsi.getId() + " from the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void delete_provinsiById(String id) {
        try {
            openWritableDb();
            provinsiDao dDao = daoSession.getProvinsiDao();
            QueryBuilder<provinsi> queryBuilder = dDao.queryBuilder().where(provinsiDao.Properties.Id.eq(id));
            List<provinsi> dToDelete = queryBuilder.list();
            for (provinsi d : dToDelete) {
                dDao.delete(d);
            }
            daoSession.clear();
            ////Log.d(TAG, "provinsi del : " + dToDelete.size() + " entry. " + id + " from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public provinsi get_provinsiById(String id) {
        provinsi d = null;
        try {
            openReadableDb();
            provinsiDao dDao = daoSession.getProvinsiDao();
            d = dDao.load(Long.parseLong(id));
            daoSession.clear();
            ////Log.i(TAG, "provinsi gid : " + d.getId().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }
    @Override
    public void delete_provinsi() {
        try {
            openWritableDb();
            provinsiDao dDao = daoSession.getProvinsiDao();
            dDao.deleteAll();
            daoSession.clear();
            ////Log.d(TAG, "Delete all provinsi from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public List<perjalanan_dinas> list_perjalanan_dinasByEDT(String Employee_id,String Date_departure,String Type_code) {
        List<perjalanan_dinas> d = null;
        try {
            openReadableDb();
            perjalanan_dinasDao dDao = daoSession.getPerjalanan_dinasDao();

            d = dDao.queryBuilder()
                    .whereOr(perjalanan_dinasDao.Properties.Employee_id.like("%" + Employee_id + "%"),
                            perjalanan_dinasDao.Properties.Date_departure.like(Date_departure + "%"),
                            perjalanan_dinasDao.Properties.Type_code.like("%" + Type_code + "%")).list();

            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public void insert_perjalanan_dinas(perjalanan_dinas perjalanan_dinas) {
        try {
            if (perjalanan_dinas != null) {
                openWritableDb();
                perjalanan_dinasDao dDao = daoSession.getPerjalanan_dinasDao();
                dDao.insertOrReplace(perjalanan_dinas);
                ////Log.d(TAG, "perjalanan_dinas add : " + perjalanan_dinas.getId() + " to the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_perjalanan_dinas_InTX(List<perjalanan_dinas> perjalanan_dinas) {
        try {
            if (perjalanan_dinas != null) {
                openWritableDb();
                perjalanan_dinasDao dDao = daoSession.getPerjalanan_dinasDao();
                dDao.insertOrReplaceInTx(perjalanan_dinas);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public List<perjalanan_dinas> list_perjalanan_dinas() {
        List<perjalanan_dinas> d = null;
        try {
            openReadableDb();
            perjalanan_dinasDao dDao = daoSession.getPerjalanan_dinasDao();
            d = dDao.loadAll();
            ////Log.i(TAG, "perjalanan_dinas get : " + d.get(0).getId().toString());
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public List<perjalanan_dinas> list_perjalanan_dinasByD(String Employee_id,String date) {
        List<perjalanan_dinas> d = null;
        try {
            openReadableDb();
            perjalanan_dinasDao dDao = daoSession.getPerjalanan_dinasDao();

            d = dDao.queryBuilder()
                    .where(perjalanan_dinasDao.Properties.Employee_id.like("%" + Employee_id + "%"),
                            perjalanan_dinasDao.Properties.Date_departure.like(date + "%")).list();

            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public void update_perjalanan_dinas(perjalanan_dinas perjalanan_dinas) {
        try {
            if (perjalanan_dinas != null) {
                openWritableDb();
                perjalanan_dinasDao dDao = daoSession.getPerjalanan_dinasDao();
                dDao.update(perjalanan_dinas);
                ////Log.d(TAG, "perjalanan_dinas put : " + perjalanan_dinas.getId() + " from the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void delete_perjalanan_dinasById(String id) {
        try {
            openWritableDb();
            perjalanan_dinasDao dDao = daoSession.getPerjalanan_dinasDao();
            QueryBuilder<perjalanan_dinas> queryBuilder = dDao.queryBuilder().where(perjalanan_dinasDao.Properties.Id.eq(id));
            List<perjalanan_dinas> dToDelete = queryBuilder.list();
            for (perjalanan_dinas d : dToDelete) {
                dDao.delete(d);
            }
            daoSession.clear();
            ////Log.d(TAG, "perjalanan_dinas del : " + dToDelete.size() + " entry. " + id + " from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public perjalanan_dinas get_perjalanan_dinasById(String id) {
        perjalanan_dinas d = null;
        try {
            openReadableDb();
            perjalanan_dinasDao dDao = daoSession.getPerjalanan_dinasDao();
            d = dDao.load(Long.parseLong(id));
            daoSession.clear();
            ////Log.i(TAG, "perjalanan_dinas gid : " + d.getId().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }
    @Override
    public void delete_perjalanan_dinas() {
        try {
            openWritableDb();
            perjalanan_dinasDao dDao = daoSession.getPerjalanan_dinasDao();
            dDao.deleteAll();
            daoSession.clear();
            ////Log.d(TAG, "Delete all perjalanan_dinas from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public List<perjalanan_dinas> list_perjalanan_dinasNotif() {
        List<perjalanan_dinas> d = null;
        try {
            openReadableDb();
            perjalanan_dinasDao dDao = daoSession.getPerjalanan_dinasDao();

            d = dDao.queryBuilder()
                    .where(perjalanan_dinasDao.Properties.Notif.eq(true)).list();

            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public void insert_place(place place) {
        try {
            if (place != null) {
                openWritableDb();
                placeDao dDao = daoSession.getPlaceDao();
                dDao.insertOrReplace(place);
                ////Log.d(TAG, "place add : " + place.getId() + " to the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_place_InTX(List<place> place) {
        try {
            if (place != null) {
                openWritableDb();
                placeDao dDao = daoSession.getPlaceDao();
                dDao.insertOrReplaceInTx(place);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public List<place> list_place() {
        List<place> d = null;
        try {
            openReadableDb();
            placeDao dDao = daoSession.getPlaceDao();
            d = dDao.loadAll();
            ////Log.i(TAG, "place get : " + d.get(0).getId().toString());
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public void update_place(place place) {
        try {
            if (place != null) {
                openWritableDb();
                placeDao dDao = daoSession.getPlaceDao();
                dDao.update(place);
                ////Log.d(TAG, "place put : " + place.getId() + " from the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void delete_placeById(String id) {
        try {
            openWritableDb();
            placeDao dDao = daoSession.getPlaceDao();
            QueryBuilder<place> queryBuilder = dDao.queryBuilder().where(placeDao.Properties.Id.eq(id));
            List<place> dToDelete = queryBuilder.list();
            for (place d : dToDelete) {
                dDao.delete(d);
            }
            daoSession.clear();
            ////Log.d(TAG, "place del : " + dToDelete.size() + " entry. " + id + " from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public place get_placeById(String id) {
        place d = null;
        try {
            openReadableDb();
            placeDao dDao = daoSession.getPlaceDao();
            d = dDao.load(Long.parseLong(id));
            daoSession.clear();
            ////Log.i(TAG, "place gid : " + d.getId().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }
    @Override
    public void delete_place() {
        try {
            openWritableDb();
            placeDao dDao = daoSession.getPlaceDao();
            dDao.deleteAll();
            daoSession.clear();
            ////Log.d(TAG, "Delete all place from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_position(position position) {
        try {
            if (position != null) {
                openWritableDb();
                positionDao dDao = daoSession.getPositionDao();
                dDao.insertOrReplace(position);
                ////Log.d(TAG, "position add : " + position.getId() + " to the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_position_InTX(List<position> position) {
        try {
            if (position != null) {
                openWritableDb();
                positionDao dDao = daoSession.getPositionDao();
                dDao.insertOrReplaceInTx(position);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public List<position> list_position() {
        List<position> d = null;
        try {
            openReadableDb();
            positionDao dDao = daoSession.getPositionDao();
            d = dDao.loadAll();
            ////Log.i(TAG, "position get : " + d.get(0).getId().toString());
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public void update_position(position position) {
        try {
            if (position != null) {
                openWritableDb();
                daoSession.update(position);
                ////Log.d(TAG, "position put : " + position.getId() + " from the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void delete_positionById(String id) {
        try {
            openWritableDb();
            positionDao dDao = daoSession.getPositionDao();
            QueryBuilder<position> queryBuilder = dDao.queryBuilder().where(positionDao.Properties.Id.eq(id));
            List<position> dToDelete = queryBuilder.list();
            for (position d : dToDelete) {
                dDao.delete(d);
            }
            daoSession.clear();
            ////Log.d(TAG, "position del : " + dToDelete.size() + " entry. " + id + " from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public position get_positionById(String id) {
        position d = null;
        try {
            openReadableDb();
            positionDao dDao = daoSession.getPositionDao();
            d = dDao.load(Long.parseLong(id));
            daoSession.clear();
            ////Log.i(TAG, "position gid : " + d.getId().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }
    @Override
    public void delete_position() {
        try {
            openWritableDb();
            positionDao dDao = daoSession.getPositionDao();
            dDao.deleteAll();
            daoSession.clear();
            ////Log.d(TAG, "Delete all position from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_roles(roles roles) {
        try {
            if (roles != null) {
                openWritableDb();
                rolesDao dDao = daoSession.getRolesDao();
                dDao.insertOrReplace(roles);
                ////Log.d(TAG, "roles add : " + roles.getId() + " to the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_notif_InTX(List<notif> notif) {
        try {
            if (notif != null) {
                openWritableDb();
                notifDao dDao = daoSession.getNotifDao();
                dDao.insertOrReplaceInTx(notif);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public List<notif> list_notif() {
        List<notif> d = null;
        try {
            openReadableDb();
            notifDao dDao = daoSession.getNotifDao();
            d = dDao.queryBuilder().orderDesc(notifDao.Properties.Id).list();
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public void delete_notif() {
        try {
            openWritableDb();
            notifDao dDao = daoSession.getNotifDao();
            dDao.deleteAll();
            daoSession.clear();
            ////Log.d(TAG, "Delete all notif from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void delete_notifById(String id, String tag) {
        try {
            openWritableDb();
            notifDao dDao = daoSession.getNotifDao();
            QueryBuilder<notif> queryBuilder = dDao.queryBuilder().where(notifDao.Properties.Data.eq(id), notifDao.Properties.Tag.eq(tag));
            List<notif> dToDelete = queryBuilder.list();
            for (notif d : dToDelete) {
                dDao.delete(d);
            }
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void insert_roles_InTX(List<roles> roles) {
        try {
            if (roles != null) {
                openWritableDb();
                rolesDao dDao = daoSession.getRolesDao();
                dDao.insertOrReplaceInTx(roles);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public List<roles> list_roles() {
        List<roles> d = null;
        try {
            openReadableDb();
            rolesDao dDao = daoSession.getRolesDao();
            d = dDao.loadAll();
            ////Log.i(TAG, "roles get : " + d.get(0).getId().toString());
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public void update_roles(roles roles) {
        try {
            if (roles != null) {
                openWritableDb();
                rolesDao dDao = daoSession.getRolesDao();
                dDao.update(roles);
                ////Log.d(TAG, "roles put : " + roles.getId() + " from the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void delete_rolesById(String id) {
        try {
            openWritableDb();
            rolesDao dDao = daoSession.getRolesDao();
            QueryBuilder<roles> queryBuilder = dDao.queryBuilder().where(rolesDao.Properties.Id.eq(id));
            List<roles> dToDelete = queryBuilder.list();
            for (roles d : dToDelete) {
                dDao.delete(d);
            }
            daoSession.clear();
            ////Log.d(TAG, "roles del : " + dToDelete.size() + " entry. " + id + " from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public roles get_rolesById(String id) {
        roles d = null;
        try {
            openReadableDb();
            rolesDao dDao = daoSession.getRolesDao();
            d = dDao.load(Long.parseLong(id));
            daoSession.clear();
            ////Log.i(TAG, "roles gid : " + d.getId().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }
    @Override
    public void delete_roles() {
        try {
            openWritableDb();
            rolesDao dDao = daoSession.getRolesDao();
            dDao.deleteAll();
            daoSession.clear();
            ////Log.d(TAG, "Delete all roles from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public List<schedule> list_scheduleByD(String forward,String date) {
        List<schedule> d = new ArrayList<>();
        try {
            openReadableDb();
            scheduleDao dDao = daoSession.getScheduleDao();
            d.addAll(dDao.queryBuilder()
                    .where(scheduleDao.Properties.Forward_to_user_id.like("%" + forward + "%"),
                            scheduleDao.Properties.Datetime_schedule_start.like(date + "%")).list());
            d.addAll(dDao.queryBuilder()
                    .where(scheduleDao.Properties.Forward_to_user_id.eq("[1]"),
                            scheduleDao.Properties.Datetime_schedule_start.like(date + "%")).list());
            d.addAll(dDao.queryBuilder()
                    .where(scheduleDao.Properties.Forward_to_user_id.isNull(),
                            scheduleDao.Properties.Datetime_schedule_start.like(date + "%")).list());

            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public List<schedule> list_scheduleByFDCP(String forward,String date,String category,String place) {
        List<schedule> d = null;
        try {
            openReadableDb();
            scheduleDao dDao = daoSession.getScheduleDao();

            d = dDao.queryBuilder()
                    .whereOr(scheduleDao.Properties.Forward_to_user_id.like("%" + forward + "%"),
                            scheduleDao.Properties.Datetime_schedule_start.like(date + "%"),
                            scheduleDao.Properties.Category_id.like("%" + category + "%"),
                            scheduleDao.Properties.Place.like("%" + place + "%")).list();

            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public void insert_schedule(schedule schedule) {
        try {
            if (schedule != null) {
                openWritableDb();
                scheduleDao dDao = daoSession.getScheduleDao();
                dDao.insertOrReplace(schedule);
                ////Log.d(TAG, "schedule add : " + schedule.getId() + " to the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_schedule_InTX(List<schedule> schedule) {
        try {
            if (schedule != null) {
                openWritableDb();
                scheduleDao dDao = daoSession.getScheduleDao();
                dDao.insertOrReplaceInTx(schedule);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public List<schedule> list_schedule() {
        List<schedule> d = null;
        try {
            openReadableDb();
            scheduleDao dDao = daoSession.getScheduleDao();
            d = dDao.loadAll();
            ////Log.i(TAG, "schedule get : " + d.get(0).getId().toString());
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public void update_schedule(schedule schedule) {
        try {
            if (schedule != null) {
                openWritableDb();
                scheduleDao dDao = daoSession.getScheduleDao();
                dDao.update(schedule);
                ////Log.d(TAG, "schedule put : " + schedule.getId() + " from the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void delete_scheduleById(String id) {
        try {
            openWritableDb();
            scheduleDao dDao = daoSession.getScheduleDao();
            QueryBuilder<schedule> queryBuilder = dDao.queryBuilder().where(scheduleDao.Properties.Id.eq(id));
            List<schedule> dToDelete = queryBuilder.list();
            for (schedule d : dToDelete) {
                dDao.delete(d);
            }
            daoSession.clear();
            ////Log.d(TAG, "schedule del : " + dToDelete.size() + " entry. " + id + " from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public schedule get_scheduleById(String id) {
        schedule d = null;
        try {
            openReadableDb();
            scheduleDao dDao = daoSession.getScheduleDao();
            d = dDao.load(Long.parseLong(id));
            daoSession.clear();
            ////Log.i(TAG, "schedule gid : " + d.getId().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }
    @Override
    public void delete_schedule() {
        try {
            openWritableDb();
            scheduleDao dDao = daoSession.getScheduleDao();
            dDao.deleteAll();
            daoSession.clear();
            ////Log.d(TAG, "Delete all schedule from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public List<schedule> list_scheduleNotif() {
        List<schedule> d = new ArrayList<>();
        try {
            openReadableDb();
            scheduleDao dDao = daoSession.getScheduleDao();
            d.addAll(dDao.queryBuilder()
                    .where(scheduleDao.Properties.Notif.eq(true)).list());

            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public void insert_taxi_fee(taxi_fee taxi_fee) {
        try {
            if (taxi_fee != null) {
                openWritableDb();
                taxi_feeDao dDao = daoSession.getTaxi_feeDao();
                dDao.insertOrReplace(taxi_fee);
                ////Log.d(TAG, "taxi_fee add : " + taxi_fee.getId() + " to the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_taxi_fee_InTX(List<taxi_fee> taxi_fee) {
        try {
            if (taxi_fee != null) {
                openWritableDb();
                taxi_feeDao dDao = daoSession.getTaxi_feeDao();
                dDao.insertOrReplaceInTx(taxi_fee);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public List<taxi_fee> list_taxi_fee() {
        List<taxi_fee> d = null;
        try {
            openReadableDb();
            taxi_feeDao dDao = daoSession.getTaxi_feeDao();
            d = dDao.loadAll();
            ////Log.i(TAG, "taxi_fee get : " + d.get(0).getId().toString());
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public void update_taxi_fee(taxi_fee taxi_fee) {
        try {
            if (taxi_fee != null) {
                openWritableDb();
                taxi_feeDao dDao = daoSession.getTaxi_feeDao();
                dDao.update(taxi_fee);
                ////Log.d(TAG, "taxi_fee put : " + taxi_fee.getId() + " from the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void delete_taxi_feeById(String id) {
        try {
            openWritableDb();
            taxi_feeDao dDao = daoSession.getTaxi_feeDao();
            QueryBuilder<taxi_fee> queryBuilder = dDao.queryBuilder().where(taxi_feeDao.Properties.Id.eq(id));
            List<taxi_fee> dToDelete = queryBuilder.list();
            for (taxi_fee d : dToDelete) {
                dDao.delete(d);
            }
            daoSession.clear();
            ////Log.d(TAG, "taxi_fee del : " + dToDelete.size() + " entry. " + id + " from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public taxi_fee get_taxi_feeById(String id) {
        taxi_fee d = null;
        try {
            openReadableDb();
            taxi_feeDao dDao = daoSession.getTaxi_feeDao();
            d = dDao.load(Long.parseLong(id));
            daoSession.clear();
            ////Log.i(TAG, "taxi_fee gid : " + d.getId().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }
    @Override
    public void delete_taxi_fee() {
        try {
            openWritableDb();
            taxi_feeDao dDao = daoSession.getTaxi_feeDao();
            dDao.deleteAll();
            daoSession.clear();
            ////Log.d(TAG, "Delete all taxi_fee from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_user_class(user_class user_class) {
        try {
            if (user_class != null) {
                openWritableDb();
                user_classDao dDao = daoSession.getUser_classDao();
                dDao.insertOrReplace(user_class);
                ////Log.d(TAG, "user_class add : " + user_class.getId() + " to the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_user_class_InTX(List<user_class> user_class) {
        try {
            if (user_class != null) {
                openWritableDb();
                user_classDao dDao = daoSession.getUser_classDao();
                dDao.insertOrReplaceInTx(user_class);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public List<user_class> list_user_class() {
        List<user_class> d = null;
        try {
            openReadableDb();
            user_classDao dDao = daoSession.getUser_classDao();
            d = dDao.loadAll();
            ////Log.i(TAG, "user_class get : " + d.get(0).getId().toString());
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public void update_user_class(user_class user_class) {
        try {
            if (user_class != null) {
                openWritableDb();
                user_classDao dDao = daoSession.getUser_classDao();
                dDao.update(user_class);
                ////Log.d(TAG, "user_class put : " + user_class.getId() + " from the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void delete_user_classById(String id) {
        try {
            openWritableDb();
            user_classDao dDao = daoSession.getUser_classDao();
            QueryBuilder<user_class> queryBuilder = dDao.queryBuilder().where(user_classDao.Properties.Id.eq(id));
            List<user_class> dToDelete = queryBuilder.list();
            for (user_class d : dToDelete) {
                dDao.delete(d);
            }
            daoSession.clear();
            ////Log.d(TAG, "user_class del : " + dToDelete.size() + " entry. " + id + " from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public user_class get_user_classById(String id) {
        user_class d = null;
        try {
            openReadableDb();
            user_classDao dDao = daoSession.getUser_classDao();
            d = dDao.load(Long.parseLong(id));
            daoSession.clear();
            ////Log.i(TAG, "user_class gid : " + d.getId().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }
    @Override
    public void delete_user_class() {
        try {
            openWritableDb();
            user_classDao dDao = daoSession.getUser_classDao();
            dDao.deleteAll();
            daoSession.clear();
            ////Log.d(TAG, "Delete all user_class from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_user_mobile(user_mobile user_mobile) {
        try {
            if (user_mobile != null) {
                openWritableDb();
                user_mobileDao dDao = daoSession.getUser_mobileDao();
                dDao.insertOrReplace(user_mobile);
                ////Log.d(TAG, "user_mobile add : " + user_mobile.getId() + " to the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_user_mobile_InTX(List<user_mobile> user_mobile) {
        try {
            if (user_mobile != null) {
                openWritableDb();
                user_mobileDao dDao = daoSession.getUser_mobileDao();
                dDao.insertOrReplaceInTx(user_mobile);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public List<user_mobile> list_user_mobile() {
        List<user_mobile> d = null;
        try {
            openReadableDb();
            user_mobileDao dDao = daoSession.getUser_mobileDao();
            d = dDao.loadAll();
            ////Log.i(TAG, "user_mobile get : " + d.get(0).getId().toString());
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public void update_user_mobile(user_mobile user_mobile) {
        try {
            if (user_mobile != null) {
                openWritableDb();
                user_mobileDao dDao = daoSession.getUser_mobileDao();
                dDao.update(user_mobile);
                ////Log.d(TAG, "user_mobile put : " + user_mobile.getId() + " from the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void delete_user_mobileById(String id) {
        try {
            openWritableDb();
            user_mobileDao dDao = daoSession.getUser_mobileDao();
            QueryBuilder<user_mobile> queryBuilder = dDao.queryBuilder().where(user_mobileDao.Properties.Id.eq(id));
            List<user_mobile> dToDelete = queryBuilder.list();
            for (user_mobile d : dToDelete) {
                dDao.delete(d);
            }
            daoSession.clear();
            ////Log.d(TAG, "user_mobile del : " + dToDelete.size() + " entry. " + id + " from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public user_mobile get_user_mobileById(String id) {
        user_mobile d = null;
        try {
            openReadableDb();
            user_mobileDao dDao = daoSession.getUser_mobileDao();
            d = dDao.load(Long.parseLong(id));
            daoSession.clear();
            ////Log.i(TAG, "user_mobile gid : " + d.getId().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }
    @Override
    public void delete_user_mobile() {
        try {
            openWritableDb();
            user_mobileDao dDao = daoSession.getUser_mobileDao();
            dDao.deleteAll();
            daoSession.clear();
            ////Log.d(TAG, "Delete all user_mobile from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_users(users users) {
        try {
            if (users != null) {
                openWritableDb();
                usersDao dDao = daoSession.getUsersDao();
                dDao.insertOrReplace(users);
                ////Log.d(TAG, "users add : " + users.getId() + " to the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_users_InTX(List<users> users) {
        try {
            if (users != null) {
                openWritableDb();
                usersDao dDao = daoSession.getUsersDao();
                dDao.insertOrReplaceInTx(users);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public List<users> list_users() {
        List<users> d = null;
        try {
            openReadableDb();
            usersDao dDao = daoSession.getUsersDao();
            d = dDao.queryBuilder().orderAsc(usersDao.Properties.Name).list();
            ////Log.i(TAG, "users get : " + d.get(0).getId().toString());
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public List<Long> get_rolesAddByIdRole() {
        List<user_role> d = null;
        List<Long> datas = new ArrayList<>();
        try {
            openReadableDb();
            user_roleDao dDao = daoSession.getUser_roleDao();
            d = dDao.queryBuilder().where(user_roleDao.Properties.Role_id.notIn("1")).list();
            for(int i = 0 ; i < d.size() ; i++){
                datas.add(datas.size(), d.get(i).getId());
            }
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return datas;
        }
        return datas;
    }
    @Override
    public List<users> list_users_add() {
        List<users> d = null;
        try {
            openReadableDb();
            usersDao dDao = daoSession.getUsersDao();
            d = dDao.queryBuilder().where(usersDao.Properties.Id.notEq("1")).orderAsc(usersDao.Properties.Name).list();
            ////Log.i(TAG, "users get : " + d.get(0).getId().toString());
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public void update_users(users users) {
        try {
            if (users != null) {
                openWritableDb();
                usersDao dDao = daoSession.getUsersDao();
                dDao.update(users);
                ////Log.d(TAG, "users put : " + users.getId() + " from the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void delete_usersById(String id) {
        try {
            openWritableDb();
            usersDao dDao = daoSession.getUsersDao();
            QueryBuilder<users> queryBuilder = dDao.queryBuilder().where(usersDao.Properties.Id.eq(id));
            List<users> dToDelete = queryBuilder.list();
            for (users d : dToDelete) {
                dDao.delete(d);
            }
            daoSession.clear();
            ////Log.d(TAG, "users del : " + dToDelete.size() + " entry. " + id + " from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public users get_usersById(String id) {
        users d = null;
        try {
            openReadableDb();
            usersDao dDao = daoSession.getUsersDao();
            d = dDao.load(Long.parseLong(id));
            daoSession.clear();
            ////Log.i(TAG, "users gid : " + d.getId().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }
    @Override
    public void delete_users() {
        try {
            openWritableDb();
            usersDao dDao = daoSession.getUsersDao();
            dDao.deleteAll();
            daoSession.clear();
            ////Log.d(TAG, "Delete all users from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_vehicle_rent(vehicle_rent vehicle_rent) {
        try {
            if (vehicle_rent != null) {
                openWritableDb();
                vehicle_rentDao dDao = daoSession.getVehicle_rentDao();
                dDao.insertOrReplace(vehicle_rent);
                ////Log.d(TAG, "vehicle_rent add : " + vehicle_rent.getId() + " to the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void insert_vehicle_rent_InTX(List<vehicle_rent> vehicle_rent) {
        try {
            if (vehicle_rent != null) {
                openWritableDb();
                vehicle_rentDao dDao = daoSession.getVehicle_rentDao();
                dDao.insertOrReplaceInTx(vehicle_rent);
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public List<vehicle_rent> list_vehicle_rent() {
        List<vehicle_rent> d = null;
        try {
            openReadableDb();
            vehicle_rentDao dDao = daoSession.getVehicle_rentDao();
            d = dDao.loadAll();
            ////Log.i(TAG, "vehicle_rent get : " + d.get(0).getId().toString());
            daoSession.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (d != null) {
            return new ArrayList<>(d);
        }
        return null;
    }
    @Override
    public void update_vehicle_rent(vehicle_rent vehicle_rent) {
        try {
            if (vehicle_rent != null) {
                openWritableDb();
                vehicle_rentDao dDao = daoSession.getVehicle_rentDao();
                dDao.update(vehicle_rent);
                ////Log.d(TAG, "vehicle_rent put : " + vehicle_rent.getId() + " from the schema.");
                daoSession.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void delete_vehicle_rentById(String id) {
        try {
            openWritableDb();
            vehicle_rentDao dDao = daoSession.getVehicle_rentDao();
            QueryBuilder<vehicle_rent> queryBuilder = dDao.queryBuilder().where(vehicle_rentDao.Properties.Id.eq(id));
            List<vehicle_rent> dToDelete = queryBuilder.list();
            for (vehicle_rent d : dToDelete) {
                dDao.delete(d);
            }
            daoSession.clear();
            ////Log.d(TAG, "vehicle_rent del : " + dToDelete.size() + " entry. " + id + " from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public vehicle_rent get_vehicle_rentById(String id) {
        vehicle_rent d = null;
        try {
            openReadableDb();
            vehicle_rentDao dDao = daoSession.getVehicle_rentDao();
            d = dDao.load(Long.parseLong(id));
            daoSession.clear();
            ////Log.i(TAG, "vehicle_rent gid : " + d.getId().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }
    @Override
    public void delete_vehicle_rent() {
        try {
            openWritableDb();
            vehicle_rentDao dDao = daoSession.getVehicle_rentDao();
            dDao.deleteAll();
            daoSession.clear();
            ////Log.d(TAG, "Delete all vehicle_rent from the schema.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void delete_all_table(){
        try {
            openWritableDb();
            delete_position();
            delete_user_mobile();
            delete_user_class();
            delete_airplane_ticket();
            delete_disposition();
            delete_file();
            delete_folder();
            delete_hotel_fee();
            delete_kecamatan();
            delete_kelurahan();
            delete_perjalanan_dinas();
            delete_place();
            delete_roles();
            delete_schedule();
            delete_taxi_fee();
            delete_vehicle_rent();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void openReadableDb() throws SQLiteException {
        database = mHelper.getReadableDatabase();
        daoMaster = new DaoMaster(database);
        daoSession = daoMaster.newSession();
        asyncSession = daoSession.startAsyncSession();
        asyncSession.setListener(this);
    }
    public void openWritableDb() throws SQLiteException {
        database = mHelper.getWritableDatabase();
        daoMaster = new DaoMaster(database);
        daoSession = daoMaster.newSession();
        asyncSession = daoSession.startAsyncSession();
        asyncSession.setListener(this);
    }
    @Override
    public void closeDbConnections() {
        if (daoSession != null) {
            daoSession.clear();
            daoSession = null;
        }
        if (database != null && database.isOpen()) {
            database.close();
        }
        if (mHelper != null) {
            mHelper.close();
            mHelper = null;
        }
        if (instance != null) {
            instance = null;
        }
    }
    @Override
    public void dropDatabase() {
        try {
            openWritableDb();
            daoMaster.dropAllTables(database, true);
            mHelper.onCreate(database);
            asyncSession.deleteAll(IDManager.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }
}
