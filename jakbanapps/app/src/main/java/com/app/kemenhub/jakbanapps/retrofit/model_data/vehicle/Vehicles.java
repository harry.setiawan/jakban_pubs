package com.app.kemenhub.jakbanapps.retrofit.model_data.vehicle;

import com.app.kemenhub.jakbanapps.retrofit.model_data.provinsi.Provinsi;

public class Vehicles {

	private String id;
	private Provinsi provinsi;
	private String unit;
	private String car;
	private String midle_bus;
	private String big_bus;

	public Vehicles(String id, Provinsi provinsi, String unit, String car, String midle_bus, String big_bus) {
		super();
		this.id = id;
		this.provinsi = provinsi;
		this.unit = unit;
		this.car = car;
		this.midle_bus = midle_bus;
		this.big_bus = big_bus;
	}

	public Vehicles(){
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Provinsi getProvinsi() {
		return provinsi;
	}

	public void setProvinsi(Provinsi provinsi) {
		this.provinsi = provinsi;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getCar() {
		return car;
	}

	public void setCar(String car) {
		this.car = car;
	}

	public String getMidle_bus() {
		return midle_bus;
	}

	public void setMidle_bus(String midle_bus) {
		this.midle_bus = midle_bus;
	}

	public String getBig_bus() {
		return big_bus;
	}

	public void setBig_bus(String big_bus) {
		this.big_bus = big_bus;
	}
}
