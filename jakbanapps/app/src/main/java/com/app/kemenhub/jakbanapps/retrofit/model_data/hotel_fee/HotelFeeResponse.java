package com.app.kemenhub.jakbanapps.retrofit.model_data.hotel_fee;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HotelFeeResponse {
	@SerializedName("code")
	String code;
	@SerializedName("message")
	String message;
	@SerializedName("hotel_fee")
	HotelFees hotel_fee;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public HotelFees getHotelFees() {
		return hotel_fee;
	}
	public void setHotelFees(HotelFees hotel_fee) {
		this.hotel_fee = hotel_fee;
	}
	
	
}
