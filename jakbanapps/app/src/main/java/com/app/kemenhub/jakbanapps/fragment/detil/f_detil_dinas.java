package com.app.kemenhub.jakbanapps.fragment.detil;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.app.kemenhub.jakbanapps.R;
import com.app.kemenhub.jakbanapps.db.airplane_ticket;
import com.app.kemenhub.jakbanapps.db.hotel_fee;
import com.app.kemenhub.jakbanapps.db.kabupaten;
import com.app.kemenhub.jakbanapps.db.perjalanan_dinas;
import com.app.kemenhub.jakbanapps.db.perjalanan_dinas_jenis;
import com.app.kemenhub.jakbanapps.db.place;
import com.app.kemenhub.jakbanapps.db.position;
import com.app.kemenhub.jakbanapps.db.provinsi;
import com.app.kemenhub.jakbanapps.db.users;
import com.app.kemenhub.jakbanapps.db.vehicle_rent;
import com.app.kemenhub.jakbanapps.retrofit.model_data.airplane.AirPlaneTiket;
import com.app.kemenhub.jakbanapps.retrofit.model_data.role.Role;
import com.app.kemenhub.jakbanapps.v_detil;
import com.app.kemenhub.jakbanapps.v_event;

public class f_detil_dinas extends Fragment{

    v_detil ac;
    View v;
    CardView line;
    EditText no_surat, nm_pegawai, jabatan, maksud_dinas, waktu_berangkat, waktu_kembali, jenis_dinas,
            tempat_berangkat, tempat_tujuan, lama_dinas, keterangan_lain;

    LinearLayout lay_tiket_tujuan;
    EditText transportasi_type, transportasi_class, transportasi_pesawat_rute,
            transportasi_jenis_harga,transportasi_harga;

    LinearLayout lay_taksi;
    Switch penggunaan_taksi;
    EditText taksi_B_tempat, taksi_B_jenis_harga, taksi_B_harga, taksi_B_jumlah;
    EditText taksi_T_tempat, taksi_T_jenis_harga, taksi_T_harga, taksi_T_jumlah;

    LinearLayout lay_sewa;
    Switch sewa_kendaraan;
    EditText sewa_model, sewa_wilayah, sewa_jenis, sewa_harga;

    LinearLayout lay_hotel;
    Switch hotel;
    EditText hotel_wilayah_tiket, hotel_jenis_harga, hotel_harga;
    perjalanan_dinas dinas = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.f_detil_dinas, container, false);
        ac = (v_detil) getActivity();

        _init();
        dinas = v_event.DBManager.get_perjalanan_dinasById(ac.id_data);
        _shown_data();
        _show_Data_transportasi();
        _shown_data_taksi();
        _shown_data_rental();
        _shown_data_hotel();

        return v;
    }

    private void _shown_data_rental() {
        sewa_kendaraan.setClickable(false);
        if(dinas.getWith_vehicle_rent().equals("Y")){
            sewa_kendaraan.setChecked(true);

            sewa_model.setText(dinas.getVehicle_rent_type());
            vehicle_rent vehicle = v_event.DBManager.get_vehicle_rentById(dinas.getVehicle_rent_price_id());
            provinsi prov = v_event.DBManager.get_provinsiById(vehicle.getProvinsi_id());
            sewa_wilayah.setText(prov.getNama());
            sewa_jenis.setText(dinas.getVehicle_amount_type());
            sewa_harga.setText("Rp. " + dinas.getVehicle_amount());
        }
        if(sewa_kendaraan.isChecked()){
            lay_sewa.setVisibility(View.VISIBLE);
        }else{
            lay_sewa.setVisibility(View.GONE);
        }
    }

    private void _shown_data_hotel() {
        hotel.setClickable(false);
        if(dinas.getWith_hotel().equals("Y")){
            hotel.setChecked(true);
            hotel_fee hotel = v_event.DBManager.get_hotel_feeById(dinas.getHotel_ticket_id());
            provinsi prov = v_event.DBManager.get_provinsiById(hotel.getProvinsi_id());
            hotel_wilayah_tiket.setText(prov.getNama());
            hotel_jenis_harga.setText(dinas.getHotel_amount_type());
            hotel_harga.setText("Rp. " + dinas.getHotel_amount());
        }
        if(hotel.isChecked()){
            lay_hotel.setVisibility(View.VISIBLE);
        }else{
            lay_hotel.setVisibility(View.GONE);
        }
    }

    private void _shown_data_taksi() {
        penggunaan_taksi.setClickable(false);
        if(dinas.getWith_taxi().equals("Y")){
            penggunaan_taksi.setChecked(true);
            place tmpt_berangkat = v_event.DBManager.get_placeById(dinas.getDeparture_place_id());
            place tmpt_tujuan = v_event.DBManager.get_placeById(dinas.getArrival_place_id());
            taksi_B_tempat.setText(tmpt_berangkat.getName());
            taksi_T_tempat.setText(tmpt_tujuan.getName());
            taksi_B_jenis_harga.setText(dinas.getTaxi_amount_departure_type());
            taksi_T_jenis_harga.setText(dinas.getTaxi_amount_arrival_type());
            taksi_B_harga.setText("Rp. " + dinas.getTaxi_amount_departure());
            taksi_T_harga.setText("Rp. " + dinas.getTaxi_amount_arrival());
            taksi_B_jumlah.setText(dinas.getTaxi_qty_departure());
            taksi_T_jumlah.setText(dinas.getTaxi_qty_arrival());
        }
        if(penggunaan_taksi.isChecked()){
            lay_taksi.setVisibility(View.VISIBLE);
        }else{
            lay_taksi.setVisibility(View.GONE);
        }

    }

    private void _show_Data_transportasi() {
        transportasi_type.setText(dinas.getTransportation_type());
        transportasi_class.setText(dinas.getTransportation_class());
        try {
            if (dinas.getAirplane_ticker_id().equals("")) {
                lay_tiket_tujuan.setVisibility(View.GONE);
            } else {
                airplane_ticket airplane = v_event.DBManager.get_airplane_ticketById(dinas.getAirplane_ticker_id());

                kabupaten kabupaten1 = v_event.DBManager.get_kabupatenById(airplane.getCity_first_id());
                kabupaten kabupaten2 = v_event.DBManager.get_kabupatenById(airplane.getCity_destination_id());
                transportasi_pesawat_rute.setText(kabupaten1.getNama() + " - " + kabupaten2.getNama());
            }
        }catch (Exception e){
            lay_tiket_tujuan.setVisibility(View.GONE);
        }
        transportasi_jenis_harga.setText(dinas.getTransportation_amount_type());
        transportasi_harga.setText("Rp. " + dinas.getTransportation_amount());
    }

    private void _shown_data() {
        no_surat.setText(dinas.getNo_surat_perintah());

        users employee = v_event.DBManager.get_usersById(dinas.getEmployee_id());
        nm_pegawai.setText(employee.getName() + " (" + employee.getNIP() + ")");

        position position_employee = v_event.DBManager.get_positionById(employee.getPosition_id());
        if(position_employee == null){
            jabatan.setText("-");
        }else {
            jabatan.setText(position_employee.getPosition());
        }

        String[] wb = dinas.getDate_departure().split("-");
        String[] wk = dinas.getDate_arrival().split("-");

        waktu_berangkat.setText(wb[2] + " " + _check_month(wb[1]) + " " + wb[0]);
        waktu_kembali.setText(wk[2] + " " + _check_month(wk[1]) + " " + wk[0]);

        perjalanan_dinas_jenis jenis_dinasnya = v_event.DBManager.get_jenis_dinasById(dinas.getType_code());
        jenis_dinas.setText(jenis_dinasnya.getJenis());

        place tmpt_berangkat = v_event.DBManager.get_placeById(dinas.getDeparture_place_id());
        place tmpt_tujuan = v_event.DBManager.get_placeById(dinas.getArrival_place_id());

        tempat_berangkat.setText(tmpt_berangkat.getName());
        tempat_tujuan.setText(tmpt_tujuan.getName());

        lama_dinas.setText(dinas.getDay_duration() + " hari");

        if(dinas.getRemarks().equals("")){
            maksud_dinas.setText("-");
        }else {
            maksud_dinas.setText(dinas.getDescription());
        }
        if(dinas.getRemarks().equals("")){
            keterangan_lain.setText("-");
        }else {
            keterangan_lain.setText(dinas.getRemarks());
        }
    }

    private void _init() {
        line = (CardView) v.findViewById(R.id.line_card);
        if (Build.VERSION.SDK_INT < 21) {
            line.setVisibility(View.GONE);
        }

        no_surat = (EditText) v.findViewById(R.id.edt_surat);
        nm_pegawai = (EditText) v.findViewById(R.id.edt_nama);
        jabatan = (EditText) v.findViewById(R.id.edt_jabatan);
        maksud_dinas = (EditText) v.findViewById(R.id.edt_maksud);
        waktu_berangkat = (EditText) v.findViewById(R.id.edt_berangkat);
        waktu_kembali = (EditText) v.findViewById(R.id.edt_kembali);
        jenis_dinas = (EditText) v.findViewById(R.id.edt_jenis);
        tempat_berangkat = (EditText) v.findViewById(R.id.edt_tempat_berangkat);
        tempat_tujuan = (EditText) v.findViewById(R.id.edt_tempat_tujuan);
        lama_dinas = (EditText) v.findViewById(R.id.edt_lama_dinas);
        keterangan_lain = (EditText) v.findViewById(R.id.edt_keterangan_lain);

        lay_taksi = (LinearLayout) v.findViewById(R.id.layout_taksi);
        penggunaan_taksi = (Switch) v.findViewById(R.id.sw_taksi);
        taksi_B_tempat = (EditText) v.findViewById(R.id.edt_tempat_taksi_berangkat);
        taksi_B_jenis_harga = (EditText) v.findViewById(R.id.edt_jenis_harga_taksi_berangkat);
        taksi_B_harga = (EditText) v.findViewById(R.id.edt_harga_taksi_berangkat);
        taksi_B_jumlah = (EditText) v.findViewById(R.id.edt_jumlah_taksi_berangkat);

        taksi_T_tempat = (EditText) v.findViewById(R.id.edt_tempat_taksi_tujuan);
        taksi_T_jenis_harga = (EditText) v.findViewById(R.id.edt_jenis_harga_taksi_tujuan);
        taksi_T_harga = (EditText) v.findViewById(R.id.edt_harga_taksi_tujuan);
        taksi_T_jumlah = (EditText) v.findViewById(R.id.edt_jumlah_taksi_tujuan);

        lay_tiket_tujuan = (LinearLayout) v.findViewById(R.id.layout_tiket_tujuan);
        transportasi_type = (EditText) v.findViewById(R.id.edt_transportasi_jenis);
        transportasi_class = (EditText) v.findViewById(R.id.edt_transportasi_class);
        transportasi_pesawat_rute = (EditText) v.findViewById(R.id.edt_transportasi_tiket_tujuan);
        transportasi_jenis_harga = (EditText) v.findViewById(R.id.edt_transportasi_jenis_harga);
        transportasi_harga = (EditText) v.findViewById(R.id.edt_transportasi_harga);

        lay_sewa = (LinearLayout) v.findViewById(R.id.layout_sewa);
        sewa_kendaraan = (Switch) v.findViewById(R.id.sw_sewa_kendaraan);
        sewa_model = (EditText) v.findViewById(R.id.edt_model_sewa_kendaraan);
        sewa_wilayah = (EditText) v.findViewById(R.id.edt_wilayah_rental);
        sewa_jenis = (EditText) v.findViewById(R.id.edt_jenis_harga_sewa);
        sewa_harga = (EditText) v.findViewById(R.id.edt_harga_sewa);

        lay_hotel = (LinearLayout) v.findViewById(R.id.layout_hotel);
        hotel = (Switch) v.findViewById(R.id.sw_hotel);
        hotel_wilayah_tiket = (EditText) v.findViewById(R.id.edt_wilayah_hotel);
        hotel_jenis_harga = (EditText) v.findViewById(R.id.edt_jenis_harga_hotel);
        hotel_harga = (EditText) v.findViewById(R.id.edt_harga_hotel);
    }

    private String _check_month(String bulan){
        String nm_bln = null;
        switch (bulan){
            case "01":
                nm_bln = "Januari";
                break;
            case "02":
                nm_bln = "Februari";
                break;
            case "03":
                nm_bln = "Maret";
                break;
            case "04":
                nm_bln = "April";
                break;
            case "05":
                nm_bln = "Mei";
                break;
            case "06":
                nm_bln = "Juni";
                break;
            case "07":
                nm_bln = "Juli";
                break;
            case "08":
                nm_bln = "Agustus";
                break;
            case "09":
                nm_bln = "September";
                break;
            case "10":
                nm_bln = "Oktober";
                break;
            case "11":
                nm_bln = "November";
                break;
            case "12":
                nm_bln = "Desember";
                break;
        }
        if(nm_bln == null){
            return bulan;
        }else{
            return nm_bln;
        }
    }
}
