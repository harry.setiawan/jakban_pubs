package com.app.kemenhub.jakbanapps.fragment;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Explode;
import android.transition.Slide;
import android.transition.Transition;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.kemenhub.jakbanapps.R;
import com.app.kemenhub.jakbanapps.adapter.DataDinasAdt;
import com.app.kemenhub.jakbanapps.db.perjalanan_dinas;
import com.app.kemenhub.jakbanapps.sys.utils;
import com.app.kemenhub.jakbanapps.v_detil;
import com.app.kemenhub.jakbanapps.v_event;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class f_dinas extends Fragment implements DataDinasAdt.itemClickCallback{
    View v;
    v_event activity;
    CompactCalendarView calender;
    private SimpleDateFormat dateFormat;
    TextView bulan, nothing;
    SwipeRefreshLayout refresh;
    private String int_bulan_grafik = "0", int_tahun = "0";
    RecyclerView recyclerview;
    String date_selected = "";
    DataDinasAdt adapter = null;
    CardView line;
    ArrayList<perjalanan_dinas> data_dinas = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.f_dinas, container, false);
        activity = (v_event) getActivity();

        _init();
        set_first_date();
        _imp_data();

        return v;
    }

    private void set_adapter() {
        data_dinas.clear();
        data_dinas.addAll(activity.DBManager.list_perjalanan_dinasByD(activity.SManager.getPreferences(activity, "status"), date_selected));
        if(data_dinas.size() > 0|| data_dinas != null){
            nothing.setVisibility(View.GONE);
            recyclerview.setVisibility(View.VISIBLE);
        }else{
            recyclerview.setVisibility(View.GONE);
            nothing.setVisibility(View.VISIBLE);
        }
        adapter.notifyDataSetChanged();
    }
    private void set_first_date() {
        String[] data = set_month_first().split("\\ - ");
        _check_month(data[0]);
        int_tahun = data[1];
        bulan.setText(_check_month(data[0]) + " " + data[1]);
    }
    private void _imp_data() {
        activity.show_sync();
        activity.rc.request_ByDATE_dinas(int_tahun, int_bulan_grafik);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                activity.hide_sync();
                _set_call();
            }
        },2000);
    }
    private void _set_call() {
        set_adapter();
        calender.removeAllEvents();
        List<perjalanan_dinas> data_dinas =  activity.DBManager.list_perjalanan_dinasByD(activity.SManager.getPreferences(activity, "status"), int_tahun+"-"+int_bulan_grafik);
        for(int i = 0 ; i < data_dinas.size() ; i++){
            try {
                String tgl_data = data_dinas.get(i).getDate_departure();
                SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
                long waktu = date.parse(tgl_data).getTime();
                Event evn = null;
                if(data_dinas.get(i).getType_code().toLowerCase().contains("luar")) {
                    evn = new Event(getResources().getColor(R.color.md_red_A200), waktu + 'L', data_dinas.get(i).getCode());
                }else{
                    evn = new Event(getResources().getColor(R.color.md_orange_A200), waktu + 'L', data_dinas.get(i).getCode());
                }
                calender.addEvent(evn);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        calender.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                date_selected = df.format(dateClicked).toString();
                set_adapter();
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                String[] data = dateFormat.format(firstDayOfNewMonth).split("\\ - ");
                _check_month(data[0]);
                int_tahun = data[1];
                bulan.setText(_check_month(data[0]) + " " + data[1]);
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                date_selected = df.format(firstDayOfNewMonth).toString();
                _imp_data();
            }
        });
    }
    private String _check_month(String bulan){
        String nm_bln = null;
        switch (bulan){
            case "January":
                nm_bln = "Januari";
                int_bulan_grafik = "01";
                break;
            case "February":
                nm_bln = "Februari";
                int_bulan_grafik = "02";
                break;
            case "March":
                nm_bln = "Maret";
                int_bulan_grafik = "03";
                break;
            case "April":
                nm_bln = "April";
                int_bulan_grafik = "04";
                break;
            case "May":
                nm_bln = "Mei";
                int_bulan_grafik = "05";
                break;
            case "June":
                nm_bln = "Juni";
                int_bulan_grafik = "06";
                break;
            case "July":
                nm_bln = "Juli";
                int_bulan_grafik = "07";
                break;
            case "August":
                nm_bln = "Agustus";
                int_bulan_grafik = "08";
                break;
            case "September":
                nm_bln = "September";
                int_bulan_grafik = "09";
                break;
            case "October":
                nm_bln = "Oktober";
                int_bulan_grafik = "10";
                break;
            case "November":
                nm_bln = "November";
                int_bulan_grafik = "11";
                break;
            case "December":
                nm_bln = "Desember";
                int_bulan_grafik = "12";
                break;
        }
        if(nm_bln == null){
            return bulan;
        }else{
            return nm_bln;
        }
    }
    private void _init() {
        dateFormat = new SimpleDateFormat("MMMM - yyyy", Locale.getDefault());

        calender = (CompactCalendarView) v.findViewById(R.id.calender_view);
        calender.setUseThreeLetterAbbreviation(true);

        nothing = (TextView) v.findViewById(R.id.nothing);
        bulan = (TextView) v.findViewById(R.id.bulan);
        refresh = (SwipeRefreshLayout) v.findViewById(R.id.swipe_dash);
        recyclerview = (RecyclerView) v.findViewById(R.id.my_recycler_message);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(activity);
        recyclerview.setLayoutManager(layoutManager);
        adapter = new DataDinasAdt("DINAS", data_dinas, activity);
        recyclerview.setAdapter(adapter);
        adapter.setItemClickCallback(this);

        line = (CardView) v.findViewById(R.id.line_card);
        if (Build.VERSION.SDK_INT < 21) {
            line.setVisibility(View.GONE);
        }

        refresh.setColorSchemeColors(getResources().getColor(R.color.md_light_green_500_25));
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh.setRefreshing(true);
                activity.rc.request_ByDATE_dinas(int_tahun, int_bulan_grafik);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        refresh.setRefreshing(false);
                        _set_call();
                    }
                },2000);
            }
        });
    }
    public  String set_month_first() {
        SimpleDateFormat sdfdate_sel = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdfdate = new SimpleDateFormat("MMMM - yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);
        Date tgll = cal.getTime();
        date_selected = sdfdate_sel.format(tgll);
        return sdfdate.format(tgll);
    }
    private static Date parseDate(String date, String format) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        return formatter.parse(date);
    }

    @Override
    public void onItemClick(int p) {
        go_to_action(v_detil.class, data_dinas.get(p).getId());
    }

    public void go_to_action(final Class classes, long id) {
        utils.TAG_LAYOUT = "f_dinas";
        if (Build.VERSION.SDK_INT >= 21) {
            Transition exitTrans = new Explode();
            activity.getWindow().setExitTransition(exitTrans);

            Transition reenterTrans = new Slide();
            activity.getWindow().setEnterTransition(reenterTrans);
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(activity);
            Intent intent = new Intent(activity, classes);
            intent.putExtra("DINAS", String.valueOf(id));
            this.startActivity(intent, options.toBundle());
        }else{
            Intent intent = new Intent(activity, classes);
            intent.putExtra("DINAS", String.valueOf(id));
            this.startActivity(intent);
        }
    }
}
