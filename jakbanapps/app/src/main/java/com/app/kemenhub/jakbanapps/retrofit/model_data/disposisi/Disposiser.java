package com.app.kemenhub.jakbanapps.retrofit.model_data.disposisi;

import com.app.kemenhub.jakbanapps.retrofit.model_data.category.Kategori;
import com.app.kemenhub.jakbanapps.retrofit.model_data.schedule.Forward;

import java.util.List;

public class Disposiser {
	private String id;

	private String from;

	private String no_surat_perintah;

	private String no_agenda;
	
	private String title;

	private String desc;

	private String notes;

	private List<Forward> forward;

	private List<Kategori> categories;
	
	private String cls;

	private String file;

	public Disposiser(){}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getNo_surat_perintah() {
		return no_surat_perintah;
	}

	public void setNo_surat_perintah(String no_surat_perintah) {
		this.no_surat_perintah = no_surat_perintah;
	}

	public String getNo_agenda() {
		return no_agenda;
	}

	public void setNo_agenda(String no_agenda) {
		this.no_agenda = no_agenda;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public List<Forward> getForward() {
		return forward;
	}

	public void setForward(List<Forward> forward) {
		this.forward = forward;
	}

	public List<Kategori> getCategories() {
		return categories;
	}

	public void setCategories(List<Kategori> categories) {
		this.categories = categories;
	}

	public String getCls() {
		return cls;
	}

	public void setCls(String cls) {
		this.cls = cls;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public Disposiser(String id, String from, String no_surat_perintah, String no_agenda, String title, String desc,
                      String notes, List<Forward> forward, List<Kategori> categories, String cls, String file) {
		super();
		this.id = id;
		this.from = from;
		this.no_surat_perintah = no_surat_perintah;
		this.no_agenda = no_agenda;
		this.title = title;
		this.desc = desc;
		this.notes = notes;
		this.forward = forward;
		this.categories = categories;
		this.cls = cls;
		this.file = file;
	}
	
	
}
