package com.app.kemenhub.jakbanapps.retrofit.model_data.place;

import com.app.kemenhub.jakbanapps.retrofit.model_data.provinsi.Provinsi;

public class Places {

	private String id;
	private Provinsi provinsi;
	private String name;
	private String type;
		
	public Places(){
	}

	public Places(String id, Provinsi provinsi, String name, String type) {
		super();
		this.id = id;
		this.provinsi = provinsi;
		this.name = name;
		this.type = type;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Provinsi getProvinsi() {
		return provinsi;
	}

	public void setProvinsi(Provinsi provinsi) {
		this.provinsi = provinsi;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}


}
