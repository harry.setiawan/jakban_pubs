package com.app.kemenhub.jakbanapps.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.kemenhub.jakbanapps.R;
import com.app.kemenhub.jakbanapps.db.disposition;
import com.app.kemenhub.jakbanapps.db.manager.DManager;
import com.app.kemenhub.jakbanapps.db.notif;
import com.app.kemenhub.jakbanapps.db.perjalanan_dinas;
import com.app.kemenhub.jakbanapps.db.place;
import com.app.kemenhub.jakbanapps.db.schedule;
import com.app.kemenhub.jakbanapps.sys.utils;
import com.app.kemenhub.jakbanapps.v_event;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class DataNotifAdt extends RecyclerView.Adapter<DataNotifAdt.Holder> {
    List<notif> itemList;
    Context ctx;
    private LayoutInflater inflater;
    Context context;
    private SparseBooleanArray mSelectedItemsIds;
    private int previous = 0;

    private itemClickCallback itemClickCallback;
    int Type;
    ViewGroup parent;
    DManager DBManager;

    public interface itemClickCallback{
        void onItemClick(int p);
    }

    public void setItemClickCallback(final itemClickCallback itemClickCallback){
        this.itemClickCallback = itemClickCallback;
    }

    public DataNotifAdt(List<notif> data, Context context){
        this.itemList = data;
        this.ctx = context;
        this.inflater = LayoutInflater.from(ctx);
        this.context = context;
        this.DBManager = new DManager(context);
        mSelectedItemsIds = new SparseBooleanArray();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        WindowManager windowManager = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);
        int width = windowManager.getDefaultDisplay().getWidth();
        view.setLayoutParams(new RecyclerView.LayoutParams(width, RecyclerView.LayoutParams.WRAP_CONTENT));

        Holder derpHolder = new Holder(view);

        return derpHolder;
    }

    public void runer(){
        onCreateViewHolder(parent, Type);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        ////Log.i("NOTIFF", itemList.get(position).getTag());
        switch(itemList.get(position).getTag()){
            case "DINAS":
                holder.item_meeting.setVisibility(View.GONE);
                holder.item_disposisi.setVisibility(View.GONE);
                holder.card_meeting.setVisibility(View.GONE);
                holder.card_disposisi.setVisibility(View.GONE);
                holder.card_dinas.setVisibility(View.VISIBLE);
                holder.item_dinas.setVisibility(View.VISIBLE);

                perjalanan_dinas data_dinas = DBManager.get_perjalanan_dinasById(itemList.get(position).getData());

                if(data_dinas.getNo_surat_perintah().length() > 20){
                    holder.dinas_code.setText("Code : " + data_dinas.getCode().substring(0,17) + "..");
                }else {
                    holder.dinas_code.setText("Code : " + data_dinas.getCode());
                }
                if(data_dinas.getNo_surat_perintah().length() > 20){
                    holder.dinas_agenda.setText("No. Surat : " + data_dinas.getNo_surat_perintah().substring(0,17) + "..");
                }else {
                    holder.dinas_agenda.setText("No. Surat : " + data_dinas.getNo_surat_perintah());
                }
                holder.dinas_desc.setText("Deskripsi : " + data_dinas.getDescription());
                place place_asal = DBManager.get_placeById(data_dinas.getDeparture_place_id());
                place place_tujuan = DBManager.get_placeById(data_dinas.getArrival_place_id());
                holder.dinas_kota.setText("Kota : " + place_asal.getName() + " - " + place_tujuan.getName());
                holder.dinas_biaya.setText("Total Biaya : " + utils.set_rupiah(utils.set_digit_rupiah(data_dinas.getTotal_amount())));
                int durasi = (Integer.parseInt(data_dinas.getDate_arrival().substring(8,10)) - Integer.parseInt(data_dinas.getDate_departure().substring(8,10)));
                if(data_dinas.getType_code().toUpperCase().contains("LUAR")){
                    holder.dinas_star.setVisibility(View.VISIBLE);
                }else{
                    holder.dinas_star.setVisibility(View.GONE);
                }
                holder.dinas_durasi.setText(durasi + "");

                break;
            case "MEETING":
                holder.card_meeting.setVisibility(View.VISIBLE);
                holder.card_disposisi.setVisibility(View.GONE);
                holder.card_dinas.setVisibility(View.GONE);
                holder.item_meeting.setVisibility(View.VISIBLE);
                holder.item_disposisi.setVisibility(View.GONE);
                holder.item_dinas.setVisibility(View.GONE);

                schedule data_meeting = DBManager.get_scheduleById(itemList.get(position).getData());

                if(data_meeting.getTitle().length() > 20){
                    holder.meeting_judul.setText("Judul : " +data_meeting.getTitle().substring(0,17) + "..");
                }else {
                    holder.meeting_judul.setText("Judul : " +data_meeting.getTitle());
                }
                if(data_meeting.getPlace().length() > 20){
                    holder.meeting_tempat.setText("Tempat Acara : " + data_meeting.getPlace().substring(0,17) + "..");
                }else {
                    holder.meeting_tempat.setText("Tempat Acara : " + data_meeting.getPlace());
                }
                try {
                    if(data_meeting.getNotes().equals("null") || data_meeting.getNotes().equals("")){
                        data_meeting.setNotes(" - ");
                    }
                }catch (Exception e){
                    data_meeting.setDesc(" - ");
                }
                if(data_meeting.getNotes().length() > 20){
                    holder.meeting_category.setText("Catatan : " + data_meeting.getNotes().substring(0,17) + "..");
                }else {
                    holder.meeting_category.setText("Catatan : " + data_meeting.getNotes());
                }
                try {
                    if (data_meeting.getDesc().equals("null") || data_meeting.getDesc().equals("")) {
                        data_meeting.setDesc(" - ");
                    }
                }catch (Exception e){
                    data_meeting.setDesc(" - ");
                }
                if(data_meeting.getDesc().length() > 20){
                    holder.meeting_desc.setText("Deskripsi : " + data_meeting.getDesc().substring(0,17) + "..");
                }else {
                    holder.meeting_desc.setText("Deskripsi : " + data_meeting.getDesc());
                }
                holder.meeting_dari.setText("Dari : " + data_meeting.getFrom());
                holder.meeting_durasi.setText(set_duration(data_meeting.getDatetime_schedule_start(), data_meeting.getDatetime_schedule_end()));

                try {
                    if (data_meeting.getCategory_id().contains("14")) {
                        holder.meeting_star.setVisibility(View.VISIBLE);
                    } else {
                        holder.meeting_star.setVisibility(View.GONE);
                    }
                }catch (Exception e){
                    holder.meeting_star.setVisibility(View.GONE);
                }
                break;
            case "DISPOSISI":
                holder.card_meeting.setVisibility(View.GONE);
                holder.card_disposisi.setVisibility(View.VISIBLE);
                holder.card_dinas.setVisibility(View.GONE);
                holder.item_meeting.setVisibility(View.GONE);
                holder.item_disposisi.setVisibility(View.VISIBLE);
                holder.item_disposisi.setClickable(true);
                holder.item_dinas.setVisibility(View.GONE);

                disposition data_disposisi = DBManager.get_dispositionById(itemList.get(position).getData());

                if(data_disposisi.getNo_surat_perintah().length() > 20){
                    holder.disposisi_surat.setText("No. Surat : " + data_disposisi.getNo_surat_perintah().substring(0,17) + "..");
                }else {
                    holder.disposisi_surat.setText("No. Surat : " + data_disposisi.getNo_surat_perintah());
                }
                if(data_disposisi.getNo_agenda().length() > 20){
                    holder.disposisi_agenda.setText("No. Agenda : " + data_disposisi.getNo_agenda().substring(0,17) + "..");
                }else {
                    holder.disposisi_agenda.setText("No. Agenda : " + data_disposisi.getNo_agenda());
                }
                if(data_disposisi.getNotes().length() > 20){
                    holder.disposisi_desc.setText("Catatan : " + data_disposisi.getNotes().substring(0,17) + "..");
                }else {
                    holder.disposisi_desc.setText("Catatan : " + data_disposisi.getNotes());
                }
                holder.disposisi_dari.setText("Dari : " +data_disposisi.getFrom());
                holder.disposisi_category.setText("Perihal : " + data_disposisi.getTitle());
                ////Log.i("CLS", data_disposisi.getCls());
                if(data_disposisi.getCls().equals("1")){
                    holder.disposisi_klasifikasi.setText("B");
                    holder.disposisi_klasifikasi.setTextColor(context.getResources().getColor(R.color.md_blue_grey_300));
                    holder.disposisi_star.setVisibility(View.GONE);
                }else if(data_disposisi.getCls().equals("2")){
                    holder.disposisi_klasifikasi.setText("S");
                    holder.disposisi_klasifikasi.setTextColor(context.getResources().getColor(R.color.md_light_blue_900));
                    holder.disposisi_star.setVisibility(View.GONE);
                }else if(data_disposisi.getCls().equals("3")){
                    holder.disposisi_klasifikasi.setText("SS");
                    holder.disposisi_klasifikasi.setTextColor(context.getResources().getColor(R.color.md_orange_A400));
                    holder.disposisi_star.setVisibility(View.GONE);
                }else if(data_disposisi.getCls().equals("4")){
                    holder.disposisi_klasifikasi.setText("R");
                    holder.disposisi_klasifikasi.setTextColor(context.getResources().getColor(R.color.md_cyan_A700));
                    holder.disposisi_star.setVisibility(View.VISIBLE);
                }
                break;
            default:
                holder.item_meeting.setVisibility(View.GONE);
                holder.item_disposisi.setVisibility(View.GONE);
                holder.item_dinas.setVisibility(View.GONE);
                break;
        }
    }

    private String set_duration(String datetime_schedule_start, String datetime_schedule_end) {
        String data = "0";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            Date d1 = format.parse(datetime_schedule_start);
            Date d2 = format.parse(datetime_schedule_end);

            long diff = d2.getTime() - d1.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffMinutes = (diff / (60 * 1000) % 60);
            long diffDays = diff / (24 * 60 * 60 * 1000);
            data = String.valueOf(((diffHours * 60) + diffMinutes));
        } catch (Exception e) {
            data = "0";
        }
        return  data;
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));
    }

    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }

    public void selectView(int position, boolean value) {
        if (value) {
            mSelectedItemsIds.put(position, value);
        }else {
            mSelectedItemsIds.delete(position);
        }
        notifyDataSetChanged();
    }


    class Holder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{

        public LinearLayout item;
        public LinearLayout item_dinas;
        public LinearLayout item_meeting;
        public LinearLayout item_disposisi;
        public TextView dinas_code, dinas_agenda, dinas_durasi, dinas_desc, dinas_kota, dinas_biaya;
        public ImageView dinas_star, meeting_star, disposisi_star;
        public TextView meeting_judul, meeting_tempat, meeting_durasi, meeting_category, meeting_desc, meeting_dari;
        public TextView disposisi_surat, disposisi_agenda, disposisi_klasifikasi, disposisi_category, disposisi_desc, disposisi_dari;
        public CardView card_meeting, card_dinas, card_disposisi;
        public Holder(View itemView) {
            super(itemView);
            item_dinas = (LinearLayout) itemView.findViewById(R.id.dinas_item);
            dinas_code = (TextView) itemView.findViewById(R.id.dinas_code);
            dinas_agenda = (TextView) itemView.findViewById(R.id.dinas_surat);
            dinas_durasi = (TextView) itemView.findViewById(R.id.dinas_durasi);
            dinas_desc = (TextView) itemView.findViewById(R.id.dinas_deskripsi);
            dinas_kota = (TextView) itemView.findViewById(R.id.dinas_kota);
            dinas_biaya = (TextView) itemView.findViewById(R.id.dinas_biaya);
            dinas_star = (ImageView) itemView.findViewById(R.id.dinas_star);
            meeting_star = (ImageView) itemView.findViewById(R.id.meeting_star);
            card_dinas = (CardView) itemView.findViewById(R.id.dinas_line_card);
            card_meeting = (CardView) itemView.findViewById(R.id.meeting_line_card);
            card_disposisi = (CardView) itemView.findViewById(R.id.disposisi_line_card);

            item_meeting = (LinearLayout) itemView.findViewById(R.id.meeting_item);
            meeting_judul = (TextView) itemView.findViewById(R.id.meeting_judul);
            meeting_tempat = (TextView) itemView.findViewById(R.id.meeting_tempat);
            meeting_durasi = (TextView) itemView.findViewById(R.id.meeting_durasi);
            meeting_category = (TextView) itemView.findViewById(R.id.meeting_category);
            meeting_desc = (TextView) itemView.findViewById(R.id.meeting_deskripsi);
            meeting_dari = (TextView) itemView.findViewById(R.id.meeting_dari);

            item_disposisi = (LinearLayout) itemView.findViewById(R.id.disposisi_item);
            disposisi_surat = (TextView) itemView.findViewById(R.id.disposisi_code);
            disposisi_klasifikasi = (TextView) itemView.findViewById(R.id.disposisi_durasi);
            disposisi_agenda = (TextView) itemView.findViewById(R.id.disposisi_agenda);
            disposisi_category = (TextView) itemView.findViewById(R.id.disposisi_kategori);
            disposisi_desc = (TextView) itemView.findViewById(R.id.disposisi_deskripsi);
            disposisi_dari = (TextView) itemView.findViewById(R.id.disposisi_dari);
            disposisi_star = (ImageView) itemView.findViewById(R.id.disposisi_star);

            item = (LinearLayout) itemView.findViewById(R.id.item);
            item_dinas.setClickable(true);
            item_meeting.setClickable(true);
            item_disposisi.setClickable(true);
            item.setClickable(true);

            item.setOnClickListener(this);
            item_dinas.setOnClickListener(this);
            item_meeting.setOnClickListener(this);
            item_disposisi.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.item:
                    itemClickCallback.onItemClick(getAdapterPosition());
                    break;
                case R.id.meeting_item:
                    itemClickCallback.onItemClick(getAdapterPosition());
                    break;
                case R.id.disposisi_item:
                    itemClickCallback.onItemClick(getAdapterPosition());
                    break;
            }
        }

        @Override
        public boolean onLongClick(View v) {
            switch (v.getId()){
            }
            return false;
        }
    }
}