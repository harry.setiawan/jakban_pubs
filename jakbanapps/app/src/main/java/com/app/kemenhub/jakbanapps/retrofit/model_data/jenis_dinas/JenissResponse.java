package com.app.kemenhub.jakbanapps.retrofit.model_data.jenis_dinas;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class JenissResponse {
	@SerializedName("code")
	String code;
	@SerializedName("message")
	String message;
	@SerializedName("jenis")
	List<Jenis> jenis;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<Jenis> getJenis() {
		return jenis;
	}
	public void setJenis(List<Jenis> jenis) {
		this.jenis = jenis;
	}
	
	
}
