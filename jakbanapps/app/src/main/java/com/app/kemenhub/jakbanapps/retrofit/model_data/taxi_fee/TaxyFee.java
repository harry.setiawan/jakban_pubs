package com.app.kemenhub.jakbanapps.retrofit.model_data.taxi_fee;

public class TaxyFee {
	private String id;
	private String provinsi_id;
	private String unit;
	private String fee;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProvinsi_id() {
		return provinsi_id;
	}

	public void setProvinsi_id(String provinsi_id) {
		this.provinsi_id = provinsi_id;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getFee() {
		return fee;
	}

	public void setFee(String fee) {
		this.fee = fee;
	}

}
