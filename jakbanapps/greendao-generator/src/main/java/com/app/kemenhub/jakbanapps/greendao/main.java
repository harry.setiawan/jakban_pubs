package com.app.kemenhub.jakbanapps.greendao;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class main {
    public static void main(String[] args) throws Exception {
        Schema schema = new Schema(1, "com.app.kemenhub.jakbanapps.db");

        Entity airplane_ticket = schema.addEntity("airplane_ticket");
        airplane_ticket.addIdProperty();
        airplane_ticket.addStringProperty("city_first_id");
        airplane_ticket.addStringProperty("city_destination_id");
        airplane_ticket.addStringProperty("business");
        airplane_ticket.addStringProperty("economy");
        airplane_ticket.addStringProperty("created_at");
        airplane_ticket.addStringProperty("updated_at");

        Entity notif = schema.addEntity("notif");
        notif.addIdProperty();
        notif.addStringProperty("data");
        notif.addStringProperty("tag");

        Entity kecamatan = schema.addEntity("kecamatan");
        kecamatan.addIdProperty();
        kecamatan.addStringProperty("kabupaten_id");
        kecamatan.addStringProperty("nama");


        Entity kelurahan = schema.addEntity("kelurahan");
        kelurahan.addIdProperty();
        kelurahan.addStringProperty("kecamatan_id");
        kelurahan.addStringProperty("nama");

        Entity kabupaten = schema.addEntity("kabupaten");
        kabupaten.addIdProperty();
        kabupaten.addStringProperty("provinsi_id");
        kabupaten.addStringProperty("nama");

        Entity user_role = schema.addEntity("user_role");
        user_role.addIdProperty();
        user_role.addStringProperty("user_id");
        user_role.addStringProperty("role_id");

        Entity provinsi = schema.addEntity("provinsi");
        provinsi.addIdProperty();
        provinsi.addStringProperty("nama");
        provinsi.addStringProperty("kode");

        Entity disposition = schema.addEntity("disposition");
        disposition.addIdProperty();
        disposition.addStringProperty("category_id");
        disposition.addStringProperty("from");
        disposition.addStringProperty("no_surat_perintah");
        disposition.addStringProperty("no_agenda");
        disposition.addStringProperty("title");
        disposition.addStringProperty("desc");
        disposition.addStringProperty("notes");
        disposition.addStringProperty("forward_to_user_id");
        disposition.addStringProperty("cls");
        disposition.addStringProperty("file");
        disposition.addBooleanProperty("notif");

        Entity file = schema.addEntity("file");
        file.addIdProperty();
        file.addStringProperty("folder_id");
        file.addStringProperty("permalink");
        file.addStringProperty("file");
        file.addStringProperty("ext");
        file.addStringProperty("title");
        file.addStringProperty("size");
        file.addStringProperty("status");

        Entity category = schema.addEntity("category");
        category.addIdProperty();
        category.addStringProperty("name");

        Entity folder = schema.addEntity("folder");
        folder.addIdProperty();
        folder.addStringProperty("parent_id");
        folder.addStringProperty("folder");
        folder.addStringProperty("desc");

        Entity hotel_fee = schema.addEntity("hotel_fee");
        hotel_fee.addIdProperty();
        hotel_fee.addStringProperty("provinsi_id");
        hotel_fee.addStringProperty("unit");
        hotel_fee.addStringProperty("eselon1");
        hotel_fee.addStringProperty("eselon2");
        hotel_fee.addStringProperty("eselon3");
        hotel_fee.addStringProperty("eselon4");
        hotel_fee.addStringProperty("golongan1_2");

        Entity perjalanan_dinas = schema.addEntity("perjalanan_dinas");
        perjalanan_dinas.addIdProperty();
        perjalanan_dinas.addStringProperty("code");
        perjalanan_dinas.addStringProperty("employee_id");
        perjalanan_dinas.addStringProperty("description");
        perjalanan_dinas.addStringProperty("remarks");
        perjalanan_dinas.addStringProperty("no_surat_perintah");
        perjalanan_dinas.addStringProperty("date_departure");
        perjalanan_dinas.addStringProperty("date_arrival");
        perjalanan_dinas.addStringProperty("day_duration");
        perjalanan_dinas.addStringProperty("type_code");
        perjalanan_dinas.addStringProperty("departure_place_id");
        perjalanan_dinas.addStringProperty("arrival_place_id");
        perjalanan_dinas.addStringProperty("transportation_type");
        perjalanan_dinas.addStringProperty("transportation_class");
        perjalanan_dinas.addStringProperty("airplane_ticker_id");
        perjalanan_dinas.addStringProperty("with_vehicle_rent");
        perjalanan_dinas.addStringProperty("vehicle_rent_type");
        perjalanan_dinas.addStringProperty("vehicle_rent_price_id");
        perjalanan_dinas.addStringProperty("with_hotel");
        perjalanan_dinas.addStringProperty("hotel_ticket_id");
        perjalanan_dinas.addStringProperty("hotel_amount_type");
        perjalanan_dinas.addStringProperty("hotel_amount");
        perjalanan_dinas.addStringProperty("transportation_amount_type");
        perjalanan_dinas.addStringProperty("transportation_amount");
        perjalanan_dinas.addStringProperty("with_taxi");
        perjalanan_dinas.addStringProperty("taxi_qty_departure");
        perjalanan_dinas.addStringProperty("taxi_qty_arrival");
        perjalanan_dinas.addStringProperty("taxi_ticket_departure");
        perjalanan_dinas.addStringProperty("taxi_ticket_arrival");
        perjalanan_dinas.addStringProperty("taxi_amount_departure_type");
        perjalanan_dinas.addStringProperty("taxi_amount_arrival_type");
        perjalanan_dinas.addStringProperty("taxi_amount_departure");
        perjalanan_dinas.addStringProperty("taxi_amount_arrival");
        perjalanan_dinas.addStringProperty("taxi_total_amount");
        perjalanan_dinas.addStringProperty("vehicle_amount_type");
        perjalanan_dinas.addStringProperty("vehicle_amount");
        perjalanan_dinas.addStringProperty("type_amount");
        perjalanan_dinas.addStringProperty("total_amount");
        perjalanan_dinas.addBooleanProperty("notif");

        Entity perjalanan_dinas_jenis = schema.addEntity("perjalanan_dinas_jenis");
        perjalanan_dinas_jenis.addStringProperty("code");
        perjalanan_dinas_jenis.addStringProperty("jenis");

        Entity place = schema.addEntity("place");
        place.addIdProperty();
        place.addStringProperty("provinsi_id");
        place.addStringProperty("name");
        place.addStringProperty("type");

        Entity roles = schema.addEntity("roles");
        roles.addIdProperty();
        roles.addStringProperty("name");
        roles.addStringProperty("display");

        Entity schedule = schema.addEntity("schedule");
        schedule.addIdProperty();
        schedule.addStringProperty("category_id");
        schedule.addStringProperty("from");
        schedule.addStringProperty("title");
        schedule.addStringProperty("datetime_schedule_start");
        schedule.addStringProperty("datetime_schedule_end");
        schedule.addStringProperty("tgl_diterima");
        schedule.addStringProperty("no_surat_perintah");
        schedule.addStringProperty("no_agenda");
        schedule.addStringProperty("place");
        schedule.addStringProperty("desc");
        schedule.addStringProperty("notes");
        schedule.addStringProperty("forward_to_user_id");
        schedule.addStringProperty("cls");
        schedule.addStringProperty("file");
        schedule.addBooleanProperty("notif");

        Entity schedule_category = schema.addEntity("schedule_category");
        schedule_category.addIdProperty();
        schedule_category.addStringProperty("category_schedule");

        Entity taxi_fee = schema.addEntity("taxi_fee");
        taxi_fee.addIdProperty();
        taxi_fee.addStringProperty("provinsi_id");
        taxi_fee.addStringProperty("unit");
        taxi_fee.addStringProperty("fee");

        Entity user_class = schema.addEntity("user_class");
        user_class.addIdProperty();
        user_class.addStringProperty("cls");
        user_class.addStringProperty("type");
        user_class.addStringProperty("desc");

        Entity user_mobile = schema.addEntity("user_mobile");
        user_mobile.addIdProperty();
        user_mobile.addStringProperty("token");

        Entity position = schema.addEntity("position");
        position.addIdProperty();
        position.addStringProperty("position");

        Entity users = schema.addEntity("users");
        users.addIdProperty();
        users.addStringProperty("name");
        users.addStringProperty("username");
        users.addStringProperty("email");
        users.addStringProperty("password");
        users.addStringProperty("NIP");
        users.addStringProperty("phone");
        users.addStringProperty("birth");
        users.addStringProperty("address");
        users.addStringProperty("position_id");
        users.addStringProperty("class_id");

        Entity vehicle_rent = schema.addEntity("vehicle_rent");
        vehicle_rent.addIdProperty();
        vehicle_rent.addStringProperty("provinsi_id");
        vehicle_rent.addStringProperty("unit");
        vehicle_rent.addStringProperty("car");
        vehicle_rent.addStringProperty("midle_bus");
        vehicle_rent.addStringProperty("big_bus");

        DaoGenerator dg = new DaoGenerator();
        dg.generateAll(schema, "./app/src/main/java");
    }
}
